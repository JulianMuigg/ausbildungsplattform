// InverseCModuleCtrl.cpp : Implementation of CTcInverseCModuleCtrl
#include "TcPch.h"
#pragma hdrstop

#include "InverseCModuleW32.h"
#include "InverseCModuleCtrl.h"

/////////////////////////////////////////////////////////////////////////////
// CInverseCModuleCtrl

CInverseCModuleCtrl::CInverseCModuleCtrl() 
	: ITcOCFCtrlImpl<CInverseCModuleCtrl, CInverseCModuleClassFactory>() 
{
}

CInverseCModuleCtrl::~CInverseCModuleCtrl()
{
}

