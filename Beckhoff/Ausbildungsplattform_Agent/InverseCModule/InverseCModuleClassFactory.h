///////////////////////////////////////////////////////////////////////////////
// InverseCModule.h

#pragma once

#include "ObjClassFactory.h"

class CInverseCModuleClassFactory : public CObjClassFactory
{
public:
	CInverseCModuleClassFactory();
	DECLARE_CLASS_MAP()
};


