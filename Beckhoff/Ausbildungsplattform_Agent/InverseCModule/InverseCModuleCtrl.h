///////////////////////////////////////////////////////////////////////////////
// InverseCModuleCtrl.h

#ifndef __INVERSECMODULECTRL_H__
#define __INVERSECMODULECTRL_H__

#include <atlbase.h>
#include <atlcom.h>


#include "resource.h"       // main symbols
#include "InverseCModuleW32.h"
#include "TcBase.h"
#include "InverseCModuleClassFactory.h"
#include "TcOCFCtrlImpl.h"

class CInverseCModuleCtrl 
	: public CComObjectRootEx<CComMultiThreadModel>
	, public CComCoClass<CInverseCModuleCtrl, &CLSID_InverseCModuleCtrl>
	, public IInverseCModuleCtrl
	, public ITcOCFCtrlImpl<CInverseCModuleCtrl, CInverseCModuleClassFactory>
{
public:
	CInverseCModuleCtrl();
	virtual ~CInverseCModuleCtrl();

DECLARE_REGISTRY_RESOURCEID(IDR_INVERSECMODULECTRL)
DECLARE_NOT_AGGREGATABLE(CInverseCModuleCtrl)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CInverseCModuleCtrl)
	COM_INTERFACE_ENTRY(IInverseCModuleCtrl)
	COM_INTERFACE_ENTRY(ITcCtrl)
	COM_INTERFACE_ENTRY(ITcCtrl2)
END_COM_MAP()

};

#endif // #ifndef __INVERSECMODULECTRL_H__
