///////////////////////////////////////////////////////////////////////////////
// InverseCModuleDriver.cpp
#include "TcPch.h"
#pragma hdrstop

#include "InverseCModuleDriver.h"
#include "InverseCModuleClassFactory.h"

DECLARE_GENERIC_DEVICE(INVERSECMODULEDRV)

IOSTATUS CInverseCModuleDriver::OnLoad( )
{
	TRACE(_T("CObjClassFactory::OnLoad()\n") );
	m_pObjClassFactory = new CInverseCModuleClassFactory();

	return IOSTATUS_SUCCESS;
}

VOID CInverseCModuleDriver::OnUnLoad( )
{
	delete m_pObjClassFactory;
}

unsigned long _cdecl CInverseCModuleDriver::INVERSECMODULEDRV_GetVersion( )
{
	return( (INVERSECMODULEDRV_Major << 8) | INVERSECMODULEDRV_Minor );
}

