///////////////////////////////////////////////////////////////////////////////
// InverseCModuleDriver.h

#ifndef __INVERSECMODULEDRIVER_H__
#define __INVERSECMODULEDRIVER_H__

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "TcBase.h"

#define INVERSECMODULEDRV_NAME        "INVERSECMODULE"
#define INVERSECMODULEDRV_Major       1
#define INVERSECMODULEDRV_Minor       0

#define DEVICE_CLASS CInverseCModuleDriver

#include "ObjDriver.h"

class CInverseCModuleDriver : public CObjDriver
{
public:
	virtual IOSTATUS	OnLoad();
	virtual VOID		OnUnLoad();

	//////////////////////////////////////////////////////
	// VxD-Services exported by this driver
	static unsigned long	_cdecl INVERSECMODULEDRV_GetVersion();
	//////////////////////////////////////////////////////
	
};

Begin_VxD_Service_Table(INVERSECMODULEDRV)
	VxD_Service( INVERSECMODULEDRV_GetVersion )
End_VxD_Service_Table


#endif // ifndef __INVERSECMODULEDRIVER_H__