///////////////////////////////////////////////////////////////////////////////
// Inverse.cpp
#include "TcPch.h"
#pragma hdrstop

#include "Inverse.h"
#include "InverseCModuleVersion.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif
DEFINE_THIS_FILE()

///////////////////////////////////////////////////////////////////////////////
// Collection of interfaces implemented by module CInverse
BEGIN_INTERFACE_MAP(CInverse)
	INTERFACE_ENTRY_ITCOMOBJECT()
	INTERFACE_ENTRY(IID_ITcADI, ITcADI)
	INTERFACE_ENTRY(IID_ITcWatchSource, ITcWatchSource)
///<AutoGeneratedContent id="InterfaceMap">
	INTERFACE_ENTRY(IID_IInverseKin, IInverseKin)
///</AutoGeneratedContent>
END_INTERFACE_MAP()

IMPLEMENT_IPERSIST_LIB(CInverse, VID_InverseCModule, CID_InverseCModuleCInverse)
IMPLEMENT_ITCOMOBJECT(CInverse)
IMPLEMENT_ITCOMOBJECT_SETSTATE_LOCKOP2(CInverse)
IMPLEMENT_ITCADI(CInverse)
IMPLEMENT_ITCWATCHSOURCE(CInverse)

///////////////////////////////////////////////////////////////////////////////
// Set parameters of CInverse 
BEGIN_SETOBJPARA_MAP(CInverse)
	SETOBJPARA_DATAAREA_MAP()
///<AutoGeneratedContent id="SetObjectParameterMap">
///</AutoGeneratedContent>
END_SETOBJPARA_MAP()

///////////////////////////////////////////////////////////////////////////////
// Get parameters of CInverse 
BEGIN_GETOBJPARA_MAP(CInverse)
	GETOBJPARA_DATAAREA_MAP()
///<AutoGeneratedContent id="GetObjectParameterMap">
///</AutoGeneratedContent>
END_GETOBJPARA_MAP()

///////////////////////////////////////////////////////////////////////////////
// Get watch entries of CInverse
BEGIN_OBJPARAWATCH_MAP(CInverse)
	OBJPARAWATCH_DATAAREA_MAP()
///<AutoGeneratedContent id="ObjectParameterWatchMap">
///</AutoGeneratedContent>
END_OBJPARAWATCH_MAP()

///////////////////////////////////////////////////////////////////////////////
// Get data area members of CInverse
BEGIN_OBJDATAAREA_MAP(CInverse)
///<AutoGeneratedContent id="ObjectDataAreaMap">
///</AutoGeneratedContent>
END_OBJDATAAREA_MAP()


///////////////////////////////////////////////////////////////////////////////
// Constructor
CInverse::CInverse()
{
///<AutoGeneratedContent id="MemberInitialization">
///</AutoGeneratedContent>
}

///////////////////////////////////////////////////////////////////////////////
// Destructor
CInverse::~CInverse() 
{
}

///////////////////////////////////////////////////////////////////////////////
// State Transitions 
///////////////////////////////////////////////////////////////////////////////
IMPLEMENT_ITCOMOBJECT_SETOBJSTATE_IP_PI(CInverse)

///////////////////////////////////////////////////////////////////////////////
// State transition from PREOP to SAFEOP
//
// Initialize input parameters 
// Allocate memory
HRESULT CInverse::SetObjStatePS(PTComInitDataHdr pInitData)
{
	HRESULT hr = S_OK;
	IMPLEMENT_ITCOMOBJECT_EVALUATE_INITDATA(pInitData);
	return hr;
}

///////////////////////////////////////////////////////////////////////////////
// State transition from SAFEOP to OP
//
// Register with other TwinCAT objects
HRESULT CInverse::SetObjStateSO()
{
	HRESULT hr = S_OK;
	return hr;
}

///////////////////////////////////////////////////////////////////////////////
// State transition from OP to SAFEOP
HRESULT CInverse::SetObjStateOS()
{
	HRESULT hr = S_OK;
	return hr;
}

///////////////////////////////////////////////////////////////////////////////
// State transition from SAFEOP to PREOP
HRESULT CInverse::SetObjStateSP()
{
	HRESULT hr = S_OK;
	return hr;
}

///<AutoGeneratedContent id="ImplementationOf_IInverseKin">
HRESULT CInverse::Inverse(double x, double y, double z, double* s1, double* s2, double* s3)
{
	double d;
	double d1;
	double d2;
	double d3;
	double d4;

	double tmp;

	// change y/z coordinates
	tmp = y;
	y = z;
	z = tmp;

	//  Geometric definitions
	//  mm
	//  mm
	// mm
	//  Vector Declaration
	d = x - z * 0.0;
	d1 = (z + x * 0.0) + 42.0;
	d2 = -2.0 * y * 0.70710678118654746 - 317.1044997386964;
	d3 = d2 + 2.0 * d1 * 0.70710678118654757;
	d4 = y * y;
	d = ((((d * d + d4) + d1 * d1) + 50277.631877264452) - 2.0 * d1 * 224.22674211)
		- 160000.0;
	if (d3 * d3 - 4.0 * d >= 0.0) {
		*s1 = (-d3 + -sqrt_(d3 * d3 - 4.0 * d)) / 2.0;
	}
	else {
		*s1 = (-d3 + -1.0) / 2.0;
	}

	d = x * -0.50000000000000044 - z * -0.86602540378443849;
	d1 = (z * -0.50000000000000044 + x * -0.86602540378443849) + 42.0;
	d3 = d2 + 2.0 * d1 * 0.70710678118654757;
	d = ((((d * d + d4) + d1 * d1) + 50277.631877264452) - 2.0 * d1 * 224.22674211)
		- 160000.0;
	if (d3 * d3 - 4.0 * d >= 0.0) {
		*s2 = (-d3 + -sqrt_(d3 * d3 - 4.0 * d)) / 2.0;
	}
	else {
		*s2 = (-d3 + -1.0) / 2.0;
	}

	d = x * -0.49999999999999978 - z * 0.86602540378443871;
	d1 = (z * -0.49999999999999978 + x * 0.86602540378443871) + 42.0;
	d3 = d2 + 2.0 * d1 * 0.70710678118654757;
	d = ((((d * d + d4) + d1 * d1) + 50277.631877264452) - 2.0 * d1 * 224.22674211)
		- 160000.0;
	if (d3 * d3 - 4.0 * d >= 0.0) {
		*s3 = (-d3 + -sqrt_(d3 * d3 - 4.0 * d)) / 2.0;
	}
	else {
		*s3 = (-d3 + -1.0) / 2.0;
	}

	HRESULT hr = S_OK;
	return hr;
}
///</AutoGeneratedContent>
