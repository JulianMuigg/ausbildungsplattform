﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.6">
  <POU Name="FB_SimulinkInterface" Id="{14a49bc0-817d-4a40-b44d-db00b877a420}" SpecialFunc="None">
    <Declaration><![CDATA[FUNCTION_BLOCK FB_SimulinkInterface
VAR_INPUT
	hmiInArray : REFERENCE TO ARRAY [1..12] OF LREAL;
	hmiOutArray : REFERENCE TO ARRAY [1..18] OF LREAL;
END_VAR

VAR
	nSimulinkMode : SimulinkMode := SimulinkMode.Disabled;
	timeout : UINT := 0;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[// Checking, if plc is controlled via simulink
IF hmiInArray[1] > 0 OR hmiOutArray[1] > 0 THEN
	// First value of hmiInArray defines, if simulink model is controlling the robot
	IF hmiInArray[1] = 1 THEN
		nSimulinkMode := SimulinkMode.Controlled;
	ELSE
		// First vale of hmiOutArray defines, if simulink model runs the software in the loop (SiL) test
		// For that the Control Model is codegenerated and integratet in the twincat
		CASE LREAL_TO_INT( hmiOutArray[1] ) OF
			1: nSimulinkMode := SimulinkMode.SiL;
			2: nSimulinkMode := SimulinkMode.HiL;
			3: nSimulinkMode := SimulinkMode.StepResponse;
		END_CASE		
	END_IF
	
	// Reset input and timeout
	hmiInArray[1] := 0;
	hmiOutArray[1] := 0;
	timeout := 0;
ELSIF nSimulinkMode <> SimulinkMode.Disabled THEN
	timeout := timeout + 1;
	
	// Disable simulink interface, if timeout happens 100 times
	IF timeout >= 200 THEN
		timeout := 0;
		nSimulinkMode := SimulinkMode.Disabled;
	END_IF
END_IF
]]></ST>
    </Implementation>
    <Method Name="fromExternalTrajectory" Id="{d3b35249-0cba-40da-b7b0-41e04bb8f4be}">
      <Declaration><![CDATA[METHOD fromExternalTrajectory
VAR_INPUT
	in : REFERENCE TO ExternalTrajectoryStruct;
	out : REFERENCE TO ARRAY [1..4001] OF LREAL;
END_VAR

VAR
	i: UINT := 1;
END_VAR]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate external trajectory from ExternalTrajectoryStruct to simulink array
FOR i := 1 TO 1000 BY 1 DO
	out[i] := in.trajectory[i-1].t;
	out[1000+i] := in.trajectory[i-1].q1;
	out[2000+i] := in.trajectory[i-1].q2;
	out[3000+i] := in.trajectory[i-1].q3;
END_FOR;
out[4001] := in.size;]]></ST>
      </Implementation>
    </Method>
    <Method Name="fromHmiIn" Id="{9187a622-0ffa-41e9-a2e3-2354807af2f4}">
      <Declaration><![CDATA[METHOD fromHmiIn : ARRAY [1..12] OF LREAL
VAR_INPUT
	in : HmiInStruct;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate data from HmiInStruct to simulink array
fromHmiIn[1] := 0; 
fromHmiIn[2] := ANY_TO_LREAL( in.state );
fromHmiIn[3] := ANY_TO_LREAL( in.positionRequest );
fromHmiIn[4] := ANY_TO_LREAL( in.robotOut.data.pos.x );
fromHmiIn[5] := ANY_TO_LREAL( in.robotOut.data.pos.y );
fromHmiIn[6] := ANY_TO_LREAL( in.robotOut.data.pos.z );
fromHmiIn[7] := ANY_TO_LREAL( in.robotOut.data.v );
fromHmiIn[8] := ANY_TO_LREAL( in.robotOut.data.a );
fromHmiIn[9] := ANY_TO_LREAL( in.robotOut.data._type );
fromHmiIn[10] := ANY_TO_LREAL( in.robotOut.data._r );
fromHmiIn[11] := ANY_TO_LREAL( in.robotOut.positionReached );
fromHmiIn[12] := ANY_TO_LREAL( in.robotOut.errorRobot );
]]></ST>
      </Implementation>
    </Method>
    <Method Name="fromHmiOut" Id="{fb38e65e-f2d6-478a-a211-323730008b80}">
      <Declaration><![CDATA[METHOD fromHmiOut : ARRAY [1..18] OF LREAL;
VAR_INPUT
	in : HmiOutStruct;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate data from HmiOutStruct in simulink array
fromHmiOut[1] := 0;
fromHmiOut[2] := DINT_TO_LREAL( in._transition );
fromHmiOut[3] := BOOL_TO_LREAL( in.touchEvent );
fromHmiOut[4] := in.nextPosition.pos1.x;
fromHmiOut[5] := in.nextPosition.pos1.y;
fromHmiOut[6] := in.nextPosition.pos1.z;
fromHmiOut[7] := in.nextPosition.pos2.x;
fromHmiOut[8] := in.nextPosition.pos2.y;
fromHmiOut[9] := in.nextPosition.pos2.z;
fromHmiOut[10] := BOOL_TO_LREAL( in.nextPosition.newPos );
fromHmiOut[11] := in.parameter.v_max;
fromHmiOut[12] := in.parameter.a_max;
fromHmiOut[13] := in.parameter.dq_max;
fromHmiOut[14] := in.parameter.ddq_max;
fromHmiOut[15] := in.parameter.z_max;
fromHmiOut[16] := in.parameter.homeX;
fromHmiOut[17] := in.parameter.homeY;
fromHmiOut[18] := in.parameter.homeZ;]]></ST>
      </Implementation>
    </Method>
    <Method Name="fromJointSpace" Id="{7c5cecb5-a9f4-4a81-bfa6-61316fe3600f}">
      <Declaration><![CDATA[METHOD fromJointSpace : ARRAY [1..10] OF LREAL
VAR_INPUT
	in : JointSpaceStruct;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate robot axis data from JointSpaceStruct to simulink array
fromJointSpace[1] := in.q1.p;
fromJointSpace[2] := in.q1.v;
fromJointSpace[3] := in.q1.a;
										 
fromJointSpace[4] := in.q2.p;
fromJointSpace[5] := in.q2.v;
fromJointSpace[6] := in.q2.a;
										 
fromJointSpace[7] := in.q3.p;
fromJointSpace[8] := in.q3.v;
fromJointSpace[9] := in.q3.a;

fromJointSpace[10] := DINT_TO_LREAL( in.error );]]></ST>
      </Implementation>
    </Method>
    <Method Name="mode" Id="{5f166160-6fdb-4f6c-8a70-99024722fd3a}">
      <Declaration><![CDATA[METHOD mode : SimulinkMode
VAR_INPUT
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[mode := nSimulinkMode;]]></ST>
      </Implementation>
    </Method>
    <Method Name="toExternalTrajectory" Id="{991539ca-6c6a-4e73-987c-74b120eee387}">
      <Declaration><![CDATA[METHOD toExternalTrajectory 
VAR_INPUT
	in : REFERENCE TO ARRAY [1..4001] OF LREAL;
	out : REFERENCE TO ExternalTrajectoryStruct;
END_VAR
VAR
	i : UINT := 1;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate external trajectory simulink array to from ExternalTrajectoryStruct
FOR i := 1 TO 1000 BY 1 DO
	out.trajectory[i-1].t := in[i];
	out.trajectory[i-1].q1 := in[1000+i];
	out.trajectory[i-1].q2 := in[2000+i];
	out.trajectory[i-1].q3 := in[3000+i];
END_FOR;
out.size := in[4001];

	]]></ST>
      </Implementation>
    </Method>
    <Method Name="toHmiIn" Id="{85327220-7c69-4076-8730-e34ad6ecb307}">
      <Declaration><![CDATA[METHOD toHmiIn : HmiInStruct
VAR_INPUT
	in : ARRAY [1..12] OF LREAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate data from simulink array in HmiInStruct 
toHmiIn.state := LREAL_TO_DINT( in[2] );
toHmiIn.positionRequest := LREAL_TO_BOOL( in[3] );
toHmiIn.robotOut.data.pos.x := in[4];
toHmiIn.robotOut.data.pos.y := in[5];
toHmiIn.robotOut.data.pos.z := in[6];
toHmiIn.robotOut.data.v := in[7];
toHmiIn.robotOut.data.a := in[8];
toHmiIn.robotOut.data._type := LREAL_TO_DINT( in[9] );
toHmiIn.robotOut.data._r := in[10];
toHmiIn.robotOut.positionReached := LREAL_TO_BOOL( in[11] );
toHmiIn.robotOut.errorRobot := LREAL_TO_DINT( in[12] );
	]]></ST>
      </Implementation>
    </Method>
    <Method Name="toHmiOut" Id="{998c427c-9239-4685-8d79-42f5f2c3ccf5}">
      <Declaration><![CDATA[METHOD toHmiOut : HmiOutStruct
VAR_INPUT
	in : ARRAY [1..18] OF LREAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate data from simulink array in HmiOutStruct
toHmiOut._transition := LREAL_TO_DINT( in[2] );
toHmiOut.touchEvent := LREAL_TO_BOOL( in[3] );
toHmiOut.nextPosition.pos1.x := in[4];
toHmiOut.nextPosition.pos1.y := in[5];
toHmiOut.nextPosition.pos1.z := in[6];
toHmiOut.nextPosition.pos2.x := in[7];
toHmiOut.nextPosition.pos2.y := in[8];
toHmiOut.nextPosition.pos2.z := in[9];
toHmiOut.nextPosition.newPos := LREAL_TO_BOOL( in[10] );
toHmiOut.parameter.v_max := in[11];
toHmiOut.parameter.a_max := in[12];
toHmiOut.parameter.dq_max := in[13];
toHmiOut.parameter.ddq_max := in[14];
toHmiOut.parameter.z_max := in[15];
toHmiOut.parameter.homeX := in[16];
toHmiOut.parameter.homeY := in[17];
toHmiOut.parameter.homeZ := in[18];]]></ST>
      </Implementation>
    </Method>
    <Method Name="toJointSpace" Id="{a9ba006e-1a27-4a28-bd4c-c05b4eff0581}">
      <Declaration><![CDATA[METHOD toJointSpace : JointSpaceStruct
VAR_INPUT
	in : ARRAY [1..10] OF LREAL;
END_VAR
]]></Declaration>
      <Implementation>
        <ST><![CDATA[// Allocate robot axis data from simulink array to JointSpaceStruct	
toJointSpace.q1.p := in[1];
toJointSpace.q1.v := in[2];
toJointSpace.q1.a := in[3];
				 
toJointSpace.q2.p := in[4];
toJointSpace.q2.v := in[5];
toJointSpace.q2.a := in[6];	
			 
toJointSpace.q3.p := in[7];
toJointSpace.q3.v := in[8];
toJointSpace.q3.a := in[9];

toJointspace.error := LREAL_TO_DINT( in[10] );]]></ST>
      </Implementation>
    </Method>
    <LineIds Name="FB_SimulinkInterface">
      <LineId Id="12" Count="2" />
      <LineId Id="43" Count="1" />
      <LineId Id="46" Count="0" />
      <LineId Id="49" Count="0" />
      <LineId Id="56" Count="0" />
      <LineId Id="48" Count="0" />
      <LineId Id="50" Count="0" />
      <LineId Id="52" Count="1" />
      <LineId Id="51" Count="0" />
      <LineId Id="45" Count="0" />
      <LineId Id="23" Count="10" />
      <LineId Id="36" Count="2" />
      <LineId Id="9" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.fromExternalTrajectory">
      <LineId Id="6" Count="6" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.fromHmiIn">
      <LineId Id="8" Count="12" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.fromHmiOut">
      <LineId Id="7" Count="17" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.fromJointSpace">
      <LineId Id="7" Count="0" />
      <LineId Id="9" Count="9" />
      <LineId Id="5" Count="0" />
      <LineId Id="24" Count="0" />
      <LineId Id="23" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.mode">
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.toExternalTrajectory">
      <LineId Id="7" Count="0" />
      <LineId Id="9" Count="5" />
      <LineId Id="27" Count="0" />
      <LineId Id="15" Count="0" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.toHmiIn">
      <LineId Id="20" Count="0" />
      <LineId Id="8" Count="10" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.toHmiOut">
      <LineId Id="23" Count="0" />
      <LineId Id="7" Count="15" />
      <LineId Id="5" Count="0" />
    </LineIds>
    <LineIds Name="FB_SimulinkInterface.toJointSpace">
      <LineId Id="8" Count="0" />
      <LineId Id="10" Count="6" />
      <LineId Id="20" Count="0" />
      <LineId Id="18" Count="1" />
      <LineId Id="5" Count="0" />
      <LineId Id="26" Count="0" />
      <LineId Id="25" Count="0" />
    </LineIds>
  </POU>
</TcPlcObject>