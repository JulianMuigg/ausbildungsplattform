REM clear directory to refill it with the needed files
rmdir /s /q %~dp0Ausbildungsplattform\

REM copy main simulink and matlab files
xcopy %~dp0Simulink\InitAusbildungsplattform.m %~dp0Ausbildungsplattform\Simulink\
xcopy %~dp0Simulink\AusbildungsplattformBusStructs.mat %~dp0Ausbildungsplattform\Simulink\

REM copy system models from simulink folder
xcopy %~dp0Simulink\SystemModels\ %~dp0Ausbildungsplattform\Simulink\SystemModels\

REM copy TE1400 from simulink folder
xcopy %~dp0Simulink\TE1400\ %~dp0Ausbildungsplattform\Simulink\TE1400\

REM copy TE1410 from simulink folder
xcopy %~dp0Simulink\TE1410\ %~dp0Ausbildungsplattform\Simulink\TE1410\

REM copy run skripts from simulink folder
xcopy %~dp0Simulink\RunSkripts\ %~dp0Ausbildungsplattform\

REM copy external functions
xcopy %~dp0Simulink\ExternalFunctions\ %~dp0Ausbildungsplattform\Simulink\ExternalFunctions\

REM copy main library
xcopy %~dp0Simulink\Library\AusbildungsplattformLibrary.slx %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\ErrorType.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\MoveType.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\ParameterType.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\RobotState.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\RobotTransition.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\RobotType.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\slblocks.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\State.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\Trajectory.m %~dp0Ausbildungsplattform\Simulink\Library\
xcopy %~dp0Simulink\Library\Transition.m %~dp0Ausbildungsplattform\Simulink\Library\

REM copy control library
xcopy %~dp0Simulink\Control\ControlLibrary.slx %~dp0Ausbildungsplattform\Simulink\Control\
xcopy %~dp0Simulink\Control\HmiInterface.m %~dp0Ausbildungsplattform\Simulink\Control\
xcopy %~dp0Simulink\Control\RobotControl.m %~dp0Ausbildungsplattform\Simulink\Control\
xcopy %~dp0Simulink\Control\SequenceControl.m %~dp0Ausbildungsplattform\Simulink\Control\

REM copy delta 360 library
xcopy %~dp0Simulink\Delta360\Delta360Library.slx %~dp0Ausbildungsplattform\Simulink\Delta360\
xcopy %~dp0Simulink\Delta360\*.STEP %~dp0Ausbildungsplattform\Simulink\Delta360\
xcopy %~dp0Simulink\Delta360\Delta360.m %~dp0Ausbildungsplattform\Simulink\Delta360\
xcopy %~dp0Simulink\Delta360\DeltaPathPlanning.m %~dp0Ausbildungsplattform\Simulink\Delta360\
xcopy %~dp0Simulink\Delta360\Delta_Robot_V3_DataFile.m %~dp0Ausbildungsplattform\Simulink\Delta360\
xcopy %~dp0Simulink\Delta360\*.stp %~dp0Ausbildungsplattform\Simulink\Delta360\

REM copy scara library
xcopy %~dp0Simulink\Scara\ScaraLibrary.slx %~dp0Ausbildungsplattform\Simulink\Scara\
xcopy %~dp0Simulink\Scara\*.STEP %~dp0Ausbildungsplattform\Simulink\Scara\
xcopy %~dp0Simulink\Scara\Scara.m %~dp0Ausbildungsplattform\Simulink\Scara\
xcopy %~dp0Simulink\Scara\ScaraPathPlanning.m %~dp0Ausbildungsplattform\Simulink\Scara\
xcopy %~dp0Simulink\Scara\*.stl %~dp0Ausbildungsplattform\Simulink\Scara\
xcopy %~dp0Simulink\Scara\*.stp %~dp0Ausbildungsplattform\Simulink\Scara\

REM copy hmi app
xcopy /e /v %~dp0HMI\App\ %~dp0Ausbildungsplattform\HMI\



