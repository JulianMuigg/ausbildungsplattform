%% Run this script before using the Simulink files
% Define the Sample time for the simulations
stepSize = 0.01;

%% Has to be changed between 25001-25100, if following error occurred
% Error reported by S-function 'slrealtimeTCPClient' in 'Control/HMI 
% TcpInterface/TCP Client': TCP Client(127.0.0.1:25001) Connect Error: 
% Only one usage of each socket address (protocol/network address/port) 
% is normally permitted (10048) 
clientPort = 25000 + randi(100,1); 

%% Init robot type
if ~exist( 'robotType', 'var' )
%    robotType = RobotType.Scara;
    robotType = RobotType.Delta360;
end

%% Init common structs
load( 'AusbildungsplattformBusStructs' );

%% Initialize Delta360
Delta_Robot_V3_DataFile;
rm = 0.042;
rb = 0.222106;
l=0.400;
w2b = -0.079855;
range=0.144;
Name='Delta';
hardStopZHeight = 0.333;
delta360 = Delta360(rm,rb,l,w2b,range,Name);

%% Initialize Scara
a1 = 0.15;
a2 = 0.15;
scara = Scara( a1, a2 );

%% Simulink Ads constants from TwinCAT target browser
if exist( 'simMode', 'var' )
    if simMode == SimMode.TE1410 || ...
       xilMode == XilMode.SiL || ...
       xilMode == XilMode.HiL || ...
       simMode == SimMode.StepResponse
        if ~exist( 'netIdStr', 'var' )
            netIdStr = '192.168.26.100.1.1';
        end

        s = strsplit( netIdStr, '.' );
        netId = uint8([str2num(s{1}), str2num(s{2}), str2num(s{3}), str2num(s{4}), ...
            str2num(s{5}), str2num(s{6})]);
        port = uint16(851);

        ads = TcAds( netIdStr, 851 );
        if ~ads.isConnected()
            waitfor( errordlg( 'AMS Net Id is not valid', 'ADS Connection failed' ) );
        end

        idxGroup = ads.idxGroup( 'PRG_GenAds.axisInArray' );

        % Robot axis adresses
        adsAxisIn = ads.idxOffset( 'PRG_GenAds.axisInArray' );
        adsAxisOut = ads.idxOffset( 'PRG_GenAds.axisOutArray' );

        % HMI adresses
        adsExternalTrajectory = ads.idxOffset( 'PRG_GenAds.externalTrajectory' );
        adsHmiIn = ads.idxOffset( 'PRG_GenAds.hmiInArray' );
        adsHmiOut = ads.idxOffset( 'PRG_GenAds.hmiOutArray' );
    end
end