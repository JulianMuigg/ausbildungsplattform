function [mStruct] = createScaraOutStruct()
%CREATEMOVESTRUCT function to create a move struct
% q_start...trajectory start point
% q_stop....trajectory stop point
% dq_start..starting speed
% dq_stop...stop speed
% dq_max....max speed
% ddq_max...max acceleration
% tolerance.acceptable radius for endpoint being reached
% oldq......old trajectory value

posStruct = createPosVelAccStruct();
jointSpaceStruct = struct( 'theta1', { posStruct }, 'theta2', { posStruct } );
taskSpaceStruct = struct( 'x', { 0 }, 'y', { 0 }, 'z', { 0 }, 'v', {0}, 'a', {0} );

mStruct = struct('jointSpace', { jointSpaceStruct }, ...
                 'taskSpace', { taskSpaceStruct }, ...
                 'error', { 0 } );
end