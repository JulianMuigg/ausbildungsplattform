% Simscape(TM) Multibody(TM) version: 7.1

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(19).translation = [0.0 0.0 0.0];
smiData.RigidTransform(19).angle = 0.0;
smiData.RigidTransform(19).axis = [0.0 0.0 0.0];
smiData.RigidTransform(19).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [0 0 0];  % mm
smiData.RigidTransform(1).angle = 0;  % rad
smiData.RigidTransform(1).axis = [0 0 0];
smiData.RigidTransform(1).ID = 'B[Base_tmotor_assembly:1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [0 0 0];  % mm
smiData.RigidTransform(2).angle = 0;  % rad
smiData.RigidTransform(2).axis = [0 0 0];
smiData.RigidTransform(2).ID = 'F[Base_tmotor_assembly:1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [-3.2862601528904634e-13 1.3322676295501875e-14 30];  % mm
smiData.RigidTransform(3).angle = 2.9537998553466113e-15;  % rad
smiData.RigidTransform(3).axis = [0.041689358925316118 -0.99913062076607195 -6.1517482623012673e-17];
smiData.RigidTransform(3).ID = 'B[Plexi1_assembly:1:-:Base_tmotor_assembly:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [1.476048490826067e-15 -9.3395391047524554e-14 102.8];  % mm
smiData.RigidTransform(4).angle = 2.9534213468522331e-15;  % rad
smiData.RigidTransform(4).axis = [0.99409760246078815 0.10848943165909143 1.5926189332120524e-16];
smiData.RigidTransform(4).ID = 'F[Plexi1_assembly:1:-:Base_tmotor_assembly:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [-1.7763568394002505e-14 -1.7311599638447269e-14 -29.999999999999964];  % mm
smiData.RigidTransform(5).angle = 3.1415926535897927;  % rad
smiData.RigidTransform(5).axis = [1 -2.4626278064468725e-32 -7.2322919909550739e-17];
smiData.RigidTransform(5).ID = 'B[Plexi2_assembly:1:-:Plexi1_assembly:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [0.038839484912642774 -150.00143502035374 122.79999999999968];  % mm
smiData.RigidTransform(6).angle = 2.3801145030174398e-15;  % rad
smiData.RigidTransform(6).axis = [-0.94253451972898594 -0.33410878336142252 3.7475981239761481e-16];
smiData.RigidTransform(6).ID = 'F[Plexi2_assembly:1:-:Plexi1_assembly:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [0 0 -8.8817841970012523e-15];  % mm
smiData.RigidTransform(7).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(7).axis = [0 -1.1102230246251547e-16 1];
smiData.RigidTransform(7).ID = 'AssemblyGround[Plexi1_assembly:1:plexi1:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [-5.4178883601707649e-13 0 4.9999999999999822];  % mm
smiData.RigidTransform(8).angle = 1.4720925152396279e-15;  % rad
smiData.RigidTransform(8).axis = [-0.075418019800504735 -0.26396306930176439 0.96157975245642657];
smiData.RigidTransform(8).ID = 'AssemblyGround[Plexi1_assembly:1:plexi1_2:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [-5.7114903084798395e-13 6.5225974429604265e-15 25];  % mm
smiData.RigidTransform(9).angle = 1.6567037377967271e-15;  % rad
smiData.RigidTransform(9).axis = [-0.16171750422764691 -0.95430317504430406 -0.25130240533417381];
smiData.RigidTransform(9).ID = 'AssemblyGround[Plexi1_assembly:1:Plexi1_3:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [-3.2862601528904634e-13 1.3322676295501875e-14 30];  % mm
smiData.RigidTransform(10).angle = 2.1158466633549562;  % rad
smiData.RigidTransform(10).axis = [0.56313670146659589 0.58432741483748651 0.5843274148374884];
smiData.RigidTransform(10).ID = 'AssemblyGround[Plexi1_assembly:1:sensorwelle:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [0.038839484912718159 -150.00143502035385 30];  % mm
smiData.RigidTransform(11).angle = 3.1415926535897927;  % rad
smiData.RigidTransform(11).axis = [-7.2664756514031373e-16 -1.1039961366528451e-16 1];
smiData.RigidTransform(11).ID = 'AssemblyGround[Plexi1_assembly:1:tmotor77:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [-1.7763568394002505e-14 0 0];  % mm
smiData.RigidTransform(12).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(12).axis = [1 0 0];
smiData.RigidTransform(12).ID = 'AssemblyGround[Plexi2_assembly:1:plexi2_1:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [67.892611136583568 -3.7747582837255354e-14 -4.9999999999999645];  % mm
smiData.RigidTransform(13).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(13).axis = [1 -1.2490009027033021e-16 4.1633363423443358e-17];
smiData.RigidTransform(13).ID = 'AssemblyGround[Plexi2_assembly:1:Plexi2_2:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [67.764545805252382 -1.2875810024864424e-14 -24.999999999999929];  % mm
smiData.RigidTransform(14).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(14).axis = [-1 5.5511151231257926e-17 -1.5291522725616911e-16];
smiData.RigidTransform(14).ID = 'AssemblyGround[Plexi2_assembly:1:Plexi2_3:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [-1.7763568394002505e-14 -1.7311599638447269e-14 -29.999999999999964];  % mm
smiData.RigidTransform(15).angle = 1.5707963267948981;  % rad
smiData.RigidTransform(15).axis = [-1 -5.9738526057953691e-17 -1.6318229049341285e-16];
smiData.RigidTransform(15).ID = 'AssemblyGround[Plexi2_assembly:1:sensorwelle:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [149.99999999999974 -8.9461251670463549e-14 -4.9999999999999645];  % mm
smiData.RigidTransform(16).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(16).axis = [3.8163916471489768e-17 -1 1.1814451177833304e-16];
smiData.RigidTransform(16).ID = 'AssemblyGround[Plexi2_assembly:1:solenoid:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [0 0 0];  % mm
smiData.RigidTransform(17).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(17).axis = [-1.1449174941446924e-16 -1.1275702593849244e-16 1];
smiData.RigidTransform(17).ID = 'AssemblyGround[Base_tmotor_assembly:1:base_mount:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [0 0 9.9999999999999947];  % mm
smiData.RigidTransform(18).angle = 2.7328427800649908e-16;  % rad
smiData.RigidTransform(18).axis = [-1 0 0];
smiData.RigidTransform(18).ID = 'AssemblyGround[Base_tmotor_assembly:1:tmotor:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [0 0 0];  % mm
smiData.RigidTransform(19).angle = 0;  % rad
smiData.RigidTransform(19).axis = [0 0 0];
smiData.RigidTransform(19).ID = 'RootGround[Base_tmotor_assembly:1]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(11).mass = 0.0;
smiData.Solid(11).CoM = [0.0 0.0 0.0];
smiData.Solid(11).MoI = [0.0 0.0 0.0];
smiData.Solid(11).PoI = [0.0 0.0 0.0];
smiData.Solid(11).color = [0.0 0.0 0.0];
smiData.Solid(11).opacity = 0.0;
smiData.Solid(11).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.22936801553799582;  % kg
smiData.Solid(1).CoM = [6.9542429408696137e-11 57.48560492842082 2.5000000000000107];  % mm
smiData.Solid(1).MoI = [948.17036958737208 180.23393470551321 1127.4486042281435];  % kg*mm^2
smiData.Solid(1).PoI = [2.2737367544323206e-13 1.4250645108421463e-13 -2.5364325866108902e-10];  % kg*mm^2
smiData.Solid(1).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'plexi1.ipt_{3972F6D3-4DC6-30C5-E102-2E821F93B50A}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.54754090625006402;  % kg
smiData.Solid(2).CoM = [-0.034168794019433499 -33.763636133036194 9.9999999999999982];  % mm
smiData.Solid(2).MoI = [1870.6959513064221 656.20443242128977 2490.3976566443735];  % kg*mm^2
smiData.Solid(2).PoI = [2.1145751816220583e-12 6.4439564795293334e-13 0.74716857008111859];  % kg*mm^2
smiData.Solid(2).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'plexi1_2.ipt_{77C72E13-4FF2-F516-01D5-868FA87C56D5}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.23378703227221295;  % kg
smiData.Solid(3).CoM = [-0.021145947242107201 -56.822962051488815 2.4999999999999978];  % mm
smiData.Solid(3).MoI = [966.36766236853828 180.69895065765797 1146.0925003917287];  % kg*mm^2
smiData.Solid(3).PoI = [6.5938365878537305e-13 3.0361130276546078e-13 0.39606030331509429];  % kg*mm^2
smiData.Solid(3).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'Plexi1_3.ipt_{122CA60E-48D8-D51B-7F3A-C99D5591AA55}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.056046010363463142;  % kg
smiData.Solid(4).CoM = [7.9137110439452115e-10 -40.041036475505969 0.035073011507994474];  % mm
smiData.Solid(4).MoI = [97.204975401587916 8.6463600735829296 97.224605470966992];  % kg*mm^2
smiData.Solid(4).PoI = [0.12422702090779364 1.547926191106145e-12 -3.9269005194088533e-09];  % kg*mm^2
smiData.Solid(4).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'sensorwelle.ipt_{69B4E723-4779-B5C5-30F4-03BF7AA6E5B7}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.25813636963155079;  % kg
smiData.Solid(5).CoM = [-7.4890617059515123e-10 0 30.149382743943853];  % mm
smiData.Solid(5).MoI = [179.58545314716531 179.58545516539363 202.98924015429827];  % kg*mm^2
smiData.Solid(5).PoI = [0 -1.4418468914262535e-09 3.637978807083199e-13];  % kg*mm^2
smiData.Solid(5).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'tmotor77.ipt_{C1D1DD1D-4A34-857F-4C8E-A4ADF820A013}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 0.13869322933003586;  % kg
smiData.Solid(6).CoM = [63.979938779547894 -1.3402094721763603e-14 2.5];  % mm
smiData.Solid(6).MoI = [49.81234367421952 459.30474189142359 508.5391971101015];  % kg*mm^2
smiData.Solid(6).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(6).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'plexi2_1.ipt_{D98B1E0A-45AA-A9C9-4195-7D97BEF5C52D}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 0.41774110755946109;  % kg
smiData.Solid(7).CoM = [-10.279411175057012 1.7798390085264416e-14 10];  % mm
smiData.Solid(7).MoI = [206.97122964515299 1524.4024691296995 1703.5242916042218];  % kg*mm^2
smiData.Solid(7).PoI = [0 0 1.2184119772783462e-13];  % kg*mm^2
smiData.Solid(7).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'Plexi2_2.ipt_{F925A8F6-40FD-AB40-4DF3-F9BC12798F2F}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 0.13845352581056691;  % kg
smiData.Solid(8).CoM = [-3.805903235223707 0 2.5];  % mm
smiData.Solid(8).MoI = [49.763529051324745 458.34391349417388 507.53055285462125];  % kg*mm^2
smiData.Solid(8).PoI = [0 0 -9.6776810479244737e-14];  % kg*mm^2
smiData.Solid(8).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(8).opacity = 1;
smiData.Solid(8).ID = 'Plexi2_3.ipt_{A5AB8A39-4CE6-4788-FBA2-A8B7B13A8E5A}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(9).mass = 0.026197212190063941;  % kg
smiData.Solid(9).CoM = [0 -3.8503580471345428e-11 18.703611193732701];  % mm
smiData.Solid(9).MoI = [4.6071298218524728 4.6071294526107582 2.8593054409218936];  % kg*mm^2
smiData.Solid(9).PoI = [3.4676215928852873e-10 0 0];  % kg*mm^2
smiData.Solid(9).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(9).opacity = 1;
smiData.Solid(9).ID = 'solenoid.ipt_{E678E49E-4B5E-7E7D-D617-B6964E99D613}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(10).mass = 0.61149775444954235;  % kg
smiData.Solid(10).CoM = [-3.6736879195923501e-11 -8.9239382662162338e-14 4.9999999999999902];  % mm
smiData.Solid(10).MoI = [4659.6156878053316 4659.6156879083474 9309.0287605164813];  % kg*mm^2
smiData.Solid(10).PoI = [-8.1854523159563473e-13 -1.8189894035437886e-13 2.9103830456735707e-11];  % kg*mm^2
smiData.Solid(10).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(10).opacity = 1;
smiData.Solid(10).ID = 'base_mount.ipt_{A0A7B7E6-4017-CD78-3FBE-7EB9F549D237}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(11).mass = 0.59253058410834869;  % kg
smiData.Solid(11).CoM = [-3.2634976354586214e-10 1.2387243804250293e-13 29.709618315584841];  % mm
smiData.Solid(11).MoI = [674.25639696818098 674.25639898640759 999.73261731292257];  % kg*mm^2
smiData.Solid(11).PoI = [2.0425937595608413e-12 -1.3581579861857529e-09 4.3655745684851293e-12];  % kg*mm^2
smiData.Solid(11).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(11).opacity = 1;
smiData.Solid(11).ID = 'tmotor.ipt_{40848DF6-4F2A-6853-1558-E2AA88714D03}';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the RevoluteJoint structure array by filling in null values.
smiData.RevoluteJoint(2).Rz.Pos = 0.0;
smiData.RevoluteJoint(2).ID = '';

smiData.RevoluteJoint(1).Rz.Pos = -89.98516455183281;  % deg
smiData.RevoluteJoint(1).ID = '[Plexi1_assembly:1:-:Base_tmotor_assembly:1]';

smiData.RevoluteJoint(2).Rz.Pos = 89.985164551832796;  % deg
smiData.RevoluteJoint(2).ID = '[Plexi2_assembly:1:-:Plexi1_assembly:1]';

