tsample=0.001;
maxv=0.1;
maxa=0.01;
q0=0.1;
q1=0.12;
k=0;

h= abs(q0-q1);
	t0=0
	if (h>=(maxv^2/maxa))
		Ta=maxv/maxa;
		T=(h*maxa+maxv^2)/(maxa*maxv);
		
		for t=0:tsample:T
            k=k+1;
			if (0<t)&&(t<=Ta)
				q(k)=q0+0.5*maxa*(t)^2;
			elseif (Ta<t)&&(t<=(T-Ta))
				q(k)=q0+maxa*Ta*(t-0.5*Ta);
			elseif ((T-Ta)<t)&&(t<=T) 
				q(k)=q1-0.5*maxa*(T-t)^2
            end
            time(k)=t*tsample;
		end
    
	else
		Ta=sqrt(h/maxa);
		T=2*Ta;
		maxv=maxa*Ta;
		
		for t=0:tsample:((T-Ta))
            k=k+1;
			if (0<=t)&&(t<=Ta)
				q(k)=q0+0.5*maxa*t^2;
            elseif (T-Ta<t)&&(t<=T)
				q(k)=q1-0.5*maxa*(T-t)^2;
            else q(k)=q(k-1);
		end
		time(k)=t;
        end
    end
    plot(time,q);