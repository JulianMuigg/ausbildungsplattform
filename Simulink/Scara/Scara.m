classdef Scara
    %SCARA
    
    properties
        a1
        a2
    end
    
    methods
        function obj = Scara(a1,a2)
            obj.a1 = a1;
            obj.a2 = a2;
        end
        
        function P = forward(obj, S)
            theta1 = S(1);
            theta2 = S(2);

            x = obj.a1*cos(theta1)+obj.a2*cos(theta1+theta2);
            y = obj.a1*sin(theta1)+obj.a2*sin(theta1+theta2);
            z = S(3);

            P = [x,y,z];
        end
        
        function S = inverse(obj, P)            
            x = P(1);
            y = P(2);

            C2 = (x^2+y^2-obj.a1^2-obj.a2^2)/(2*obj.a1*obj.a2);
            if C2^2>1
               error('Inverse kinematics cannot be resolved, trajectory goes through singularity');
            end
            S2 = sqrt(1-C2^2);
            T2_1 = atan2( S2, C2 );
            T2_2 = atan2( -S2, C2 );
            
            S1_1=(y*(obj.a1+obj.a2*cos(T2_1))-x*(obj.a2*sin(T2_1)))/(obj.a1^2+obj.a2^2+2*obj.a1*obj.a2*cos(T2_1));
            C1_1=(x*(obj.a1+obj.a2*cos(T2_1))+y*(obj.a2*sin(T2_1)))/(obj.a1^2+obj.a2^2+2*obj.a1*obj.a2*cos(T2_1));
            T1_1 = atan2( S1_1, C1_1 );

            S1_2=(y*(obj.a1+obj.a2*cos(T2_2))-x*(obj.a2*sin(T2_2)))/(obj.a1^2+obj.a2^2+2*obj.a1*obj.a2*cos(T2_2));
            C1_2=(x*(obj.a1+obj.a2*cos(T2_2))+y*(obj.a2*sin(T2_2)))/(obj.a1^2+obj.a2^2+2*obj.a1*obj.a2*cos(T2_2));
            T1_2 = atan2( S1_2, C1_2 );

            S = [T1_1,T2_1;T1_2,T2_2];
        end
        
        function S = inverseImproved( obj, trajIn )
            persistent T1_
            persistent T2_
            persistent stopT1_
            persistent stopT2_
            persistent stopZ_

            T = obj.inverse( [trajIn.s1.p, trajIn.s2.p] );

            T1_1 = T(1,1);
            T2_1 = T(1,2);

            T1_2 = T(2,1);
            T2_2 = T(2,2);

            if isempty( T1_ )
                T1_ = trajIn.q1.pCurr;
                T2_ = trajIn.q2.pCurr;
                stopT1_ = trajIn.q1.stop;
                stopT2_ = trajIn.q2.stop;
                stopZ_ = trajIn.q3.stop;
            end
            if stopT1_ ~= trajIn.q1.stop || stopT2_ ~= trajIn.q2.stop || stopZ_ ~= trajIn.q3.stop
                T1_ = trajIn.q1.pCurr;
                T2_ = trajIn.q2.pCurr;

                if T1_ > pi
                    T1_ = T1_ - 2*pi;
                elseif T1_ <= -pi
                    T1_ = T1_ + 2*pi;
                end

                if T2_ > pi
                    T2_ = T2_ - 2*pi;
                elseif T2_ <= -pi
                    T2_ = T2_ + 2*pi;
                end

                stopT1_ = trajIn.q1.stop;
                stopT2_ = trajIn.q2.stop;
                stopZ_ = trajIn.q3.stop;
            end

            if abs(T1_1-T1_) + abs(T2_1-T2_) < 5e-2 || abs(T1_1-T1_) <= abs(T1_2-T1_)
                T1 = T1_1;
                T2 = T2_1;
            else
                T1 = T1_2;
                T2 = T2_2;
            end
            
            S = [T1, T2];            
        end
        
        function TJ = moveL( obj, dt, p1, p2, v_max, a_max )
            %% Generate trajectory
            traj = Trajectory();
            TJ = traj.execAll( dt, p1, p2, v_max, a_max, MoveType.MoveL );
                        
            % Convert to joint space
            for n = 1:1:size(TJ,1)
                res = obj.inverse( [TJ(n,2), TJ(n,3)] );
                TJ(n,2:3) = res(1,:);
            end
        end
        
        function TJ = moveJ( obj, dt, p1, p2, v_max, a_max )
            res = obj.inverse( p1 );
            q1 = [res(1,:) p1(3)];
            
            res = obj.inverse( p2 );
            q2 = [res(1,:) p2(3)];
            
            %% Generate trajectory
            traj = Trajectory();
            TJ = traj.execAll( dt, q1', q2', v_max, a_max, MoveType.MoveJ );            
        end
    end
end

