function [q]= trapezoidaltraj(q0,q1,v0,v1,maxv,maxa, tsample)
	
	h= abs(q0-q1);
	t0=0
	if (h>=(maxv^2/maxa))
		Ta=maxv/maxa;
		T=(h*maxa+maxv^2)/(maxa*maxv);
		
		for t=1:tsample:T 
			if 0<t<=Ta
				q(t)=q0+0.5*maxa*(t)^2;
			elseif Ta<t<=(T-Ta)
				q(t)=q0+maxa*Ta*(t-0.5*Ta);
			elseif (T-Ta)<t<=T 
				q(t)=q1-0.5*maxa*(T-t)^2
			end
		end
	
	else
		Ta=sqrt(h/maxa);
		T=2*Ta;
		maxv=maxa*Ta;
		
		for t=1:tsample:T-Ta
			if 0<t<=Ta
				q(t)=q0+0.5*maxa*t^2
			elseif T-Ta<t<=T
				q(t)=q1-0.5*maxa*(T-t)^2;
		end
		
	end
end