function [mStruct] = createScaraInStruct()
%CREATEMOVESTRUCT function to create a move struct
% theta1 ... input values of first motor
% theta2 ... input values of second motor
% z ... input values of linear z axis

posStruct = createPosVelAccStruct();
mStruct = struct( 'theta1', { posStruct }, ...
                  'theta2', { posStruct }, ...
                  'z', { posStruct } );

end