clear all

%% Init Scara struct
scara = Scara( 0.15, 0.15 );

x0 = -0.05;
y0 = -0.05;
x1 = -0.075;
y1 = -0.075;
z1 = 0.020;
x2 = 0.150;
y2 = 0.100;
z2 = 0.020;
p0 = [x0; y0; 0.000];
p1 = [x1; y1; 0.000];
p2 = [x1; y1; z1];
p3 = [x2; y2; 0.000];
p4 = [x2; y2; z2];

dt = 0.01;

%% Generate trajectory
traj = Trajectory();
TJ1 = scara.moveJ( dt, p0, p1, 2.0, 1.5 );
TJ2 = scara.moveL( dt, p1, p2, 0.5, 0.5 );
TJ3 = scara.moveL( dt, p2, p1, 0.5, 0.5 );
TJ4 = scara.moveJ( dt, p1, p3, 2.0, 1.5 );
TJ5 = scara.moveL( dt, p3, p4, 0.5, 0.5 );
TJ6 = scara.moveJ( dt, p4, p3, 0.5, 0.5 );
TJ7 = scara.moveJ( dt, p3, p0, 2.0, 1.5 );

TJ = [TJ1; TJ2; TJ3; TJ4; TJ5; TJ6; TJ7];
TJ(:,1) = (0:dt:(size(TJ,1)-1)*dt)';



%% Plot results
figure
plot( TJ(:,1), TJ(:,2), 'r-' );
hold on
plot( TJ(:,1), TJ(:,3), 'g--' );
plot( TJ(:,1), TJ(:,4), 'b-.' );
legend('q1 in rad','q2 in rad', 's3 in m');

%% Save to csv
s = length(TJ(:,1));
for i = s:1:1000
   TJ(i,:) = [-1,0,0,0]; 
end
csvwrite('TrajectoryScara.csv', TJ );




