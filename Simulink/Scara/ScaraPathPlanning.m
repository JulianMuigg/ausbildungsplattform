classdef ScaraPathPlanning
    %SCARAPATHPLANNING Class, which includes all neede scara specific
    %functions for path planning
    
    properties
    end
    
    methods
        function obj = ScaraPathPlanning()
        end
        
        %% Creates the inital trajectory struct, depending on the input
        function [trajectoryL, trajectoryJ, trajectoryExtern, moveType] = ...
                inputHandler(obj, dt, externalTrajectory, robotIn, jointSpace, taskSpace)
            
            persistent lastMoveType;
            if isempty(lastMoveType)
                lastMoveType = 0;
            end
            
            if robotIn.type == MoveType.MoveExternal
                moveType = 2;
            else
                %% Handle move type and remember it for next cycle
                if robotIn.type == MoveType.MoveL
                    moveType = 0;
                elseif robotIn.type == MoveType.MoveJ
                    moveType = 1;
                else
                    moveType = lastMoveType;
                end
                lastMoveType = moveType;
            end     
            
            [trajectoryExtern] = obj.moveExternal( dt, externalTrajectory, robotIn, jointSpace );            
            [trajectoryL, trajectoryJ] = obj.moveLJ( dt, robotIn, jointSpace, taskSpace );
        end
        
        %% Uses the current joint and trajectory values to define the new
        % axisIn and robotOut data
        function [scaraIn, robotOut] = outputHandler(obj, traj, taskSpace, scaraOut, moveType)
            
            % Manipulate trajectory, depending on current joint position
            [traj.q1.p, q1Reached] = obj.manipulateJointValues( traj.q1, scaraOut.q1 );
            [traj.q2.p, q2Reached] = obj.manipulateJointValues( traj.q2, scaraOut.q2 );
                        
            scaraIn.q1.p = traj.q1.p;
            scaraIn.q1.v = traj.q1.v;
            scaraIn.q1.a = traj.q1.a;
            scaraIn.q2.p = traj.q2.p;
            scaraIn.q2.v = traj.q2.v;
            scaraIn.q2.a = traj.q2.a;
            scaraIn.q3.p = traj.q3.p;
            scaraIn.q3.v = traj.q3.v;
            scaraIn.q3.a = traj.q3.a;
            scaraIn.error = ErrorType.NoError;
            
            robotOut.data.pos.x = taskSpace.x.p;
            robotOut.data.pos.y = taskSpace.y.p;
            robotOut.data.pos.z = taskSpace.z.p;
            robotOut.data.v = taskSpace.v;
            robotOut.data.a = taskSpace.a;
            robotOut.data.type = MoveType( moveType );
            robotOut.data.r = 0;
            robotOut.positionReached = traj.q1.reached && traj.q2.reached && traj.q3.reached;
            robotOut.errorRobot = scaraOut.error;
            
            if robotOut.positionReached 
                robotOut.positionReached = q1Reached && q2Reached &&...
                    abs( scaraOut.q3.p - traj.q3.p ) < 1e-3;
            end
        end
        
        %% Defines the shortest angle for moveJ
        function y = shortestAngle(obj, u)
           % y = u;
         
            % Manipulate start and stop values, if they are not beteween
            % +pi and -pi
            u.q1.start = u.q1.start - obj.calcRelativeOffset( u.q1.start );
            u.q1.stop = u.q1.stop - obj.calcRelativeOffset( u.q1.stop );
            
            u.q2.start = u.q2.start - obj.calcRelativeOffset( u.q2.start );
            u.q2.stop = u.q2.stop - obj.calcRelativeOffset( u.q2.stop );
            y = u;
        
            % when angle between start and stop is greater than pi (180°)
            % use the shorter angle
            if abs( u.q1.stop - u.q1.start ) >= pi
                if u.q1.stop <= 0
                    y.q1.stop = u.q1.stop + 2*pi;
                else
                    y.q1.stop = u.q1.stop - 2*pi;
                end
            end
            if abs( u.q2.stop - u.q2.start ) >= pi
                if u.q2.stop <= 0
                    y.q2.stop = u.q2.stop + 2*pi;
                else
                    y.q2.stop = u.q2.stop - 2*pi;
                end
            end              
        end
    end
    
    methods (Access = private)
        %% Creates teh trajectory struct for the moveL and moveJ calculations
        function trajectory = create( obj, t, startPoint, stopPoint, robotIn, type )
            %% Define output sturcts
            trajectory.q1.start = startPoint.x;
            trajectory.q1.stop = stopPoint.x;
            trajectory.q1.p = stopPoint.x;
            trajectory.q1.v = 0;
            trajectory.q1.a = 0;
            trajectory.q1.reached = false;

            trajectory.q2.start = startPoint.y;
            trajectory.q2.stop = stopPoint.y;
            trajectory.q2.p = stopPoint.y;
            trajectory.q2.v = 0;
            trajectory.q2.a = 0;
            trajectory.q2.reached = false;

            trajectory.q3.start = startPoint.z;
            trajectory.q3.stop = stopPoint.z;
            trajectory.q3.p = stopPoint.z;
            trajectory.q3.v = 0;
            trajectory.q3.a = 0;
            trajectory.q3.reached = false;

            trajectory.v_max = robotIn.v;
            trajectory.a_max = robotIn.a;
            trajectory.tolerance = robotIn.r;
            if type == MoveType.MoveJ
                trajectory.tolerance = robotIn.r;
            end
            trajectory.t = t;
            trajectory.moveType = type;
        end   
        
        %% Defines the initial structs for trajectory calculation
        % depending on the robotOut data and the new move commands in
        % robotIn
        function [trajectoryL, trajectoryJ] = moveLJ( obj, dt, robotIn, jointSpace, taskSpace )
            persistent startTaskSpace;
            persistent startJointSpace;
            persistent stopTaskSpace;
            persistent t
            
            if isempty(stopTaskSpace)
                stopTaskSpace.x = robotIn.pos.x;
                stopTaskSpace.y = robotIn.pos.y;
                stopTaskSpace.z = robotIn.pos.z;

                startTaskSpace = stopTaskSpace;

                startJointSpace.x = jointSpace.q1.p;
                startJointSpace.y = jointSpace.q2.p;
                startJointSpace.z = jointSpace.q2.p;

                t = 0;
            end
            
            %% Check if new stop position is available
            if robotIn.pos.x ~= stopTaskSpace.x || ...
               robotIn.pos.y ~= stopTaskSpace.y || ...
               robotIn.pos.z ~= stopTaskSpace.z

               startTaskSpace.x = taskSpace.x.p;
               startTaskSpace.y = taskSpace.y.p;
               startTaskSpace.z = taskSpace.z.p;

               startJointSpace.x = jointSpace.q1.p;
               startJointSpace.y = jointSpace.q2.p;
               startJointSpace.z = jointSpace.q3.p;

               stopTaskSpace.x = robotIn.pos.x;
               stopTaskSpace.y = robotIn.pos.y;
               stopTaskSpace.z = robotIn.pos.z;
               t = 0;
            else
                t = t + dt;
            end

            trajectoryL = obj.create( t, startTaskSpace, stopTaskSpace, robotIn, MoveType.MoveL );
            trajectoryJ = obj.create( t, startJointSpace, stopTaskSpace, robotIn, MoveType.MoveJ );
        end
        
        %% Passes every step of the given external trajectory
        function [trajectoryExtern] = moveExternal( obj, dt, externalTrajectory, robotIn, jointSpace )
            
            persistent n
            persistent isExternalEnabled;
            persistent t
            if isempty(isExternalEnabled)
                n = 1;
                isExternalEnabled = false;
                t = 0;
            end
           
            % Start / Restart / Continue    
            if ~isExternalEnabled && robotIn.type == MoveType.MoveExternal
                isExternalEnabled = true;
                n = 1;
                t = 0;
                disp( 'Start external' );
            elseif n <= externalTrajectory.size
                t = t + dt;
                
                if externalTrajectory.trajectory(n).t < t
                    n = n + 1;
                end                
            end 
                        
            start.x = externalTrajectory.trajectory(1).q1;
            start.y = externalTrajectory.trajectory(1).q2;
            start.z = externalTrajectory.trajectory(1).q3;
            
            if n <= externalTrajectory.size 
                stop.x = externalTrajectory.trajectory(n).q1;
                stop.y = externalTrajectory.trajectory(n).q2;
                stop.z = externalTrajectory.trajectory(n).q3;
            else
                size = externalTrajectory.size;
                if size == 0
                    stop = start;
                else
                    stop.x = externalTrajectory.trajectory(size).q1;
                    stop.y = externalTrajectory.trajectory(size).q2;
                    stop.z = externalTrajectory.trajectory(size).q3;
                end
            end
            
            trajectoryExtern = obj.create( externalTrajectory.trajectory(n).t, start, stop, robotIn, MoveType.MoveExternal );
                        
            if isExternalEnabled && n > 1 && n >= externalTrajectory.size
                trajectoryExtern.q1.reached = true; 
                trajectoryExtern.q2.reached = true; 
                trajectoryExtern.q3.reached = true; 
                
                if robotIn.type == MoveType.Undefined
                    isExternalEnabled = false;
                    disp( 'Stop external' );
                end
            end               
        end 
        
        %% Calculates the realtive offset
        %b example: pos = 5*pi -> offset = 6*pi, newPos = -pi
          function [offset] = calcRelativeOffset( obj, pos )
            counter = 0;
            value = abs(pos);

            while value >= 2*pi
                counter = counter + 1;
                value = value - 2*pi;
            end

            if value > pi
                counter = counter + 1;
            end

            if pos < 0
                counter = counter*(-1);
            end
            
            offset = 2*pi*counter;
        end
        
        function [jointInPos, reached] = manipulateJointValues( obj, traj, jointOut )
            % Calc offset to zero
            inOffset = obj.calcRelativeOffset(traj.p);
            outOffset = obj.calcRelativeOffset( jointOut.p );
            
            % Subtract offset
            traj.p = traj.p - inOffset;
            out = jointOut.p - outOffset;
            
            % Check +-pi crossover
            if abs(traj.p-out)>pi
                if traj.p > 0 && out <= 0
                    traj.p = traj.p - 2*pi;
                elseif traj.p < 0 && out > 0
                    traj.p = traj.p + 2*pi;
                end
            end
    
            % Calc manipulated in position and if pos is reached
            jointInPos = jointOut.p + (traj.p-out); %outOffset - p;
            reached = abs( out - traj.p ) < 1e-3;
        end
    end
end

