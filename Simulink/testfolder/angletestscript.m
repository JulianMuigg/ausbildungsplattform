%angle testscript
u.q1.start = -pi/4;
u.q1.stop = pi/4;

u.q2.start = -10*pi+7*pi/4;
u.q2.stop = pi/4 ;


if abs( u.q1.stop - mod(u.q1.start,2*pi) ) >= pi
    
    diff_q= u.q1.stop-mod(u.q1.start,2*pi);
    complement_q = 2*pi-abs(diff_q);
    if u.q1.start>=0
        if diff_q > 0
            y.q1.stop= u.q1.start+complement_q;
        else
            y.q1.stop= u.q1.start-complement_q;
        end
    else
        if diff_q > 0
            y.q1.stop= u.q1.start-complement_q;
        else
            y.q1.stop= u.q1.start+complement_q;
        end    
    end
    
else
    diff_q = u.q1.stop-u.q1.start;
    y.q1.stop= u.q1.start+diff_q;
end
            
if abs( u.q2.stop - mod(u.q2.start,2*pi) ) >= pi
    
    diff_q= u.q2.stop-mod(u.q2.start,2*pi);
    complement_q = 2*pi-abs(diff_q);
    if u.q2.start>=0
        if diff_q > 0
            y.q2.stop= u.q2.start+complement_q;
        else
            y.q2.stop= u.q2.start-complement_q;
        end
    else
        if diff_q > 0
            y.q2.stop= u.q2.start-complement_q;
        else
            y.q2.stop= u.q2.start+complement_q;
        end
    end
else
    diff_q = u.q2.stop-u.q2.start;
    y.q2.stop= u.q2.start+diff_q;
end