
modelName = ['Ausbildungsplattform' char( robotType )];
modelNameXil = [modelName '_' char( xilMode )];

yLabel = 'Position in m';

%% Save output value to mat file
if exist( 'out', 'var' )
    save(['./TestModelsXiL/' modelNameXil '_Results.mat'],'out');
end

%% Plot results
if exist(['./TestModelsXiL/' modelName '_MiL_Results.mat'], 'file')

    % Load MiL results
    load(['./TestModelsXiL/' modelName '_MiL_Results.mat']);
    t_MiL = out.tout;
    switch robotType
        case RobotType.Scara
            inMiL = out.scaraInMiL;
            outMiL = out.scaraOutMiL;
        case RobotType.Delta360
            inMiL = out.deltaInMiL;
            outMiL = out.deltaOutMiL;
    end
    
    % Load SiL results
    if exist(['./TestModelsXiL/' modelName '_SiL_Results.mat'], 'file')
        load(['./TestModelsXiL/' modelName '_SiL_Results.mat']);
        t_SiL = out.tout;
        switch robotType
            case RobotType.Scara
                inSiL = out.scaraInSiL;
                outSiL = out.scaraOutSiL;
            case RobotType.Delta360
                inSiL = out.deltaInSiL;
                outSiL = out.deltaOutSiL;
        end
    else
        inSiL = inMiL;
        outSiL = outMiL;
    end
    if exist(['./TestModelsXiL/' modelName '_HiL_Results.mat'], 'file')
        % Load HiL results
        load(['./TestModelsXiL/' modelName '_HiL_Results.mat']);
        t_HiL = out.tout;
        switch robotType
            case RobotType.Scara
                inHiL = out.scaraInHiL;
                outHiL = out.scaraOutHiL;
            case RobotType.Delta360
                inHiL = out.deltaInHiL;
                outHiL = out.deltaOutHiL;
        end
    else
        inHiL = inMiL;
        outHiL = outMiL;
    end
    
    
    % Plot input data
    %figure('Name','position input')
     plotData( inMiL.q1.p, inSiL.q1.p, inHiL.q1.p, 'axisInMiL.q1.p', 'axisInSiL.q1.p', 'axisInHiL.q1.p', yLabel, 3, 1, 1 );
     plotData( inMiL.q2.p, inSiL.q2.p, inHiL.q2.p, 'axisInMiL.q2.p', 'axisInSiL.q2.p', 'axisInHiL.q2.p', yLabel, 3, 1, 2 );
     plotData( inMiL.q3.p, inSiL.q3.p, inHiL.q3.p, 'axisInMiL.q3.p', 'axisInSiL.q3.p', 'axisInHiL.q3.p', yLabel, 3, 1, 3 );
    %plotData( inMiL.q1.p,inMiL.q1.p,inMiL.q1.p,'axisInMiL.q1.p','axisInMiL.q1.p', 'axisInMiL.q1.p', yLabel,3,1,1);
   % plotData( inMiL.q2.p,inMiL.q2.p,inMiL.q2.p, 'axisInMiL.q2.p','axisInMiL.q2.p','axisInMiL.q2.p', yLabel,3,1,2);
  %  plotData( inMiL.q3.p, inMiL.q3.p,inMiL.q3.p,'axisInMiL.q3.p','axisInMiL.q3.p','axisInMiL.q3.p', yLabel,3,1,3);
%     plotData( inMiL.q2.p, inSiL.q2.p, inHiL.q2.p, 'axisInMiL.q2.p', 'axisInSiL.q2.p', 'axisInHiL.q2.p', yLabel, 3, 1, 2 );
%     plotData( inMiL.q3.p, inSiL.q3.p, inHiL.q3.p, 'axisInMiL.q3.p', 'axisInSiL.q3.p', 'axisInHiL.q3.p', yLabel, 3, 1, 3 );
    %figure('Name','velocity input')
    %plotData( inMiL.q1.v, inSiL.q1.v, inHiL.q1.v, 'axisInMiL.q1.v', 'axisInSiL.q1.v', 'axisInHiL.q1.v', yLabel, 3, 1, 1 );
    %plotData( inMiL.q2.v, inSiL.q2.v, inHiL.q2.v, 'axisInMiL.q2.v', 'axisInSiL.q2.v', 'axisInHiL.q2.v', yLabel, 3, 1, 2 );
    %plotData( inMiL.q3.v, inSiL.q3.v, inHiL.q3.v, 'axisInMiL.q3.v', 'axisInSiL.q3.v', 'axisInHiL.q3.v', yLabel, 3, 1, 3 );
    
    %figure('Name','accelleration input')
    %plotData( inMiL.q1.a, inSiL.q1.a, inHiL.q1.a, 'axisInMiL.q1.a', 'axisInSiL.q1.a', 'axisInHiL.q1.a', yLabel, 3, 1, 1 );
    %plotData( inMiL.q2.a, inSiL.q2.a, inHiL.q2.a, 'axisInMiL.q2.a', 'axisInSiL.q2.a', 'axisInHiL.q2.a', yLabel, 3, 1, 2 );
    %plotData( inMiL.q3.a, inSiL.q3.a, inHiL.q3.a, 'axisInMiL.q3.a', 'axisInSiL.q3.a', 'axisInHiL.q3.a', yLabel, 3, 1, 3 );
    
    % Plot output data
    %figure('Name','position output')
    plotData( outMiL.q1.p, outSiL.q1.p, outHiL.q1.p, 'axisOutMiL.q1.p', 'axisOutSiL.q1.p', 'axisOutHiL.q1.p', yLabel, 3, 1, 1 );
    plotData( outMiL.q2.p, outSiL.q2.p, outHiL.q2.p, 'axisOutMiL.q2.p', 'axisOutSiL.q2.p', 'axisOutHiL.q2.p', yLabel, 3, 1, 2 );
    plotData( outMiL.q3.p, outSiL.q3.p, outHiL.q3.p, 'axisOutMiL.q3.p', 'axisOutSiL.q3.p', 'axisOutHiL.q3.p', yLabel, 3, 1, 3 );
    
    %figure('Name','velocity output')
    %plotData( outMiL.q1.v, outSiL.q1.v, outHiL.q1.v, 'axisOutMiL.q1.v', 'axisOutSiL.q1.v', 'axisOutHiL.q1.v', yLabel, 3, 1, 1 );
    %plotData( outMiL.q2.v, outSiL.q2.v, outHiL.q2.v, 'axisOutMiL.q2.v', 'axisOutSiL.q2.v', 'axisOutHiL.q2.v', yLabel, 3, 1, 2 );
    %plotData( outMiL.q3.v, outSiL.q3.v, outHiL.q3.v, 'axisOutMiL.q3.v', 'axisOutSiL.q3.v', 'axisOutHiL.q3.v', yLabel, 3, 1, 3 );
    
    %figure('Name','accelleration output')
    %plotData( outMiL.q1.a, outSiL.q1.a, outHiL.q1.a, 'axisOutMiL.q1.a', 'axisOutSiL.q1.a', 'axisOutHiL.q1.a', yLabel, 3, 1, 1 );
    %plotData( outMiL.q2.a, outSiL.q2.a, outHiL.q2.a, 'axisOutMiL.q2.a', 'axisOutSiL.q2.a', 'axisOutHiL.q2.a', yLabel, 3, 1, 2 );
    %plotData( outMiL.q3.a, outSiL.q3.a, outHiL.q3.a, 'axisOutMiL.q3.a', 'axisOutSiL.q3.a', 'axisOutHiL.q3.a', yLabel, 3, 1, 3 );

end