function plotData(data1, data2, data3, text1, text2, text3, yLabel, rows, cols, pos )
%PLOTDATA Summary of this function goes here
%   Detailed explanation goes here
%subplot(rows,cols,pos)
figure
plot( data1, 'color',[0.8,0.8,0.8], 'LineWidth', 3 )
hold on
plot( data2, '--', 'color',[0.6,0.6,0.6], 'LineWidth',2 ) 
plot( data3, '-.k' ) 

legend( [text1; text2; text3] )

title( '' )
xlabel( 't in s' )
ylabel( yLabel )
end

