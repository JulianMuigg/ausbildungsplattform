
%% Save output value to mat file
if exist( 'out', 'var' )
    % Define name and save data, depending on robot type
    switch robotType
        case RobotType.Delta360
            stepResponseDelta360 = out.stepResponseData;
            save(['./MotorStepResponse/stepResponse' char( robotType ) 'Data.mat'],'stepResponseDelta360');
        case RobotType.Scara
            stepResponseScara = out.stepResponseData;
            save(['./MotorStepResponse/stepResponse' char( robotType ) 'Data.mat'],'stepResponseScara');
    end    
end

%% Call controller design scirpt, depending on robot type
switch robotType
    case RobotType.Delta360
        AusbildungsplattformDelta360_ControllerDesign;
    case RobotType.Scara
        AusbildungsplattformScara_ControllerDesign;
end