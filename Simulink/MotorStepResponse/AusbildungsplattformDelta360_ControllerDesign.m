%% Fit PT1 System
close all
ts = stepSize;

%% Load data from file
load( 'stepResponseDelta360Data' );
xdata = zeros(length(stepResponseDelta360.signals(2).values(1,1,:)),1);
ydata = xdata;
for i = 1:length(xdata)
    xdata(i) = stepResponseDelta360.time(i);
    ydata(i) = stepResponseDelta360.signals(2).values(1,1,i);
end

%% Cut data
xdata = xdata(5/ts:10/ts) - 5;
ydata = ydata(5/ts:10/ts);

Tsim = 5;
Umag = 0.02;

%% find transfer function
funPT1T = @(x,xdata) max(0,x(1)*Umag*(1-exp(-(xdata-x(3))/x(2))));
funPT2 = @(x,xdata) x(1)*Umag*(1-(1/(x(2)-x(3)))*(x(2)*exp(-(xdata/x(2)))-x(3)*exp(-(xdata/x(3)))));

x0 = [0.7,0.5,0];
[x,resnorm] = lsqcurvefit(funPT1T,x0,xdata,ydata);
x1 = [2,0.2,0.1];
[x2,resnorm2] = lsqcurvefit(funPT2,x1,xdata,ydata);

%% plot results
figure
time = 0:ts:Tsim;
plot( xdata, ydata, 'o')
hold on
plot( time,  max(0,x(1)*Umag*(1-exp(-(time-x(3))/x(2)))) )
hold on
plot( time,  x2(1)*Umag*(1-(1/(x2(2)-x2(3)))*(x2(2)*exp(-(time/x2(2)))-x2(3)*exp(-(time/x2(3))))) )
legend('RawData','PT1T','PT2');

%% get the laplace transfere function
syms t y
ts_plc = 0.002;
Gs_PT1 = tf([0 x(1)],[x(2) 1]);
Gs = Gs_PT1 * tf([0 1],[1 0]);
[num, den] = tfdata(Gs);

%% simulation parameters
t_end = 20;
T = 1e-1;
k_end = t_end/T;
tr = 5;

%% continouse system
[Ac,Bc,Cc,Dc] = tf2ss(num{1},den{1});
sysc = ss(Ac,Bc,Cc,Dc);

a = 1/den{1}(1); % 22.4432
K = a*num{1}(end); %22.1136

%% discrete system
sys=c2d(sysc,T);
A = sys.A;
B = sys.B;
C = sys.C;
D = sys.D;
I = eye(size(A));

% initialization
x = [0;0];
xr = [0;0];
dxr = [0;pi^2/(2*tr^2)];
t = 0;
y = C*x;

% trajectory
for k=1:k_end
    t(k+1) = k*T;
    if t(k+1)<tr
        xr(1,k+1) = (1-cos(pi*t(k+1)/tr))/2;
        xr(2,k+1) = pi*sin(pi*t(k+1)/tr)/(2*tr);
        dxr(1,k+1) = xr(2,k+1);
        dxr(2,k+1) = pi^2*cos(pi*t(k+1)/tr)/(2*tr^2);
    else
        xr(1,k+1) = 1;
        xr(2,k+1) = 0;
        dxr(1,k+1) = 0;
        dxr(2,k+1) = 0;
    end
end

%% controller design
Kp = 10;
Ki = 0.01;
b0 = -Kp;
b1 = Kp + Ki;
pi_x = 0;
y_error = 0;
yr = 0;

%% simulation
for k=1:k_end
    acc = dxr(1,k);
    vel = xr(1,k);
    pos = xr(2,k);
    
    % Feedforward calculation
    ur = acc + a*vel;
    ur2 = pinv(Bc)*(dxr(:,k) - Ac*xr(:,k));
    yr(k) = K*pos;
    
    % PI Controller calculation
    y_error(k) = yr(k) - y(k);
    u(k) = pi_x(k) + b1*y_error(k);
    pi_x(k+1) = b0 *y_error(k) + u(k);
    
    % System calculation
    x(:,k+1) = A*x(:,k) + B*u(k);
    y(k+1) = C * x(:,k+1);
end
yr(k_end+1) = yr(k_end);

%% plot simulation
figure(1);
plot( t, xr(1,: ) );
grid on
hold on
plot( t, xr(2,: ) );
plot( t, dxr(1,: ) );    
plot( t, dxr(2,: ) );
legend( 'x_r(1)', 'x_r(2)', 'dx_r(1)', 'dx_r(2)' );

figure(2);
plot( t, yr, 'k-' );
hold on
grid on
plot( t, y, 'r--' );