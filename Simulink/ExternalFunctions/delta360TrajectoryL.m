function [trajOut] = delta360TrajectoryL(trajIn)
%DELTA360TRAJECTORYL External implementation of the MoveL trajectory for the
%delta 360 robot

trajOut = trajIn;

% Alocation of persistent Variables to store Trajectory parameters of
% Timestep k-1
persistent s_old
persistent ds_old
persistent dds_old
if isempty( s_old )
    s_old = zeros(3,1);
    ds_old = zeros(3,1);
    dds_old = zeros(3,1);
end

[Ta, T, v, a, h] = BoundaryConditionsL(trajIn);
[s,ds,dds,reached] = Interpolation(Ta,T,v,a,h,trajIn);

trajOut.q1.p = s(1,1);
trajOut.q2.p = s(2,1);
trajOut.q3.p = s(3,1);
trajOut.q1.v = ds(1,1);
trajOut.q2.v = ds(2,1);
trajOut.q3.v = ds(3,1);
trajOut.q1.a = dds(1,1);
trajOut.q2.a = dds(2,1);
trajOut.q3.a = dds(3,1);
trajOut.q1.reached = reached(1,1);
trajOut.q2.reached = reached(2,1);
trajOut.q3.reached = reached(3,1);


    function [Ta, T, vmax, amax,h] = BoundaryConditionsL(In)
          %% Variables out of Trajectory in Struct
          vmax_in = In.v_max;
          amax_in = In.a_max;
          %% Calculate traveldistance
          P1 = [In.q1.start; In.q2.start; In.q3.start];
          P2 = [In.q1.stop; In.q2.stop; In.q3.stop];
          h = P2-P1;
          %% Prealocation of Output parameters
          Ta = zeros(size(h,1),1); % Duration of Accelaration and Deccelaration
          T =  zeros(size(h,1),1); % Total Duration of Trajectory
          vmax = zeros(size(h,1),1); % max. Velocity per axis (xyz)
          amax = zeros(size(h,1),1); % max. Accelaration per axis (xyz)
          
          %% deconstruction of max. Absolute Velocity to xyz Components  
          for j = 1:size(h,1)
              % Factoring velocity and acceleration vektor
              vmax(j) = vmax_in * abs((h(j)/norm(h)));
              amax(j) = amax_in * abs((h(j)/norm(h)));
          end
          
          %% Calculation of Durations 
          for j = 1:size(h,1)
              if abs(h(j)) > vmax(j)^2/amax(j)
                  Ta(j) = vmax(j)/amax(j); %acceleration and decelaration time
                  T(j) = (abs(h(j))*amax(j) + vmax(j)^2)/(amax(j)*vmax(j)); %total duration
              elseif abs(h(j)) <= vmax(j)^2/amax(j)
                  Ta(j) = sqrt(abs(h(j))/amax(j)); % acceleration and deceleration time
                  T(j) = 2*Ta(j); % total duration
              end
          end
    end
    function [s,ds,dds,reached] = Interpolation(Ta,T,vmax,amax,h,In)
        %% Variables out of Trajectory In Struct
        t = In.t;
        tolerance = In.tolerance;
        S1 = [In.q1.start; In.q2.start; In.q3.start];
        S2 = [In.q1.stop; In.q2.stop; In.q3.stop];
        %% Define max time and max velocity/acceleration of each joint
        Ta_final = max( Ta );
        T_final = max( T );
        %% Assigning propper sign to Velocity and Acceleration
        vj = zeros(size(vmax,1),1);
        aj = zeros(size(amax,1),1);
        for j = 1:size(S1,1)
            vj(j) = abs(vmax(j));
            aj(j) = abs(amax(j));
            % Change velocity/acceleration depinding on direction
            if h(j) < 0
                vj(j) = -vj(j);
                aj(j) = -aj(j);
            end
        end
        %% Prallocation of Trajectory parameter
        s = zeros(length(S1),1); % preallocation of q(t)
        ds = zeros(length(S1),1); %preallocation of dq(t)
        dds = zeros(length(S1),1); %preallocation of ddq(t)
        reached = [false; false; false];
        
        %% Interpolation
        
        for n = 1 : length(S1)
            % constant velocity YES or No
            if h(n) == 0
                s(n,1) = S2(n);
                ds(n,1) = 0;
                dds(n,1) = 0;
            elseif abs(h(n)) > abs(vj(n)^2/aj(n))
                %constant velocity interpolation
                if  t <= Ta_final
                    s(n,1) = S1(n) + 0.5 * aj(n) * t^2; % acceleration phase
                    ds(n,1) = aj(n)*t;
                    dds(n,1) = aj(n);
                elseif Ta_final < t && t <= (T_final- Ta_final)
                    s(n,1) = S1(n) + aj(n)*Ta_final * (t - Ta_final/2); % constant velocity phase
                    ds(n,1) = vj(n);
                    dds(n,1) = 0;
                elseif (T_final-Ta_final) < t && t <= T_final
                    s(n,1) = S2(n) - 0.5 * aj(n) * (T_final - t)^2; %deceleration phase
                    ds(n,1) = vj(n)-aj(n)*(t-(T_final-Ta_final));
                    dds(n,1) = -aj(n);
                else
                    s(n,1) = s_old(n,1);
                    ds(n,1) = ds_old(n,1);
                    dds(n,1) = dds_old(n,1);
                end
            else
                % no constant velocity interpolation
                vj(n) = aj(n)*Ta_final; % maximum velocity peak
                if 0 <= t && t <= Ta_final
                    s(n,1) = S1(n) + 0.5 * aj(n) * t^2; %acceleration phase
                    ds(n,1) = aj(n)*t;
                    dds(n,1) = aj(n);
                elseif (T_final-Ta_final) < t && t <= T_final
                    s(n,1) = S2(n) - 0.5 * aj(n) * (T_final - t)^2; % deceleration phase
                    ds(n,1)= aj(n)*Ta_final-aj(n)*(t-(T_final-Ta_final));
                    dds(n,1) = -aj(n);
                else
                    s(n,1) = s_old(n,1);
                    ds(n,1) = ds_old(n,1);
                    dds(n,1) = dds_old(n,1);
                end
            end
        end
        
        
        %% Remember results and updated values for next run
          s_old = s;
          ds_old = ds;
          dds_old = dds;
         
         %% Check if position has already reached stop within tolerance
            for n = 1 : length(S1)
                reached(n,1) = abs(S2(n,1) - s_old(n,1)) <= tolerance;
            end
        
    end

end

