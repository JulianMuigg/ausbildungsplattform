function [taskSpace] = scaraForwardKinematics(jointSpace)
%SCARAFORWARDKINEMATICS External implementation of the forward kinematics
%for the scara robot

% Direct feedthrough for internal compare function
taskSpace.x = jointSpace.q1;
taskSpace.y = jointSpace.q2;
taskSpace.z = jointSpace.q3;
taskSpace.v = 0;
taskSpace.a = 0;

end

