function [trajOut] = delta360InverseKinematics(trajIn)
%DELTA360INVERSEKINEMATICS External implementation of the inverse kinematics
%for the delta 360 robot

% Through put of unchanged parameters
trajOut = trajIn;

% x, y, z to s1 ,s2 ,s3 
currentS = inverseKinematic([trajIn.q1.p,trajIn.q2.p,trajIn.q3.p]);
currentdS = inverseJacobi([trajIn.q1.p,trajIn.q2.p,trajIn.q3.p])*[trajIn.q1.v;trajIn.q2.v;trajIn.q3.v];
stopS = inverseKinematic([trajIn.q1.stop,trajIn.q2.stop,trajIn.q3.stop]);

    trajOut.q1.p = currentS(1); % Übergabe zum Output Handler -> DeltaIn
    trajOut.q2.p = currentS(2); % Übergabe zum Output Handler -> DeltaIn
    trajOut.q3.p = currentS(3); % Übergabe zum Output Handler -> DeltaIn
    trajOut.q1.v = currentdS(1); % Übergabe zum Output Handler -> DeltaIn
    trajOut.q2.v = currentdS(2); % Übergabe zum Output Handler -> DeltaIn
    trajOut.q3.v = currentdS(3); % Übergabe zum Output Handler -> DeltaIn
    trajOut.q1.stop = stopS(1);% currentS(1); % Übergabe zur TrajectoryJ -> StopPunkt
    trajOut.q2.stop = stopS(2);%currentS(2); % Übergabe zur TrajectoryJ -> StopPunkt
    trajOut.q3.stop = stopS(3);%currentS(3); % Übergabe zur TrajectoryJ -> StopPunkt

% Comment out when funktions are inkluded
    %trajOut = trajIn;

function S = inverseKinematic(P)
            rbase= 0.222106;
            rmove = 0.042;
            transform = 0.079885;
            length = 0.400;
            delta = pi/4;
            theta = [0 4*pi/3  2*pi/3];
            x = P(1);
            y = P(2);
            z = P(3)+transform;
            
            % Vector Declaration
            ym = [0 0 0];
            zm = [0 0 0];
            xm = [0 0 0];
            a = [0 0 0];
            b = [0 0 0];
            c = [0 0 0];
            s = [0 0 0];
            sigma = [-1 -1 -1];
            
            for i = 1:3
                xm(i) = x*cos(theta(i))+y*sin(theta(i));
                ym(i) = y*cos(theta(i))-x*sin(theta(i))+rmove;
                zm(i) = z;
                
                a(i) = 1;
                b(i) = -2*zm(i)*sin(delta)-2*rbase*cos(delta)+2*ym(i)*cos(delta);
                c(i) = xm(i)^2 + ym(i)^2 + zm(i)^2 + rbase^2 - 2*ym(i)*rbase - length^2;
                
                if b(i)^2-4*a(i)*c(i)>=0
                    s(i) = (-b(i)+sigma(i)*sqrt(b(i)^2-4*a(i)*c(i)))/2;
                else
                    s(i) = (-b(i)+sigma(i)*sqrt(1))/2;
                end
            end
            S = zeros(3,1);
            S(1) = s(1);
            S(2) = s(2);
            S(3) = s(3);
    end
 function invJ = inverseJacobi(P)
        rbase= 0.222106;
        rmove = 0.042;
        transform = 0.079885;
        lengthPipe = 0.400;
        delta = pi/4;
        theta = [0 4*pi/3  2*pi/3];
        x = P(1);
        y = P(2);
        z = P(3) + transform;
        
        %Vector Declaration
        ym = [0 0 0];
        zm = [0 0 0];
        xm = [0 0 0];
        a = [0 0 0];
        b = [0 0 0];
        c = [0 0 0];
        
        invJ = zeros(3);
        hypJ =  [                                                                                                                                                                                                                                                    (2*x)/(4*lengthPipe^2 - 4*(rmove + y)^2 - 4*rbase^2 - 4*x^2 - 4*z^2 + 4*rbase*(2*rmove + 2*y) + (2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove + 2*y))/2)^2)^(1/2),                    0,                                                                                                                   (8*rmove - 8*rbase + 8*y + 2*2^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove + 2*y))/2))/(4*(4*lengthPipe^2 - 4*(rmove + y)^2 - 4*rbase^2 - 4*x^2 - 4*z^2 + 4*rbase*(2*rmove + 2*y) + (2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove + 2*y))/2)^2)^(1/2)) - 2^(1/2)/2, -2^(1/2)/2,                                                                     (8*z - 2*2^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove + 2*y))/2))/(4*(4*lengthPipe^2 - 4*(rmove + y)^2 - 4*rbase^2 - 4*x^2 - 4*z^2 + 4*rbase*(2*rmove + 2*y) + (2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove + 2*y))/2)^2)^(1/2)) + 2^(1/2)/2, 2^(1/2)/2;
            (2*x - 4*3^(1/2)*rbase + 2*3^(1/2)*y + 4*3^(1/2)*(rmove - y/2 + (3^(1/2)*x)/2) + 2^(1/2)*3^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove - y + 3^(1/2)*x))/2))/(4*(4*rbase*(2*rmove - y + 3^(1/2)*x) + (2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove - y + 3^(1/2)*x))/2)^2 - 4*(rmove - y/2 + (3^(1/2)*x)/2)^2 + 4*lengthPipe^2 - 4*(x/2 + (3^(1/2)*y)/2)^2 - 4*rbase^2 - 4*z^2)^(1/2)) - (2^(1/2)*3^(1/2))/4, -(2^(1/2)*3^(1/2))/4, 2^(1/2)/4 + (4*rbase - 4*rmove + 2*y + 4*3^(1/2)*(x/2 + (3^(1/2)*y)/2) - 2*3^(1/2)*x - 2^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove - y + 3^(1/2)*x))/2))/(4*(4*rbase*(2*rmove - y + 3^(1/2)*x) + (2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove - y + 3^(1/2)*x))/2)^2 - 4*(rmove - y/2 + (3^(1/2)*x)/2)^2 + 4*lengthPipe^2 - 4*(x/2 + (3^(1/2)*y)/2)^2 - 4*rbase^2 - 4*z^2)^(1/2)),  2^(1/2)/4, 2^(1/2)/2 + (8*z - 2*2^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove - y + 3^(1/2)*x))/2))/(4*(4*rbase*(2*rmove - y + 3^(1/2)*x) + (2^(1/2)*rbase + 2^(1/2)*z - (2^(1/2)*(2*rmove - y + 3^(1/2)*x))/2)^2 - 4*(rmove - y/2 + (3^(1/2)*x)/2)^2 + 4*lengthPipe^2 - 4*(x/2 + (3^(1/2)*y)/2)^2 - 4*rbase^2 - 4*z^2)^(1/2)), 2^(1/2)/2;
            (2^(1/2)*3^(1/2))/4 + (2*x + 4*3^(1/2)*rbase - 2*3^(1/2)*y + 4*3^(1/2)*(y/2 - rmove + (3^(1/2)*x)/2) - 2^(1/2)*3^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z + (2^(1/2)*(y - 2*rmove + 3^(1/2)*x))/2))/(4*((2^(1/2)*rbase + 2^(1/2)*z + (2^(1/2)*(y - 2*rmove + 3^(1/2)*x))/2)^2 - 4*rbase*(y - 2*rmove + 3^(1/2)*x) - 4*(y/2 - rmove + (3^(1/2)*x)/2)^2 + 4*lengthPipe^2 - 4*(x/2 - (3^(1/2)*y)/2)^2 - 4*rbase^2 - 4*z^2)^(1/2)),  (2^(1/2)*3^(1/2))/4, 2^(1/2)/4 + (4*rbase - 4*rmove + 2*y - 2^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z + (2^(1/2)*(y - 2*rmove + 3^(1/2)*x))/2) - 4*3^(1/2)*(x/2 - (3^(1/2)*y)/2) + 2*3^(1/2)*x)/(4*((2^(1/2)*rbase + 2^(1/2)*z + (2^(1/2)*(y - 2*rmove + 3^(1/2)*x))/2)^2 - 4*rbase*(y - 2*rmove + 3^(1/2)*x) - 4*(y/2 - rmove + (3^(1/2)*x)/2)^2 + 4*lengthPipe^2 - 4*(x/2 - (3^(1/2)*y)/2)^2 - 4*rbase^2 - 4*z^2)^(1/2)),  2^(1/2)/4, 2^(1/2)/2 + (8*z - 2*2^(1/2)*(2^(1/2)*rbase + 2^(1/2)*z + (2^(1/2)*(y - 2*rmove + 3^(1/2)*x))/2))/(4*((2^(1/2)*rbase + 2^(1/2)*z + (2^(1/2)*(y - 2*rmove + 3^(1/2)*x))/2)^2 - 4*rbase*(y - 2*rmove + 3^(1/2)*x) - 4*(y/2 - rmove + (3^(1/2)*x)/2)^2 + 4*lengthPipe^2 - 4*(x/2 - (3^(1/2)*y)/2)^2 - 4*rbase^2 - 4*z^2)^(1/2)), 2^(1/2)/2];
        
        for i = 1:3
            xm(i) = x*cos(theta(i))+y*sin(theta(i));
            ym(i) = y*cos(theta(i))-x*sin(theta(i))+rmove;
            zm(i) = z;
            
            a(i) = 1;
            b(i) = -2*zm(i)*sin(delta)-2*rbase*cos(delta)+2*ym(i)*cos(delta);
            c(i) = xm(i)^2 + ym(i)^2 + zm(i)^2 + rbase^2 - 2*ym(i)*rbase - lengthPipe^2;
            
            if b(i)^2-4*a(i)*c(i)>=0
                invJ(i,:) = [hypJ(i,1) , hypJ(i,3), hypJ(i,5)];
            else
                invJ(i,:) = [hypJ(i,2) , hypJ(i,4), hypJ(i,6)];
            end
        end
        
    end
end

