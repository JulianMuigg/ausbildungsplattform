function [taskSpace] = delta360ForwardKinematics(jointSpace)
%DELTA360FORWARDKINEMATICS External implementation of the forward kinematics
%for the delta 360 robot


            rbase= 0.222106;
            rmove = 0.042;
            transform = 0.079885;
            length = 0.400;
            
            % Geometric definitions
            phi = pi/6;
            theta = pi/4;
            s1 = jointSpace.q1.p;
            s2 = jointSpace.q2.p;
            s3 = jointSpace.q3.p;
            
            % Abfrage ob Einträge in S > 0 && S < 144 sind, entsprechendes Errorhandling
            
            % Leg 1 Koordinates
            y1 = rbase-s1*cos(theta)-rmove;
            z1 = s1 * sin(theta);
            
            % Leg 2 Koordinates
            y2s = rbase - s2*cos(theta)-rmove;
            y2 = -y2s*sin(phi);
            x2 = y2s*cos(phi);
            z2 = s2*sin(theta);
            
            % Leg 3 Koordinates
            y3s = rbase - s3*cos(theta)-rmove;
            y3 = -y3s*sin(phi);
            x3 = -y3s*cos(phi);
            z3 = s3*sin(theta);
            
            % Leg Koordinate substituion
            w1 = y1^2 + z1^2;
            w2 = x2^2 + y2^2 + z2^2;
            w3 = x3^2 + y3^2 + z3^2;
            
            % Koordinate substitution 1
            alpha_1 = (x3*(w2-w1)-x2*(w3-w1))/2;
            beta_1 = x2*(z3-z1)-x3*(z2-z1);
            d = x3*(y2-y1)-x2*(y3-y1);
            
            % Koordinat substitution 2
            alpha_2 = ((y2-y1)*(w3-w1)-(y3-y1)*(w2-w1))/2;
            beta_2 = (y3-y1)*(z2-z1)-(y2-y1)*(z3-z1);
            
            % Quadratic substitution
            a = beta_2^2 + d^2 + beta_1^2;
            b = 2*(alpha_2*beta_2 + alpha_1 * beta_1 - d^2*z1 - y1*d*beta_1);
            c = alpha_2^2 + alpha_1^2 - 2*y1*d*alpha_1 - d^2*length^2 + d^2*w1;
            
            % Taskspace koordinates
            z = (-b + sqrt(b^2-4*a*c))/(2*a);
            x = (alpha_2 + beta_2*z)/d;
            y = (alpha_1 + beta_1*z)/d;
            
    taskSpace.x.p = x;
    taskSpace.y.p = y;
    taskSpace.z.p = z-transform;
    taskSpace.v=0;
    taskSpace.a=0;
    
%     taskSpace.x = jointSpace.q1;
%     taskSpace.y = jointSpace.q2;
%     taskSpace.z = jointSpace.q3;
%     taskSpace.v = 0;
%     taskSpace.a = 0;
end

