classdef RobotControl
    %ROBOTCONTROL Class, which includes all needed functions for Robot
    %Control state machine
    
    properties
    end
    
    methods
        function obj = RobotControl()
        end
        
        function currentMoveData = homing(obj, robotOut, moveArray)
            currentMoveData.pos.x = moveArray.cmd1.pos.x;
            currentMoveData.pos.y = moveArray.cmd1.pos.y;
            currentMoveData.pos.z = moveArray.cmd1.pos.z;
            currentMoveData.v = moveArray.cmd1.v;
            currentMoveData.a = moveArray.cmd1.a;
            currentMoveData.type  = moveArray.cmd1.type;
            currentMoveData.r = moveArray.cmd1.r;

            if robotOut.positionReached && ...
               abs( currentMoveData.pos.x - robotOut.data.pos.x ) < currentMoveData.r && ...
               abs( currentMoveData.pos.y - robotOut.data.pos.y ) < currentMoveData.r && ...
               abs( currentMoveData.pos.z - robotOut.data.pos.z ) < currentMoveData.r
                currentMoveData.type = MoveType.Undefined;
            end
        end
        
        function currentMoveData = busy(obj, transition, moveArray, touchEvent, robotOut)
            persistent index_
            persistent lastMoveCmd_

            % Initialize persistent variables
            if isempty(lastMoveCmd_)
                % Predefine currentMoveData
                currentMoveData.pos.x = 0;
                currentMoveData.pos.y = 0;
                currentMoveData.pos.z = 0;
                currentMoveData.v = 0.5;
                currentMoveData.a = 0.5;
                currentMoveData.type = MoveType.Undefined;
                currentMoveData.r = 0.0001;
                lastMoveCmd_ = currentMoveData;
                index_ = 0;
            end


            if transition == RobotTransition.Start
                index_ = 0;
            end

            currentMoveData = lastMoveCmd_;

            % Set new move command, if last one is reached or
            % touch event is triggered
            if robotOut.positionReached %|| touchEvent %todo: include touchEvent
                index_ = index_+1;
                switch index_
                    case 1
                        currentMoveData = moveArray.cmd1;
                    case 2
                        currentMoveData = moveArray.cmd2;
                    case 3
                        currentMoveData = moveArray.cmd3;
                    case 4
                        currentMoveData = moveArray.cmd4;
                    case 5
                        currentMoveData = moveArray.cmd5;
                    case 6
                        currentMoveData = moveArray.cmd6;
                    otherwise
                        currentMoveData.type = MoveType.Undefined;
                end
            end

            % Set output data
            if currentMoveData.type == MoveType.Undefined
                currentMoveData = lastMoveCmd_;
                currentMoveData.type = MoveType.Undefined;
                index_ = 0;
            else
                lastMoveCmd_ = currentMoveData;
            end
        end
    end
end

