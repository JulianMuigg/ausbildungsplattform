classdef SequenceControl
    %SEQUENCE Class, which includes all needed functions for Sequence
    %Control state machine
    
    properties
        deltaZ
        r
    end
    methods
        function obj = SequenceControl()
            obj.deltaZ = 0.015;
            obj.r = 0.001;
        end
        
        function [moveArray] = initMoveArray(obj, parameter)
            moveArray.cmd1.pos.x = parameter.homeX;
            moveArray.cmd1.pos.y = parameter.homeY;
            moveArray.cmd1.pos.z = parameter.homeZ;
            moveArray.cmd1.v = 0;
            moveArray.cmd1.a = 0;
            moveArray.cmd1.type = MoveType.Undefined;
            moveArray.cmd1.r = obj.r;
            
            moveArray.cmd2.pos.x = parameter.homeX;
            moveArray.cmd2.pos.y = parameter.homeY;
            moveArray.cmd2.pos.z = parameter.homeZ;
            moveArray.cmd2.v = 0;
            moveArray.cmd2.a = 0;
            moveArray.cmd2.type = MoveType.Undefined;
            moveArray.cmd2.r = obj.r;
            
            moveArray.cmd3.pos.x = parameter.homeX;
            moveArray.cmd3.pos.y = parameter.homeY;
            moveArray.cmd3.pos.z = parameter.homeZ;
            moveArray.cmd3.v = 0;
            moveArray.cmd3.a = 0;
            moveArray.cmd3.type = MoveType.Undefined;
            moveArray.cmd3.r = obj.r;
            
            moveArray.cmd4.pos.x = parameter.homeX;
            moveArray.cmd4.pos.y = parameter.homeY;
            moveArray.cmd4.pos.z = parameter.homeZ;
            moveArray.cmd4.v = 0;
            moveArray.cmd4.a = 0;
            moveArray.cmd4.type = MoveType.Undefined;
            moveArray.cmd4.r = obj.r;
            
            moveArray.cmd5.pos.x = parameter.homeX;
            moveArray.cmd5.pos.y = parameter.homeY;
            moveArray.cmd5.pos.z = parameter.homeZ;
            moveArray.cmd5.v = 0;
            moveArray.cmd5.a = 0;
            moveArray.cmd5.type = MoveType.Undefined;
            moveArray.cmd5.r = obj.r;
            
            moveArray.cmd6.pos.x = parameter.homeX;
            moveArray.cmd6.pos.y = parameter.homeY;
            moveArray.cmd6.pos.z = parameter.homeZ;
            moveArray.cmd6.v = 0;
            moveArray.cmd6.a = 0;
            moveArray.cmd6.type = MoveType.Undefined;
            moveArray.cmd6.r = obj.r;
        end
        
        function [moveArray, robotTransition, positionRequest] = jogging(obj, nextPosition, parameter, robotState)
            moveArray = obj.initMoveArray( parameter );
            % Move from current safe position above pick position
            moveArray.cmd1.pos.x = nextPosition.pos1.x;
            moveArray.cmd1.pos.y = nextPosition.pos1.y;
            moveArray.cmd1.pos.z = nextPosition.pos1.z;
            moveArray.cmd1.v = parameter.v_max;
            moveArray.cmd1.a = parameter.a_max;
            moveArray.cmd1.type = MoveType.MoveL;
            moveArray.cmd1.r = obj.r;

            % Send state transition start for robot when new position is available
            positionRequest = robotState == RobotState.Idle;
            robotTransition = RobotTransition.NoAction;
            if nextPosition.newPos && robotState == RobotState.Idle
                robotTransition = RobotTransition.Start;
            end
        end
        
        function [moveArray, robotTransition, positionRequest]  = calibration(obj, nextPosition, parameter, robotState)
            moveArray = obj.initMoveArray( parameter );
            
            % Move from current safe position above pick position
            moveArray.cmd1.pos.x = nextPosition.pos1.x;
            moveArray.cmd1.pos.y = nextPosition.pos1.y;
            moveArray.cmd1.pos.z = max( parameter.z_max - obj.deltaZ, 0 );
            moveArray.cmd1.v = parameter.dq_max;
            moveArray.cmd1.a = parameter.ddq_max;
            moveArray.cmd1.type = MoveType.MoveJ;
            moveArray.cmd1.r = obj.r;

            % Move down to pick position
            moveArray.cmd2.pos.x = nextPosition.pos1.x;
            moveArray.cmd2.pos.y = nextPosition.pos1.y;
            moveArray.cmd2.pos.z = parameter.z_max;
            moveArray.cmd2.v = parameter.v_max;
            moveArray.cmd2.a = parameter.a_max;
            moveArray.cmd2.type = MoveType.MoveL;
            moveArray.cmd2.r = obj.r;

            % Move up again
            moveArray.cmd3.pos.x = nextPosition.pos1.x;
            moveArray.cmd3.pos.y = nextPosition.pos1.y;
            moveArray.cmd3.pos.z = max( parameter.z_max - obj.deltaZ, 0 );
            moveArray.cmd3.v = parameter.v_max;
            moveArray.cmd3.a = parameter.a_max;
            moveArray.cmd3.type = MoveType.MoveL;
            moveArray.cmd3.r = obj.r;

            % Send state transition start for robot when new position is available
            positionRequest = robotState == RobotState.Idle;
            robotTransition = RobotTransition.NoAction;
            if nextPosition.newPos && robotState == RobotState.Idle
                robotTransition = RobotTransition.Start;
            end
        end
        
        function [moveArray, robotTransition, positionRequest]  = automatic(obj, nextPosition, parameter, robotState)
            moveArray = obj.initMoveArray( parameter );
            
            % Move from current safe position above pick position
            moveArray.cmd1.pos.x = nextPosition.pos1.x;
            moveArray.cmd1.pos.y = nextPosition.pos1.y;
            moveArray.cmd1.pos.z = max( parameter.z_max - obj.deltaZ, 0 );
            moveArray.cmd1.v = parameter.dq_max;
            moveArray.cmd1.a = parameter.ddq_max;
            moveArray.cmd1.type = MoveType.MoveJ;
            moveArray.cmd1.r = obj.r;

            % Move down to pick position
            moveArray.cmd2.pos.x = nextPosition.pos1.x;
            moveArray.cmd2.pos.y = nextPosition.pos1.y;
            moveArray.cmd2.pos.z = parameter.z_max;
            moveArray.cmd2.v = parameter.v_max;
            moveArray.cmd2.a = parameter.a_max;
            moveArray.cmd2.type = MoveType.MoveL;
            moveArray.cmd2.r = obj.r;

            % Move up again
            moveArray.cmd3.pos.x = nextPosition.pos1.x;
            moveArray.cmd3.pos.y = nextPosition.pos1.y;
            moveArray.cmd3.pos.z = max( parameter.z_max - obj.deltaZ, 0 );
            moveArray.cmd3.v = parameter.v_max;
            moveArray.cmd3.a = parameter.a_max;
            moveArray.cmd3.type = MoveType.MoveL;
            moveArray.cmd3.r = obj.r;

            % Move to place position
            moveArray.cmd4.pos.x = nextPosition.pos2.x;
            moveArray.cmd4.pos.y = nextPosition.pos2.y;
            moveArray.cmd4.pos.z = max( parameter.z_max - obj.deltaZ, 0 );
            moveArray.cmd4.v = parameter.dq_max;
            moveArray.cmd4.a = parameter.ddq_max;
            moveArray.cmd4.type = MoveType.MoveJ;
            moveArray.cmd4.r = obj.r;

            % Move down to place position
            moveArray.cmd5.pos.x = nextPosition.pos2.x;
            moveArray.cmd5.pos.y = nextPosition.pos2.y;
            moveArray.cmd5.pos.z = parameter.z_max;
            moveArray.cmd5.v = parameter.v_max;
            moveArray.cmd5.a = parameter.a_max;
            moveArray.cmd5.type = MoveType.MoveL;
            moveArray.cmd5.r = obj.r;

            % Move down to place position
            moveArray.cmd6.pos.x = nextPosition.pos2.x;
            moveArray.cmd6.pos.y = nextPosition.pos2.y;
            moveArray.cmd6.pos.z = max( parameter.z_max - obj.deltaZ, 0 );
            moveArray.cmd6.v = parameter.v_max;
            moveArray.cmd6.a = parameter.a_max;
            moveArray.cmd6.type = MoveType.MoveL;
            moveArray.cmd6.r = obj.r;

            % Send state transition start for robot when new position is available
            positionRequest = robotState == RobotState.Idle;
            robotTransition = RobotTransition.NoAction;
            if nextPosition.newPos && robotState == RobotState.Idle
                robotTransition = RobotTransition.Start;
            end
        end
        
        function [moveArray, robotTransition, positionRequest]  = externalTrajectory(obj, nextPosition, parameter, robotState)
            moveArray = obj.initMoveArray( parameter );
            
            % Move from current safe position above pick position
            moveArray.cmd1.pos.x = parameter.homeX;
            moveArray.cmd1.pos.y = parameter.homeY;
            moveArray.cmd1.pos.z = parameter.homeZ;
            moveArray.cmd1.v = 0;
            moveArray.cmd1.a = 0;
            moveArray.cmd1.type = MoveType.MoveExternal;
            moveArray.cmd1.r = obj.r;

            % Send state transition start for robot when new position is available
            positionRequest = robotState == RobotState.Idle;
            robotTransition = RobotTransition.NoAction;
            if nextPosition.newPos && robotState == RobotState.Idle
                robotTransition = RobotTransition.Start;
            elseif positionRequest
                moveArray.cmd1.type = MoveType.Undefined;
            end
        end
        
        function [moveArray, robotTransition]  = homing(obj, parameter)
            moveArray = obj.initMoveArray( parameter );
            
            % Move from current safe position above pick position
            moveArray.cmd1.pos.x = parameter.homeX;
            moveArray.cmd1.pos.y = parameter.homeY;
            moveArray.cmd1.pos.z = parameter.homeZ;
            moveArray.cmd1.v = parameter.dq_max;
            moveArray.cmd1.a = parameter.ddq_max;
            moveArray.cmd1.type = MoveType.MoveJ;
            moveArray.cmd1.r = obj.r;

            % Send state transition start for robot when new position is availabl
            robotTransition = RobotTransition.Home;
        end
        
        
    end
end


