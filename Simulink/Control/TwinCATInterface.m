classdef TwinCATInterface
    %HMIINTERFACE Class, which includes all needed functions for Robot
    %Control state machine
    
    properties
    end
    
    methods
        function obj = HmiInterface()
        end
        
        
        function [y] = fromHmiOut(obj, u, enableSiL )
            y = zeros( 18, 1 );
            y(1) = enableSiL;
            y(2) = u.transition;
            y(3) = u.touchEvent;
            y(4) = u.nextPosition.pos1.x;
            y(5) = u.nextPosition.pos1.y;
            y(6) = u.nextPosition.pos1.z;
            y(7) = u.nextPosition.pos2.x;
            y(8) = u.nextPosition.pos2.y;
            y(9) = u.nextPosition.pos2.z;
            y(10) = u.nextPosition.newPos;

            y(11) = u.parameter.v_max;
            y(12) = u.parameter.a_max;
            y(13) = u.parameter.dq_max;
            y(14) = u.parameter.ddq_max;
            y(15) = u.parameter.z_max;
            y(16) = u.parameter.homeX;
            y(17) = u.parameter.homeY;
            y(18) = u.parameter.homeZ;
        end
        
        function [y] = toHmiOut(obj, u )
            y.transition = SeqTransition( u(2) );
            y.touchEvent = logical( u(3) );
            y.nextPosition.pos1.x = double( u(4) );
            y.nextPosition.pos1.y = double( u(5) );
            y.nextPosition.pos1.z = double( u(6) );
            y.nextPosition.pos2.x = double( u(7) );
            y.nextPosition.pos2.y = double( u(8) );
            y.nextPosition.pos2.z = double( u(9) );
            y.nextPosition.newPos = logical( u(10) );

            y.parameter.v_max = double( u(11) );
            y.parameter.a_max = double( u(12) );
            y.parameter.dq_max = double( u(13) );
            y.parameter.ddq_max = double( u(14) );
            y.parameter.z_max = double( u(15) );
            y.parameter.homeX = double( u(16) );
            y.parameter.homeY = double( u(17) );
            y.parameter.homeZ = double( u(18) );
            y.parameter.request = false;
        end
        
        function [y] = fromHmiIn(obj, u, enable )
            y = zeros( 12, 1 );
            y(1) = enable;
            y(2) = u.state;
            y(3) = u.positionRequest;
            y(4) = u.robotOut.data.pos.x;
            y(5) = u.robotOut.data.pos.y;
            y(6) = u.robotOut.data.pos.z;
            y(7) = u.robotOut.data.v;
            y(8) = u.robotOut.data.a;
            y(9) = u.robotOut.data.type;
            y(10) = u.robotOut.data.r;
            y(11) = u.robotOut.positionReached;
            y(12) = u.robotOut.errorRobot;
        end
        
        function [y] = toHmiIn(obj, u )
            y.state = SeqState( u(2) );
            y.positionRequest = logical( u(3) );
            y.robotOut.data.pos.x = u(4);
            y.robotOut.data.pos.y = u(5);
            y.robotOut.data.pos.z = u(6);
            y.robotOut.data.v = u(7);
            y.robotOut.data.a = u(8);
            y.robotOut.data.type = MoveType( u(9) );
            y.robotOut.data.r = u(10);
            y.robotOut.positionReached = logical( u(11) );
            y.robotOut.errorRobot = ErrorType( u(12) );
        end
        
        function [y] = fromJointSpace(obj, u )
            y = zeros(10,1);

            y(1) = u.q1.p;
            y(2) = u.q1.v;
            y(3) = u.q1.a;

            y(4) = u.q2.p;
            y(5) = u.q2.v;
            y(6) = u.q2.a;

            y(7) = u.q3.p;
            y(8) = u.q3.v;
            y(9) = u.q3.a;
            
            y(10) = u.error;
        end
        
        function [y] = toJointSpace(obj, u )
            y.q1.p = u(1);
            y.q1.v = u(2);
            y.q1.a = u(3);

            y.q2.p = u(4);
            y.q2.v = u(5);
            y.q2.a = u(6);

            y.q3.p = u(7);
            y.q3.v = u(8);
            y.q3.a = u(9);
            
            y.error = ErrorType( u(10) );
        end
        
        function [y] = fromExternalTrajectory(obj, u )
            y = zeros(4001,1);
            for i=1:1000
               y(i) = u.trajectory(i).t; 
               y(1000+i) = u.trajectory(i).q1;
               y(2000+i) = u.trajectory(i).q2;
               y(3000+i) = u.trajectory(i).q3;    
            end

            y(4001) = u.size;
        end
        
        function [y] = toExternalTrajectory(obj, y_old, u )
            y = y_old;
            for i=1:1000
               y.trajectory(i).t = u(i); 
               y.trajectory(i).q1 = u(1000+i);
               y.trajectory(i).q2 = u(2000+i);
               y.trajectory(i).q3 = u(3000+i);    
            end

            y.size = u(4001);
        end
    end
end

