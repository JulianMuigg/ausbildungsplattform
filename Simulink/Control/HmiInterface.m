classdef HmiInterface
    %HMIINTERFACE Class, which includes all needed functions for Robot
    %Control state machine
    
    properties
    end
    
    methods
        function obj = HmiInterface()
        end
        
        function [ enable,data] = write(obj, connected, state, positionRequest, robotOut, p, robotType )
            persistent state_
            persistent pos_
            persistent positionRequest_
            persistent lastTouchEvent_
            persistent errorRobot_
            persistent counter_
            persistent firstConnection_
            if isempty( state_ ) || connected == 0
                state_ = SeqState.Undefined;
                positionRequest_ = false;
                pos_ = robotOut.data.pos; 
                errorRobot_ = ErrorType.HardStopError;
                lastTouchEvent_ = false;

                % Set pos_ to invalid values to force update
                pos_.x = -9999;
                pos_.y = -9999;
                pos_.z = -9999;
                counter_ = 0;
                firstConnection_ = 1;
            end

            enable = 0;
            data = uint8(zeros(64,1));
            temp = char(zeros(64,1));
            if connected == 1
                if firstConnection_ == 1
                    if robotType == RobotType.Scara
                        temp = 'RobotType=0';
                    elseif robotType == RobotType.Delta360
                        temp = 'RobotType=1';
                    else
                        temp = 'RobotType=-1';
                    end
                    firstConnection_ = 0;
                elseif p.request ~= 0
                    temp = ['Parameter=', sprintf('%.3f',p.v_max),';', ...
                        sprintf('%.3f',p.a_max),';', ...
                        sprintf('%.3f',p.dq_max),';', ...
                        sprintf('%.3f',p.ddq_max),';', ...
                        sprintf('%.3f',p.z_max),';', ...
                        sprintf('%.3f',p.homeX),';', ...
                        sprintf('%.3f',p.homeY),';', ...
                        sprintf('%.3f',p.homeZ)];
                    state_ = SeqState.Undefined;
                elseif state_ ~= state
                    temp = ['State=', int2str(uint8(state))] ;
                    state_ = state;

                    % reset positionRequest
                    if state > SeqState.Idle
                        positionRequest_ = false;
                    end
                elseif (robotOut.data.pos.z - (p.z_max-0.004)) > 0 && ~lastTouchEvent_
                    temp = ['TouchEvent=', ...
                        sprintf('%.3f',robotOut.data.pos.x),';', ...
                        sprintf('%.3f',robotOut.data.pos.y),';',...
                        sprintf('%.3f',robotOut.data.pos.z)];
                    lastTouchEvent_ = true;
                    counter_ = 0;
                elseif positionRequest ~= positionRequest_ && state_ > SeqState.Idle 
                    temp = ['PositionRequest=', int2str(positionRequest)];
                    positionRequest_ = positionRequest;
                elseif robotOut.data.pos.x ~= pos_.x || ...
                       robotOut.data.pos.y ~= pos_.y || ...
                       robotOut.data.pos.z ~= pos_.z 
                    temp = ['CurrentPos=',...
                        sprintf('%.3f',robotOut.data.pos.x),';', ...
                        sprintf('%.3f',robotOut.data.pos.y),';',...
                        sprintf('%.3f',robotOut.data.pos.z)];
                    pos_ = robotOut.data.pos;
                elseif robotOut.errorRobot ~= errorRobot_
                    temp = ['RobotError=', int2str(uint8(robotOut.errorRobot))];
                    errorRobot_ = robotOut.errorRobot;
                end

                % Reset touch event
                if robotOut.data.pos.z - (p.z_max-0.001) < 0 && lastTouchEvent_
                    if counter_ > 100
                        lastTouchEvent_ = false;
                    else
                        counter_ = counter_ + 1;
                    end
                end
            else
                firstConnection_ = 1;
            end

            data(1:length(temp),1) = rot90(flip(temp));

            if ~isempty( temp )
                enable = 1;
            end
  
        end
        
        function parameter = initParameter( obj, robotType )
            if robotType == RobotType.Delta360
               parameter.v_max = 0.1;
               parameter.a_max = 0.05;
               parameter.dq_max = 0.1;
               parameter.ddq_max = 0.05;
               parameter.z_max = 0.330;
               parameter.homeX = 0;
               parameter.homeY = 0;
               parameter.homeZ = 0.277;
           elseif robotType == RobotType.Scara
               parameter.v_max = 0.1;
               parameter.a_max = 0.5;
               parameter.dq_max = 2.0;
               parameter.ddq_max = 1.5;
               parameter.z_max = 0.02;
               parameter.homeX = -0.05;
               parameter.homeY = -0.05;
               parameter.homeZ = 0;
           else
               parameter.v_max = 0;
               parameter.a_max = 0;
               parameter.dq_max = 0;
               parameter.ddq_max = 0;
               parameter.z_max = 0;
               parameter.homeX = 0;
               parameter.homeY = 0;
               parameter.homeZ = 0;
            end
            
            parameter.request = false;
        end
        
        function [transition, touchEvent, nextPosition, parameter, externalTrajectory] = ...
                read(obj,connected, cmd, value, length, x1, y1, z1, x2, y2, z2, robotType, externalTrajectory_)

            % connected ... client is connected to tcp server
            % cmd ... received cmd
            % value ... received value
            % length ... size of received string
            % x1 ... pos1.x in pixel / type of parameter
            % y1 ... pos1.y in pixel / value of parameter
            % z1 ... pos1.z in pixel
            % x2 ... pos2.x in pixel
            % y2 ... pos2.y in pixel
            % z2 ... pos2.z in pixel
            
            coder.extrinsic('Simulink.Bus.createMATLABStruct');

            persistent transition_
            persistent nextPosition_
            persistent parameter_
            if isempty( transition_ )
               transition_ = "Undefined";

               nextPosition.pos1.x = 0;
               nextPosition.pos1.y = 0;
               nextPosition.pos1.z = 0;
               nextPosition.pos2.x = 0;
               nextPosition.pos2.y = 0;
               nextPosition.pos2.z = 0;
               nextPosition.newPos = false;
               nextPosition_ = nextPosition;

               parameter_ = obj.initParameter( robotType );
            end

            transition = transition_;
            touchEvent = false;
            nextPosition_.newPos = false;
            nextPosition = nextPosition_;
            parameter = parameter_;
            externalTrajectory = externalTrajectory_;

            if connected == 1
                if length > 0
                    if contains( cmd, 'Transition' )
                        transition = value;
                    elseif contains( cmd, 'PositionRequest' )
                        nextPosition.pos1.x = x1;
                        nextPosition.pos1.y = y1;
                        nextPosition.pos1.z = z1;
                        nextPosition.pos2.x = x2;
                        nextPosition.pos2.y = y2;
                        nextPosition.pos2.z = z2;
                        nextPosition.newPos = true;
                        nextPosition_ = nextPosition;
                    elseif  contains( cmd, 'Parameter' ) && value == ""
                        parameter.request = true;
                    elseif contains( cmd, 'Parameter' )

                        switch ParameterType( x1 )
                            case ParameterType.v_max
                                parameter.v_max = y1;
                            case ParameterType.a_max
                                parameter.a_max = y1;
                            case ParameterType.dq_max
                                parameter.dq_max = y1;
                            case ParameterType.ddq_max
                                parameter.ddq_max = y1;
                            case ParameterType.z_max
                                parameter.z_max = y1;
                            case ParameterType.homeX
                                parameter.homeX = y1;
                            case ParameterType.homeY
                                parameter.homeY = y1;
                            case ParameterType.homeZ
                                parameter.homeZ = y1;

                        end
                        parameter_ = parameter;
                    elseif contains( cmd, 'TouchEvent' )
                        touchEvent = true;
                    elseif contains( cmd, 'ExternalTrajectory' )
                        externalTrajectory = obj.loadTrajectory( value, externalTrajectory_ );
                        if externalTrajectory.size ~= -1
                            nextPosition.newPos = true;
                        end
                    end
                end
            end
        end
        
        function trajectory = loadTrajectory( obj, file, trajectory_ )
            coder.extrinsic('csvread');
            coder.extrinsic('exist');
            trajectory = trajectory_;
            temp = zeros(1000,4);
            if exist(file, 'file')
                temp = csvread(file);
                trajectory.size = -1;
                for n = 1:1:size( temp, 1 )
                    trajectory.trajectory(n).t = temp(n,1);
                    trajectory.trajectory(n).q1 = temp(n,2);
                    trajectory.trajectory(n).q2 = temp(n,3);
                    trajectory.trajectory(n).q3 = temp(n,4);

                    % Get real size of trajectory
                    if n > 1 && temp(n,1) == -1 && trajectory.size == -1
                        trajectory.size = n - 1;
                    end
                end
            end
        end
    end
end

