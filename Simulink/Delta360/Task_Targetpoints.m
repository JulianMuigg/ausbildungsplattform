z = 326;
z1 = z;
d = 1000;
p.A1 = [93;93;z]/d;
p.A2 = [70;93;z]/d;
p.A3 = [47;93;z]/d;
p.A4 = [93;70;z]/d;
p.A5 = [70;70;z]/d;
p.A6 = [47;70;z]/d;
p.A7 = [93;47;z]/d;
p.A8 = [70;47;z]/d;
p.A9 = [47;47;z]/d;

p.B1 = [-47;93;z]/d;
p.B2 = [-70;93;z]/d;
p.B3 = [-93;93;z]/d;
p.B4 = [-47;70;z]/d;
p.B5 = [-70;70;z]/d;
p.B6 = [-93;70;z]/d;
p.B7 = [-47;47;z]/d;
p.B8 = [-70;47;z]/d;
p.B9 = [-93;47;z]/d;

p.C1 = [93;-47;z1]/d;
p.C2 = [70;-47;z1]/d;
p.C3 = [47;-47;z1]/d;
p.C4 = [93;-70;z1]/d;
p.C5 = [70;-70;z1]/d;
p.C6 = [47;-70;z1]/d;
p.C7 = [93;-93;z1]/d;
p.C8 = [70;-93;z1]/d;
p.C9 = [47;-93;z1]/d;

p.D1 = [-47;-47;z1]/d;
p.D2 = [-70;-47;z1]/d;
p.D3 = [-93;-47;z1]/d;
p.D4 = [-47;-70;z1]/d;
p.D5 = [-70;-70;z1]/d;
p.D6 = [-93;-70;z1]/d;
p.D7 = [-47;-93;z1]/d;
p.D8 = [-70;-93;z1]/d;
p.D9 = [-93;-93;z1]/d;

save('Task_Targetpoints.mat')