classdef DeltaPathPlanning
    properties 
        delta360;
    end
    methods 
        function obj = DeltaPathPlanning()
            rm = 0.042;
            rb = 0.222106;
            l=0.400;
            w2b = -0.079855;
            range=0.144;
            Name='Delta';
            obj.delta360 = Delta360(rm,rb,l,w2b,range,Name);
        end
        
        function [trajectoryL, trajectoryJ, trajectoryExtern, moveType] = inputHandler(obj, dt, externalTrajectory, robotIn, jointSpace, taskSpace)
            
            persistent lastMoveType;
            if isempty(lastMoveType)
                lastMoveType = 0;
            end
            softLimitDetected = false;
            if robotIn.type == MoveType.MoveExternal
                moveType = 2;
            else
                %% Handle move type and remember it for next cycle
                if robotIn.type == MoveType.MoveL
                      moveType = 0;
                elseif robotIn.type == MoveType.MoveJ
                      moveType = 1;
                else
                     moveType = lastMoveType;
                end
                lastMoveType = moveType;
                
                %% Check for softlimit
                [moveType, softLimit, softLimitDetected] = obj.softLimit(taskSpace, robotIn, moveType);
            end     
     
            [trajectoryExtern] = obj.moveExternal( dt, externalTrajectory, robotIn );
            [trajectoryL, trajectoryJ] = obj.moveLJ( dt, robotIn, jointSpace, taskSpace, softLimitDetected); 
            
        end
        function [deltaIn, robotOut] = outputHandler(obj, traj, taskSpace, deltaOut, moveType)
            %% Defines the deltaIn and robotOut struct
            deltaIn.q1.p = traj.q1.p;
            deltaIn.q1.v = traj.q1.v;
            deltaIn.q1.a = traj.q1.a;
            deltaIn.q2.p = traj.q2.p;
            deltaIn.q2.v = traj.q2.v;
            deltaIn.q2.a = traj.q2.a;
            deltaIn.q3.p = traj.q3.p;
            deltaIn.q3.v = traj.q3.v;
            deltaIn.q3.a = traj.q3.a;
            deltaIn.error = ErrorType.NoError;
            
            % TODO: Sollte bei richitger Trajectoriengenerierung nicht
            % benötigt werden
            if deltaIn.q1.p < 0.0
                deltaIn.q1.p = 0.0;
            end
            
            if deltaIn.q2.p < 0.0
                deltaIn.q2.p = 0.0;
            end
            
            if deltaIn.q3.p < 0.0
                deltaIn.q3.p = 0.0;
            end
            
            robotOut.data.pos.x = taskSpace.x.p;
            robotOut.data.pos.y = taskSpace.y.p;
            robotOut.data.pos.z = taskSpace.z.p;
            robotOut.data.v = taskSpace.v;
            robotOut.data.a = taskSpace.a;
            robotOut.data.type = MoveType( moveType + 1 );
            robotOut.data.r = 0;
            robotOut.positionReached = traj.q1.reached && traj.q2.reached && traj.q3.reached;
            robotOut.errorRobot = deltaOut.error;
            
            if robotOut.positionReached 
                robotOut.positionReached = abs( deltaOut.q1.p - traj.q1.p ) < 1e-4 && ...
                    abs( deltaOut.q2.p - traj.q2.p ) < 1e-4 && ...
                    abs( deltaOut.q3.p - traj.q3.p ) < 1e-4;
            end
            
        end
        function [moveType, softLimit, softLimitDetected] = softLimit(obj, taskSpace, robotIn, currMoveType)
            persistent stopTaskSpace; 
            persistent lastSoftLimit;
            persistent timeSteps;
            
            %% Init persistent variables
            if isempty(stopTaskSpace)
                stopTaskSpace.x = robotIn.pos.x;
                stopTaskSpace.y = robotIn.pos.y;
                stopTaskSpace.z = robotIn.pos.z;                
                lastSoftLimit = false;
                timeSteps = 0;
            end
            %% check for new position, to reset lastSoftLimit
            if robotIn.pos.x ~= stopTaskSpace.x || ...
               robotIn.pos.y ~= stopTaskSpace.y || ...
               robotIn.pos.z ~= stopTaskSpace.z
           
                stopTaskSpace.x = robotIn.pos.x;
                stopTaskSpace.y = robotIn.pos.y;
                stopTaskSpace.z = robotIn.pos.z;                
                                
                if lastSoftLimit
                    disp( ['SoftLimit was active for ' sprintf('%.0f', timeSteps) ' timesteps'] );  
                    disp( 'SoftLimit reset' );
                end
                lastSoftLimit = false;
                timeSteps = 0;
            end 
            
            %% Check current values for softLimit
            refVal = 0.328;
            moveType = currMoveType;
            softLimit = lastSoftLimit;
            if robotIn.type == MoveType.MoveJ && refVal < taskSpace.z.p 
                moveType = 0; 
                softLimit = true;
                
                if ~lastSoftLimit
                    disp( 'SoftLimit reached' )              
                end
            elseif lastSoftLimit
                moveType = 0;
                timeSteps = timeSteps + 1;
            end
            
            %% Define hasChanged value
            softLimitDetected = softLimit ~= lastSoftLimit;
            if softLimitDetected
               disp( 'SoftLimit detected' );
            end
            
            %% Remember softLimit value for next run
            lastSoftLimit = softLimit;
        end
    end

    methods (Access = private)
        function trajectory = create( obj, t, startPoint, stopPoint, robotIn, type, softLimit )
            %% Define output sturcts
            trajectory.q1.start = startPoint.x;
            trajectory.q1.stop = stopPoint.x;
            trajectory.q1.p = 0; %stopPoint.x;
            trajectory.q1.v = 0;
            trajectory.q1.a = 0;
            trajectory.q1.reached = false;

            trajectory.q2.start = startPoint.y;
            trajectory.q2.stop = stopPoint.y;
            trajectory.q2.p = 0 ; %stopPoint.y;
            trajectory.q2.v = 0;
            trajectory.q2.a = 0;
            trajectory.q2.reached = false;

            trajectory.q3.start = startPoint.z;
            trajectory.q3.stop = stopPoint.z;
            trajectory.q3.p = 0.278; %stopPoint.z;
            trajectory.q3.v = 0;
            trajectory.q3.a = 0;
            trajectory.q3.reached = false;

            trajectory.v_max = robotIn.v;
            trajectory.a_max = robotIn.a;
            trajectory.tolerance = robotIn.r;
            trajectory.t = t;
            trajectory.moveType = type;
%             if type == MoveType.MoveJ
%             trajectory.softLimit = softLimit;
%             else 
%             trajectory.softLimit = false;
%             end
        end   
        
        function [trajectoryL, trajectoryJ] = moveLJ( obj, dt, robotIn, jointSpace, taskSpace,forceNewPosition);%, softLimit )
            persistent startTaskSpace;
            persistent startJointSpace;
            persistent stopTaskSpace;
            persistent t
            
            if isempty(stopTaskSpace)
                stopTaskSpace.x = robotIn.pos.x;
                stopTaskSpace.y = robotIn.pos.y;
                stopTaskSpace.z = robotIn.pos.z;

                startTaskSpace = stopTaskSpace;

                startJointSpace.x = jointSpace.q1.p;
                startJointSpace.y = jointSpace.q2.p;
                startJointSpace.z = jointSpace.q3.p;

                t = 0;
            end
            
            %% Check if new stop position is available
            if robotIn.pos.x ~= stopTaskSpace.x || ...
               robotIn.pos.y ~= stopTaskSpace.y || ...
               robotIn.pos.z ~= stopTaskSpace.z || forceNewPosition
                
                startTaskSpace.x = taskSpace.x.p;
                startTaskSpace.y = taskSpace.y.p;
                startTaskSpace.z = taskSpace.z.p;

                startJointSpace.x = jointSpace.q1.p;
                startJointSpace.y = jointSpace.q2.p;
                startJointSpace.z = jointSpace.q3.p;

                stopTaskSpace.x = robotIn.pos.x;
                stopTaskSpace.y = robotIn.pos.y;
                stopTaskSpace.z = robotIn.pos.z;
                t = 0;
               
            else
                t = t + dt;
            end

            trajectoryL = obj.create( t, startTaskSpace, stopTaskSpace, robotIn, MoveType.MoveL); %, softLimit );
            trajectoryJ = obj.create( t, startJointSpace, stopTaskSpace, robotIn, MoveType.MoveJ); %, softLimit );
        end
        
        function [trajectoryExtern] = moveExternal( obj, dt, externalTrajectory, robotIn )
            
            persistent n
            persistent isExternalEnabled;
            persistent t
            if isempty(isExternalEnabled)
                n = 1;
                isExternalEnabled = false;
                t = 0;
            end
            
            % Start / Restart / Continue    
            if ~isExternalEnabled && robotIn.type == MoveType.MoveExternal
                isExternalEnabled = true;
                n = 1;
                t = 0;
                disp( 'Start external' );
            elseif n <= externalTrajectory.size 
                t = t + dt;
                
                if externalTrajectory.trajectory(n).t < t
                    n = n + 1;
                end
            end     
            
            start.x = externalTrajectory.trajectory(1).q1;
            start.y = externalTrajectory.trajectory(1).q2;
            start.z = externalTrajectory.trajectory(1).q3;
            
            if n <= externalTrajectory.size 
                stop.x = externalTrajectory.trajectory(n).q1;
                stop.y = externalTrajectory.trajectory(n).q2;
                stop.z = externalTrajectory.trajectory(n).q3;
            else
                size = externalTrajectory.size;
                if size == 0
                    stop = start;
                else
                    stop.x = externalTrajectory.trajectory(size).q1;
                    stop.y = externalTrajectory.trajectory(size).q2;
                    stop.z = externalTrajectory.trajectory(size).q3;
                end
            end
            
            trajectoryExtern = obj.create( externalTrajectory.trajectory(n).t, start, stop, robotIn, MoveType.MoveExternal );
            
            if isExternalEnabled && n > 1 && n == externalTrajectory.size
                trajectoryExtern.q1.reached = true; 
                trajectoryExtern.q2.reached = true; 
                trajectoryExtern.q3.reached = true; 
                if robotIn.type == MoveType.Undefined
                    isExternalEnabled = false;
                    disp( 'Stop external' );
                end
            end               
        end 
        
    end
    
end
