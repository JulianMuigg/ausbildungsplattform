function [mStruct] = createDeltaInStruct()
%CREATEMOVESTRUCT function to create a move struct
% theta1 ... input values of first motor
% theta2 ... input values of second motor
% z ... input values of linear z axis

posStruct = createPosVelAccStruct();
mStruct = struct( 's1', { posStruct }, ...
                  's2', { posStruct }, ...
                  's3', { posStruct } );

end