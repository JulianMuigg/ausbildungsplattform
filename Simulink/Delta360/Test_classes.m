close all
%clear all

run('Delta_Robot_V3_DataFile.m')

Delta = Delta360(0.042,0.222106,0.400,-0.079885,0.144,'Delta');
MoveDelta = Move(0.050,0.100,0.070,0.100,0.01,Delta);


% Trajectory generation
[MoveDelta.TrajJ.('J1'), MoveDelta.TrajT.('T1')] = MoveDelta.J([0.000;0.000;0.300], [0.000;0.050;0.300]);
[MoveDelta.TrajJ.('J2'), MoveDelta.TrajT.('T2')] = MoveDelta.L([0.000;0.050;0.300], [0.000;0.050;0.310]);
[MoveDelta.TrajJ.('J3'), MoveDelta.TrajT.('T3')] = MoveDelta.L([0.000;0.050;0.310], [0.000;0.050;0.300]);
[MoveDelta.TrajJ.('J4'), MoveDelta.TrajT.('T4')] = MoveDelta.J([0.000;0.050;0.300], [0.100;-0.150;0.3195]);

Joint = MoveDelta.combineJ;
TCP = MoveDelta.combineT;

figure %XY-Plane
 plot(TCP(1,:),TCP(2,:))
 title('Task Space XY-Plane')
 
 figure % Cartesian Space
 plot3(TCP(1,:),TCP(2,:),TCP(3,:))
 title('Task Space 3D')
 
 figure % Joint 1 to 3
 plot(Joint(4,:),Joint(1,:))
 hold on 
 plot(Joint(4,:),Joint(2,:))
 hold on
 plot(Joint(4,:),Joint(3,:))
 title('Joint Space')
 

%% Check
% Check validity of Trajectories
[MoveDelta.TrajJ,MoveDelta.TrajT]= MoveDelta.overtravelZ(0.320);

% combine individual Trajectories
Joint1 = MoveDelta.combineJ;
TCP1 = MoveDelta.combineT;

% creat Timeseries
TJ1 = timeseries(Joint1(1:3,:), Joint1(4,:));

% Trajectories Graph 

figure %XY-Plane
 plot(TCP1(1,:),TCP1(2,:))
 title('Task Space XY-Plane1')
 
 figure % Cartesian Space
 plot3(TCP1(1,:),TCP1(2,:),TCP1(3,:))
 title('Task Space 3D1')
 
 figure % Joint 1 to 3
 plot(Joint1(4,:),Joint1(1,:))
 hold on 
 plot(Joint1(4,:),Joint1(2,:))
 hold on
 plot(Joint1(4,:),Joint1(3,:))
 title('Joint Space1')
 
 
%  figure 
%  plot( Joint(4,:), Joint(5,:) )
%  hold on 
%  plot( Joint(4,:),Joint(6,:) )
%  hold on 
%  plot( Joint(4,:), Joint(7,:) )
%  hold on 
