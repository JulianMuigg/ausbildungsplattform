function pick_and_place(Pick,Place,offs,simulation)

p = load('Task_Targetpoints.mat');

run('Delta_Robot_V3_DataFile.m')

offs = -abs(offs);
pick = p.p.(Pick);
place = p.p.(Place);

Delta = Delta360(0.042,0.222106,0.400,-0.079885,0.144,[],'Delta');
assignin('base','Delta',Delta);
MoveDelta = Move(0.050,0.100,0.070,0.100,0.01,Delta,[],[]);
assignin('base','MoveDelta',MoveDelta);

[MoveDelta.TrajJ.('J1'), MoveDelta.TrajT.('T1')] = ...
    MoveDelta.J([0;0;0.2773],MoveDelta.offset(pick,offs));
[MoveDelta.TrajJ.('J2'), MoveDelta.TrajT.('T2')] = ...
    MoveDelta.L(MoveDelta.offset(pick,offs),pick);
[MoveDelta.TrajJ.('J3'), MoveDelta.TrajT.('T3')] = ...
    MoveDelta.L(pick,MoveDelta.offset(pick,offs));
[MoveDelta.TrajJ.('J4'), MoveDelta.TrajT.('T4')] = ...
    MoveDelta.J(MoveDelta.offset(pick,offs),MoveDelta.offset(place,offs));
[MoveDelta.TrajJ.('J5'), MoveDelta.TrajT.('T5')] = ...
    MoveDelta.L(MoveDelta.offset(place,offs),place);
[MoveDelta.TrajJ.('J6'), MoveDelta.TrajT.('T6')] = ...
    MoveDelta.L(place,MoveDelta.offset(place,offs));
[MoveDelta.TrajJ.('J7'), MoveDelta.TrajT.('T7')] = ...
    MoveDelta.J(MoveDelta.offset(place,offs),[0;0;0.2773]);

[MoveDelta.TrajJ,MoveDelta.TrajT]= MoveDelta.overtravelZ(0.3266);
RangeOK = MoveDelta.overtravelAxis;

Joint = MoveDelta.combineJ;
TJs =  timeseries(Joint(1:3,:),Joint(4,:));

assignin('base','TJ',TJs);

if simulation == 1
    run('Delta_Robot_V3.slx')
    sim('Delta_Robot_V3.slx')
else
    if RangeOK == 1
        run('Delta_Task_Lecture.slx')
        sim('Delta_Task_Lecture.slx')
    end
end
end