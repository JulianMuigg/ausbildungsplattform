clear all
syms mu1 mu2 mu3 delta1 delta2 delta3 l rm rb s1 s2 s3


Rz2 = [cosd(-120), -sind(120), 0;
       sind(120), cosd(120),0;
       0, 0, 1];
Rz3 = [cosd(120), -sind(120), 0;
       sind(120), cosd(120),0;
       0, 0, 1];
   
pose_l1 = [l*sin(mu1)*cos(delta1); l*sin(mu1)*sin(delta1); l*cos(mu1)];
pose_l2 = [l*sin(mu2)*cos(delta2); l*sin(mu2)*sin(delta2); l*cos(mu2)];
pose_l3 = [l*sin(mu3)*cos(delta3); l*sin(mu3)*sin(delta3); l*cos(mu3)];

rs1 = [0 ; rb-s1/sqrt(2)-rm; s1/sqrt(2)];
rs2 = [0 ; rb-s2/sqrt(2)-rm; s2/sqrt(2)];
rs3 = [0 ; rb-s3/sqrt(2)-rm; s3/sqrt(2)];

a2_1 = Rz2*rs2-rs1;
a3_1 = Rz3*rs3-rs1;

u2_1 = simplify(Rz2*pose_l2-pose_l1);
u3_1 = simplify(Rz3*pose_l3-pose_l1);

mu3(mu1) = acos(-a3_1(3)/l+cos(mu1));
mu2(mu1) = acos(-a2_1(3)/l+cos(mu1));
IIIdelta1(mu1,delta2) = acos((a2_1(1)/l+cos(delta2)*(-1/2)*sin(mu2)-sin(delta2)...
    * (-0.5806)*sin(mu2))/sin(mu1));
IVdelta1(mu1,delta2) = asin((a2_1(2)-cos(delta2)*(sqrt(3)/2)*sin(mu2)-1/2*sin(delta2)...
    *sin(mu2))/sin(mu1));
IIIinIV = IIIdelta1==IVdelta1;
assume(mu1 > 0)
assume(delta1 > 0)
mu1 = solve(IIIinIV,mu1);

% eq1 = a2_1/l == u2_1;
% eq2 = a3_1/l == u3_1;
% eq = [eq1, eq2];

%b = solve(eq,[mu1,mu2,mu3,delta1,delta2,delta3])
