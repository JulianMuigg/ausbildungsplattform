clear all 

syms s1 s2 s3 mu1 mu2 mu3 delta1 delta2 delta3
rm = 0.042;
rb = 0.222106;
theta = 120;
csphi = 1/sqrt(2);
l = 400;

pose_b = [0;rb;0];
pose_s = [0;-csphi;csphi];
pose_l1 = [l*sin(mu1)*cos(delta1); l*sin(mu1)*sin(delta1); l*cos(mu1)];
pose_l2 = [l*sin(mu2)*cos(delta2); l*sin(mu2)*sin(delta2); l*cos(mu2)];
pose_l3 = [l*sin(mu3)*cos(delta3); l*sin(mu3)*sin(delta3); l*cos(mu3)];
pose_m = [0; -rm; 0];
Rz2 = [cos(-120), -sin(-120), 0;
       sin(-120), cos(-120),0;
       0, 0, 1];
Rz3 = [cos(120), -sin(120), 0;
       sin(120), cos(120),0;
       0, 0, 1];

f1(s1, mu1, delta1) = pose_b+s1 * pose_s + pose_l1 + pose_m;
f2(s2, mu2, delta2) = Rz2*(pose_b+s2 * pose_s + pose_l2 + pose_m);
f3(s3, mu3, delta3) = Rz3*(pose_b+s3 * pose_s + pose_l3 + pose_m);

g1(s1,s2,mu1,mu2,delta1,delta2) = f2(s2,mu2,delta2) - f1(s1,mu1,delta1);
g2(s1,s3,mu1,mu3,delta1,delta3) = f3(s3,mu3,delta3) - f1(s1,mu1,delta1);

g(s1,s2,s3,mu1,mu2,mu3,delta1,delta2,delta3) = [g1(s1,s2,mu1,mu2,delta1,delta2);
                                                g2(s1,s3,mu1,mu3,delta1,delta3)];


eq = g == [0;0;0;0;0;0];
res = solve(eq,[mu1,mu2,mu3,delta1,delta2,delta3]);