close all 
clear all

%initializing Classes
Delta = Delta360(0.042,0.222106,0.400,-0.079885,0.144,'Delta');
MoveDelta = Move(0.09,0.18,0.09,0.18,0.01,0.333,Delta);
% Define Target Points
p.('P1') = [-0.135/2;-0.130/2;0.270];
p.('P2') = [-0.135/2+0.122;0.130/2;0.270];
p.('P3') = [0.135/2; -0.130/2+0.0633;0.328];
Task = Delta_Task(p,MoveDelta,'Delta_Robot_V3_DataFile.m','Delta_Robot_V3.slx');

[out1, overtravel1, rangeOK1] = Task.pick_and_place('P1','P2',0.02,0);
[out2, overtravel2, rangeOK2] = Task.pick_and_place('P1','P3',0.02,0);