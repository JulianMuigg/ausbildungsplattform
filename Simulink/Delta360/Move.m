classdef Move < handle
    properties
        vtmax % max speed of the actuator
        atmax % max acceleration of the actuator
        vjmax % max joint speed
        ajmax % max joint acceleration
        splane % safty plane of Manipulator
        Delta360 % class of Manipulator
        timestep % Interpolation timestep
        TrajJ % Struct of Joint Trajectories J1 to Jn
        TrajT % Struct of Task Trajectories T1 to Tn
        Axis % Struct of Axis Data T1 to Tn and J1 to Jn
        time % Time of Trajektory n
        
    end
    methods
        function obj = Move(vtmax,atmax,vjmax,ajmax,timestep,splane,Delta360)
            obj.vtmax = vtmax;
            obj.atmax = atmax;
            obj.vjmax = vjmax;
            obj.ajmax = ajmax;
            obj.splane = splane;
            obj.Delta360 = Delta360;
            obj.timestep = timestep;
        end
        function J(obj,pos1, pos2,num)
            
            v = obj.vjmax;
            a = obj.ajmax;
            
            S1 = obj.Delta360.inverse(pos1);
            S2 = obj.Delta360.inverse(pos2);
            
            h = S2-S1;
            
            %% Trajectory Form Querry
            Ta = zeros(size(S1,1),1);
            T =  zeros(size(S1,1),1);
            for j = 1:size(S1,1)
                if abs(h(j)) > abs(v^2/a)
                    Ta(j) = v/a; %acceleration and decelaration time
                    T(j) = (abs(h(j))*a + v^2)/(a*v); %total duration
                elseif abs(h(j)) <= v^2/a
                    Ta(j) = sqrt(abs(h(j))/a); % acceleration and deceleration time
                    T(j) = 2*Ta(j); % total duration
                end
            end
            
            Ta_max = max(Ta);
            T_max = max(T);
            vj = zeros(size(S1,1),1);
            aj = zeros(size(S1,1),1);
            
            
            for j = 1:size(S1,1)
                if (Ta_max == Ta(j) && T_max == T(j))
                    vj(j) = v;
                    aj(j) = a;
                else
                    vj(j) = abs(h(j)/(T_max-Ta_max));
                    aj(j) = abs(h(j)/(Ta_max*(T_max-Ta_max)));
                end
                % Change velocity/acceleration depinding on direction
                if h(j) < 0
                    vj(j) = -vj(j);
                    aj(j) = -aj(j);
                end
            end
            
            
            % creation of time vector
            int = obj.timestep;
            t = 0:int:max(T);
            s = zeros(length(S1),length(t)); % preallocation of q(t)
            ds = zeros(length(S1),length(t)); %preallocation of dq(t)
            dds = zeros(length(S1),length(t)); %preallocation of ddq(t)
            
            %Trajectory Joint n
            for n = 1 : length(S1)
                
                % constant velocity YES or No
                if abs(h(n)) > abs(vj(n)^2/aj(n))
                    %constant velocity interpolation
                    for i = 1:length(t)
                        if  t(i) <= Ta_max
                            s(n,i) = S1(n) + 0.5 * aj(n) * t(i)^2; % acceleration phase
                            ds(n,i) = aj(n)*t(i);
                            dds(n,i) = aj(n);
                        elseif Ta_max < t(i) && t(i) <= (T_max-Ta_max)
                            s(n,i) = S1(n) + aj(n)*Ta_max * (t(i)-Ta_max/2); % constant velocity phase
                            ds(n,i) = vj(n);
                            dds(n,i) = 0;
                        elseif (T_max-Ta_max) < t(i) && t(i) <= T_max
                            s(n,i) = S2(n) - 0.5 * aj(n) * (T_max-t(i))^2; %deceleration phase
                            ds(n,i) = vj(n)-aj(n)*(t(i)-(T_max-Ta_max));
                            dds(n,i) = -aj(n);
                        else
                            s(n,i) = s(n,i-1);
                        end
                    end
                else
                    % no constant velocity interpolation
                    vj = aj*Ta_max; % maximum velocity peak
                    for i = 1:length(t)
                        if 0 <= t(i) && t(i) < Ta_max
                            s(n,i) = S1(n) + 0.5 * aj(n) * t(i)^2; %acceleration phase
                            ds(n,i) = aj(n)*t(i);
                            dds(n,i) = aj(n);
                        elseif (T(n)-Ta_max) <= t(i) && t(i) < T_max
                            s(n,i) = S2(n) - 0.5 * aj(n) * (T_max - t(i))^2; % deceleration phase
                            ds(n,i)= vj(n)-aj(n)*(t(i)-(T_max-Ta_max));
                            dds(n,i) = -aj(n);
                        else
                            s(n,i) = s(n,i-1);
                        end
                    end
                end
            end
            
            % Position, Velocity or Acceleration Struct
            obj.TrajJ.(strcat('J',num)).s = s;
            obj.TrajJ.(strcat('J',num)).ds = ds;
            obj.TrajJ.(strcat('J',num)).dds = dds;
            
            % time of Trajectory num
            obj.time.(strcat('Traj',num)) = t;
            
            % Axis Data s,ds,dds 
            obj.Axis.(strcat('J',num)).s1 = [s(1,:);ds(1,:);dds(1,:)];
            obj.Axis.(strcat('J',num)).s2 = [s(2,:);ds(2,:);dds(2,:)];
            obj.Axis.(strcat('J',num)).s3 = [s(3,:);ds(3,:);dds(3,:)];
           
            % Cartesian Coordinates of Joint interpolation
            pos = zeros(3,length(t));
            vel = zeros(3,length(t));
            for i =1:length(t)
                pos(:,i) = obj.Delta360.forward(s(:,i));
                vel(:,i) = obj.Delta360.jacobi(s(:,i)) * ds(:,i);
            end
            % Cartesian Acceleration through differentiation
            acc(1,:) = diff(vel(1,:))./int;
            acc(2,:) = diff(vel(2,:))./int;
            acc(3,:) = diff(vel(3,:))./int;
            acc(1,1:end+1) = [acc(1,:),acc(1,end)];
            acc(2,1:end) = [acc(2,1:end-1),acc(2,end-1)];
            acc(3,1:end) = [acc(3,1:end-1),acc(3,end-1)];
            
            % Position, Velocity or Acceleration Struct
            obj.TrajT.(strcat('T',num)).p = pos;
            obj.TrajT.(strcat('T',num)).v = vel;
            obj.TrajT.(strcat('T',num)).a = acc;
            obj.TrajT.(strcat('T',num)).time = t;
            % Axis Data pos,vel,acc
            obj.Axis.(strcat('T',num)).x = [pos(1,:);vel(1,:);acc(1,:)];
            obj.Axis.(strcat('T',num)).y = [pos(2,:);vel(2,:);acc(2,:)];
            obj.Axis.(strcat('T',num)).z = [pos(3,:);vel(3,:);acc(3,:)];
            
            
        end
        function L(obj,pos1, pos2,num)
            p1(1,1) = pos1(1);
            p1(2,1) = pos1(2);
            p1(3,1) = pos1(3);
            p2(1,1) = pos2(1);
            p2(2,1) = pos2(2);
            p2(3,1) = pos2(3);
            
            h = p2-p1; % displacment and direction
            % Factoring velocity and acceleration vektor
            v = obj.vtmax .* (h./norm(h));
            a = obj.atmax .* (h./norm(h));
            
            % Calculation of durations
            
            if max(abs(h)) >= max(abs(v)).^2/max(abs(a))
                Ta = max(abs(v))./max(abs(a)); % acceleration and deceleration time
                T = (max(abs(h)) .* max(abs(a)) + max(abs(v)).^2)/(max(abs(a)).*max(abs(v))); % total duration
            else
                Ta = sqrt(max(abs(h))./max(abs(a))); % acceleration and deceleration time
                T = 2*Ta; % totalduration
                vnew = max(abs(a))*Ta; % maximum velocity peak
            end
            
            %prealocation
            int = obj.timestep;
            t = 0:int:T;
            pos = zeros(3,length(t));
            dpos = zeros(3,length(t));
            ddpos = zeros(3,length(t));
            
            for n = 1:length(h)
                
                if abs(h(n)) >= abs(v(n))^2/abs(a(n))
                    % linear movement computation
                    for i = 1:length(t)
                        if t(i) <= Ta
                            pos(n,i) = pos1(n) + 0.5 *a(n) * t(i)^2; % acceleration phase
                            dpos(n,i) = a(n) * t(i);
                            ddpos(n,i) = a(n);
                        elseif Ta < t(i) && t(i) <= (T-Ta)
                            pos(n,i) = pos1(n) + a(n) * Ta *(t(i)-Ta/2); %linear movement phase
                            dpos(n,i) = v(n);
                            ddpos(n,i) = 0;
                        elseif (T-Ta) < t(i) && t(i)<=T
                            pos(n,i) = pos2(n)- 0.5 * a(n) *(T-t(i))^2; % deceleration
                            dpos(n,i) = v(n)-a(n)*(t(i)-(T-Ta));
                            ddpos(n,i) = -a(n);
                        end
                    end
                elseif h(n) == 0
                    for i = 1:length(t)
                        pos(n,i) = pos1(n);
                        dpos(n,i) = 0;
                        ddpos(n,i) = 0;
                    end
                else
                    % no linear movement computation
                    for i = 1:length(t)
                        if 0<=t(i) && t(i) <= Ta
                            pos(n,i) = pos1(n) + 0.5 * a(n) * t(i)^2; %acceleration phase
                            dpos(n,i) = a(n) * t(i);
                            ddpos(n,i) = a(n);
                        elseif (T-Ta) < t(i) && t(i) <= T
                            pos(n,i) = pos2(n) - 0.5 * a(n) * (T-t(i))^2; % deceleration phase
                            dpos(n,i) = vnew-a(n)*(t(i)-(T-Ta));
                            ddpos(n,i) = -a(n);
                        end
                    end
                end
            end
            
            % Position, Velocity or Acceleration Struct
            obj.TrajT.(strcat('T',num)).p = pos;
            obj.TrajT.(strcat('T',num)).v = dpos;
            obj.TrajT.(strcat('T',num)).a = ddpos;
            obj.TrajT.(strcat('T',num)).time = t;
            
            % time of Trajectory num
            obj.time.(strcat('Traj',num)) = t;
            
             % Axis Data pos,vel,acc
            obj.Axis.(strcat('T',num)).x = [pos(1,:);dpos(1,:);ddpos(1,:)];
            obj.Axis.(strcat('T',num)).y = [pos(2,:);dpos(2,:);ddpos(2,:)];
            obj.Axis.(strcat('T',num)).z = [pos(3,:);dpos(3,:);ddpos(3,:)];
            
            % Joint Space Trajectory for Linear interpolation
            %prealocation
            s = zeros(3,length(t));
            ds = zeros(3,length(t));
            for j = 1:length(t)
                s(:,j) = obj.Delta360.inverse(pos(:,j));
                ds(:,j) = obj.Delta360.invJacobi(pos(:,j)) * dpos(:,j);
            end  
            % Axis Acceleration through differentiation
            dds(1,:) = diff(ds(1,:))./int;
            dds(2,:) = diff(ds(2,:))./int;
            dds(3,:) = diff(ds(3,:))./int;
            dds(1,1:end+1) = [dds(1,:),dds(1,end)];
            dds(2,1:end) = [dds(2,1:end-1),dds(2,end-1)];
            dds(3,1:end) = [dds(3,1:end-1),dds(3,end-1)];
            
            % Position, Velocity or Acceleration Struct
            obj.TrajJ.(strcat('J',num)).s = s;
            obj.TrajJ.(strcat('J',num)).ds = ds;
            obj.TrajJ.(strcat('J',num)).dds = dds;
            
            % Axis Data s,ds,dds 
            obj.Axis.(strcat('J',num)).s1 = [s(1,:);ds(1,:);dds(1,:)];
            obj.Axis.(strcat('J',num)).s2 = [s(2,:);ds(2,:);dds(2,:)];
            obj.Axis.(strcat('J',num)).s3 = [s(3,:);ds(3,:);dds(3,:)];
        end        
        function combine(obj,n) 
            % n Number of Trajectories to combine
            obj.TrajT.('TCom') = obj.TrajT.('T1');
            obj.TrajJ.('JCom') = obj.TrajJ.('J1');
            obj.Axis.('JCom') = obj.Axis.('J1');
            obj.Axis.('TCom') = obj.Axis.('T1');
            obj.time.('TrajCom') = obj.time.('Traj1'); 
            
            update_time = obj.time.('Traj1');
            
           
            for i=2:n
                % creat string counters
                Jn = strcat('J',num2str(i));
                Tn = strcat('T',num2str(i));
                Trajn = strcat('Traj',num2str(i));
                
                %Combine Time vectors
                update_time = update_time(end) + obj.time.(Trajn) + obj.timestep;
                obj.time.('TrajCom') = [obj.time.('TrajCom') , update_time];
                
                %Combine Joint Trajectories
                obj.TrajJ.('JCom').s = [obj.TrajJ.('JCom').s, obj.TrajJ.(Jn).s];
                obj.TrajJ.('JCom').ds = [obj.TrajJ.('JCom').ds, obj.TrajJ.(Jn).ds];
                obj.TrajJ.('JCom').dds = [obj.TrajJ.('JCom').dds, obj.TrajJ.(Jn).dds];
               
                obj.TrajT.('TCom').p = [obj.TrajT.('TCom').p, obj.TrajT.(Tn).p];
                obj.TrajT.('TCom').v = [obj.TrajT.('TCom').v, obj.TrajT.(Tn).v];
                obj.TrajT.('TCom').a = [obj.TrajT.('TCom').a, obj.TrajT.(Tn).a];
                
                obj.Axis.('JCom').s1 = [obj.Axis.('JCom').s1, obj.Axis.(Jn).s1];
                obj.Axis.('JCom').s2 = [obj.Axis.('JCom').s2, obj.Axis.(Jn).s2];
                obj.Axis.('JCom').s3 = [obj.Axis.('JCom').s3, obj.Axis.(Jn).s3];
                
                obj.Axis.('TCom').x = [obj.Axis.('TCom').x, obj.Axis.(Tn).x];
                obj.Axis.('TCom').y = [obj.Axis.('TCom').y, obj.Axis.(Tn).y];
                obj.Axis.('TCom').z = [obj.Axis.('TCom').z, obj.Axis.(Tn).z];
            end
        end
        function overtravel = overtravelZ(obj,num) % todo: Update to new Data handling
            % refVal is the distance from the Base to the safety plane
            refVal = obj.splane;
            % uses Joint Space Trajektories
            TL = obj.TrajT;
            TrajTNum = strcat('T',num);
            for j = 1:length(TL.(TrajTNum).p(3,:))
                if refVal >= TL.(TrajTNum).p(3,j)
                    overtravel = 0;
                else
                    %Warning if not safe
                    % Interpolate Trajektory with Move L from start to end
                    % point
                    str1 = 'Trajectory';
                    str2 = ' ';
                    str3 = num;
                    str4 = ' exceeds max. Z value.';
                    str = [str1, str2, str3, str4];
                    msgbox(str,'Warning','warn');
                    overtravel = 1;
                    obj.plotOvertravel(num)
                    break
                    %New Trajectory if not (unsafe)
                end
            end
        end
        function poffs = offset(obj,p,offs)
            poffs = [p(1);p(2);p(3)+offs];
        end
        function RangeOK = overtravelAxis(obj,num)
            
            Jn = strcat('J',num);
            TJ = obj.TrajJ.(Jn).s;
            range = obj.Delta360.range;
            RangeOK = zeros(1,3);
            for k = 1:3
                for j = 1:length(TJ(3,:))
                    if 0 <= TJ(k,j) && range >= TJ(k,j)
                        RangeOK(k) = 1;
                    else
                        RangeOK(k) = 0;
                        %Warning if not safe
                        % Interpolate Trajektory with Move L from start to end
                        % point
                        str1 = 'Trajectory';
                        str2 = ' ';
                        str3 = num;
                        str4 = ' exceeds Range of Axis';
                        str5 = ' ' ;
                        str6 = num2str(k);
                        str = [str1, str2, str3, str4, str5, str6];
                        msgbox(str,'Error','error');
                        break
                        %New Trajectory if not (unsafe)
                    end
                    
                end
            end
            if ~isequal([1 1 1],RangeOK)
                obj.plotRange(num)
            end
        end
        function TrajJplot(obj,num)
            Jn = strcat('J',num);
            Trajn = strcat('Traj',num);
           % plot Axis 1
           figure
           title('Axis 1')
           subplot(3,1,1)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).s(1,:))
           ylabel('$s_1 \, / \, mm$','interpreter','Latex')
           subplot(3,1,2)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).ds(1,:))
           ylabel('$v_1 \, / \, mms^{-1}$','interpreter','Latex')
           subplot(3,1,3)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).dds(1,:))
           ylabel('$a_1 \, / \, mms^{-2}$','interpreter','Latex')
           xlabel('$t \, / \, s$','interpreter','Latex')
           
           % plot Axis 2
           figure
           title('Axis 2')
           subplot(3,1,1)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).s(2,:))
           ylabel('$s_2 \, / \, mm$','interpreter','Latex')
           subplot(3,1,2)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).ds(2,:))
           ylabel('$v_2 \, / \, mms^{-1}$','interpreter','Latex')
           subplot(3,1,3)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).dds(2,:))
           ylabel('$a_2 \, / \, mms^{-2}$','interpreter','Latex')
           xlabel('$t \, / \, s$','interpreter','Latex')
           
           % plot Axis 3
           figure
           title('Axis 3')
           subplot(3,1,1)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).s(3,:))
           ylabel('$s_3 \, / \, mm$','interpreter','Latex')
           subplot(3,1,2)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).ds(3,:))
           ylabel('$v_3 \, / \, mms^{-1}$','interpreter','Latex')
           subplot(3,1,3)
           plot(obj.time.(Trajn),obj.TrajJ.(Jn).dds(3,:))
           ylabel('$a_3 \, / \, mms^{-2}$','interpreter','Latex')
           xlabel('$t \, / \, s$','interpreter','Latex')
           
        end
        function TrajTplot(obj,num)
            Tn = strcat('T',num);
            Trajn = strcat('Traj',num);
           % plot Axis 1
           figure
           title('Axis 1')
           subplot(3,1,1)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).p(1,:))
           ylabel('$x \, / \, mm$','interpreter','Latex')
           subplot(3,1,2)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).v(1,:))
           ylabel('$v_x \, / \, mms^{-1}$','interpreter','Latex')
           subplot(3,1,3)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).a(1,:))
           ylabel('$a_x \, / \, mms^{-2}$','interpreter','Latex')
           xlabel('$t \, / \, s$','interpreter','Latex')
           
           % plot Axis 2
           figure
           title('Axis 2')
           subplot(3,1,1)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).p(2,:))
           ylabel('$y \, / \, mm$','interpreter','Latex')
           subplot(3,1,2)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).v(2,:))
           ylabel('$v_y \, / \, mms^{-1}$','interpreter','Latex')
           subplot(3,1,3)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).a(2,:))
           ylabel('$a_y \, / \, mms^{-2}$','interpreter','Latex')
           xlabel('$t \, / \, s$','interpreter','Latex')
           
           % plot Axis 3
           figure
           title('Axis 3')
           subplot(3,1,1)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).p(3,:))
           ylabel('$z \, / \, mm$','interpreter','Latex')
           subplot(3,1,2)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).v(3,:))
           ylabel('$v_z \, / \, mms^{-1}$','interpreter','Latex')
           subplot(3,1,3)
           plot(obj.time.(Trajn),obj.TrajT.(Tn).a(3,:))
           ylabel('$a_z \, / \, mms^{-2}$','interpreter','Latex')
           xlabel('$t \, / \, s$','interpreter','Latex')
           
        end
        function SimStruct = SimData(obj,num)
            Trajn = strcat('Traj',num);
            Jn = strcat('J',num);
            % Time Structure
            SimStruct.s1.time = obj.time.(Trajn);
            SimStruct.s2.time = obj.time.(Trajn);
            SimStruct.s3.time = obj.time.(Trajn);
            SimStruct.ds1.time = obj.time.(Trajn);
            SimStruct.ds2.time = obj.time.(Trajn);
            SimStruct.ds3.time = obj.time.(Trajn);
%             SimStruct.a1.time = obj.time.(Trajn);
%             SimStruct.a2.time = obj.time.(Trajn);
%             SimStruct.a3.time = obj.time.(Trajn);
            % Data Structure
            SimStruct.s1.signals.values = obj.TrajJ.(Jn).s(1,:)';
            SimStruct.s2.signals.values = obj.TrajJ.(Jn).s(2,:)';
            SimStruct.s3.signals.values = obj.TrajJ.(Jn).s(3,:)';
            SimStruct.ds1.signals.values = obj.TrajJ.(Jn).ds(1,:)';
            SimStruct.ds2.signals.values = obj.TrajJ.(Jn).ds(2,:)';
            SimStruct.ds3.signals.values = obj.TrajJ.(Jn).ds(3,:)';
%             SimStruct.a1.signals.values = obj.TrajJ.(Jn).dds(1,:)';
%             SimStruct.a2.signals.values = obj.TrajJ.(Jn).dds(2,:)';
%             SimStruct.a3.signals.values = obj.TrajJ.(Jn).dds(3,:)';
            % Dimension Structure
            SimStruct.s1.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
            SimStruct.s2.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
            SimStruct.s3.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
            SimStruct.ds1.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
            SimStruct.ds2.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
            SimStruct.ds3.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
%             SimStruct.a1.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
%             SimStruct.a2.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
%             SimStruct.a3.signals.dimension = [1 size(obj.TrajJ.(Jn),2)];
        end   
        function plotOvertravel(obj,num)
            Tn = strcat('T',num);
            Trajn = strcat('Traj',num);
            sPlane = zeros(1,length(obj.TrajT.(Tn).p))+obj.splane;
            figure 
            plot(obj.time.(Trajn),obj.TrajT.(Tn).p(3,:))
            hold on 
            plot(obj.time.(Trajn),sPlane)
            xlabel('$t \textrm{ / } s$','interpreter','latex')
            ylabel('$z \textrm{ / } m$','interpreter','latex')
            legend('Trajectory z','Safety Plane')
        end   
        function plotRange(obj,num)
            Jn = strcat('J',num);
            Trajn = strcat('Traj',num);
            upperRange = zeros(1,length(obj.TrajJ.(Jn).s))+obj.Delta360.range;
            lowerRange = zeros(1,length(obj.TrajJ.(Jn).s));
            figure 
            subplot(3,1,1)
            plot(obj.time.(Trajn),obj.TrajJ.(Jn).s(1,:))
            hold on 
            plot(obj.time.(Trajn),upperRange,'--r',obj.time.(Trajn),lowerRange,'--r')
            ylabel('$s_1 \textrm{ / } m$','interpreter','latex')
            axis([0, obj.time.(Trajn)(end), -0.01, obj.Delta360.range+0.01]) 
            subplot(3,1,2)
            plot(obj.time.(Trajn),obj.TrajJ.(Jn).s(2,:))
            hold on 
            plot(obj.time.(Trajn),upperRange,'--r',obj.time.(Trajn),lowerRange,'--r')
            ylabel('$s_2 \textrm{ / } m$','interpreter','latex')
            axis([0, obj.time.(Trajn)(end), -0.01, obj.Delta360.range+0.01]) 
            subplot(3,1,3)
            plot(obj.time.(Trajn),obj.TrajJ.(Jn).s(3,:))
            hold on 
            plot(obj.time.(Trajn),upperRange,'--r',obj.time.(Trajn),lowerRange,'--r')
            ylabel('$s_3 \textrm{ / } m$','interpreter','latex')
            xlabel('$t \textrm{ / } s$','interpreter','latex')
            axis([0, obj.time.(Trajn)(end), -0.01, obj.Delta360.range+0.01]) 
        end
    end   
end

%         function velT = TCPspeed(obj,TrajNum)
%             TJ = obj.TrajJ.(TrajNum);
%             Delta = obj.Delta360;
%             velT = zeros(3,size(TJ,2));
%             for i = 1:length(TJ(1,:))
%                 velT(:,i) = Delta.jacobi(TJ(1:3,i)) * TJ(5:7,i);
%             end
%         end
%         function velJ = jointSpeed(obj,TrajNum)
%             TJ = obj.TrajT.(TrajNum);
%             Delta = obj.Delta360;
%             velJ = zeros(3,size(TJ,2));
%             for i = 1:length(TJ(1,:))
%                 velJ(:,i) = Delta.invJacobi(TJ(1:3,i)) * TJ(5:7,i);
%             end
%         end

%       function Traj = wait(obj,time)
%       end


%  function Japrox(obj,pos1, pos2,num)
%             
%             v = obj.vjmax;
%             a = obj.ajmax;
%             
%             S1 = obj.Delta360.inverse(pos1);
%             S2 = obj.Delta360.inverse(pos2);
%             
%             h = S2-S1;
%             
%             %% Trajectory Form Querry
%             Ta = zeros(size(S1,1),1);
%             T =  zeros(size(S1,1),1);
%             for j = 1:size(S1,1)
%                 if abs(h(j)) > abs(v^2/a)
%                     Ta(j) = v/a; %acceleration and decelaration time
%                     T(j) = (abs(h(j))*a + v^2)/(a*v); %total duration
%                 elseif abs(h(j)) <= v^2/a
%                     Ta(j) = sqrt(abs(h(j))/a); % acceleration and deceleration time
%                     T(j) = 2*Ta(j); % total duration
%                 end
%             end
%             
%             Ta_max = max(Ta);
%             T_max = max(T);
%             vj = zeros(size(S1,1),1);
%             aj = zeros(size(S1,1),1);
%             
%             
%             for j = 1:size(S1,1)
%                 if (Ta_max == Ta(j) && T_max == T(j))
%                     vj(j) = v;
%                     aj(j) = a;
%                 else
%                     vj(j) = abs(h(j)/(T_max-Ta_max));
%                     aj(j) = abs(h(j)/(Ta_max*(T_max-Ta_max)));
%                 end
%                 % Change velocity/acceleration depinding on direction
%                 if h(j) < 0
%                     vj(j) = -vj(j);
%                     aj(j) = -aj(j);
%                 end
%             end
%             
%             
%             % creation of time vector
%             int = obj.timestep;
%             t = 0:int:max(T);
%             s = zeros(length(S1),length(t)); % preallocation of q(t)
%             ds = zeros(length(S1),length(t)); %preallocation of dq(t)
%             dds = zeros(length(S1),length(t)); %preallocation of ddq(t)
%             
%             %Trajectory Joint n
%             for n = 1 : length(S1)
%                 
%                 % constant velocity YES or No
%                 if abs(h(n)) > abs(vj(n)^2/aj(n))
%                     %constant velocity interpolation
%                     for i = 1:length(t)
%                         if  t(i) <= Ta_max
%                             s(n,i) = S1(n) + 0.5 * aj(n) * t(i)^2; % acceleration phase
%                             ds(n,i) = aj(n)*t(i);
%                             dds(n,i) = aj(n);
%                         elseif Ta_max < t(i) && t(i) <= (T_max-Ta_max)
%                             s(n,i) = S1(n) + aj(n)*Ta_max * (t(i)-Ta_max/2); % constant velocity phase
%                             ds(n,i) = vj(n);
%                             dds(n,i) = 0;
%                         elseif (T_max-Ta_max) < t(i) && t(i) <= T_max
%                             s(n,i) = S2(n) - 0.5 * aj(n) * (T_max-t(i))^2; %deceleration phase
%                             ds(n,i) = vj(n)-aj(n)*(t(i)-(T_max-Ta_max));
%                             dds(n,i) = -aj(n);
%                         else
%                             s(n,i) = s(n,i-1);
%                         end
%                     end
%                 else
%                     % no constant velocity interpolation
%                     vj = aj*Ta_max; % maximum velocity peak
%                     for i = 1:length(t)
%                         if 0 <= t(i) && t(i) < Ta_max
%                             s(n,i) = S1(n) + 0.5 * aj(n) * t(i)^2; %acceleration phase
%                             ds(n,i) = aj(n)*t(i);
%                             dds(n,i) = aj(n);
%                         elseif (T(n)-Ta_max) <= t(i) && t(i) < T_max
%                             s(n,i) = S2(n) - 0.5 * aj(n) * (T_max - t(i))^2; % deceleration phase
%                             ds(n,i)= vj(n)-aj(n)*(t(i)-(T_max-Ta_max));
%                             dds(n,i) = -aj(n);
%                         else
%                             s(n,i) = s(n,i-1);
%                         end
%                     end
%                 end
%             end
%             
%             % Position, Velocity or Acceleration Struct
%             obj.TrajJ.(strcat('J',num)).s = s;
%             obj.TrajJ.(strcat('J',num)).ds = ds;
%             obj.TrajJ.(strcat('J',num)).dds = dds;
%             
%             % time of Trajectory num
%             obj.time.(strcat('Traj',num)) = t;
%             
%             % Axis Data s,ds,dds 
%             obj.Axis.(strcat('J',num)).s1 = [s(1,:);ds(1,:);dds(1,:)];
%             obj.Axis.(strcat('J',num)).s2 = [s(2,:);ds(2,:);dds(2,:)];
%             obj.Axis.(strcat('J',num)).s3 = [s(3,:);ds(3,:);dds(3,:)];
%            
%             % Cartesian Coordinates of Joint interpolation
%             pos = zeros(3,length(t));
%             vel = zeros(3,length(t));
%             for i =1:length(t)
%                 pos(:,i) = obj.Delta360.forward(s(:,i));
%                 vel(:,i) = obj.Delta360.jacobiaprox(s(:,i)) * ds(:,i);
%             end
%             acc = zeros(3,length(t));
%             % Position, Velocity or Acceleration Struct
%             obj.TrajT.(strcat('T',num)).p = pos;
%             obj.TrajT.(strcat('T',num)).v = vel;
%             obj.TrajT.(strcat('T',num)).a = acc;
%             obj.TrajT.(strcat('T',num)).time = t;
%             % Axis Data pos,vel,acc
%             obj.Axis.(strcat('T',num)).p1 = [pos(1,:);vel(1,:);acc(1,:)];
%             obj.Axis.(strcat('T',num)).p2 = [pos(2,:);vel(2,:);acc(2,:)];
%             obj.Axis.(strcat('T',num)).p3 = [pos(3,:);vel(3,:);acc(3,:)];
%             
%             
%         end
%         