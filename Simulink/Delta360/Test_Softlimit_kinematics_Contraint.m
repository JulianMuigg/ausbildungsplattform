clear all
close all
%initialize Object
Delta = Delta360(0.042,0.222106,0.400,-0.079885,0.144,'Delta');
MoveDelta = Move(0.05,0.10,0.050,0.10,0.04,Delta);

%initialize Sim 
run('Delta_Robot_V3_DataFile.m')

% Start end Position Taskspace
P_1 = [-0.07,0,0.327];
P_2 = [0.07,0,0.327];

% P2P Trajektorie
MoveDelta.J(P_1,P_2,'1')
% Positon of Softlimit transgression
for i=1: length(MoveDelta.TrajJ.J1.s(1,:))
    if MoveDelta.TrajT.T1.p(3,i) >= 0.330 
        P_3 = MoveDelta.TrajT.T1.p(:,i+1)';
        break
    end
end
% Linear Motion from Softlimit transgression to endpoint
MoveDelta.L(P_3,P_2,'2')
% plots
MoveDelta.TrajTplot('1')
MoveDelta.TrajJplot('2')
MoveDelta.TrajTplot('2')
% creation of Trajectory with Movetype switch
MoveDelta.TrajJ.Jn.s=[MoveDelta.TrajJ.J1.s(:,1:13), MoveDelta.TrajJ.J2.s(:,:)];
MoveDelta.TrajJ.Jn.ds=zeros(3,length(MoveDelta.TrajJ.Jn.s(1,:)));
% creat Simdata
MoveDelta.time.Trajn=[MoveDelta.time.Traj1(1:13)';MoveDelta.time.Traj1(13)+MoveDelta.time.Traj2'];
TJ = MoveDelta.SimData('n');
