function [mStruct] = createVelocityStruct()
%CREATEMOVESTRUCT function to create a xyz coordinate struct

mStruct = struct('dx', {0}, 'dy', {0}, 'dz', {0} );

end