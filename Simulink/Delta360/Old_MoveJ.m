function [PosVal, TimeVal] = MoveJ(Pos1, Pos2, MaxSpeed, Acceleration)

S1 = InverseKinematik_Delta360(Pos1);
S2 = InverseKinematik_Delta360(Pos2);

h = S2-S1;

%% Trajectory Form Querry
Ta = zeros(size(S1,1),1);
T =  zeros(size(S1,1),1);
for j = 1:size(S1,1)
    if abs(h(j)) > MaxSpeed^2/Acceleration
        Ta(j) = MaxSpeed/Acceleration; %acceleration and decelaration time
        T(j) = (abs(h(j))*Acceleration + MaxSpeed^2)/(Acceleration*MaxSpeed); %total duration
    elseif abs(h(j)) <= MaxSpeed^2/Acceleration
        Ta(j) = sqrt(abs(h(j))/Acceleration); % acceleration and deceleration time
        T(j) = 2*Ta(j); % total duration
    end
end

% creation of time vector
int = 0.02;
t = 0:int:max(T);
s = zeros(length(t),length(S1)); % preallocation of q(t)
ds = zeros(length(t),length(S1)); %preallocation of dq(t)
dds = zeros(length(t),length(S1)); %preallocation of ddq(t)

%Trajectory Joint n
for n = 1 : length(S1)
    % Direction of Joint Motion
    if h(n) < 0 
        MaxSpeed = -abs(MaxSpeed);
        Acceleration = -abs(Acceleration);
    end
    % constant velocity YES or No
    if abs(h(n)) > abs(MaxSpeed^2)/abs(Acceleration) 
        %constant velocity interpolation
        for i = 1:length(t) 
            if  t(i) <= Ta(n) 
                s(i,n) = S1(n) + 0.5 * Acceleration * t(i)^2; % acceleration phase 
                ds(i,n) = Acceleration*t(i);
                dds(i,n) = Acceleration;
            elseif Ta(n) < t(i) && t(i) <= (T(n)-Ta(n))
                s(i,n) = S1(n) + Acceleration*Ta(n) * (t(i)-Ta(n)/2); % constant velocity phase
                ds(i,n) = MaxSpeed;
                dds(i,n) = 0;
            elseif (T(n)-Ta(n)) < t(i) && t(i) <= T(n)
                s(i,n) = S2(n) - 0.5 * Acceleration * (T(n)-t(i))^2; %deceleration phase
                ds(i,n) = MaxSpeed-Acceleration*(t(i)-(T(n)-Ta(n)));
                dds(i,n) = -Acceleration;
            else
                s(i,n) = s(i-1,n);
            end
        end
    else
        % no constant velocity interpolation
        MaxSpeed = Acceleration*Ta(n); % maximum velocity peak
        for i = 1:length(t)
            if 0 <= t(i) && t(i) <= Ta(n)
                s(i,n) = S1(n) + 0.5 * Acceleration * t(i)^2; %acceleration phase
                ds(i,n) = Acceleration*t(i);
                dds(i,n) = Acceleration;
            elseif (T(n)-Ta(n)) < t(i) && t(i) <= T(n)
                s(i,n) = S2(n) - 0.5 * Acceleration * (T(n) - t(i))^2; % deceleration phase
                ds(i,n)= Acceleration*Ta(n)-Acceleration*(t(i)-(T(n)-Ta(n)));
                dds(i,n) = -Acceleration;
            else
                s(i,n) = s(i-1,n); 
            end
        end
    end
end

PosVal = s;
TimeVal = t;
end