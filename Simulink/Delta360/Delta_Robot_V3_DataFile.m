% Simscape(TM) Multibody(TM) version: 7.2

% This is a model data file derived from a Simscape Multibody Import XML file using the smimport function.
% The data in this file sets the block parameter values in an imported Simscape Multibody model.
% For more information on this file, see the smimport function help page in the Simscape Multibody documentation.
% You can modify numerical values, but avoid any other changes to this file.
% Do not add code to this file. Do not edit the physical units shown in comments.

%%%VariableName:smiData


%============= RigidTransform =============%

%Initialize the RigidTransform structure array by filling in null values.
smiData.RigidTransform(152).translation = [0.0 0.0 0.0];
smiData.RigidTransform(152).angle = 0.0;
smiData.RigidTransform(152).axis = [0.0 0.0 0.0];
smiData.RigidTransform(152).ID = '';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(1).translation = [0 0 0];  % mm
smiData.RigidTransform(1).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(1).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(1).ID = 'B[Fix_Platform_V2:1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(2).translation = [0 0 0];  % mm
smiData.RigidTransform(2).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(2).axis = [7.8504622934188758e-17 -7.8504622934188758e-17 1];
smiData.RigidTransform(2).ID = 'F[Fix_Platform_V2:1:-:]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(3).translation = [53.015879185632187 14.756553776852392 -53.7024402805405];  % mm
smiData.RigidTransform(3).angle = 2.6645921122411584;  % rad
smiData.RigidTransform(3).axis = [0.7700732066298529 -0.3518081384340474 0.5321825722084349];
smiData.RigidTransform(3).ID = 'B[Fix_Platform_V2:1:-:Slider:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(4).translation = [-20.000000000000426 -8.9999999999994387 207.00000000000043];  % mm
smiData.RigidTransform(4).angle = 3.1415926535897913;  % rad
smiData.RigidTransform(4).axis = [0.98127980196649078 -0.19258751323127005 1.3877787807814453e-15];
smiData.RigidTransform(4).ID = 'F[Fix_Platform_V2:1:-:Slider:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(5).translation = [-73.015879179765648 14.756553776852961 -19.06142425602286];  % mm
smiData.RigidTransform(5).angle = 2.0546312990310072;  % rad
smiData.RigidTransform(5).axis = [0.39939720224077396 -0.87424095907295607 0.27601561608256919];
smiData.RigidTransform(5).ID = 'B[Fix_Platform_V2:1:-:Slider:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(6).translation = [-20.000000000000128 -8.9999999999999716 207.00000000000003];  % mm
smiData.RigidTransform(6).angle = 3.1415926535897918;  % rad
smiData.RigidTransform(6).axis = [0.19258751287071735 -0.9812798020372534 2.4980018054066017e-16];
smiData.RigidTransform(6).ID = 'F[Fix_Platform_V2:1:-:Slider:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(7).translation = [19.999999999998149 14.756553776851625 72.764772091256546];  % mm
smiData.RigidTransform(7).angle = 1.7177715174584027;  % rad
smiData.RigidTransform(7).axis = [0.35740674433659397 -0.35740674433659381 0.86285620946101627];
smiData.RigidTransform(7).ID = 'B[Fix_Platform_V2:1:-:Slider:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(8).translation = [-20.000000000000046 -8.9999999999999716 206.99999999999883];  % mm
smiData.RigidTransform(8).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(8).axis = [0.70710678118654891 0.70710678118654624 -4.7234762030770921e-16];
smiData.RigidTransform(8).ID = 'F[Fix_Platform_V2:1:-:Slider:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(9).translation = [0 0 200.00000000000099];  % mm
smiData.RigidTransform(9).angle = 0;  % rad
smiData.RigidTransform(9).axis = [0 0 0];
smiData.RigidTransform(9).ID = 'B[Arm:1:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(10).translation = [55.208003449485361 5.9999999999999574 -16.623066958946527];  % mm
smiData.RigidTransform(10).angle = 0;  % rad
smiData.RigidTransform(10).axis = [0 0 0];
smiData.RigidTransform(10).ID = 'F[Arm:1:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(11).translation = [39.500000000000014 29.700000000000006 34.499999999999993];  % mm
smiData.RigidTransform(11).angle = 0;  % rad
smiData.RigidTransform(11).axis = [0 0 0];
smiData.RigidTransform(11).ID = 'B[Slider:3:-:Arm:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(12).translation = [3.5527136788005009e-15 -5.6843418860808015e-14 -200.00000000000097];  % mm
smiData.RigidTransform(12).angle = 0;  % rad
smiData.RigidTransform(12).axis = [0 0 0];
smiData.RigidTransform(12).ID = 'F[Slider:3:-:Arm:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(13).translation = [0 0 200.00000000000108];  % mm
smiData.RigidTransform(13).angle = 0;  % rad
smiData.RigidTransform(13).axis = [0 0 0];
smiData.RigidTransform(13).ID = 'B[Arm:2:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(14).translation = [-13.208003449485332 6.0000000000001208 -56.123066958946481];  % mm
smiData.RigidTransform(14).angle = 0;  % rad
smiData.RigidTransform(14).axis = [0 0 0];
smiData.RigidTransform(14).ID = 'F[Arm:2:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(15).translation = [-39.500000000000007 29.699999999999989 34.499999999999993];  % mm
smiData.RigidTransform(15).angle = 0;  % rad
smiData.RigidTransform(15).axis = [0 0 0];
smiData.RigidTransform(15).ID = 'B[Slider:3:-:Arm:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(16).translation = [-6.3948846218409017e-14 0 -200.00000000000094];  % mm
smiData.RigidTransform(16).angle = 0;  % rad
smiData.RigidTransform(16).axis = [0 0 0];
smiData.RigidTransform(16).ID = 'F[Slider:3:-:Arm:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(17).translation = [0 8.8817841970012523e-15 200.00000000000097];  % mm
smiData.RigidTransform(17).angle = 0;  % rad
smiData.RigidTransform(17).axis = [0 0 0];
smiData.RigidTransform(17).ID = 'B[Arm:3:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(18).translation = [-42 5.9999999999999094 -39.5];  % mm
smiData.RigidTransform(18).angle = 0;  % rad
smiData.RigidTransform(18).axis = [0 0 0];
smiData.RigidTransform(18).ID = 'F[Arm:3:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(19).translation = [39.500000000000043 29.700000000000024 34.500000000000028];  % mm
smiData.RigidTransform(19).angle = 0;  % rad
smiData.RigidTransform(19).axis = [0 0 0];
smiData.RigidTransform(19).ID = 'B[Slider:1:-:Arm:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(20).translation = [4.2632564145606011e-14 -2.6290081223123707e-13 -200.00000000000097];  % mm
smiData.RigidTransform(20).angle = 0;  % rad
smiData.RigidTransform(20).axis = [0 0 0];
smiData.RigidTransform(20).ID = 'F[Slider:1:-:Arm:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(21).translation = [0 1.7763568394002505e-14 200.00000000000097];  % mm
smiData.RigidTransform(21).angle = 0;  % rad
smiData.RigidTransform(21).axis = [0 0 0];
smiData.RigidTransform(21).ID = 'B[Arm:4:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(22).translation = [-41.999999999999936 6.0000000000000568 39.500000000000043];  % mm
smiData.RigidTransform(22).angle = 0;  % rad
smiData.RigidTransform(22).axis = [0 0 0];
smiData.RigidTransform(22).ID = 'F[Arm:4:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(23).translation = [-39.499999999999986 29.699999999999989 34.500000000000028];  % mm
smiData.RigidTransform(23).angle = 0;  % rad
smiData.RigidTransform(23).axis = [0 0 0];
smiData.RigidTransform(23).ID = 'B[Slider:1:-:Arm:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(24).translation = [-3.979039320256561e-13 1.3500311979441904e-13 -200.00000000000102];  % mm
smiData.RigidTransform(24).angle = 0;  % rad
smiData.RigidTransform(24).axis = [0 0 0];
smiData.RigidTransform(24).ID = 'F[Slider:1:-:Arm:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(25).translation = [0 1.7763568394002505e-14 200.00000000000099];  % mm
smiData.RigidTransform(25).angle = 0;  % rad
smiData.RigidTransform(25).axis = [0 0 0];
smiData.RigidTransform(25).ID = 'B[Arm:5:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(26).translation = [-13.208003449485297 6.0000000000000284 56.123066958946566];  % mm
smiData.RigidTransform(26).angle = 0;  % rad
smiData.RigidTransform(26).axis = [0 0 0];
smiData.RigidTransform(26).ID = 'F[Arm:5:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(27).translation = [39.500000000000014 29.700000000000006 34.499999999999993];  % mm
smiData.RigidTransform(27).angle = 0;  % rad
smiData.RigidTransform(27).axis = [0 0 0];
smiData.RigidTransform(27).ID = 'B[Slider:2:-:Arm:5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(28).translation = [-4.2632564145606011e-14 -1.4210854715202004e-13 -200.00000000000114];  % mm
smiData.RigidTransform(28).angle = 0;  % rad
smiData.RigidTransform(28).axis = [0 0 0];
smiData.RigidTransform(28).ID = 'F[Slider:2:-:Arm:5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(29).translation = [1.7763568394002505e-14 0 200.00000000000102];  % mm
smiData.RigidTransform(29).angle = 0;  % rad
smiData.RigidTransform(29).axis = [0 0 0];
smiData.RigidTransform(29).ID = 'B[Arm:6:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(30).translation = [55.208003449469601 5.9999999999999325 16.62306695895553];  % mm
smiData.RigidTransform(30).angle = 0;  % rad
smiData.RigidTransform(30).axis = [0 0 0];
smiData.RigidTransform(30).ID = 'F[Arm:6:-:Moving_Plattform:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(31).translation = [-39.499999999999986 29.700000000000006 34.500000000000028];  % mm
smiData.RigidTransform(31).angle = 0;  % rad
smiData.RigidTransform(31).axis = [0 0 0];
smiData.RigidTransform(31).ID = 'B[Slider:2:-:Arm:6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(32).translation = [-2.8421709430404007e-14 -1.2079226507921703e-13 -200.00000000000099];  % mm
smiData.RigidTransform(32).angle = 0;  % rad
smiData.RigidTransform(32).axis = [0 0 0];
smiData.RigidTransform(32).ID = 'F[Slider:2:-:Arm:6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(33).translation = [0 -50.000000000000036 -3.2975503118216645e-15];  % mm
smiData.RigidTransform(33).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(33).axis = [-1.1775693440128307e-16 0.70710678118654768 0.70710678118654746];
smiData.RigidTransform(33).ID = 'AssemblyGround[Fix_Platform_V2:1:ETY-DR-0011_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(34).translation = [195.00000000000006 -249.1960000000002 -112.58300001603651];  % mm
smiData.RigidTransform(34).angle = 0;  % rad
smiData.RigidTransform(34).axis = [0 0 0];
smiData.RigidTransform(34).ID = 'AssemblyGround[Fix_Platform_V2:1:ETY-DR-0008_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(35).translation = [235.0129669788692 -237.53914575050794 -158.77850813036505];  % mm
smiData.RigidTransform(35).angle = 2.3843413507899882;  % rad
smiData.RigidTransform(35).axis = [0.2573444146468013 0.10659554674767045 0.96042242875916872];
smiData.RigidTransform(35).ID = 'AssemblyGround[Fix_Platform_V2:1:ETY-DR-0013_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(36).translation = [19.999999999998067 -237.53914575050783 282.91690745989752];  % mm
smiData.RigidTransform(36).angle = 2.5935642459694801;  % rad
smiData.RigidTransform(36).axis = [-0.67859834454584689 -0.2810846377148204 0.67859834454584722];
smiData.RigidTransform(36).ID = 'AssemblyGround[Fix_Platform_V2:1:ETY-DR-0013_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(37).translation = [-255.01296706855211 -237.53914575050777 -124.1374919403487];  % mm
smiData.RigidTransform(37).angle = 2.9431758126107508;  % rad
smiData.RigidTransform(37).axis = [0.89680881774452648 0.3714703751655643 0.2402991984797696];
smiData.RigidTransform(37).ID = 'AssemblyGround[Fix_Platform_V2:1:ETY-DR-0013_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(38).translation = [-1.8564805153798047e-12 13.863953495941885 71.872171810346813];  % mm
smiData.RigidTransform(38).angle = 0.78539816339744895;  % rad
smiData.RigidTransform(38).axis = [1 1.7148656816006061e-15 -1.7212447967641015e-15];
smiData.RigidTransform(38).ID = 'AssemblyGround[Fix_Platform_V2:1:ZSX-104001-konfig_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(39).translation = [62.242864683095874 13.863953495942617 -35.935632072787627];  % mm
smiData.RigidTransform(39).angle = 2.1812305367023654;  % rad
smiData.RigidTransform(39).axis = [0.21573940505627145 0.9021230714989078 -0.37367161114452146];
smiData.RigidTransform(39).ID = 'AssemblyGround[Fix_Platform_V2:1:ZSX-104001-konfig_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(40).translation = [-62.242864661072176 13.863953495943178 -35.935632191256438];  % mm
smiData.RigidTransform(40).angle = 2.1812305358820239;  % rad
smiData.RigidTransform(40).axis = [0.21573940527225904 -0.9021230714548184 0.37367161112626229];
smiData.RigidTransform(40).ID = 'AssemblyGround[Fix_Platform_V2:1:ZSX-104001-konfig_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(41).translation = [-217.333732975103 -174.74806358114176 -125.4773867612205];  % mm
smiData.RigidTransform(41).angle = 2.1812305358820234;  % rad
smiData.RigidTransform(41).axis = [0.21573940527225968 -0.90212307145481951 0.37367161112625918];
smiData.RigidTransform(41).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104004_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(42).translation = [217.33373291570251 -174.74806358114253 -125.47738678378339];  % mm
smiData.RigidTransform(42).angle = 2.1812305367023646;  % rad
smiData.RigidTransform(42).axis = [0.21573940505627232 0.90212307149890791 -0.37367161114452091];
smiData.RigidTransform(42).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104004_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(43).translation = [-2.0174302769226246e-12 -174.74806358114245 250.95568095026562];  % mm
smiData.RigidTransform(43).angle = 0.78539816339744872;  % rad
smiData.RigidTransform(43).axis = [1 4.0642673585363055e-16 -1.9269698856348428e-15];
smiData.RigidTransform(43).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104004_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(44).translation = [-30.000000000002093 -202.6787814380109 278.88639880713419];  % mm
smiData.RigidTransform(44).angle = 3.1415926535897922;  % rad
smiData.RigidTransform(44).axis = [2.2343225744847565e-16 -0.38268343236508962 0.92387953251128696];
smiData.RigidTransform(44).ID = 'AssemblyGround[Fix_Platform_V2:1:ZTY-104004_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(45).translation = [-226.52244418508582 -202.67878143801025 -165.42350780318804];  % mm
smiData.RigidTransform(45).angle = 2.4659897971095357;  % rad
smiData.RigidTransform(45).axis = [0.84802901144343779 0.20280301033359951 -0.48960977803746492];
smiData.RigidTransform(45).ID = 'AssemblyGround[Fix_Platform_V2:1:ZTY-104004_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(46).translation = [256.52244413661276 -202.67878143801113 -113.46198363432167];  % mm
smiData.RigidTransform(46).angle = 2.4659897969251228;  % rad
smiData.RigidTransform(46).axis = [0.84802901169351752 -0.20280301018045746 0.48960977766774755];
smiData.RigidTransform(46).ID = 'AssemblyGround[Fix_Platform_V2:1:ZTY-104004_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(47).translation = [-1.9223799798363931e-12 2.0286317154938258 74.178985653629681];  % mm
smiData.RigidTransform(47).angle = 3.1415926535897922;  % rad
smiData.RigidTransform(47).axis = [-1.4191524595690586e-15 -0.92387953251128641 -0.38268343236509061];
smiData.RigidTransform(47).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104001_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(48).translation = [64.240624072131595 2.0286317154945626 -37.089038996245762];  % mm
smiData.RigidTransform(48).angle = 1.2866583601678878;  % rad
smiData.RigidTransform(48).axis = [-0.55248261519986552 -0.77007615213230751 -0.31897598627328744];
smiData.RigidTransform(48).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104001_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(49).translation = [-64.240624051156715 2.0286317154951226 -37.089039112897865];  % mm
smiData.RigidTransform(49).angle = 1.2866583608681568;  % rad
smiData.RigidTransform(49).axis = [-0.55248261479682181 0.77007615237911919 0.31897598637552327];
smiData.RigidTransform(49).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104001_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(50).translation = [-1.9405896631204397e-12 38.798184337194364 37.409433031929289];  % mm
smiData.RigidTransform(50).angle = 0.78539816339745017;  % rad
smiData.RigidTransform(50).axis = [1 1.4409442686281422e-15 -3.3429265244665288e-15];
smiData.RigidTransform(50).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104002_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(51).translation = [-32.397257394976037 38.798184337195721 -18.704262802046841];  % mm
smiData.RigidTransform(51).angle = 2.181230535882023;  % rad
smiData.RigidTransform(51).axis = [0.21573940527226088 -0.90212307145481851 0.37367161112626091];
smiData.RigidTransform(51).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104002_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(52).translation = [32.397257432668859 38.798184337195053 -18.704262656438011];  % mm
smiData.RigidTransform(52).angle = 2.1812305367023641;  % rad
smiData.RigidTransform(52).axis = [0.21573940505627187 0.90212307149890802 -0.37367161114452091];
smiData.RigidTransform(52).ID = 'AssemblyGround[Fix_Platform_V2:1:SAXT-104002_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(53).translation = [-72.000000000002075 -192.07217972021266 268.27979708933606];  % mm
smiData.RigidTransform(53).angle = 1.7177715174584005;  % rad
smiData.RigidTransform(53).axis = [-0.35740674433659309 -0.86285620946101682 0.35740674433659331];
smiData.RigidTransform(53).ID = 'AssemblyGround[Fix_Platform_V2:1:ZTY-104010_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(54).translation = [-196.33685764964812 -192.07217972021203 -196.49327390323413];  % mm
smiData.RigidTransform(54).angle = 2.6586786425867537;  % rad
smiData.RigidTransform(54).axis = [0.38068731750227797 0.91906048493743353 0.10200485929351298];
smiData.RigidTransform(54).ID = 'AssemblyGround[Fix_Platform_V2:1:ZTY-104010_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(55).translation = [268.33685763907516 -192.07217972021297 -71.785615827219829];  % mm
smiData.RigidTransform(55).angle = 0.93632438129100559;  % rad
smiData.RigidTransform(55).axis = [0.21949345510767487 0.52990407617307345 0.81916072490083203];
smiData.RigidTransform(55).ID = 'AssemblyGround[Fix_Platform_V2:1:ZTY-104010_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(56).translation = [0 0 0];  % mm
smiData.RigidTransform(56).angle = 0;  % rad
smiData.RigidTransform(56).axis = [0 0 0];
smiData.RigidTransform(56).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:1:AM8121-xx10:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(57).translation = [0 0 0];  % mm
smiData.RigidTransform(57).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(57).axis = [-0 -1 -0];
smiData.RigidTransform(57).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:1:ag2250-ple40-m02-i-1b1-f2:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(58).translation = [314.58685771192256 -192.07217972021309 8.3217339807822643];  % mm
smiData.RigidTransform(58).angle = 2.9431758122729543;  % rad
smiData.RigidTransform(58).axis = [-0.89680881765034581 -0.37147037512655257 0.24029919889156309];
smiData.RigidTransform(58).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(59).translation = [-1.7763568394002505e-14 0 0];  % mm
smiData.RigidTransform(59).angle = 0;  % rad
smiData.RigidTransform(59).axis = [0 0 0];
smiData.RigidTransform(59).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:2:AM8121-xx10:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(60).translation = [-1.7763568394002505e-14 0 0];  % mm
smiData.RigidTransform(60).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(60).axis = [-0 -1 -0];
smiData.RigidTransform(60).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:2:ag2250-ple40-m02-i-1b1-f2:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(61).translation = [-164.50000000000202 -192.0721797202126 268.27979708933617];  % mm
smiData.RigidTransform(61).angle = 2.5935642459694814;  % rad
smiData.RigidTransform(61).axis = [0.67859834454584755 0.2810846377148204 0.67859834454584655];
smiData.RigidTransform(61).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(62).translation = [0 0 3.5527136788005009e-14];  % mm
smiData.RigidTransform(62).angle = 0;  % rad
smiData.RigidTransform(62).axis = [0 0 0];
smiData.RigidTransform(62).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:3:AM8121-xx10:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(63).translation = [0 0 3.5527136788005009e-14];  % mm
smiData.RigidTransform(63).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(63).axis = [-0 -1 -0];
smiData.RigidTransform(63).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:3:ag2250-ple40-m02-i-1b1-f2:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(64).translation = [-150.08685764964557 -192.07217972021195 -276.60062375329335];  % mm
smiData.RigidTransform(64).angle = 2.3843413506930595;  % rad
smiData.RigidTransform(64).axis = [-0.25734441421508591 -0.10659554656884909 0.96042242889469354];
smiData.RigidTransform(64).ID = 'AssemblyGround[Fix_Platform_V2:1:Assambly_Motor:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(65).translation = [1.7763161818187823e-14 1.7763568394002505e-14 0];  % mm
smiData.RigidTransform(65).angle = 5.5511151231257827e-17;  % rad
smiData.RigidTransform(65).axis = [-1 0 0];
smiData.RigidTransform(65).ID = 'AssemblyGround[Slider:1:ZSY-104001_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(66).translation = [1.0379778904879428e-14 5.7000000000000384 0];  % mm
smiData.RigidTransform(66).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(66).axis = [-1 -7.2596363685286375e-17 6.4796051278854753e-17];
smiData.RigidTransform(66).ID = 'AssemblyGround[Slider:1:ZWY-104001_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(67).translation = [2.4163037835780304e-14 13.70000000000001 22.5];  % mm
smiData.RigidTransform(67).angle = 1.570796326794897;  % rad
smiData.RigidTransform(67).axis = [-1 -0 6.4796051278854741e-17];
smiData.RigidTransform(67).ID = 'AssemblyGround[Slider:1:ETY-DR-0010_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(68).translation = [-19.999999999999996 -9.0000000000000036 -35.500000000000007];  % mm
smiData.RigidTransform(68).angle = 8.5323010802195403e-17;  % rad
smiData.RigidTransform(68).axis = [-0.65060000472732382 0 0.75942059087754943];
smiData.RigidTransform(68).ID = 'AssemblyGround[Slider:1:WMA-01-10-AL_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(69).translation = [-19.999999999999989 -9.0000000000000213 35.500000000000007];  % mm
smiData.RigidTransform(69).angle = 3.2251646440124447e-16;  % rad
smiData.RigidTransform(69).axis = [-0.17211881363735931 0.98507619704877047 0];
smiData.RigidTransform(69).ID = 'AssemblyGround[Slider:1:WMA-01-10-AL_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(70).translation = [20.000000000000004 -8.9999999999999858 -35.500000000000007];  % mm
smiData.RigidTransform(70).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(70).axis = [-9.6451783097699539e-17 1 -2.7755575615628914e-17];
smiData.RigidTransform(70).ID = 'AssemblyGround[Slider:1:WMA-01-10-AL_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(71).translation = [20.000000000000036 -9.0000000000000036 35.500000000000007];  % mm
smiData.RigidTransform(71).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(71).axis = [-5.1043331388440705e-17 1 2.7755575615628926e-17];
smiData.RigidTransform(71).ID = 'AssemblyGround[Slider:1:WMA-01-10-AL_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(72).translation = [35.500000000000036 -7.2499999999999787 35.500000000000007];  % mm
smiData.RigidTransform(72).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(72).axis = [-0.57735026918962562 -0.57735026918962562 -0.57735026918962606];
smiData.RigidTransform(72).ID = 'AssemblyGround[Slider:1:WRZ-011007_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(73).translation = [35.500000000000043 -7.2499999999999964 -35.500000000000007];  % mm
smiData.RigidTransform(73).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(73).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(73).ID = 'AssemblyGround[Slider:1:WRZ-011007_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(74).translation = [-35.500000000000028 -7.2499999999999964 -35.500000000000007];  % mm
smiData.RigidTransform(74).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(74).axis = [-0.57735026918962595 0.57735026918962573 0.57735026918962562];
smiData.RigidTransform(74).ID = 'AssemblyGround[Slider:1:WRZ-011007_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(75).translation = [-35.500000000000021 -7.2500000000000142 35.500000000000007];  % mm
smiData.RigidTransform(75).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(75).axis = [-0.57735026918962562 0.57735026918962573 0.57735026918962573];
smiData.RigidTransform(75).ID = 'AssemblyGround[Slider:1:WRZ-011007_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(76).translation = [-19.999999999999989 -9.0000000000000213 35.500000000000007];  % mm
smiData.RigidTransform(76).angle = 3.2251646440124438e-16;  % rad
smiData.RigidTransform(76).axis = [-0.17211881363735937 0.98507619704877047 0];
smiData.RigidTransform(76).ID = 'AssemblyGround[Slider:1:J200UMA-01-10_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(77).translation = [-19.999999999999996 -9.0000000000000036 -35.500000000000007];  % mm
smiData.RigidTransform(77).angle = 5.5514642340122873e-16;  % rad
smiData.RigidTransform(77).axis = [-0.099993711372859687 0 -0.99498806911735438];
smiData.RigidTransform(77).ID = 'AssemblyGround[Slider:1:J200UMA-01-10_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(78).translation = [20.000000000000036 -9.0000000000000036 35.500000000000007];  % mm
smiData.RigidTransform(78).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(78).axis = [-2.9088531797897345e-16 1 -2.7755575615628914e-17];
smiData.RigidTransform(78).ID = 'AssemblyGround[Slider:1:J200UMA-01-10_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(79).translation = [20.000000000000004 -8.9999999999999858 -35.500000000000007];  % mm
smiData.RigidTransform(79).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(79).axis = [-1.2277171348545333e-16 1 -2.7755575615628914e-17];
smiData.RigidTransform(79).ID = 'AssemblyGround[Slider:1:J200UMA-01-10_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(80).translation = [12.500000000000007 29.700000000000024 34.499999999999993];  % mm
smiData.RigidTransform(80).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(80).axis = [-1 -1.3162622181048597e-16 1.2030720251011257e-16];
smiData.RigidTransform(80).ID = 'AssemblyGround[Slider:1:ETY-DR-0012_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(81).translation = [-12.499999999999993 29.700000000000024 34.500000000000028];  % mm
smiData.RigidTransform(81).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(81).axis = [0 0.70710678118654746 0.70710678118654757];
smiData.RigidTransform(81).ID = 'AssemblyGround[Slider:1:ETY-DR-0012_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(82).translation = [39.500000000000036 29.700000000000024 34.500000000000028];  % mm
smiData.RigidTransform(82).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(82).axis = [-1.3634263808257107e-16 1.847067556803493e-17 -1];
smiData.RigidTransform(82).ID = 'AssemblyGround[Slider:1:WKM-10-14_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(83).translation = [-39.499999999999986 29.699999999999989 34.500000000000028];  % mm
smiData.RigidTransform(83).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(83).axis = [1.572485536461798e-16 -3.0190815611201983e-17 1];
smiData.RigidTransform(83).ID = 'AssemblyGround[Slider:1:WKM-10-14_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(84).translation = [0 1.7763568394002505e-14 0];  % mm
smiData.RigidTransform(84).angle = 0;  % rad
smiData.RigidTransform(84).axis = [0 0 0];
smiData.RigidTransform(84).ID = 'AssemblyGround[Slider:2:ZSY-104001_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(85).translation = [3.0203839646372743e-15 5.6999999999999851 0];  % mm
smiData.RigidTransform(85).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(85).axis = [-1 -0 -5.7013111420837525e-17];
smiData.RigidTransform(85).ID = 'AssemblyGround[Slider:2:ZWY-104001_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(86).translation = [1.9182517987242909e-14 13.700000000000028 22.5];  % mm
smiData.RigidTransform(86).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(86).axis = [-1 -0 -5.4979309696759079e-17];
smiData.RigidTransform(86).ID = 'AssemblyGround[Slider:2:ETY-DR-0010_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(87).translation = [-19.999999999999986 -8.9999999999999858 -35.500000000000007];  % mm
smiData.RigidTransform(87).angle = 0;  % rad
smiData.RigidTransform(87).axis = [0 0 0];
smiData.RigidTransform(87).ID = 'AssemblyGround[Slider:2:WMA-01-10-AL_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(88).translation = [-20.000000000000004 -8.9999999999999858 35.500000000000007];  % mm
smiData.RigidTransform(88).angle = 2.7445763956537734e-16;  % rad
smiData.RigidTransform(88).axis = [0 1 0];
smiData.RigidTransform(88).ID = 'AssemblyGround[Slider:2:WMA-01-10-AL_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(89).translation = [19.999999999999993 -9.0000000000000036 -35.499999999999972];  % mm
smiData.RigidTransform(89).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(89).axis = [-2.8506555710417733e-17 1 0];
smiData.RigidTransform(89).ID = 'AssemblyGround[Slider:2:WMA-01-10-AL_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(90).translation = [20.000000000000014 -8.9999999999999947 35.500000000000043];  % mm
smiData.RigidTransform(90).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(90).axis = [-5.7129493064029405e-17 1 0];
smiData.RigidTransform(90).ID = 'AssemblyGround[Slider:2:WMA-01-10-AL_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(91).translation = [35.5 -7.2499999999999964 35.500000000000043];  % mm
smiData.RigidTransform(91).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(91).axis = [-0.57735026918962562 -0.57735026918962562 -0.57735026918962606];
smiData.RigidTransform(91).ID = 'AssemblyGround[Slider:2:WRZ-011007_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(92).translation = [35.500000000000021 -7.2499999999999787 -35.500000000000007];  % mm
smiData.RigidTransform(92).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(92).axis = [-0.57735026918962573 -0.57735026918962551 -0.57735026918962595];
smiData.RigidTransform(92).ID = 'AssemblyGround[Slider:2:WRZ-011007_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(93).translation = [-35.500000000000014 -7.2499999999999787 -35.500000000000007];  % mm
smiData.RigidTransform(93).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(93).axis = [-0.57735026918962595 0.57735026918962584 0.57735026918962551];
smiData.RigidTransform(93).ID = 'AssemblyGround[Slider:2:WRZ-011007_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(94).translation = [-35.500000000000014 -7.2500000000000053 35.500000000000007];  % mm
smiData.RigidTransform(94).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(94).axis = [-0.57735026918962573 0.57735026918962573 0.57735026918962584];
smiData.RigidTransform(94).ID = 'AssemblyGround[Slider:2:WRZ-011007_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(95).translation = [-20.000000000000004 -8.9999999999999858 35.500000000000007];  % mm
smiData.RigidTransform(95).angle = 2.7445763956537724e-16;  % rad
smiData.RigidTransform(95).axis = [0 1 0];
smiData.RigidTransform(95).ID = 'AssemblyGround[Slider:2:J200UMA-01-10_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(96).translation = [-19.999999999999986 -8.9999999999999858 -35.500000000000007];  % mm
smiData.RigidTransform(96).angle = 5.3973620404038716e-16;  % rad
smiData.RigidTransform(96).axis = [-0.1028486709168468 0 -0.99469701461834015];
smiData.RigidTransform(96).ID = 'AssemblyGround[Slider:2:J200UMA-01-10_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(97).translation = [20.000000000000004 -8.9999999999999947 35.500000000000043];  % mm
smiData.RigidTransform(97).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(97).axis = [-2.5699699308402162e-16 1 0];
smiData.RigidTransform(97).ID = 'AssemblyGround[Slider:2:J200UMA-01-10_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(98).translation = [19.999999999999993 -9.0000000000000036 -35.499999999999972];  % mm
smiData.RigidTransform(98).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(98).axis = [-1.0276117639831643e-16 1 0];
smiData.RigidTransform(98).ID = 'AssemblyGround[Slider:2:J200UMA-01-10_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(99).translation = [12.500000000000016 29.700000000000006 34.500000000000028];  % mm
smiData.RigidTransform(99).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(99).axis = [-1 -1.1604296954603485e-16 9.5869380600289875e-17];
smiData.RigidTransform(99).ID = 'AssemblyGround[Slider:2:ETY-DR-0012_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(100).translation = [-12.500000000000002 29.700000000000006 34.500000000000028];  % mm
smiData.RigidTransform(100).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(100).axis = [0 0.70710678118654746 0.70710678118654757];
smiData.RigidTransform(100).ID = 'AssemblyGround[Slider:2:ETY-DR-0012_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(101).translation = [39.500000000000014 29.700000000000006 34.499999999999993];  % mm
smiData.RigidTransform(101).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(101).axis = [-1.796814659626493e-16 -4.0358229369030199e-17 -1];
smiData.RigidTransform(101).ID = 'AssemblyGround[Slider:2:WKM-10-14_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(102).translation = [-39.499999999999986 29.700000000000006 34.500000000000028];  % mm
smiData.RigidTransform(102).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(102).axis = [5.5511151231257821e-17 1.3148012268875969e-17 1];
smiData.RigidTransform(102).ID = 'AssemblyGround[Slider:2:WKM-10-14_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(103).translation = [0 0 0];  % mm
smiData.RigidTransform(103).angle = 0;  % rad
smiData.RigidTransform(103).axis = [0 0 0];
smiData.RigidTransform(103).ID = 'AssemblyGround[Slider:3:ZSY-104001_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(104).translation = [-3.7470027081096509e-15 5.7000000000000028 0];  % mm
smiData.RigidTransform(104).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(104).axis = [-1 -0 -0];
smiData.RigidTransform(104).ID = 'AssemblyGround[Slider:3:ZWY-104001_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(105).translation = [-1.9428902930937715e-15 13.70000000000001 22.5];  % mm
smiData.RigidTransform(105).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(105).axis = [-1 -0 -0];
smiData.RigidTransform(105).ID = 'AssemblyGround[Slider:3:ETY-DR-0010_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(106).translation = [-20.000000000000004 -9.0000000000000036 -35.500000000000043];  % mm
smiData.RigidTransform(106).angle = 0;  % rad
smiData.RigidTransform(106).axis = [0 0 0];
smiData.RigidTransform(106).ID = 'AssemblyGround[Slider:3:WMA-01-10-AL_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(107).translation = [-20.000000000000007 -9.0000000000000124 35.499999999999972];  % mm
smiData.RigidTransform(107).angle = 2.7755575615630619e-16;  % rad
smiData.RigidTransform(107).axis = [0 1 0];
smiData.RigidTransform(107).ID = 'AssemblyGround[Slider:3:WMA-01-10-AL_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(108).translation = [19.999999999999989 -9.0000000000000036 -35.500000000000007];  % mm
smiData.RigidTransform(108).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(108).axis = [0 1 0];
smiData.RigidTransform(108).ID = 'AssemblyGround[Slider:3:WMA-01-10-AL_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(109).translation = [20.000000000000007 -9.0000000000000124 35.500000000000007];  % mm
smiData.RigidTransform(109).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(109).axis = [-5.2639860775507614e-17 1 0];
smiData.RigidTransform(109).ID = 'AssemblyGround[Slider:3:WMA-01-10-AL_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(110).translation = [35.500000000000014 -7.2500000000000142 35.500000000000043];  % mm
smiData.RigidTransform(110).angle = 2.0943951023931948;  % rad
smiData.RigidTransform(110).axis = [-0.57735026918962562 -0.57735026918962562 -0.57735026918962606];
smiData.RigidTransform(110).ID = 'AssemblyGround[Slider:3:WRZ-011007_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(111).translation = [35.499999999999986 -7.2499999999999964 -35.500000000000007];  % mm
smiData.RigidTransform(111).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(111).axis = [-0.57735026918962573 -0.57735026918962562 -0.57735026918962595];
smiData.RigidTransform(111).ID = 'AssemblyGround[Slider:3:WRZ-011007_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(112).translation = [-35.500000000000028 -7.2500000000000142 -35.500000000000043];  % mm
smiData.RigidTransform(112).angle = 2.0943951023931962;  % rad
smiData.RigidTransform(112).axis = [-0.57735026918962595 0.57735026918962584 0.5773502691896254];
smiData.RigidTransform(112).ID = 'AssemblyGround[Slider:3:WRZ-011007_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(113).translation = [-35.500000000000021 -7.2500000000000142 35.499999999999972];  % mm
smiData.RigidTransform(113).angle = 2.0943951023931957;  % rad
smiData.RigidTransform(113).axis = [-0.57735026918962573 0.57735026918962573 0.57735026918962562];
smiData.RigidTransform(113).ID = 'AssemblyGround[Slider:3:WRZ-011007_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(114).translation = [-20.000000000000007 -9.0000000000000124 35.499999999999972];  % mm
smiData.RigidTransform(114).angle = 2.7755575615630629e-16;  % rad
smiData.RigidTransform(114).axis = [0 1 0];
smiData.RigidTransform(114).ID = 'AssemblyGround[Slider:3:J200UMA-01-10_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(115).translation = [-20.000000000000007 -9.0000000000000036 -35.500000000000043];  % mm
smiData.RigidTransform(115).angle = 5.5006504401882702e-16;  % rad
smiData.RigidTransform(115).axis = [0 0 -1];
smiData.RigidTransform(115).ID = 'AssemblyGround[Slider:3:J200UMA-01-10_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(116).translation = [20.000000000000004 -9.0000000000000124 35.500000000000007];  % mm
smiData.RigidTransform(116).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(116).axis = [-2.6928994109791313e-16 1 0];
smiData.RigidTransform(116).ID = 'AssemblyGround[Slider:3:J200UMA-01-10_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(117).translation = [19.999999999999989 -9.0000000000000036 -35.500000000000007];  % mm
smiData.RigidTransform(117).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(117).axis = [-9.9537140639514901e-17 1 0];
smiData.RigidTransform(117).ID = 'AssemblyGround[Slider:3:J200UMA-01-10_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(118).translation = [12.500000000000007 29.699999999999989 34.499999999999993];  % mm
smiData.RigidTransform(118).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(118).axis = [-1 -8.0395436391136786e-17 8.0395436391136971e-17];
smiData.RigidTransform(118).ID = 'AssemblyGround[Slider:3:ETY-DR-0012_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(119).translation = [-12.500000000000002 29.700000000000006 34.499999999999993];  % mm
smiData.RigidTransform(119).angle = 3.1415926535897931;  % rad
smiData.RigidTransform(119).axis = [0 0.70710678118654746 0.70710678118654757];
smiData.RigidTransform(119).ID = 'AssemblyGround[Slider:3:ETY-DR-0012_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(120).translation = [39.500000000000014 29.700000000000006 34.499999999999993];  % mm
smiData.RigidTransform(120).angle = 1.5707963267948966;  % rad
smiData.RigidTransform(120).axis = [-1.7094911345080526e-16 -5.109549147422573e-17 -1];
smiData.RigidTransform(120).ID = 'AssemblyGround[Slider:3:WKM-10-14_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(121).translation = [-39.5 29.699999999999989 34.499999999999993];  % mm
smiData.RigidTransform(121).angle = 1.5707963267948968;  % rad
smiData.RigidTransform(121).axis = [1.7094911345080536e-16 -5.1095491474225823e-17 1];
smiData.RigidTransform(121).ID = 'AssemblyGround[Slider:3:WKM-10-14_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(122).translation = [0 0 0];  % mm
smiData.RigidTransform(122).angle = 0;  % rad
smiData.RigidTransform(122).axis = [0 0 0];
smiData.RigidTransform(122).ID = 'AssemblyGround[Arm:1:Rohr 8-10-adaptiv_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(123).translation = [0 -3.5527136788005009e-14 -200.00000000000108];  % mm
smiData.RigidTransform(123).angle = 1.7177715174584018;  % rad
smiData.RigidTransform(123).axis = [-0.86285620946101671 -0.35740674433659353 0.35740674433659347];
smiData.RigidTransform(123).ID = 'AssemblyGround[Arm:1:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(124).translation = [0 0 200.00000000000099];  % mm
smiData.RigidTransform(124).angle = 2.593564245969481;  % rad
smiData.RigidTransform(124).axis = [0.28108463771482001 -0.67859834454584711 -0.67859834454584711];
smiData.RigidTransform(124).ID = 'AssemblyGround[Arm:1:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(125).translation = [0 0 0];  % mm
smiData.RigidTransform(125).angle = 0;  % rad
smiData.RigidTransform(125).axis = [0 0 0];
smiData.RigidTransform(125).ID = 'AssemblyGround[Arm:2:Rohr 8-10-adaptiv_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(126).translation = [0 -3.5527136788005009e-14 -200.00000000000108];  % mm
smiData.RigidTransform(126).angle = 1.7177715174584018;  % rad
smiData.RigidTransform(126).axis = [-0.86285620946101671 -0.35740674433659364 0.35740674433659353];
smiData.RigidTransform(126).ID = 'AssemblyGround[Arm:2:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(127).translation = [0 0 200.00000000000108];  % mm
smiData.RigidTransform(127).angle = 2.593564245969481;  % rad
smiData.RigidTransform(127).axis = [0.28108463771482001 -0.67859834454584711 -0.678598344545847];
smiData.RigidTransform(127).ID = 'AssemblyGround[Arm:2:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(128).translation = [1.7763568394002505e-14 0 -4.4408920985006262e-15];  % mm
smiData.RigidTransform(128).angle = 0;  % rad
smiData.RigidTransform(128).axis = [0 0 0];
smiData.RigidTransform(128).ID = 'AssemblyGround[Arm:3:Rohr 8-10-adaptiv_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(129).translation = [3.5527136788005009e-14 -1.7763568394002505e-14 -200.00000000000099];  % mm
smiData.RigidTransform(129).angle = 1.7177715174584018;  % rad
smiData.RigidTransform(129).axis = [-0.86285620946101671 -0.35740674433659347 0.35740674433659353];
smiData.RigidTransform(129).ID = 'AssemblyGround[Arm:3:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(130).translation = [0 0 200.00000000000099];  % mm
smiData.RigidTransform(130).angle = 2.593564245969481;  % rad
smiData.RigidTransform(130).axis = [0.28108463771482006 -0.67859834454584711 -0.67859834454584711];
smiData.RigidTransform(130).ID = 'AssemblyGround[Arm:3:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(131).translation = [8.8817841970012523e-15 1.7763568394002505e-14 1.3322676295501878e-14];  % mm
smiData.RigidTransform(131).angle = 0;  % rad
smiData.RigidTransform(131).axis = [0 0 0];
smiData.RigidTransform(131).ID = 'AssemblyGround[Arm:4:Rohr 8-10-adaptiv_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(132).translation = [3.5527136788005009e-14 1.7763568394002505e-14 -200.00000000000097];  % mm
smiData.RigidTransform(132).angle = 1.7177715174584021;  % rad
smiData.RigidTransform(132).axis = [-0.8628562094610166 -0.35740674433659342 0.35740674433659342];
smiData.RigidTransform(132).ID = 'AssemblyGround[Arm:4:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(133).translation = [0 1.7763568394002505e-14 200.00000000000097];  % mm
smiData.RigidTransform(133).angle = 2.593564245969481;  % rad
smiData.RigidTransform(133).axis = [0.28108463771481995 -0.67859834454584711 -0.67859834454584711];
smiData.RigidTransform(133).ID = 'AssemblyGround[Arm:4:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(134).translation = [-1.7763568394002505e-14 0 -1.3322676295501878e-14];  % mm
smiData.RigidTransform(134).angle = 0;  % rad
smiData.RigidTransform(134).axis = [0 0 0];
smiData.RigidTransform(134).ID = 'AssemblyGround[Arm:5:Rohr 8-10-adaptiv_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(135).translation = [0 -1.7763568394002505e-14 -200.00000000000099];  % mm
smiData.RigidTransform(135).angle = 1.7177715174584018;  % rad
smiData.RigidTransform(135).axis = [-0.86285620946101671 -0.35740674433659347 0.35740674433659347];
smiData.RigidTransform(135).ID = 'AssemblyGround[Arm:5:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(136).translation = [0 1.7763568394002505e-14 200.00000000000099];  % mm
smiData.RigidTransform(136).angle = 2.593564245969481;  % rad
smiData.RigidTransform(136).axis = [0.28108463771482001 -0.67859834454584711 -0.67859834454584711];
smiData.RigidTransform(136).ID = 'AssemblyGround[Arm:5:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(137).translation = [1.7763568394002505e-14 1.7763568394002505e-14 -4.4408920985006262e-15];  % mm
smiData.RigidTransform(137).angle = 0;  % rad
smiData.RigidTransform(137).axis = [0 0 0];
smiData.RigidTransform(137).ID = 'AssemblyGround[Arm:6:Rohr 8-10-adaptiv_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(138).translation = [1.7763568394002505e-14 5.3290705182007514e-14 -200.00000000000108];  % mm
smiData.RigidTransform(138).angle = 1.7177715174584016;  % rad
smiData.RigidTransform(138).axis = [-0.86285620946101671 -0.35740674433659353 0.3574067443365937];
smiData.RigidTransform(138).ID = 'AssemblyGround[Arm:6:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(139).translation = [1.7763568394002505e-14 0 200.00000000000102];  % mm
smiData.RigidTransform(139).angle = 2.593564245969481;  % rad
smiData.RigidTransform(139).axis = [0.28108463771482001 -0.67859834454584711 -0.678598344545847];
smiData.RigidTransform(139).ID = 'AssemblyGround[Arm:6:_D_KARM-10 CL KDGM Kopfteil_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(140).translation = [0 0 0];  % mm
smiData.RigidTransform(140).angle = 0;  % rad
smiData.RigidTransform(140).axis = [0 0 0];
smiData.RigidTransform(140).ID = 'AssemblyGround[Moving_Plattform:1:ETY-DR-0009_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(141).translation = [10.17468245269454 5.9999999999999787 -42.623066958946481];  % mm
smiData.RigidTransform(141).angle = 2.7734925708571123;  % rad
smiData.RigidTransform(141).axis = [0.18615678789738557 0.6947465906068655 -0.69474659060686594];
smiData.RigidTransform(141).ID = 'AssemblyGround[Moving_Plattform:1:ETY-DR-0012_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(142).translation = [31.825317547305517 6.0000000000000142 -30.123066958946502];  % mm
smiData.RigidTransform(142).angle = 1.6378338249998232;  % rad
smiData.RigidTransform(142).axis = [0.93511312653102951 -0.25056280708573131 0.25056280708573142];
smiData.RigidTransform(142).ID = 'AssemblyGround[Moving_Plattform:1:ETY-DR-0012_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(143).translation = [31.825317547305559 6.0000000000000142 30.123066958946474];  % mm
smiData.RigidTransform(143).angle = 1.6378338249998232;  % rad
smiData.RigidTransform(143).axis = [-0.93511312653102951 0.25056280708573125 0.25056280708573159];
smiData.RigidTransform(143).ID = 'AssemblyGround[Moving_Plattform:1:ETY-DR-0012_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(144).translation = [-42.000000000000014 6.0000000000000142 -12.500000000000007];  % mm
smiData.RigidTransform(144).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(144).axis = [-0.57735026918962573 0.57735026918962573 0.57735026918962584];
smiData.RigidTransform(144).ID = 'AssemblyGround[Moving_Plattform:1:ETY-DR-0012_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(145).translation = [10.174682452694556 6.0000000000000142 42.623066958946495];  % mm
smiData.RigidTransform(145).angle = 2.773492570857111;  % rad
smiData.RigidTransform(145).axis = [-0.18615678789738449 -0.69474659060686605 -0.69474659060686572];
smiData.RigidTransform(145).ID = 'AssemblyGround[Moving_Plattform:1:ETY-DR-0012_Default_sldprt:5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(146).translation = [-42 6.0000000000000142 12.5];  % mm
smiData.RigidTransform(146).angle = 2.0943951023931953;  % rad
smiData.RigidTransform(146).axis = [-0.57735026918962584 -0.57735026918962584 -0.57735026918962584];
smiData.RigidTransform(146).ID = 'AssemblyGround[Moving_Plattform:1:ETY-DR-0012_Default_sldprt:6]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(147).translation = [-42 6.0000000000000142 -39.500000000000007];  % mm
smiData.RigidTransform(147).angle = 1.7482517736842615;  % rad
smiData.RigidTransform(147).axis = [-0.83661256156122665 -0.38734959780407857 0.38734959780407852];
smiData.RigidTransform(147).ID = 'AssemblyGround[Moving_Plattform:1:WKM-10-14_Default_sldprt:1]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(148).translation = [-42 6.0000000000000142 39.5];  % mm
smiData.RigidTransform(148).angle = 1.7409383717915548;  % rad
smiData.RigidTransform(148).axis = [0.84284776553694796 -0.38053097385717094 -0.38053097385717083];
smiData.RigidTransform(148).ID = 'AssemblyGround[Moving_Plattform:1:WKM-10-14_Default_sldprt:2]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(149).translation = [-13.208003449485364 6.0000000000000142 56.123066958946552];  % mm
smiData.RigidTransform(149).angle = 1.5747774339307925;  % rad
smiData.RigidTransform(149).axis = [0.44347916356600381 0.062970710144424694 0.89406986368331653];
smiData.RigidTransform(149).ID = 'AssemblyGround[Moving_Plattform:1:WKM-10-14_Default_sldprt:3]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(150).translation = [55.208003449485361 6.0000000000000142 -16.623066958946509];  % mm
smiData.RigidTransform(150).angle = 2.7304028547788231;  % rad
smiData.RigidTransform(150).axis = [0.49463776334228199 -0.6915599894370591 0.52638224142305501];
smiData.RigidTransform(150).ID = 'AssemblyGround[Moving_Plattform:1:WKM-10-14_Default_sldprt:4]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(151).translation = [-13.20800344948533 6.0000000000000142 -56.123066958946509];  % mm
smiData.RigidTransform(151).angle = 1.5753016853268118;  % rad
smiData.RigidTransform(151).axis = [0.43975365460573707 -0.066971159552257631 -0.89561799169574763];
smiData.RigidTransform(151).ID = 'AssemblyGround[Moving_Plattform:1:WKM-10-14_Default_sldprt:5]';

%Translation Method - Cartesian
%Rotation Method - Arbitrary Axis
smiData.RigidTransform(152).translation = [55.208003449469665 6.0000000000000142 16.623066958955587];  % mm
smiData.RigidTransform(152).angle = 3.0795366540095781;  % rad
smiData.RigidTransform(152).axis = [-0.6275963790391853 -0.70676610168559173 0.32650338822905806];
smiData.RigidTransform(152).ID = 'AssemblyGround[Moving_Plattform:1:WKM-10-14_Default_sldprt:6]';


%============= Solid =============%
%Center of Mass (CoM) %Moments of Inertia (MoI) %Product of Inertia (PoI)

%Initialize the Solid structure array by filling in null values.
smiData.Solid(22).mass = 0.0;
smiData.Solid(22).CoM = [0.0 0.0 0.0];
smiData.Solid(22).MoI = [0.0 0.0 0.0];
smiData.Solid(22).PoI = [0.0 0.0 0.0];
smiData.Solid(22).color = [0.0 0.0 0.0];
smiData.Solid(22).opacity = 0.0;
smiData.Solid(22).ID = '';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(1).mass = 0.21394462375453638;  % kg
smiData.Solid(1).CoM = [3.1030491540730857e-10 -0.0041436701800086923 30.799738095945056];  % mm
smiData.Solid(1).MoI = [214.16674269275359 214.2004332863097 386.80012692396605];  % kg*mm^2
smiData.Solid(1).PoI = [0.015552333221399917 -4.1811058966926626e-10 1.8380542562670413e-09];  % kg*mm^2
smiData.Solid(1).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(1).opacity = 1;
smiData.Solid(1).ID = 'ETY-DR-0011_Default_sldprt.ipt_{9137274B-416C-693F-2500-5FB8E82661B4}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(2).mass = 0.35526942635150571;  % kg
smiData.Solid(2).CoM = [-195.00000862312564 3.0050662750528212 112.65174496171173];  % mm
smiData.Solid(2).MoI = [6206.4728224317196 12447.597024676423 6243.2521052220054];  % kg*mm^2
smiData.Solid(2).PoI = [0.00012318911774400476 0.00076885342714376752 -1.5516843632212841e-08];  % kg*mm^2
smiData.Solid(2).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(2).opacity = 1;
smiData.Solid(2).ID = 'ETY-DR-0008_Default_sldprt.ipt_{A2B5D828-44D2-6531-8DE6-F7A0096587EA}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(3).mass = 0.14101552979977047;  % kg
smiData.Solid(3).CoM = [50.539659787875337 20.753140238728164 20.000000000000004];  % mm
smiData.Solid(3).MoI = [46.511470683001015 104.75748493773852 112.88605271224024];  % kg*mm^2
smiData.Solid(3).PoI = [0 0 -16.412711579733834];  % kg*mm^2
smiData.Solid(3).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(3).opacity = 1;
smiData.Solid(3).ID = 'ETY-DR-0013_Default_sldprt.ipt_{295FBFBC-4BC9-F593-FD1E-F39216870F88}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(4).mass = 0.13043735748350449;  % kg
smiData.Solid(4).CoM = [-0.0025532128999953634 -11.590640346444438 134.96197360208689];  % mm
smiData.Solid(4).MoI = [807.51335175468841 819.07958326958556 41.349089776330899];  % kg*mm^2
smiData.Solid(4).PoI = [-0.077342245550448746 1.2663384110295217e-05 -0.0055016299260007501];  % kg*mm^2
smiData.Solid(4).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(4).opacity = 1;
smiData.Solid(4).ID = 'ZSX-104001-konfig_Default_sldprt.ipt_{B3887833-4FBE-C7F7-252F-DB8FB656C40D}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(5).mass = 0.056432052746126532;  % kg
smiData.Solid(5).CoM = [0.34364449686216597 0.27732714399858738 23.128581189013218];  % mm
smiData.Solid(5).MoI = [25.830220932340964 41.370004495860471 42.731981564136937];  % kg*mm^2
smiData.Solid(5).PoI = [0.84684287861644736 -0.23231690886953665 0.0053647161082034335];  % kg*mm^2
smiData.Solid(5).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(5).opacity = 1;
smiData.Solid(5).ID = 'SAXT-104004_Default_sldprt.ipt_{16A5ED28-45EC-ED58-E4D4-25A323EE4A35}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(6).mass = 0.081692402562356478;  % kg
smiData.Solid(6).CoM = [-13.988743938701466 9.9910588934326153e-08 17.901322255162285];  % mm
smiData.Solid(6).MoI = [15.234937020808429 82.377881043951803 91.722138246560974];  % kg*mm^2
smiData.Solid(6).PoI = [-7.4849609967594518e-08 -1.6398676841421946 1.0744114724538401e-07];  % kg*mm^2
smiData.Solid(6).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(6).opacity = 1;
smiData.Solid(6).ID = 'ZTY-104004_Default_sldprt.ipt_{EBB5B103-42DC-DA64-AD59-9F94DB7F8D38}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(7).mass = 0.057416431418494104;  % kg
smiData.Solid(7).CoM = [9.7001441868681779e-06 0.22137386374286164 23.090518098294936];  % mm
smiData.Solid(7).MoI = [26.107838670683552 42.813927713532934 43.977611470635182];  % kg*mm^2
smiData.Solid(7).PoI = [0.82776515660199523 3.4769517660806914e-06 5.5373870997487439e-05];  % kg*mm^2
smiData.Solid(7).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(7).opacity = 1;
smiData.Solid(7).ID = 'SAXT-104001_Default_sldprt.ipt_{1C0686F1-4684-4BCA-211C-E49EE34D3A89}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(8).mass = 0.0074896923933980191;  % kg
smiData.Solid(8).CoM = [-6.8297740564533186e-06 0.24151289765136832 2.3215731039679026];  % mm
smiData.Solid(8).MoI = [1.2338412212621255 4.2365138870282752 5.2493101929995571];  % kg*mm^2
smiData.Solid(8).PoI = [0.0023712909718148407 1.3707295066094545e-07 1.6840121266218421e-06];  % kg*mm^2
smiData.Solid(8).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(8).opacity = 1;
smiData.Solid(8).ID = 'SAXT-104002_Default_sldprt.ipt_{BBD24CC9-41E6-059B-666B-5CBF56C81B36}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(9).mass = 0.03417974762973594;  % kg
smiData.Solid(9).CoM = [1.5723524627547441e-10 -6.7106804574244272 5.3091946459844328];  % mm
smiData.Solid(9).MoI = [14.984131160242359 10.029298605202438 24.479889150504171];  % kg*mm^2
smiData.Solid(9).PoI = [-0.038955644989490779 2.1581758335548683e-11 -6.6368175103388722e-11];  % kg*mm^2
smiData.Solid(9).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(9).opacity = 1;
smiData.Solid(9).ID = 'ZTY-104010_Default_sldprt.ipt_{8BDC050F-4AFB-F8DE-5032-DB91289663EC}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(10).mass = 0.35194570776724959;  % kg
smiData.Solid(10).CoM = [-0.00016056174538037587 0.25511477340715161 -5.6681559656108718];  % cm
smiData.Solid(10).MoI = [4.805022690495476 4.4456211049691188 1.9449956901521575];  % kg*cm^2
smiData.Solid(10).PoI = [0.1533656757509741 -0.00030566784129178973 -0.00012215197362634391];  % kg*cm^2
smiData.Solid(10).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(10).opacity = 1;
smiData.Solid(10).ID = 'AM8121-xx10.ipt_{CB93E626-45A6-D4C1-55F3-36BF6C2699E4}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(11).mass = 0.15765839265532411;  % kg
smiData.Solid(11).CoM = [3.2800052119705607 -0.0011259643047391822 0.0010236883714882946];  % cm
smiData.Solid(11).MoI = [0.71415864473448509 1.2404310542561969 1.2406911678387915];  % kg*cm^2
smiData.Solid(11).PoI = [-0.00011699329705913132 0.00031085447845947435 -4.1575096717124782e-05];  % kg*cm^2
smiData.Solid(11).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(11).opacity = 1;
smiData.Solid(11).ID = 'ag2250-ple40-m02-i-1b1-f2.ipt_{ACE1919F-474B-2CBE-1D75-2BB62B319A9B}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(12).mass = 0.039282055552561722;  % kg
smiData.Solid(12).CoM = [0.00024747263812255864 2.7510078238942088 -0.0005271966419554807];  % mm
smiData.Solid(12).MoI = [32.531600898690463 49.006800227735738 16.703539056334296];  % kg*mm^2
smiData.Solid(12).PoI = [-2.8404816473210324e-07 0.00070239027300053147 3.0334938785084524e-08];  % kg*mm^2
smiData.Solid(12).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(12).opacity = 1;
smiData.Solid(12).ID = 'ZSY-104001_Default_sldprt.ipt_{B3DBB1F2-4A92-2A40-0F7D-9CB8B2747E9E}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(13).mass = 0.050344480484908381;  % kg
smiData.Solid(13).CoM = [2.0374418078477611e-11 0.0032885816131449294 3.985256374440187];  % mm
smiData.Solid(13).MoI = [40.028069275372289 20.938167829444691 60.427539935401633];  % kg*mm^2
smiData.Solid(13).PoI = [-2.4409831448402726e-06 -1.8423548182924397e-12 -6.9315238729808923e-12];  % kg*mm^2
smiData.Solid(13).color = [0.41176470588235292 0.41176470588235292 0.41176470588235292];
smiData.Solid(13).opacity = 1;
smiData.Solid(13).ID = 'ZWY-104001_Default_sldprt.ipt_{FAB50681-4055-D7AB-5362-72939952815D}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(14).mass = 0.041476367815596202;  % kg
smiData.Solid(14).CoM = [1.9060247785473155e-11 -5.6799208159304495 11.492559126871111];  % mm
smiData.Solid(14).MoI = [7.7420734025206288 10.693306921932527 14.441121282576402];  % kg*mm^2
smiData.Solid(14).PoI = [-0.090868210643768071 -9.8547862957639165e-12 1.517756238925172e-11];  % kg*mm^2
smiData.Solid(14).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(14).opacity = 1;
smiData.Solid(14).ID = 'ETY-DR-0010_Default_sldprt.ipt_{2DA03336-422A-37AD-351B-45B477C76B1F}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(15).mass = 0.0055612572254856727;  % kg
smiData.Solid(15).CoM = [-4.7697003035390768 2.2034386879030095 7.2645467245322545e-05];  % mm
smiData.Solid(15).MoI = [0.51080456729057544 0.74382965014766278 0.51723953319102356];  % kg*mm^2
smiData.Solid(15).PoI = [-1.5084472494730779e-06 1.3715988250529593e-06 -0.071987067442968525];  % kg*mm^2
smiData.Solid(15).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(15).opacity = 1;
smiData.Solid(15).ID = 'WMA-01-10-AL_Default_sldprt.ipt_{12DF5051-4ED3-8B41-BBD8-ABA226FD6822}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(16).mass = 0.00061306724555182802;  % kg
smiData.Solid(16).CoM = [0.0012946763627019083 -1.4188170704353051 0.67237059615234784];  % mm
smiData.Solid(16).MoI = [0.0095397010816251921 0.051359300791549004 0.051203263979271808];  % kg*mm^2
smiData.Solid(16).PoI = [0.0012827951008857359 9.2422597414507162e-07 -1.9992445847076056e-06];  % kg*mm^2
smiData.Solid(16).color = [0.41176470588235292 0.41176470588235292 0.41176470588235292];
smiData.Solid(16).opacity = 1;
smiData.Solid(16).ID = 'WRZ-011007_Default_sldprt.ipt_{FCC394C8-402F-AFCA-A523-2B978823B615}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(17).mass = 0.0013207621034308315;  % kg
smiData.Solid(17).CoM = [-1.4556226348338519 1.8006989577915449 0];  % mm
smiData.Solid(17).MoI = [0.10401742207789197 0.10987476549501043 0.041878569452981212];  % kg*mm^2
smiData.Solid(17).PoI = [0 0 -0.0019347224286448701];  % kg*mm^2
smiData.Solid(17).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(17).opacity = 1;
smiData.Solid(17).ID = 'J200UMA-01-10_Default_sldprt.ipt_{6183AC1E-4022-72FA-A7A6-99866D045244}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(18).mass = 0.0021208928207807308;  % kg
smiData.Solid(18).CoM = [17.247606935928744 -3.7811022847994627e-10 1.2416382611064348e-10];  % mm
smiData.Solid(18).MoI = [0.02820073122430454 0.18889266697174395 0.19247703712822242];  % kg*mm^2
smiData.Solid(18).PoI = [0 -2.3209434726471449e-12 4.2768451766556609e-12];  % kg*mm^2
smiData.Solid(18).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(18).opacity = 1;
smiData.Solid(18).ID = 'ETY-DR-0012_Default_sldprt.ipt_{BA04AC76-4CA1-5E2A-FE50-F29A67A5C567}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(19).mass = 0.002150641734441214;  % kg
smiData.Solid(19).CoM = [1.134115259222999e-09 0.0026490295994350799 2.6832329792832774e-10];  % mm
smiData.Solid(19).MoI = [0.081254636769158783 0.11108026412865908 0.081254633711615523];  % kg*mm^2
smiData.Solid(19).PoI = [0 0 0];  % kg*mm^2
smiData.Solid(19).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(19).opacity = 1;
smiData.Solid(19).ID = 'WKM-10-14_Default_sldprt.ipt_{6E4856FA-4717-0FE6-CB0B-E694AED11B5F}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(20).mass = 0.010123073537128942;  % kg
smiData.Solid(20).CoM = [0.0010745904769237707 -0.0022243621469029383 -0.07404378622115565];  % mm
smiData.Solid(20).MoI = [108.37820344695555 108.37819406168717 0.18527345732422409];  % kg*mm^2
smiData.Solid(20).PoI = [-0.001698761312029225 0.002299381727434636 4.337455368687375e-05];  % kg*mm^2
smiData.Solid(20).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(20).opacity = 1;
smiData.Solid(20).ID = 'Rohr 8-10-adaptiv_Default_sldprt.ipt_{76344D1A-4953-7164-75E8-C4B68441EC78}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(21).mass = 0.0059482243567816496;  % kg
smiData.Solid(21).CoM = [-0.00042279719651739246 -9.594340175810558 -0.00032354040557492649];  % mm
smiData.Solid(21).MoI = [1.5768136740085772 0.35644070735389732 1.8538707749241796];  % kg*mm^2
smiData.Solid(21).PoI = [-1.3794532313481124e-05 2.86166096362756e-06 -1.5985832749263874e-05];  % kg*mm^2
smiData.Solid(21).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(21).opacity = 1;
smiData.Solid(21).ID = '_D_KARM-10 CL KDGM Kopfteil_Default_sldprt.ipt_{891982B5-4292-F0CE-1E63-93AFBACB649A}';

%Inertia Type - Custom
%Visual Properties - Simple
smiData.Solid(22).mass = 0.052198871807768554;  % kg
smiData.Solid(22).CoM = [-0.012170614720373629 6.0020224775080768 0.0098026100372342086];  % mm
smiData.Solid(22).MoI = [28.852357613250934 56.404562458709492 28.84501084764694];  % kg*mm^2
smiData.Solid(22).PoI = [0.0028441362323643727 -0.017224758899866216 -0.0035322457944023345];  % kg*mm^2
smiData.Solid(22).color = [0.74901960784313726 0.74901960784313726 0.74901960784313726];
smiData.Solid(22).opacity = 1;
smiData.Solid(22).ID = 'ETY-DR-0009_Default_sldprt.ipt_{ECE69A2F-4C88-D368-9C2D-7284E50DC4AB}';


%============= Joint =============%
%X Revolute Primitive (Rx) %Y Revolute Primitive (Ry) %Z Revolute Primitive (Rz)
%X Prismatic Primitive (Px) %Y Prismatic Primitive (Py) %Z Prismatic Primitive (Pz) %Spherical Primitive (S)
%Constant Velocity Primitive (CV) %Lead Screw Primitive (LS)
%Position Target (Pos)

%Initialize the PrismaticJoint structure array by filling in null values.
smiData.PrismaticJoint(3).Pz.Pos = 0.0;
smiData.PrismaticJoint(3).ID = '';

smiData.PrismaticJoint(1).Pz.Pos = 0;  % m
smiData.PrismaticJoint(1).ID = '[Fix_Platform_V2:1:-:Slider:1]';

smiData.PrismaticJoint(2).Pz.Pos = 0;  % m
smiData.PrismaticJoint(2).ID = '[Fix_Platform_V2:1:-:Slider:2]';

smiData.PrismaticJoint(3).Pz.Pos = 0;  % m
smiData.PrismaticJoint(3).ID = '[Fix_Platform_V2:1:-:Slider:3]';


%Initialize the SphericalJoint structure array by filling in null values.
smiData.SphericalJoint(12).S.Pos.Angle = 0.0;
smiData.SphericalJoint(12).S.Pos.Axis = [0.0 0.0 0.0];
smiData.SphericalJoint(12).ID = '';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(1).S.Pos.Angle = 160.68028999517279;  % deg
smiData.SphericalJoint(1).S.Pos.Axis = [-0.02482267641582226 0.55968864273483898 0.82833112819040511];
smiData.SphericalJoint(1).ID = '[Arm:1:-:Moving_Plattform:1]';

smiData.SphericalJoint(2).S.Pos.Angle = 138.59554201242534;  % deg
smiData.SphericalJoint(2).S.Pos.Axis = [-0.060665147664043427 -0.1582056818108403 -0.98554081706526331];
smiData.SphericalJoint(2).ID = '[Slider:3:-:Arm:1]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(3).S.Pos.Angle = 73.67131582162645;  % deg
smiData.SphericalJoint(3).S.Pos.Axis = [0.92061064686203897 -0.033678153923061019 -0.38902675850459517];
smiData.SphericalJoint(3).ID = '[Arm:2:-:Moving_Plattform:1]';

smiData.SphericalJoint(4).S.Pos.Angle = 54.17131703461677;  % deg
smiData.SphericalJoint(4).S.Pos.Axis = [-0.31389052723863564 0.15048700808010507 0.93745741093073121];
smiData.SphericalJoint(4).ID = '[Slider:3:-:Arm:2]';

smiData.SphericalJoint(5).S.Pos.Angle = 135.39911371592871;  % deg
smiData.SphericalJoint(5).S.Pos.Axis = [0.075837355827953604 -0.76049698486625639 -0.64489769070012759];
smiData.SphericalJoint(5).ID = '[Arm:3:-:Moving_Plattform:1]';

smiData.SphericalJoint(6).S.Pos.Angle = 54.754284288685376;  % deg
smiData.SphericalJoint(6).S.Pos.Axis = [-0.30999015196485757 0.15068997747138532 0.93872180989602971];
smiData.SphericalJoint(6).ID = '[Slider:1:-:Arm:3]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(7).S.Pos.Angle = 129.27508663821831;  % deg
smiData.SphericalJoint(7).S.Pos.Axis = [0.14292141189480387 -0.76936773877028408 -0.62261284323526112];
smiData.SphericalJoint(7).ID = '[Arm:4:-:Moving_Plattform:1]';

smiData.SphericalJoint(8).S.Pos.Angle = 45.800279546341429;  % deg
smiData.SphericalJoint(8).S.Pos.Axis = [-0.38001749656052941 0.14660700470259747 0.91328696940227971];
smiData.SphericalJoint(8).ID = '[Slider:1:-:Arm:4]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(9).S.Pos.Angle = 137.10746057146147;  % deg
smiData.SphericalJoint(9).S.Pos.Axis = [0.41082932701770469 0.79588462983462394 0.44473241399203556];
smiData.SphericalJoint(9).ID = '[Arm:5:-:Moving_Plattform:1]';

smiData.SphericalJoint(10).S.Pos.Angle = 40.104022324972675;  % deg
smiData.SphericalJoint(10).S.Pos.Axis = [-0.439801055489912 0.1423459437417304 0.88674272700160084];
smiData.SphericalJoint(10).ID = '[Slider:2:-:Arm:5]';

%This joint has been chosen as a cut joint. Simscape Multibody treats cut joints as algebraic constraints to solve closed kinematic loops. The imported model does not use the state target data for this joint.
smiData.SphericalJoint(11).S.Pos.Angle = 129.85492867309372;  % deg
smiData.SphericalJoint(11).S.Pos.Axis = [0.54044727640220469 0.7449858979397026 0.39104060313529315];
smiData.SphericalJoint(11).ID = '[Arm:6:-:Moving_Plattform:1]';

smiData.SphericalJoint(12).S.Pos.Angle = 56.006740072829608;  % deg
smiData.SphericalJoint(12).S.Pos.Axis = [-0.30186410612338599 0.15110384634785251 0.94129999949708587];
smiData.SphericalJoint(12).ID = '[Slider:2:-:Arm:6]';

