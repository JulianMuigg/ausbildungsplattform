function plotAxisData(inter_data,sim_data,time,varargin)
% plots a graph with 3 windows of position, velocity or acceleration
% of all 3 axis in Joint or Task Space 
s1 = '$s_1 \, / \,mm$';
s2 = '$s_2 \, / \,mm$';
s3 = '$s_3 \, / \,mm$';
ds1 = '$ds_1 \, / \,mm\,s^{-1}$';
ds2 = '$ds_2 \, / \,mm\,s^{-1}$';
ds3 = '$ds_3 \, / \,mm\,s^{-1}$';
dds1 = '$dds_1 \, / \,mm\,s^{-2}$';
dds2 = '$dds_2 \, / \,mm\,s^{-2}$';
dds3 = '$dds_3 \, / \,mm\,s^{-2}$';
x = '$x \, / \,mm$';
y = '$y \, / \,mm$';
z = '$z \, / \,mm$';
vx = '$vx \, / \,mm\,s^{-1}$';
vy = '$vy \, / \,mm\,s^{-1}$';
vz = '$vz \, / \,mm\,s^{-1}$';
ax = '$ax \, / \,mm\,s^{-2}$';
ay = '$ay \, / \,mm\,s^{-2}$';
az = '$az \, / \,mm\,s^{-2}$';

switch char(varargin)
    case 's1'
        name1 = s1;
        name2 = ds1;
        name3 = dds1;
        
    case 's2'
        name1 = s2;
        name2 = ds2;
        name3 = dds2;
        
    case 's3' 
        name1 = s3;
        name2 = ds3;
        name3 = dds3;
        
    case 'x'
        name1 = x;
        name2 = vx;
        name3 = ax;
        
    case 'y' 
        name1 = y;
        name2 = vy;
        name3 = ay;
        
    case 'z' 
        name1 = z;
        name2 = vz;
        name3 = az;
        
    otherwise
        error('Invalid optional argument')
end

figure
subplot(3,1,1)
plot(time,inter_data(1,:))
hold on 
plot(time,sim_data(1,:))
ylabel(name1,'interpreter','Latex')
subplot(3,1,2)
plot(time,inter_data(2,:))
hold on 
plot(time,sim_data(2,:))
ylabel(name2,'interpreter','Latex')
subplot(3,1,3)
plot(time,inter_data(3,:))
hold on 
plot(time,sim_data(3,:))
ylabel(name3,'interpreter','Latex')
xlabel('$t \, / \, s$','interpreter','Latex')
legend('interpolated','simulated','location','northeast')