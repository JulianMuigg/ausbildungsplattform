function [mStruct] = createTrajectoryDeltaStruct()

%CREATEMOVESTRUCT function to create a move struct
% q_start...trajectory start point 
% q_stop...trajectory stop point
%dq_start...starting speed
%dq_stop...stop speed
%dq_max...max speed
%ddq_max...max acceleration
%tolerance...acceptable radius for endpoint being reached
%oldq... old trajectory value

mStruct = struct('q_start', {createPositionStruct()}, 'q_stop', {createPositionStruct()}, ...
    'dq_start', {createVelocityStruct()}, 'dq_stop', {createVelocityStruct()}, 'dq_max', {0},'ddq_max', {0},'tolerance',{0},'oldq',{0},...
    'dq_max_x',{0},'ddq_max_x',{0},'dq_max_y',{0},'ddq_max_y',{0},'dq_max_z',{0},'ddq_max_z',{0} );