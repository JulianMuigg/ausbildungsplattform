syms z0 s1 s2 s3 l delta0 delta1 delta2 delta3 mu1 mu2 mu3 

x = 0;
y = 0;
z = 0.2773;

lyz1 = sqrt((l* sin(delta1))^2 + (l * cos(mu1))^2);
lxz1 = sqrt((l* sin(mu1))^2 + (l * cos(delta1))^2);
lyz2 = sqrt((l* sin(delta2))^2 + (l * cos(mu2))^2);
lxz2 = sqrt((l* sin(mu2))^2 + (l * cos(delta2))^2);
lyz3 = sqrt((l* sin(delta3))^2 + (l * cos(mu3))^2);
lxz3 = sqrt((l* sin(mu3))^2 + (l * cos(delta3))^2);

% Rotationsvektor für Leg 2 und 3
R120 = [cosd(120) -sind(120) 0;
        sind(120) cosd(120) 0;
        0 0 1];
R_120 = [cosd(-120) -sind(-120) 0;
        sind(-120) cosd(120) 0;
        0 0 1];

% nicht Rotierter Positionsvektor
Ps2 =  [lxz2*sin(mu2);
        lyz2*(sin(delta0)-sin(delta2))+s2/sqrt(2);
        z0 + s2/sqrt(2) - lyz2*(cos(delta0)-cos(delta2))-lxz2*(1-cos(mu2))];

Ps3 =  [lxz3*sin(mu3);
        lyz3*(sin(delta0)-sin(delta3))+s3/sqrt(2);
        z0 + s3/sqrt(2) - lyz3*(cos(delta0)-cos(delta3))-lxz3*(1-cos(mu3))];
    

% Transformierte Postitionsvektoren    
A2(mu2,s2,delta2) = R_120 * Ps2;
A3(mu3,s3,delta3) = R120 * Ps3;
  
  
A1(mu1,s1,delta1)= [ lxz1*sin(mu1);
      lyz1*(sin(delta0)-sin(delta1))+s1/sqrt(2);
      z0 + s1/sqrt(2) - lyz1*(cos(delta0)-cos(delta1))-lxz1*(1-cos(mu1))];
  
% Gleichungssystem
A(mu1,s1,delta1,mu2,s2,delta2,mu3,s3,delta3) = [A1(mu1,s1,delta1);
                                                A2(mu2,s2,delta2);
                                                A3(mu3,s3,delta3)];
                                            
Gl = [x;y;z;x;y;z;x;y;z] == A;

res = solve(Gl,[mu1,mu2,mu3,delta1,delta2,delta3,s1,s2,s3]);


      