close all
pos1 = [120;0;300];
pos2 = [0;-10;350];

v = 70;
a= 100;

 [TrajL, TrajS] = MoveL(pos1,pos2,MaxTCPspeed,TCPAcceleration);
 
 figure 
 plot(TrajL(4,:),TrajL(1,:))
 hold on
 plot(TrajL(4,:),TrajL(2,:))
 
 figure
 plot3(TrajL(1,:),TrajL(2,:),TrajL(3,:))
 
 figure
 plot(TrajS(4,:),TrajS(1,:))
 hold on 
 plot(TrajS(4,:),TrajS(2,:))
 hold on
 plot(TrajS(4,:),TrajS(3,:))
 
 TJ = timeseries(TrajS(1:3,:),TrajS(4,:));