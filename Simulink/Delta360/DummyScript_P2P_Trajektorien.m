%% Scipt wich generates a Table with time to Simulate a P&P task

%% Definition of Position
% Position of Display [0 0 360] with a Range of x +- 265 y +- 150
Home = ForwardKinematik_Delta360([0;0;0]);
Pos1 = [120;10;330];
Pos1Offs = Pos1 + [0;0;-10];
%Pos2 = ForwardKinematik_Delta360([50;0;20]);
%Pos2Offs = Pos2 + [0;0;10];

%% Definition of Dynamics
MaxSpeed = 50; % mm/s
Acceleration = 100; % mm/s^2

%% Calculation of Move Comands
[MoveJPos1Offs, Time1] = MoveJ(Home, Pos1Offs, MaxSpeed, Acceleration);
[MoveJLPos1,Time2] = MoveJ(Pos1Offs, Pos1, MaxSpeed, Acceleration);
%MoveLPos1 = MoveL(Pos1Offs; Pos1; MaxSpeed; Accelaration; WaitTime)
[MoveJPosHome, Time3] = MoveJ(Pos1, Home, MaxSpeed, Acceleration);
%MoveLPos1Offs

%% Output 
Time = [Time1, Time1(end)+Time2(2:end), Time1(end)+Time2(end)+Time3(2:end)];
Move = [MoveJPos1Offs;MoveJLPos1(2:end,:);MoveJPosHome(2:end,:)];
TJ = timeseries(Move,Time);%; MoveLPos1 ; MoveLPos1Offs; MoveJPos2Offs; ...
    %MoveLPos2 ; MoveLPos2Offs; Home]
