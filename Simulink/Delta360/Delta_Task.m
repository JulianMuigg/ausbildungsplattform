classdef Delta_Task
    properties
        Targetpoints % fieldstruct of Targetpoint p.('Point_n')
        Move % Move Class
        SimScape_DataFile % string of the Simscape Data File name
        SimScape_Model % string of the Simscape Model name
    end
    
    methods
        function obj = Delta_Task(Targetpoints,Move,SimScape_DataFile,SimScape_Model)
            obj.Targetpoints = Targetpoints;
            obj.Move = Move;
            obj.SimScape_DataFile = SimScape_DataFile;
            obj.SimScape_Model = SimScape_Model;
        end
            
        function [out, overtravelz, RangeOK] =pick_and_place(obj,P1,P2,offs,simulation)
            % P1 and P2 are Strings with the field names of the Targetpoints struct  
            run(obj.SimScape_DataFile);
            assignin('base','smiData',smiData)
            offs = -abs(offs); % offset in -z direction to pick or place point
            pick = obj.Targetpoints.(P1); 
            place = obj.Targetpoints.(P2);
            home = [0;0;0.310]; % task position of s1=s2=s3=0
            % Move Sequence of P&P Task
            obj.Move.J(home,obj.Move.offset(pick,offs),'1');
            obj.Move.L(obj.Move.offset(pick,offs),pick,'2');
            obj.Move.L(pick,obj.Move.offset(pick,offs),'3');
            obj.Move.J(obj.Move.offset(pick,offs),obj.Move.offset(place,offs),'4');
            obj.Move.L(obj.Move.offset(place,offs),place,'5');
            obj.Move.L(place,obj.Move.offset(place,offs),'6');
            obj.Move.J(obj.Move.offset(place,offs),home,'7');
            % Combine Trajectories from Move Sequence
            obj.Move.combine(7);
            % Check validity of Trajectory
            overtravelz = obj.Move.overtravelZ('Com');
            RangeOK = obj.Move.overtravelAxis('Com');
            
            % Creat SimData Struct
            TJ = obj.Move.SimData('Com');
            assignin('base','TJ',TJ)
            if simulation == 1 && overtravelz == 0 && isequal(RangeOK,[1 1 1])
                run(obj.SimScape_Model)
                out=sim(obj.SimScape_Model,'ReturnWorkspaceOutputs','on');
            end
        end
    end
end
    