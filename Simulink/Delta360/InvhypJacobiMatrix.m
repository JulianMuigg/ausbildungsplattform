syms x y z rmove rbase lengthPipe 

theta = [0, 4*pi/3, 2*pi/3];
delta = pi/4;

xm1(x,y) = x * cos(theta(1))+ y * sin(theta(1));
ym1(x,y) = y * cos(theta(1)) - x * sin(theta(1)) + rmove;
zm1(z) = z;

% s1
a1 = 1;
b1(x,y,z) = -2*zm1(z)*sin(delta)-2*rbase*cos(delta)+2*ym1(x,y)*cos(delta);
c1(x,y,z) = xm1(x,y)^2 + ym1(x,y)^2 + zm1(z)^2 + rbase^2 - 2*ym1(x,y)*rbase - lengthPipe^2;
%s1_1 wenn b1^2-4*a1*c1 >= 0
s1_1(x,y,z) =  (-b1(x,y,z)+(-1)*sqrt(b1(x,y,z)^2-4*a1*c1(x,y,z)))/2;
%sonst s1_2
s1_2(x,y,z) =  (-b1(x,y,z)+(-1)*sqrt(1))/2;


%s2
xm2(x,y) = x * cos(theta(2))+ y * sin(theta(2));
ym2(x,y) = y * cos(theta(2)) - x * sin(theta(2)) + rmove;
zm2(z) = z;

a2 = 1;
b2(x,y,z) = -2*zm2(z)*sin(delta)-2*rbase*cos(delta)+2*ym2(x,y)*cos(delta);
c2(x,y,z) = xm2(x,y)^2 + ym2(x,y)^2 + zm2(z)^2 + rbase^2 - 2*ym2(x,y)*rbase - lengthPipe^2;
%s2_1 wenn b1^2-4*a1*c1 >= 0
s2_1(x,y,z) =  (-b2(x,y,z)+(-1)*sqrt(b2(x,y,z)^2-4*a2*c2(x,y,z)))/2;
%sonst s1_2
s2_2(x,y,z) =  (-b2(x,y,z)+(-1)*sqrt(1))/2;


%s3
xm3(x,y) = x * cos(theta(3))+ y * sin(theta(3));
ym3(x,y) = y * cos(theta(3)) - x * sin(theta(3)) + rmove;
zm3(z) = z;

a3 = 1;
b3(x,y,z) = -2*zm3(z)*sin(delta)-2*rbase*cos(delta)+2*ym3(x,y)*cos(delta);
c3(x,y,z) = xm3(x,y)^2 + ym3(x,y)^2 + zm3(z)^2 + rbase^2 - 2*ym3(x,y)*rbase - lengthPipe^2;
%s1_1 wenn b1^2-4*a1*c1 >= 0
s3_1(x,y,z) =  (-b3(x,y,z)+(-1)*sqrt(b3(x,y,z)^2-4*a3*c3(x,y,z)))/2;
%sonst s1_2
s3_2(x,y,z) =  (-b3(x,y,z)+(-1)*sqrt(1))/2;


% Partial derivative of s1_1
dsx1_1 = diff(s1_1,1,x);
dsy1_1 = diff(s1_1,1,y);
dsz1_1 = diff(s1_1,1,z);
% Partial derivative of s1_2
dsx1_2 = diff(s1_2,1,x);
dsy1_2 = diff(s1_2,1,y);
dsz1_2 = diff(s1_2,1,z);

% Partial derivative of s2_1
dsx2_1 = diff(s2_1,1,x);
dsy2_1 = diff(s2_1,1,y);
dsz2_1 = diff(s2_1,1,z);
% Partial derivative of s2_2
dsx2_2 = diff(s2_2,1,x);
dsy2_2 = diff(s2_2,1,y);
dsz2_2 = diff(s2_2,1,z);

% Partial derivative of s3_1
dsx3_1 = diff(s3_1,1,x);
dsy3_1 = diff(s3_1,1,y);
dsz3_1 = diff(s3_1,1,z);
%Partial derivative of s3_2
dsx3_2 = diff(s3_2,1,x);
dsy3_2 = diff(s3_2,1,y);
dsz3_2 = diff(s3_2,1,z);

hypJ = [dsx1_1, dsx1_2, dsy1_1, dsy1_2, dsz1_1, dsz1_2;
        dsx2_1, dsx2_2, dsy2_1, dsy2_2, dsz2_1, dsz2_2;
        dsx3_1, dsx3_2, dsy3_1, dsy3_2, dsz3_1, dsz3_2];