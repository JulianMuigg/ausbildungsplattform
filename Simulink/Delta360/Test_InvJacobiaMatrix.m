close all
%Initialize Classes
Delta = Delta360(0.042,0.222106,0.400,-0.079885,0.144,'Delta');
MoveDelta = Move(0.05,0.10,0.050,0.10,0.04,0.330,Delta);

% Definition of Start-(P_1) and Endpoint(P_2)
P_1=[0.0,0.05,0.330];
P_2=[0.05,0.0,0.320];


% Trajectory interpolation
MoveDelta.L(P_1,P_2,'1');


Traj = [MoveDelta.time.Traj1',MoveDelta.TrajJ.J1.s']
csvwrite('TrajectoryA1D1.csv', Traj );

% create Data struct as Simulink Input
TJ = MoveDelta.SimData('1');

% Trajectory Name Assingment
Tn = strcat('T','1');
Jn = strcat('J','1');
Trajn = strcat('Traj','1');

% Initialize and Run Simulation
run('Delta_Robot_V3_DataFile.m')
out=sim('Delta_Robot_V3.slx','ReturnWorkspaceOutputs','on'); % store Sim Output Data to Workspace


% Create Figure Name out of Start and End Point
txt_P1 = strcat(num2str(P_1(1)),'_',num2str(P_1(2)),'_',num2str(P_1(3)));
txt_P2 = strcat(num2str(P_2(1)),'_',num2str(P_2(2)),'_',num2str(P_2(3)));

% preparing sim_data
% sim_s1_in_p = [out.s_in_p.signals(1).values';
%                out.ds_in_p.signals(1).values';
%                out.dds_in_p.signals(1).values'];
% sim_s2_in_p = [out.s_in_p.signals(2).values';
%                out.ds_in_p.signals(2).values';
%                out.dds_in_p.signals(2).values'];
% sim_s3_in_p = [out.s_in_p.signals(3).values';
%                out.ds_in_p.signals(3).values';
%                out.dds_in_p.signals(3).values'];
% sim_p1_in_p = [out.p_in_p.signals(1).values';
%                out.v_in_p.signals(1).values';
%                out.a_in_p.signals(1).values'];
% sim_p2_in_p = [out.p_in_p.signals(2).values';
%                out.v_in_p.signals(2).values';
%                out.a_in_p.signals(2).values'];
% sim_p3_in_p = [out.p_in_p.signals(3).values';
%                out.v_in_p.signals(3).values';
%                out.a_in_p.signals(3).values'];
           
           
sim_s1_in_pv = [out.s_in_pv.signals(1).values';
               out.ds_in_pv.signals(1).values';
               out.dds_in_pv.signals(1).values'];
sim_s2_in_pv = [out.s_in_pv.signals(2).values';
               out.ds_in_pv.signals(2).values';
               out.dds_in_pv.signals(2).values'];
sim_s3_in_pv = [out.s_in_pv.signals(3).values';
               out.ds_in_pv.signals(3).values';
               out.dds_in_pv.signals(3).values'];
sim_p1_in_pv = [out.p_in_pv.signals(1).values';
               out.v_in_pv.signals(1).values';
               out.a_in_pv.signals(1).values'];
sim_p2_in_pv = [out.p_in_pv.signals(2).values';
               out.v_in_pv.signals(2).values';
               out.a_in_pv.signals(2).values'];
sim_p3_in_pv = [out.p_in_pv.signals(3).values';
               out.v_in_pv.signals(3).values';
               out.a_in_pv.signals(3).values'];
%            
% sim_s1_in_pva = [out.s_in_pva.signals(1).values';
%                out.ds_in_pva.signals(1).values';
%                out.dds_in_pva.signals(1).values'];
% sim_s2_in_pva = [out.s_in_pva.signals(2).values';
%                out.ds_in_pva.signals(2).values';
%                out.dds_in_pva.signals(2).values'];
% sim_s3_in_pva = [out.s_in_pva.signals(3).values';
%                out.ds_in_pva.signals(3).values';
%                out.dds_in_pva.signals(3).values'];
% sim_p1_in_pva = [out.p_in_pva.signals(1).values';
%                out.v_in_pva.signals(1).values';
%                out.a_in_pva.signals(1).values'];
% sim_p2_in_pva = [out.p_in_pva.signals(2).values';
%                out.v_in_pva.signals(2).values';
%                out.a_in_pva.signals(2).values'];
% sim_p3_in_pva = [out.p_in_pva.signals(3).values';
%                out.v_in_pva.signals(3).values';
%                out.a_in_pva.signals(3).values'];
%            
           
% plot Data from Jacobi against Simulation        
% plotAxisData(MoveDelta.Axis.(Jn).s1, sim_s1_in_p, MoveDelta.time.(Trajn), 's1');
% plotAxisData(MoveDelta.Axis.(Jn).s2, sim_s2_in_p, MoveDelta.time.(Trajn), 's2');
% plotAxisData(MoveDelta.Axis.(Jn).s3, sim_s3_in_p, MoveDelta.time.(Trajn), 's3');
% plotAxisData(MoveDelta.Axis.(Tn).x, sim_p1_in_p, MoveDelta.time.(Trajn), 'x');
% plotAxisData(MoveDelta.Axis.(Tn).y, sim_p2_in_p, MoveDelta.time.(Trajn), 'y');
% plotAxisData(MoveDelta.Axis.(Tn).z, sim_p3_in_p, MoveDelta.time.(Trajn), 'z');

plotAxisData(MoveDelta.Axis.('J1').s1, sim_s1_in_pv, MoveDelta.time.('Traj1'), 's1');
plotAxisData(MoveDelta.Axis.('J1').s2, sim_s2_in_pv, MoveDelta.time.('Traj1'), 's2');
plotAxisData(MoveDelta.Axis.('J1').s3, sim_s3_in_pv, MoveDelta.time.('Traj1'), 's3');
plotAxisData(MoveDelta.Axis.('T1').x, sim_p1_in_pv, MoveDelta.time.('Traj1'), 'x');
plotAxisData(MoveDelta.Axis.('T1').y, sim_p2_in_pv, MoveDelta.time.('Traj1'), 'y');
plotAxisData(MoveDelta.Axis.('T1').z, sim_p3_in_pv, MoveDelta.time.('Traj1'), 'z');



% calculate diviation of Calculated TCP speed to Simulated TCP Speed
% errorVx_in_p = MoveDelta.TrajJ.(Jn).ds(1,:)-out.ds_in_p.signals(1).values';
% errorVy_in_p = MoveDelta.TrajJ.(Jn).ds(2,:)-out.ds_in_p.signals(2).values';
% errorVz_in_p = MoveDelta.TrajJ.(Jn).ds(3,:)-out.ds_in_p.signals(3).values';

errorVx_in_pv = MoveDelta.TrajT.('T1').v(1,:)-out.v_in_pv.signals(1).values';
errorVy_in_pv = MoveDelta.TrajT.('T1').v(2,:)-out.v_in_pv.signals(2).values';
errorVz_in_pv = MoveDelta.TrajT.('T1').v(3,:)-out.v_in_pv.signals(3).values';

% errorVx_in_pva = MoveDelta.TrajT.('T1').v(1,:)-out.v_in_pva.signals(1).values';
% errorVy_in_pva = MoveDelta.TrajT.('T1').v(2,:)-out.v_in_pva.signals(2).values';
% errorVz_in_pva = MoveDelta.TrajT.('T1').v(3,:)-out.v_in_pva.signals(3).values';

% plot Error

% simulation with s
% figure
% plot(0:1:length(errorVx_in_p)-1,errorVx_in_p)
% hold on 
% plot(0:1:length(errorVy_in_p)-1,errorVy_in_p)
% hold on 
% plot(0:1:length(errorVz_in_p)-1,errorVz_in_p)
% ylabel('$e_v \, / \, mms^{-1}$','interpreter','Latex')
% xlabel('$t \, / \, s$','interpreter','Latex')
% legend('error $v_x$','error $v_y$', 'error $v_z$','interpreter','latex')
% title('Simulation Input $\vec{s}$','interpreter','latex')
% save figure
%saveas(2,strcat('err_P1_P2',txt_P1,',',txt_P2,'.pdf'));

% simulation with s and ds
figure
plot(0:1:length(errorVx_in_pv)-1,errorVx_in_pv)
hold on 
plot(0:1:length(errorVy_in_pv)-1,errorVy_in_pv)
hold on 
plot(0:1:length(errorVz_in_pv)-1,errorVz_in_pv)
ylabel('$e_v \, / \, mms^{-1}$','interpreter','Latex')
xlabel('$t \, / \, s$','interpreter','Latex')
legend('error $v_x$','error $v_y$', 'error $v_z$','interpreter','latex')
title('Simulation Input $\vec{s}$ \& d$\vec{s}$','interpreter','latex')
