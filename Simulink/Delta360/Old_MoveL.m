function [TrajL, TrajS] = MoveL(pos1,pos2, MaxTCPspeed, TCPAcceleration)

h = pos2-pos1; % displacment and direction
% Factoring velocity and acceleration vektor
vmax = MaxTCPspeed .* (h./norm(h));
amax = TCPAcceleration .* (h./norm(h)); 

% Calculation of durations

if max(abs(h)) >= max(abs(vmax))^2/max(abs(amax))
    Ta = max(abs(vmax))/max(abs(amax)); % acceleration and deceleration time
    T = round((max(abs(h)) * max(abs(amax)) + max(abs(vmax))^2)/(max(abs(amax))*max(abs(vmax))),2); % total duration
else
    Ta = sqrt(max(abs(h))/max(abs(amax))); % acceleration and deceleration time
    T = 2*Ta; % totalduration 
    vnew = max(abs(amax))/Ta; % maximum velocity peak
end

%prealocation
t = 0:0.02:T;
pos = zeros(3,length(t));

for n = 1:length(h)
    
if abs(h(n)) >= abs(vmax(n))^2/abs(amax(n))
    % linear movement computation
    for i = 1:length(t)
        if t(i) <= Ta
            pos(n,i) = pos1(n) + 0.5 *amax(n) * t(i)^2; % acceleration phase
            dpos(n,i) = amax(n) * t(i);
            ddpos(n,i) = amax(n);
        elseif Ta < t(i) && t(i) <= (T-Ta)
            pos(n,i) = pos1(n) + amax(n) * Ta *(t(i)-Ta/2); %linear movement phase
            dpos(n,i) = vmax(n);
            ddpos(n,i) = 0;
        elseif (T-Ta) < t(i) && t(i)<=T
            pos(n,i) = pos2(n)- 0.5 * amax(n) *(T-t(i))^2; % deceleration
            dpos(n,i) = vmax(n)-amax(n)*(t(i)-(T-Ta));
            ddpos(n,i) = -amax(n);
        end
    end
elseif h(n) == 0 
    for i = 1:length(t)
    pos(n,i) = pos1(n);
    dpos(n,i) = 0;
    ddpos(n,i) = 0;
    end
else
    % no linear movement computation
    for i = 1:length(t)
        if 0<=t(i) && t(i) <= Ta
            pos(n,i) = pos1(n) + 0.5 * amax(n) * t(i)^2; %acceleration phase
            dpos(n,i) = amax(n) * t(i);
            ddpos(n,i) = amax(n);
        elseif (T-Ta) < t(i) && t(i) <= T
            pos(n,i) = pos2(n) - 0.5 * amax(n) * (T-t(i))^2; % deceleration phase
            dpos(n,i) = vnew-amax(n)*(t(i)-(T-Ta));
            ddpos(n,i) = -amax(n);
        end
    end
end
end
% Joint Space Trajectory for Linear interpolation
%prealocation
TrajS = zeros(3,length(t));
for j = 1:length(t)
    TrajS(:,j) = InverseKinematik_Delta360(pos(:,j));
end
TrajS=[TrajS;t];
% Cartesian Trajectory for Linear interpolation
TrajL = [pos;t];
end