close all 
clear all

Delta = Delta360(0.042,0.222106,0.400,-0.079885,0.144,'Delta');

% s1 = linspace(0,0.144,144);
% s2 = linspace(0,0.144,144);
% s3 = linspace(0,0.144,144);
% l = 0;
% for i = 1:144
%     for j = 1:144
%         for k = 1:144
%             l = l+1;
%             TS(:,l)=Delta.forward([s1(i),s2(j),s3(k)]);
%         end
%     end
% end
% 
% plot3(TS(1,:),TS(2,:),TS(3,:))

[Z300, x,y] = WorkspaceZ(0.300,Delta);
figure
surf(y,x,Z300)
xlabel('$y \textrm{ / m}$','interpreter','latex')
ylabel('$x \textrm{ / m}$','interpreter','latex')
title('$z = 0.300 \textrm{ m}$','interpreter','latex')

[Z330,x,y] = WorkspaceZ(0.330,Delta);
figure 
surf(y,x,Z330)
xlabel('$y \textrm{ / m}$','interpreter','latex')
ylabel('$x \textrm{ / m}$','interpreter','latex')
title('$z = 0.330 \textrm{ m}$','interpreter','latex')

[Z350,x,y] = WorkspaceZ(0.350,Delta);
figure
surf(y,x,Z350)
xlabel('$y \textrm{ / m}$','interpreter','latex')
ylabel('$x \textrm{ / m}$','interpreter','latex')
title('$z = 0.350 \textrm{ m}$','interpreter','latex')

[Z310,x,y] = WorkspaceZ(0.310,Delta);
figure
surf(y,x,Z310)
xlabel('$y \textrm{ / m}$','interpreter','latex')
ylabel('$x \textrm{ / m}$','interpreter','latex')
title('$z = 0.310 \textrm{ m}$','interpreter','latex')

function [WS, x,y] = WorkspaceZ(z,Delta)
    % xy - planesize
    intervals = 110;
    fieldxy = 0.25;
    x = linspace(-fieldxy,fieldxy,intervals);
    y = linspace(-fieldxy,fieldxy,intervals);
    WS = zeros(intervals,intervals);
    % Inversekinematik ergebniss
    for i = 1:intervals
        for j = 1:intervals
            ws = Delta.inverse([x(i),y(j),z]); 
            if (0 <= ws(1)) && (ws(1) <= 0.144) &&...
               (0 <= ws(2)) && (ws(2) <= 0.144) &&...
               (0 <= ws(3)) && (ws(3) <= 0.144)
           WS(i,j) = 1;
            end
        end
    end
end
