classdef TcAds
    %TCADS uses the .NET dlls of TwinCAT ads to communicate via ads
    
    properties
        adsClt
    end
    
    methods
        function obj = TcAds(amsNetId, port)
            file = mfilename('fullpath');
            [path,name,ext] = fileparts( file );
            asm = NET.addAssembly([path '\TwinCAT.Ads.dll']);
            import TwinCAT.Ads.*;
            
            obj.adsClt=TwinCAT.Ads.TcAdsClient;
            obj.adsClt.Connect(amsNetId,port);
        end
        
        %% Is true, if the connection is valid
        function b = isConnected( obj )
           b = obj.adsClt.TryReadState() == TwinCAT.Ads.AdsErrorCode.NoError;
        end
        
        %% Reads the group id of the variable via string
        function group = idxGroup( obj, varName )
            group = uint32( 0 );
            if obj.isConnected() 
                info = obj.adsClt.ReadSymbolInfo( varName );
                group = uint32(info.IndexGroup);  
            end
        end
        
        %% Reads the offset id of the variable via string
        function offset = idxOffset( obj,varName )
            offset = uint32( 0 );
            if obj.isConnected()
                info = obj.adsClt.ReadSymbolInfo( varName );
                offset = uint32(info.IndexOffset);  
            end
        end
        
        %% Reads the value of a single variable, no array
        function value = read( obj,varName )
            value = 0;
            if obj.isConnected()
                info = obj.adsClt.ReadSymbolInfo( varName );
                value = obj.adsClt.ReadSymbol(info)  
            end
        end
        
        %% Writes the value to a single variable, no array
        function valid = write( obj, varName, value )
            valid = false;
            if obj.isConnected()
                info = obj.adsClt.ReadSymbolInfo( varName );
                obj.adsClt.WriteSymbol( info, value )  
                valid = true;
            end
        end
    end
end

