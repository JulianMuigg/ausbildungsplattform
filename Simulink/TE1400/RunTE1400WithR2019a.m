%% Skirpt to run TE1400 in R2019a
file = mfilename('fullpath');
[path,name,ext] = fileparts( file );
addpath( [path '/../'] );
InitAusbildungsplattform;


file = 'AusbildungsplattformDelta360_TE1400_R2019a';
if exist( file, 'file' )
    stepSize = 0.01;
    disp( ['Building ' file ' ...'] );
    rtwbuild(file,'OpenBuildStatusAutomatically',true);
    quit
end

file = 'ForwardKinematicDelta360_TE1400_R2019a';
if exist( file, 'file' )
    stepSize = 0.002;
    disp( ['Building ' file ' ...'] );
    rtwbuild(file,'OpenBuildStatusAutomatically',true);
    quit
end

file = 'AusbildungsplattformScara_TE1400_R2019a';
if exist( file, 'file' )
    stepSize = 0.01;
    disp( ['Building ' file ' ...'] );
    rtwbuild(file,'OpenBuildStatusAutomatically',true);
    quit
end

file = 'ForwardKinematicScara_TE1400_R2019a';
if exist( file, 'file' )
    stepSize = 0.002;
    disp( ['Building ' file ' ...'] );
    rtwbuild(file,'OpenBuildStatusAutomatically',true);
    quit
end