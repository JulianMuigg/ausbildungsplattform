%% Main script to start the different types of simulink models
%% HAllo Matthias
close all
clearvars -except robotType netIdStr
clc

%% Add project paths
if exist('./Simulink', 'dir')
    addpath( './Simulink' );
end

path = fileparts( mfilename('fullpath') );
addpath( [path '/Library'] );
addpath( [path '/Control'] );
addpath( [path '/Delta360'] );
addpath( [path '/Scara'] );
addpath( [path '/ExternalFunctions'] );
addpath( [path '/TE1400'] );
addpath( [path '/TE1410'] );
addpath( [path '/TestModelsXiL'] );
addpath( [path '/SystemModels'] );
addpath( [path '/TwinCATDlls'] );
addpath( [path '/MotorStepResponse'] );

%% Create object of Ausbildungsplattform class
obj = Ausbildungsplattform();

%% Ask user for robot type
robotType = obj.askForRobotType();

%% Ask for simulation mode
simMode = obj.askForSimMode();

%% Ask for XiL mode. Nothing happens, if simulation mode is not XiL
xilMode = obj.askForXilMode(); 

%% Ask for AMS Net Id.  Nothing happens, if not needed
if ~exist( 'netIdStr', 'var' )
    netIdStr = '192.168.26.103.1.1';
end
netIdStr = obj.askForAmsNetId( netIdStr );

%% Init Ausbildungsplattform
InitAusbildungsplattform;

%% Execute the education plattform depending on the user input
obj.execute( path );

%% Run scripts depending on sim mode, which needs results from workspace
switch( simMode )
    case SimMode.XiL
        SaveAndPlotXilResults; 
    case SimMode.StepResponse
        RunControllerDesign;
end


