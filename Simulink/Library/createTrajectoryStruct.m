function [mStruct] = createTrajectoryStruct()
%CREATEMOVESTRUCT function to create a move struct
% s1 ... start, stop, position, velocity and acceleration of first joint/coordinate
% s2 ... start, stop, position, velocity and acceleration of second joint/coordinate
% s3 ... start, stop, position, velocity and acceleration of third joint/coordinate

xyzStruct = struct( 'start', {0}, 'stop', {0}, 'p', {0}, 'v', {0}, 'a', {0}, 'reached', {false} );

mStruct = struct('s1', { xyzStruct }, ...
                 's2', { xyzStruct }, ...
                 's3', { xyzStruct }, ...
                 'v_max', {0}, 'a_max', {0}, 'tolerance', {0}, 't', {0}, 'moveType', {MoveType.Undefined} );
end