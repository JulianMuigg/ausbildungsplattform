function [mStruct] = createMoveStruct()
%CREATEMOVESTRUCT function to create a move struct
% v_max ... max endeffector velocity
% a_max ... max endeffector acceleration
% dq_max ... max joint velocity
% ddq_max ... max joint acceleration
% z_max ... z position of endeffector to reach the touch display
% mmPerPixel ... calibration factor
% request ... send all variables to HMI

mStruct = struct('v_max', {0.5}, 'a_max', {0.5}, 'dq_max', {0.5},...
    'ddq_max', {0.5}, 'z_max', {0}, 'homeX', {-0.06}, 'homeY', {-0.06}, ...
    'homeZ', {0}, 'request', {false} );

end

