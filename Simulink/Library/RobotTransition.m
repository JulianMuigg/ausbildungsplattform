classdef RobotTransition < Simulink.IntEnumType
    enumeration
        Home(0)
        Start(1)
        Stop(2)
        NoAction(4) % causes no state change
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            