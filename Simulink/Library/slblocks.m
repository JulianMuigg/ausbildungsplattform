function blkStruct = slblocks

        % Add filepaths for this library and sublibraries
        filepath = fileparts(which('slblocks.m'));
        addpath( filepath );
        addpath( [filepath '\..\Control'] );
        addpath( [filepath '\..\Delta360'] );
        addpath( [filepath '\..\Scara'] );
        
		% This function specifies that the library should appear
		% in the Library Browser
		% and be cached in the browser repository

		Browser.Library = 'AusbildungsplattformLibrary';
		% 'AusbildungsplattformLibrary' is the name of the library

		Browser.Name = 'MCI Robotic Ausbildungsplattform';
		% 'MCI Robotic Ausbildungsplattform' is the library name that appears 
        % in the Library Browser

		blkStruct.Browser = Browser; 