classdef RobotState < Simulink.IntEnumType
    enumeration
        Undefined(0)
        Idle(1)         % is ready for new path
        Started(2)      % is ready to move
        Busy(3)         % is moving
        Stopped(4)      % is stopped, home requiered
        Error(5)        % like singularity
        Homing(6)       % Robot is moved to home position
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            
            
            