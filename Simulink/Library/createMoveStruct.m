function [mStruct] = createMoveStruct()
%CREATEMOVESTRUCT function to create a move struct
% pos ... position for movment
% v ... velocity
% a ... acceleration
% type ... type of moving
% r ... tolerance radius

mStruct = struct('pos', {createPositionStruct()}, 'v', {0}, 'a', {0}, 'type', {MoveType.Undefined}, 'r', {0} );

end

