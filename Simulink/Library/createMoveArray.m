function [mStruct] = createMoveArray()
%CREATEMOVESTRUCT function to create a move struct
% pos ... position for movment
% v ... velocity
% a ... acceleration
% type ... type of moving
% r ... tolerance radius

mStruct = struct('cmd1', {createMoveStruct()}, ...
    'cmd2', {createMoveStruct()}, ...
    'cmd3', {createMoveStruct()}, ...
    'cmd4', {createMoveStruct()}, ...
    'cmd5', {createMoveStruct()}, ...
    'cmd6', {createMoveStruct()} );

end

