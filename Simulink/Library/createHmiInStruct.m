function [mStruct] = createHmiInStruct()
%CREATEMOVESTRUCT function to create a HMI out struct

robotOutStruct = createRobotOutStruct();

mStruct = struct('state', {State.Undefined}, ...
                 'positionRequest', { false }, ...
                 'robotOut', { robotOutStruct } );
end

