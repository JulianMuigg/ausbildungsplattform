function [mStruct] = createNextPositionStruct()
%CREATEMOVESTRUCT function to create a next position struct
% pos1 ... position for picking
% pos2 ... position for placing
% new ... pos1 and/or pos2 are changed
pos = createPositionStruct();
mStruct = struct('pos1', {pos}, 'pos2', {pos}, 'new', {false} );

end

