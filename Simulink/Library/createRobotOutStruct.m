function [mStruct] = createMoveStruct()
%CREATEMOVESTRUCT function to create a move struct
% data ... current robot move information
% positionReached ... robot is at defined position
% errorRobot ... robot is in error state

mStruct = struct('data', {createMoveStruct()}, 'positionReached', {false}, 'errorRobot', {0} );

end

