classdef SimMode < Simulink.IntEnumType
    enumeration
        Virtual(0)
        XiL(1)
        TE1410(2)
        TE1400(3)
        StepResponse(4)
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            