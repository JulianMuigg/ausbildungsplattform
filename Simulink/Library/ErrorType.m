classdef ErrorType < Simulink.IntEnumType
    enumeration
        NoError(0) 
        Singularity(1)
        NotReachable(2)
        AxisLimitError(3)
        HardStopError(4)
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            
            
            