function [mStruct] = createHmiOutStruct()
%CREATEMOVESTRUCT function to create a HMI out struct

posStruct = createNextPositionStruct();
parameterStruct = createParameterStruct();
mStruct = struct('transition', {Transition.Undefined}, ...
                 'touchEvent', { false }, ...
                 'nextPosition', { posStruct }, ...
                 'parameter', { parameterStruct } );

end

