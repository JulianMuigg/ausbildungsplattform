classdef SeqTransition < Simulink.IntEnumType
    enumeration
        Undefined(0)
        Home(1)
        StartJogging(2)
        StartCalibration(3)
        StartAutomatic(4)
        StartExternalTrajectory(5)
        Stop(6)
        ErrorQuit(7)
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            