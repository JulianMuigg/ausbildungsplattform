classdef Ausbildungsplattform < handle
    %AUSBILDUNGSPLATTFORM Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        robotType = RobotType.Scara;
        simMode = SimMode.Virtual;
        xilMode = XilMode.MiL;
        netIdStr = '192.168.26.100.1.1';
    end
    
    methods
        function obj = Ausbildungsplattform()
            
        end
        
        %% Asking for the wanted robot type
        function robotType = askForRobotType(obj)
            robotType = RobotType( menu('Which type of robot are you using', ...
                'Scara','Delta360') - 1 );
               
            obj.robotType = robotType;
        end
        
        %% Asking for the wanted simulation mode
        function simMode = askForSimMode(obj)
            simMode = SimMode(  menu('Which simulation mode do you want to use?', ...
                'Virtual System simulation (standalone)', ...
                'X in the Loop validation', ...
                'Controlling real robot with Simulink (TE1410)', ...
                'Codegeneration with TE1400', ...
                'Velocity Step Response' ) - 1 );
            
            obj.simMode = simMode;
        end
        
        %% Asking for the wanted XiL validation method
        function mode = askForXilMode(obj)
            if obj.simMode == SimMode.XiL
                mode = XilMode( menu('Which XiL simulation would you like to start?', ...
                    'Model in the Loop (MiL)', ...
                    'Software in the Loop (SiL)', ...
                    'Hardware in the Loop (HiL)') - 1 );

                obj.xilMode = mode;
            else
                mode = obj.xilMode;
            end
        end
        
        %% If SiL, HiL, TE1410 or StepResponse, ask for the defined TwinCAT AMS NetId to the plc
        function netIdStr = askForAmsNetId(obj,in)
            if obj.xilMode == XilMode.SiL || ...
               obj.xilMode == XilMode.HiL || ...
               obj.simMode == SimMode.TE1410 || ...
               obj.simMode == SimMode.StepResponse
           
                prompt = {'Enter the TwinCAT AMS NetId of your route'};
                dlgtitle = 'TwinCAT AMS NetId';
                dims = [1 35];
                definput = {in};
                temp = inputdlg(prompt,dlgtitle,dims,definput);
                netIdStr = temp{1};
                
                obj.netIdStr = netIdStr;
            else
                netIdStr = in;
            end
        end
        
        function ans = execute(obj, path)
           
            switch( obj.simMode )
                case SimMode.Virtual
                    modelName = ['Ausbildungsplattform' char( obj.robotType )];
                    obj.runSystemModels( modelName );
                case SimMode.XiL
                    modelName = ['Ausbildungsplattform' char( obj.robotType ) '_' char(obj.xilMode)];
                    obj.runSimulinkModel( modelName );
                case SimMode.TE1410
                    modelName = ['Ausbildungsplattform' char( obj.robotType ) '_TE1410'];
                    obj.runTE1410( modelName );
                case SimMode.TE1400
                    % Run code generation for Ausbildungsplattform..._TE1400.slx
                    stepSize = 0.04;
                    modelName = ['Ausbildungsplattform' char( obj.robotType ) '_TE1400'];
                    obj.runTE1400WithR2019a( path, modelName );
                    
                    % Run code generation for ForwardKinematic..._TE1400.slx
                    stepSize = 0.002;
                    modelName = ['ForwardKinematic' char( obj.robotType ) '_TE1400'];
                    obj.runTE1400WithR2019a( path, modelName );
                case SimMode.StepResponse
                    modelName = ['Ausbildungsplattform' char( obj.robotType ) '_StepResponse'];
                    obj.runStepResponse( modelName );
            end
        end
    end
    
    methods (Access = private)
        %% Start HMI only if not running
        function startHmi(obj, netIdStr)
            if exist('./Simulink', 'dir')
                system( ['start ./HMI/HMI_Ausbildungsplattform.exe ' netIdStr] );
            else
                system( ['start ../HMI/App/HMI_Ausbildungsplattform.exe ' netIdStr] );
            end
        end
        
        %% Close the HMI if running
        function stopHmi( obj )
            [status,result] = system('tasklist /FI "imagename eq HMI_Ausbildungsplattform.exe" /fo table /nh');
            if contains( result, 'HMI_Ausbildungsplattform' )
                system( ['taskkill /F /IM "HMI_Ausbildungsplattform.exe" /T'] );
            end
        end   
        
        %% Run virtual system model simulation
        function runSystemModels(obj, modelName)
            % Starting hmi to run the system virtual
            obj.startHmi( '' );
            
            % Run simulink model
            obj.runSimulinkModel( modelName );
            
            % Stop hmi after simulinkmodel is stopped
            obj.stopHmi();
        end
        
        %% Run XiL simuliation
        function runSimulinkModel(obj, modelName)
            if ~strcmp( modelName, '' )
                % Open and start Simulink Model
                open_system(modelName)
                set_param(modelName,'SimulationCommand', 'start');

                % Wait until simulation is finished
                while strcmp( get_param(modelName,'SimulationStatus'), 'stopped') == 0
                    pause(1)
                end

                % Close simulinkt model
                close_system(modelName);
            end
        end
        
        %% Run the simulink model, which is controlled by the real HMI and
        % controls the real robot
        function runTE1410(obj, modelName)
            % Starting hmi to run the system virtual
            obj.startHmi( obj.netIdStr );
            
            % Run simulink model
            obj.runSimulinkModel( modelName );
            
            % Stop hmi after simulinkmodel is stopped
            obj.stopHmi();            
        end
        
        %% Funktion to run the TE1400 code generation from any matlab version
        % with matlab 2019a
        function runTE1400WithR2019a(obj, path, fileName, scriptFile)
           
            close_system( [fileName '_standalone'], 0 );

            %%% Copy and Open system
            in = fullfile([path '\TE1400'], [fileName '.slx'] );
            % Prepare the output filename.
            out = fullfile([path '\TE1400'], [fileName '_standalone.slx'] );
            % Do the copying and renaming all at once.
            copyfile(in, out);
            % Open system
            open_system( [fileName '_standalone'] );

            %%% Break links to library, by saveing the model again
            save_system( [fileName '_standalone'], [], 'BreakUserLinks', true );

            %%% Export to Matlab R2019a version
            Simulink.exportToVersion( [fileName '_standalone'], ...
                [path '\TE1400\' fileName '_R2019a'], 'R2019a' );
            close_system( [fileName '_standalone'] );

            %%% Open simulink file, run code generation and close it again
            if ~exist( 'C:\Program Files\MATLAB\R2019a\bin\matlab.exe', 'file' )
                msgbox('Matlab R2019a not installed', 'Error','error');
            else
                system( [path '\TE1400\RunMatlabSkript.bat ' path '\TE1400\RunTE1400WithR2019a'] );
            end

            %%% Clear workspace
            delete([path '\TE1400\' fileName '_R2019a.slx']);
            delete([path '\TE1400\' fileName '_R2019a.slxc']);
            delete([path '\TE1400\' fileName '_standalone.slx']);

        end
        
        %% Run the simulink model, to capture a velocity step response of the joints
        % controls the real robot the calculate the pi controller values
        function runStepResponse(obj, modelName)            
            % Run simulink model
            obj.runSimulinkModel( modelName );            
        end
    end
end

