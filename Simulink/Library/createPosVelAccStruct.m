function [mStruct] = createPosVelAccStruct()
%CREATEMOVESTRUCT function to create a move struct
% p ... position
% v ... velocity
% a ... acceleration


mStruct = struct( 'p', {0}, 'v', {0}, 'a', {0} );

end