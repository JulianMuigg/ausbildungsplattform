classdef Trajectory
    properties 
    end
    methods 
        function obj = Trajectory()
        end
        function TJ = execAll(obj, dt, S1, S2, vmax_in, amax_in, moveType )
            h = S2-S1;
            
            [Ta,T,vj,aj,vmax,amax] = obj.calcBoundaryConditions( h, vmax_in, amax_in, moveType );
            T_final = max( T );
            
            TJ = zeros(1,size(S1,1)+1);
            index = 1;
            for t = 0:dt:T_final
                [s,ds,dds] = obj.calc( t, S1, S2, h, vmax_in, amax_in, moveType, 0.0 );
                TJ(index,:) = [t, s'];
                index = index+1;
            end
        end
        
        function trajOut = exec(obj,trajIn)
            %% calculates a trajectory for all joints.

            % Define needed values
            S1 = [trajIn.q1.start; trajIn.q2.start; trajIn.q3.start];
            S2 = [trajIn.q1.stop; trajIn.q2.stop; trajIn.q3.stop];
            h = S2-S1;
            moveType = trajIn.moveType;
            t = trajIn.t;
            
            [s,ds,dds,reached] = obj.calc( t, S1, S2, h, trajIn.v_max, trajIn.a_max, moveType, trajIn.tolerance); % , trajIn.softLimit);
           
            % Set output
            trajOut = trajIn;
            trajOut.q1.p = s(1,1);
            trajOut.q1.v = ds(1,1);
            trajOut.q1.a = dds(1,1);
            trajOut.q1.reached = reached(1,1);

            trajOut.q2.p = s(2,1);
            trajOut.q2.v = ds(2,1);
            trajOut.q2.a = dds(2,1);
            trajOut.q2.reached = reached(2,1);

            trajOut.q3.p = s(3,1);
            trajOut.q3.v = ds(3,1);
            trajOut.q3.a = dds(3,1);
            trajOut.q3.reached = reached(3,1);

        end
        
        function y = compareTrajectory(obj,u1,u2)
            %% Compares two trajectory structs u1 and u2
            if u1.q1.start == u2.q1.start && ...
               u1.q1.stop == u2.q1.stop && ...
               u1.q1.p == u2.q1.p && ...
               u1.q1.v == u2.q1.v && ...
               u1.q1.a == u2.q1.a && ...
               u1.q2.start == u2.q2.start && ...
               u1.q2.stop == u2.q2.stop && ...
               u1.q2.p == u2.q2.p && ...
               u1.q2.v == u2.q2.v && ...
               u1.q2.a == u2.q2.a && ...
               u1.q3.start == u2.q3.start && ...
               u1.q3.stop == u2.q3.stop && ...
               u1.q3.p == u2.q3.p && ...
               u1.q3.v == u2.q3.v && ...
               u1.q3.a == u2.q3.a

                y = 1;
            else
                y = 0;
            end 
        end
        
        function y = compareJointSpace(obj,u1,u2)
            %% Compares two joint space structs u1 and u2
            if u1.q1.p == u2.q1.p && ...
               u1.q1.v == u2.q1.v && ...
               u1.q1.a == u2.q1.a && ...
               u1.q2.p == u2.q2.p && ...
               u1.q2.v == u2.q2.v && ...
               u1.q2.a == u2.q2.a && ...
               u1.q3.p == u2.q3.p && ...
               u1.q3.v == u2.q3.v && ...
               u1.q3.a == u2.q3.a

                y = 1;
            else
                y = 0;
            end 
        end
    end
    
    methods (Access = private)
        function [Ta,T,vj,aj,vmax,amax] = calcBoundaryConditions( obj, h, vmax_in, amax_in, moveType )
            %% Calculates the boundary conditions for time, velocity and acceleration
            % Trajectory Form Querry
            Ta = zeros(size(h,1),1);
            T =  zeros(size(h,1),1);
            vj = zeros(size(h,1),1);
            aj = zeros(size(h,1),1);
            
            vmax = zeros(size(h,1),1);
            amax = zeros(size(h,1),1);

            if moveType==MoveType.MoveJ
                for j = 1:size(h,1)
                    vmax(j) = vmax_in;
                    amax(j) = amax_in;
                end
            elseif moveType==MoveType.MoveL
                for j = 1:size(h,1)
                     % Factoring velocity and acceleration vektor
                     vmax(j) = vmax_in * abs((h(j)/norm(h)));
                     amax(j) = amax_in * abs((h(j)/norm(h))); 
                end
            end

            for j = 1:size(h,1)
                if abs(h(j)) > vmax(j)^2/amax(j)
                    Ta(j) = vmax(j)/amax(j); %acceleration and decelaration time
                    T(j) = (abs(h(j))*amax(j) + vmax(j)^2)/(amax(j)*vmax(j)); %total duration
                elseif abs(h(j)) <= vmax(j)^2/amax(j)
                    Ta(j) = sqrt(abs(h(j))/amax(j)); % acceleration and deceleration time
                    T(j) = 2*Ta(j); % total duration
                end
            end
        end
        
        function [s,ds,dds,reached] = calc( obj, t, S1, S2, h, vmax_in, amax_in, moveType, tolerance, softLimit )
            %% Calculates the current trajectory point depending on t for size(S1) joints
            % Define persistent values
            persistent s_old
            persistent ds_old
            persistent dds_old
%             persistent stopped_old
%             persistent k
%             persistent tsl1
%             persistent tsl2
%             persistent S3
            if isempty( s_old )
                s_old = zeros(length(S1),1);
                ds_old = zeros(length(S1),1);
                dds_old = zeros(length(S1),1);
%                 stopped_old = [false; false; false];
%                 k = 0;
%                 tsl1 = 0;
%                 tsl2 =0;
%                 S3 = [0;0;0];
            end
            

            
            [Ta,T,vj,aj,vmax,amax] = obj.calcBoundaryConditions( h, vmax_in, amax_in, moveType );

            %% Define max time and max velocity/acceleration of each joint
            Ta_final = max( Ta );
            T_final = max( T );

            for j = 1:size(S1,1)
                if (Ta_final == Ta(j) && T_final == T(j)) || moveType == MoveType.MoveL 
                    vj(j) = abs(vmax(j));
                    aj(j) = abs(amax(j));
                elseif moveType == MoveType.MoveJ
                    vj(j) = abs(h(j)/(T_final-Ta_final));
                    aj(j) = abs(h(j)/(Ta_final*(T_final-Ta_final)));
                end

                % Change velocity/acceleration depinding on direction
                if h(j) < 0
                    vj(j) = -vj(j);
                    aj(j) = -aj(j);
                end   
            end

            %% creation of time vector
            s = zeros(length(S1),1); % preallocation of q(t)
            ds = zeros(length(S1),1); %preallocation of dq(t)
            dds = zeros(length(S1),1); %preallocation of ddq(t)
            reached = [false; false; false]; 
            
  %% normal Interpolation
%           if moveType == MoveType.MoveL || (softLimit == false && moveType == MoveType.MoveJ)
            %% Trajectory Joint n
%             k=0;
            for n = 1 : length(S1)    
                % constant velocity YES or No
                if h(n) == 0
                    s(n,1) = S2(n);
                    ds(n,1) = 0;
                    dds(n,1) = 0;
                elseif abs(h(n)) > abs(vj(n)^2/aj(n)) 
                    %constant velocity interpolation
                    if  t <= Ta_final 
                        s(n,1) = S1(n) + 0.5 * aj(n) * t^2; % acceleration phase 
                        ds(n,1) = aj(n)*t;
                        dds(n,1) = aj(n);
                    elseif Ta_final < t && t <= (T_final- Ta_final)
                        s(n,1) = S1(n) + aj(n)*Ta_final * (t - Ta_final/2); % constant velocity phase
                        ds(n,1) = vj(n);
                        dds(n,1) = 0;
                    elseif (T_final-Ta_final) < t && t <= T_final
                        s(n,1) = S2(n) - 0.5 * aj(n) * (T_final - t)^2; %deceleration phase
                        ds(n,1) = vj(n)-aj(n)*(t-(T_final-Ta_final));
                        dds(n,1) = -aj(n);
                    else
                        s(n,1) = s_old(n,1);
                        ds(n,1) = ds_old(n,1); 
                        dds(n,1) = dds_old(n,1); 
                    end
                else
                    % no constant velocity interpolation
                    vj(n) = aj(n)*Ta_final; % maximum velocity peak
                    if 0 <= t && t <= Ta_final
                        s(n,1) = S1(n) + 0.5 * aj(n) * t^2; %acceleration phase
                        ds(n,1) = aj(n)*t;
                        dds(n,1) = aj(n);
                    elseif (T_final-Ta_final) < t && t <= T_final
                        s(n,1) = S2(n) - 0.5 * aj(n) * (T_final - t)^2; % deceleration phase
                        ds(n,1)= aj(n)*Ta_final-aj(n)*(t-(T_final-Ta_final));
                        dds(n,1) = -aj(n);
                    else
                        s(n,1) = s_old(n,1); 
                        ds(n,1) = ds_old(n,1); 
                        dds(n,1) = dds_old(n,1); 
                    end
                end
            end

  %% Solution to Softlimit violation  
%           elseif softLimit == true && moveType == MoveType.MoveJ
              
              % Bedingung bevor gestoppt wurde
              
%               if ~isequal(stopped_old,[true; true; true])
%                   if k == 0
%                       Ta_final = max(ds_old./aj);
%                       vmax = ds_old;
%                       S3 = s_old + ds_old.*Ta_final/2;
%                       h = S3-s_old;
%                       tsl1 = 0;
%                       tsl2 = 0;
%                   end
%                 
%                   for n = 1 :length(S1)
%                       if h(n)<0
%                           aj(n)= -aj(n);
%                           vj(n)= -vj(n);
%                       end
%                       s(n,1) = S3(n) - 0.5 * aj(n) * (Ta_final - tsl1)^2; %deceleration phase
%                       ds(n,1) = vmax(n)-aj(n)*tsl1;
%                       dds(n,1) = -aj(n);
%                       stopped_old(n,1) = abs(S3(n,1) - s_old(n,1)) <= tolerance;
%                   end
%                    k = k+1;
%                    tsl1 = tsl1 + 0.01;
%                     
% %                          s = s_old;
% %                          ds = [0;0;0];
% %                          dds = [0;0;0];
% %                          stopped_old = [true; true; true];
% %                          S3 = s_old;
% %                          tsl = 0;
%                      
%               elseif isequal(stopped_old,[true; true; true])
%                   k = 0;
%                   %% An Taktzeit anpassen
%                   tsl2 = tsl2 + 0.01;
%                   % Bedingung wann gestoppt ist
%                   h = S2-S3;
%                   v_ss = 0.05;
%                   a_ss = 0.1;
%                   moveType_sl = MoveType.MoveL;
%                   [Ta,T,vj,aj,vmax,amax] = obj.calcBoundaryConditions( h, v_ss, a_ss, moveType_sl );
%                   % Lineare Interpolation von S3 weg
%                   Ta_final = max( Ta );
%                   T_final = max( T );
%                   vj(j) = abs(vmax(j));
%                   aj(j) = abs(amax(j));
%                   
%                   if h(j) < 0
%                       vj(j) = -vj(j);
%                       aj(j) = -aj(j);
%                   end
%                   
%                   for n = 1 : length(S3)
%                       % constant velocity YES or No
%                       if abs(h(n)) > abs(vj(n)^2/aj(n))
%                           %constant velocity interpolation
%                           if  t <= Ta_final
%                               s(n,1) = S3(n) + 0.5 * aj(n) * tsl2^2; % acceleration phase
%                               ds(n,1) = aj(n)*tsl2;
%                               dds(n,1) = aj(n);
%                           elseif Ta_final < tsl2 && tsl2 <= (T_final- Ta_final)
%                               s(n,1) = S3(n) + aj(n)*Ta_final * (tsl2 - Ta_final/2); % constant velocity phase
%                               ds(n,1) = vj(n);
%                               dds(n,1) = 0;
%                           elseif (T_final-Ta_final) < tsl2 && tsl2 <= T_final
%                               s(n,1) = S2(n) - 0.5 * aj(n) * (T_final - tsl2)^2; %deceleration phase
%                               ds(n,1) = vj(n)-aj(n)*(tsl2-(T_final-Ta_final));
%                               dds(n,1) = -aj(n);
%                           end
%                       else
%                           % no constant velocity interpolation
%                           vj(n) = aj(n)*Ta_final; % maximum velocity peak
%                           if 0 <= tsl2 && tsl2 <= Ta_final
%                               s(n,1) = S3(n) + 0.5 * aj(n) * tsl2^2; %acceleration phase
%                               ds(n,1) = aj(n)*tsl2;
%                               dds(n,1) = aj(n);
%                           elseif (T_final-Ta_final) < tsl2 && tsl2 <= T_final
%                               s(n,1) = S2(n) - 0.5 * aj(n) * (T_final - tsl2)^2; % deceleration phase
%                               ds(n,1)= aj(n)*Ta_final-aj(n)*(tsl2-(T_final-Ta_final));
%                               dds(n,1) = -aj(n);
%                           end
%                       end
%                   end
%               end
%           end
              % Remember results and updated values for next run
          s_old = s;
          ds_old = ds;
          dds_old = dds;
         
              %% Check if position has already reached stop within tolerance
            for n = 1 : length(S1)
                reached(n,1) = abs(S2(n,1) - s_old(n,1)) <= tolerance;
            end
              % zurücksetzen der stopped Variable
%             if isequal(reached,[true;true;true])
%                 stopped_old = [false;false;false];
%             end
        end
    end
      
end



