classdef XilMode < Simulink.IntEnumType
    enumeration
        MiL(0)
        SiL(1)
        HiL(2)
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            