classdef ParameterType < Simulink.IntEnumType
    enumeration
        v_max(0)
        a_max(1)
        dq_max(2)
        ddq_max(3)
        z_max(4)
        homeX(5)
        homeY(6)
        homeZ(7)
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            