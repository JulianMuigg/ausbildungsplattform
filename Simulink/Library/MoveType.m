classdef MoveType < Simulink.IntEnumType
    enumeration
        Undefined(0)
        MoveL(1)            % Task space movement
        MoveJ(2)            % Joint space movement  
        MoveExternal(3)     % Uses external trajectory for movement
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            
            
            