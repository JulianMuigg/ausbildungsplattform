classdef RobotType < Simulink.IntEnumType
    enumeration
        Scara(0) 
        Delta360(1)
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            
            
            