classdef SeqState < Simulink.IntEnumType
    enumeration
        Undefined(0)
        SafetyError(1)
        Stopped(2)        
        Idle(3)
        WaitForRobot(4)
        Jogging(5)
        Calibration(6)
        Automatic(7)
        ExternalTrajectory(8)
    end
    % Needed for code generation to avoid "Potential conflicting usages..."
    methods (Static = true)
        function retVal = addClassNameToEnumNames()
          retVal = true;
        end
    end
end
            
            
            
            