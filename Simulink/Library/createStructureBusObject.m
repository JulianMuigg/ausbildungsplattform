function [busObject] = createStructureBusObject(struct)
%CREATEBUSOBJECT function to create a bus object with strcut

busInfo = Simulink.Bus.createObject(struct);
busObject = evalin('base', busInfo.busName);
evalin('base', 'clear slBus1' );

end

