#pragma once

#include <QObject>
#include <QGraphicsScene>
#include <QVector3D>
#include "Object.h"

enum class RobotType;

class PickAndPlaceApp : public QObject
{
	Q_OBJECT

public:
	PickAndPlaceApp( QGraphicsScene* scene, QObject *parent = Q_NULLPTR);
	~PickAndPlaceApp();

	virtual void generate( RobotType robotType ) = 0;
	virtual QVector3D nextPickPosition( bool& valid ) = 0;
	virtual QVector3D nextPlacePosition( bool& valid ) = 0;
	QPair<QVector3D, QVector3D> nextPosition( bool& valid );
	void select( const QPointF& point );
	void setSceneRect( const QRectF& sceneRect ) { sceneRect_ = sceneRect; }
	virtual QString name() = 0;

	virtual void hideItems() = 0;


protected:
	QGraphicsScene* scene_ = nullptr;
	QRectF sceneRect_;

	QVector<Object*> objects_;
	Object* selectedObj_ = nullptr;
	int currentIndex_ = INT_MAX;
	
	QGraphicsItem* selection_ = nullptr;
	double pickTolerance_ = 512 / 1.9;
};
