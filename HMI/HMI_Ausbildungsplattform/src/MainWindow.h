#pragma once

#include <QtWidgets/QMainWindow>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>
#include <QTimer>
#include <QVariant>
#include "ui_MainWindow.h"

#include "Controller.h"
#include "JoggingWidget.h"
#include "AutomaticWidget.h"
#include "ParameterWidget.h"
#include "CalibrationWidget.h"
#include "LoadTrajectoryWidget.h"

enum class Command;
enum class State;
enum class Parameter;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QString netId, QWidget *parent = Q_NULLPTR);
    void updateTitle( RobotType type );
public slots:
    // Actions triggered
    void on_actionHome_triggered();
    void on_actionStop_triggered();
    void on_actionJogging_triggered();
    void on_actionCalibration_triggered();
    void on_actionAutomatic_triggered();
    void on_actionLoadTrajectory_triggered();
    void on_actionErrorQuit_triggered();

    // From Controller
    void onStateChanged( const State& state );
    void onRobotErrorChanged( const QString& error );
    void onPositionRequest( bool on );
    void onCurrentPosChanged( const double& x, const double& y, const double& z );
    void onParameterChanged( const Parameter& param, const double& val );
    void onConnectionChanged( bool connected );

    // From application Widgets
    void onJoggingMove( double x, double y, double z );
    void onParameterChangedLocal( Parameter type, double value );
    void onTouchEventTriggerd();
    void onCalibrationFinished( const QVector3D& offset, const QQuaternion& q, double mmPerPixel );
    void onSetCurrentPosAsHome();

signals:
    void transitionChanged( const Transition& transition );
    void positionRequest( const QVector3D& pos1, const QVector3D& pos2 );
    void parameterChanged( const Parameter& param, const double& val );
    void touchEventTriggered( bool on );
    void externalTrajectory( const QString& fileName, const QVector<QVector<double>>& trajectory );

private:
    // GUI widgets
    Ui::MainWindowClass ui_;
    QComboBox* appSelection_ = new QComboBox( this );
    QPushButton* connectionLabel_ = new QPushButton( this );
    QLabel* stateLabel_ = new QLabel( this );
    QLabel* robotErrorLabel_ = new QLabel( this );
    QLabel* robotPosition_ = new QLabel( this );
    QLabel* lastPositionRequest_ = new QLabel( this );
    QLabel* calibrationData_ = new QLabel( this );
    QWidget* secondWindow_ = new QWidget( this );

    // Application objects and widget
    Controller* controller_ = nullptr;
    JoggingWidget* jogging_ = new JoggingWidget( this );
    AutomaticWidget* automatic_ = new AutomaticWidget( this );
    ParameterWidget* parameter_ = new ParameterWidget( this );
    CalibrationWidget* calibration_ = new CalibrationWidget( this );
    LoadTrajectoryWidget* loadTrajectory_ = new LoadTrajectoryWidget( this );

    // Application values
    State currentState_;
    QVector3D currentPosition_ = QVector3D( 0, 0, 0 );    
    QVector3D currentJoggingPosition_ = QVector3D( 0, 0, 0 );

    // Others
    QTimer setHomePosTimer_;
};
