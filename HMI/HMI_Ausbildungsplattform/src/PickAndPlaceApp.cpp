#include "PickAndPlaceApp.h"
#include <QDebug>
#include "Controller.h"


PickAndPlaceApp::PickAndPlaceApp(QGraphicsScene* scene, QObject *parent) : QObject(parent)
{
    scene_ = scene;
}

PickAndPlaceApp::~PickAndPlaceApp()
{
    /*if( selection_ )
        delete selection_;

    for( auto obj : objects_ )
        delete obj;*/
}

void PickAndPlaceApp::select( const QPointF& point )
{
    bool selectionChanged = false;
    for( auto obj : objects_ )
    {
        auto pos = obj->sceneCenterPos(); //ui_.graphicsView->mapFromScene( obj->sceneCenterPos() );
        qDebug() << pos << point << (pos - point).manhattanLength() << "<" << pickTolerance_;

        if( (pos - point).manhattanLength() < pickTolerance_ )
        {
            selectionChanged = true;
            selectedObj_ = obj;
            obj->setSelected( true );
            qDebug() << "Object selected " << (pos - point).manhattanLength();
        }
        else
        {
            obj->setSelected( false );
        }
    }

    if( selectedObj_ )
    {    // Place selected object at new position
        auto br = selectedObj_->boundingRect();
        if( !selectionChanged )
        {
            double rotation = selectedObj_->rotation();
            selectedObj_->setRotation( 0 );
            selectedObj_->setPos( point.toPoint() - QPoint( br.width() / 2, br.height() / 2 ) );
            selectedObj_->setRotation( rotation );

            selectedObj_ = nullptr;
            selection_->hide();
            qDebug() << "Object placed";
        }
        else // Set circle around selected object
        {
            selection_->show();
            auto cr = selection_->boundingRect();

            selection_->setPos( selectedObj_->pos() - QPoint( (cr.width() - br.width()) / 2, (cr.height() - br.height()) / 2 ) );
            selectedObj_->setPicked();
            qDebug() << "Object picked";
        }
    }
    else
    {
        selection_->hide();
    }
}

QPair<QVector3D,QVector3D> PickAndPlaceApp::nextPosition( bool& valid )
{
    bool pickValid = false;
    bool placeValid = false;
    
    QVector3D pickPos = nextPickPosition( pickValid );
    QVector3D placePos = nextPlacePosition( placeValid );
    
    valid = pickValid && placeValid;

    if( !valid )
        qDebug() << "Automatic app finished";

    currentIndex_++;

    return qMakePair<QVector3D, QVector3D>( pickPos, placePos );
}


