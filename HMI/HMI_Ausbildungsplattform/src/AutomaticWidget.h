#pragma once

#include <QWidget>
#include "ui_AutomaticWidget.h"
#include "Object.h"
#include "PickAndPlaceApp.h"

enum class RobotType;

class AutomaticWidget : public QWidget
{
	Q_OBJECT

public:
	AutomaticWidget(QWidget *parent = Q_NULLPTR);
	~AutomaticWidget();

	void start( RobotType robotType );
	void selectObject( const QPointF& point );
	QPair<QVector3D, QVector3D> nextPositionPair( bool& valid, RobotType robotType );

	QStringList appList();
	void setApp( const QString& name );

protected:
	void resizeEvent( QResizeEvent* );
	void showEvent( QShowEvent* );
	void fitView();

	virtual bool eventFilter( QObject* obj, QEvent* event ) override;

signals:
	void touchEventTriggered();

private:
	Ui::AutomaticWidget ui_;
	QGraphicsScene* scene_ = nullptr;
	QRectF rect_;
	QVector<PickAndPlaceApp*> apps_;
	PickAndPlaceApp* currentApp_;
};
