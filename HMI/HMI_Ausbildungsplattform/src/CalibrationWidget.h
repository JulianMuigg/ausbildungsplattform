#pragma once

#include <QWidget>
#include <QMatrix4x4>
#include <Qt3DCore/QTransform>
#include "ui_CalibrationWidget.h"
#include "Arrow.h"

struct CalibrationData
{
	double mmPerPixel_ = 0; // mm per pixel
	QVector3D offset_; // x,y,z in meter
	QQuaternion q_; // rotation as quaternion
};

class CalibrationWidget : public QWidget
{
	Q_OBJECT

public:
	CalibrationWidget(QWidget *parent = Q_NULLPTR);
	~CalibrationWidget();

	void touchEvent( const QPointF& pos );
	void start();
	QPair<QVector3D, QVector3D> nextPosition( bool& valid );
	void setLastZPosition( double val ) { lastZPosition_ = val; }


	QVector3D translatePosition( const QVector3D& val );
	CalibrationData& data() { return data_; }

protected:
	void resizeEvent( QResizeEvent* );
	void showEvent( QShowEvent* );
	void fitView();

	virtual bool eventFilter( QObject* obj, QEvent* event ) override;

signals:
	void touchEventTriggered();
	void calibrationFinished( const QVector3D& offset, const QQuaternion& q, double mmPerPixel );
	void resizeTriggered( const QRectF& rect );

private:
	// GUI objects
	Ui::CalibrationWidget ui_;

	// Display output and touch input
	QGraphicsScene* scene_ = nullptr;	
	QGraphicsEllipseItem* circleCenter_ = nullptr;
	Arrow* arrowX_ = nullptr;
	Arrow* arrowY_ = nullptr;

	// Data
	CalibrationData data_;

	// Internal needed values
	unsigned int index_ = 0;
	double lastZPosition_ = 0;
	QVector<QVector3D> touchPositions_;
};
