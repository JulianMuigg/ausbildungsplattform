#include <QRandomGenerator>
#include <qdebug.h>
#include <QMouseEvent>
#include "TrayPickAndPlace.h"
#include "Controller.h"


TrayPickAndPlace::TrayPickAndPlace(QGraphicsScene* scene, QObject *parent) : PickAndPlaceApp(scene, parent)
{
    pickArea_ = new QGraphicsEllipseItem( 0, 0, 0, 0 );
    pickArea_->setBrush( Qt::lightGray );
    pickArea_->hide();
    scene_->addItem( pickArea_ );

    trayNuts_ = new Object( ":/MainWindow/tray.png", QPointF( 100, 100 ), Qt::white, 0, 0 );
    scene_->addItem( trayNuts_ );

    trayGears_ = new Object( ":/MainWindow/tray.png", QPointF( 100, 1400 ), Qt::white, 0, 0 );
    scene_->addItem( trayGears_ );

    QGraphicsEllipseItem* circle = new QGraphicsEllipseItem( 0, 0, 600, 600 );
    circle->setBrush( QColor( 0, 73, 130 ) );
    circle->hide();
    scene_->addItem( circle );
    selection_ = circle;
}

TrayPickAndPlace::~TrayPickAndPlace()
{
}

void TrayPickAndPlace::generate( RobotType robotType )
{
    int count = 20;
    scene_->setBackgroundBrush( QBrush( Qt::gray ) );

    // Show all objects
    for( auto obj : objects_ )
        obj->show();

    trayNuts_->show();
    trayGears_->show();

    // Use current object positions, if automatic mode has not finished
    if( currentIndex_ < count )
    {
        if( selectedObj_ )
        {
            selectedObj_->setPicked( false );
            currentIndex_--;
            selection_->hide();
        }

        //return;
    }

    // Clear positions
    trayNutsPositions_.clear();
    trayGearsPositions_.clear();
    
    // Show all objects
    for( auto obj : objects_ )
    {
        obj->setPos( 0, 0 );
        obj->setPicked( false );
    }

    // Create pick area
    double d = (sceneRect_.height() < sceneRect_.width() ? sceneRect_.height() : sceneRect_.width()) * 0.65;
    QPointF pickAreaCenter = QPointF( sceneRect_.width() * 0.7, sceneRect_.height() * 0.3 );

    switch( robotType )
    {
    case RobotType::Scara:
        pickAreaCenter = QPointF( sceneRect_.width() * 0.80, sceneRect_.height() * 0.5 );
        break;
    }

    pickArea_->setRect( 0, 0, d, d );
    pickArea_->setPos( pickAreaCenter.x() - d / 2, pickAreaCenter.y() - d / 2 );
    pickArea_->show();

    for( int i = 0; i < count; i++ )
    {
        // Generate object, if needed
        if( objects_.size() < (i + 1) )
        {
            if( i%2 == 0 )
                objects_.push_back( new Object( ":/MainWindow/nut.png", QPointF( 0, 0 ), Qt::blue, 0, 0 ) );
            else
                objects_.push_back( new Object( ":/MainWindow/gear.png", QPointF( 0, 0 ), Qt::blue, 0, 0 ) );
            scene_->addItem( objects_.last() );
        }        

        // Position object without collision to another one
        bool isValid = true;
        do
        {
            isValid = true;
            QPointF pos = QPointF( QRandomGenerator::global()->bounded( int( sceneRect_.width() * 0.1 ), int( sceneRect_.width() * 0.9 ) ), QRandomGenerator::global()->bounded( int( sceneRect_.height() * 0.10 ), int( sceneRect_.height() * 0.90 ) ) );
            objects_[i]->setPos( pos );

            for( int j = 0; j < i; j++ )
            {
                if( objects_.at( j )->collidesWithItem( objects_[i] ) )
                {
                    isValid = false;
                    break;
                }
            }

            // Check if object is in pick area
            if( (objects_.at( i )->sceneCenterPos() - pickAreaCenter).manhattanLength() > ( d * 0.5 - 260 ) )
                isValid = false;

        } while( !isValid );
    }

    // Set position of trays and define place positions
    QRectF trayRect = trayNuts_->boundingRect();
    trayNuts_->setPos( sceneRect_.width() * 0.39 - trayRect.width() * 0.5, sceneRect_.height() * 0.5 - trayRect.height() * 0.5 );
    trayGears_->setPos( sceneRect_.width() * 0.45 - trayRect.width() * 0.5, sceneRect_.height() * 0.75 -trayRect.height() * 0.5 );
    switch( robotType )
    {
    case RobotType::Scara:
        trayNuts_->setPos( sceneRect_.width() * 0.15 - trayRect.width() * 0.5, sceneRect_.height() * 0.35 - trayRect.height() * 0.5 );
        trayGears_->setPos( sceneRect_.width() * 0.15 - trayRect.width() * 0.5, sceneRect_.height() * 0.65 - trayRect.height() * 0.5 ); 
        break;
    }

    for( int i = 0; i < 5; i++ )
    {
        for( int j = 0; j < 2; j++ )
        {
            trayNutsPositions_.push_back( QVector3D( trayNuts_->pos() + QPointF( 256 + 40 + i * (520 + 40), 256 + 40 + j * (520 + 40) ) ) );
            trayGearsPositions_.push_back( QVector3D( trayGears_->pos() + QPointF( 256 + 40 + i * (520 + 40), 256 + 40 + j * (520 + 40) ) ) );
        }
    }

    currentIndex_ = 0;
}

QVector3D TrayPickAndPlace::nextPickPosition( bool& valid )
{
    currentObject_ = nullptr;
    valid = false;
    if( objects_.size() > currentIndex_ )
    {
        // Get nearest object
        auto pos1 = QPointF( DBL_MAX, DBL_MAX );
        for( auto obj : objects_ )
        {
            auto temp = obj->sceneCenterPos();
            if( !obj->isPicked() && pos1.manhattanLength() > temp.manhattanLength() )
            {
                pos1 = temp;
                currentObject_ = obj;
            }
        }
        valid = true;
        return QVector3D( pos1.x(), pos1.y(), 0 );
    }

    return QVector3D();
}

QVector3D TrayPickAndPlace::nextPlacePosition( bool& valid )
{
    QVector3D ret;
    valid = false;
    if( currentObject_ )
    {
        if( objects_.indexOf( currentObject_ ) % 2 == 0 && !trayNutsPositions_.isEmpty() )
        {
            ret = trayNutsPositions_.first();
            trayNutsPositions_.pop_front();
            valid = true;
        }
        else if( objects_.indexOf( currentObject_ ) % 2 != 0 && !trayGearsPositions_.isEmpty() )
        {
            ret = trayGearsPositions_.first();
            trayGearsPositions_.pop_front();
            valid = true;
        }
    }
    return ret;
}

void TrayPickAndPlace::hideItems()
{
    for( auto obj : objects_ )
        obj->hide();

    trayNuts_->hide();
    trayGears_->hide();
    selection_->hide();
    pickArea_->hide();
}
