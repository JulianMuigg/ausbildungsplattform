#include <QMessageBox>

#include "TcpServer.h"

#include <qstring.h>



TcpServer::TcpServer( QObject* parent ) : Controller( parent )
{

	connect( server_, &QTcpServer::newConnection, this, &TcpServer::onNewConnection );

	server_->listen( QHostAddress::Any, 4242 );
	
}

void TcpServer::onTransitionChanged( const Transition& transition )
{
    QString s = commandName_[Command::Transition] + " = " + transitionName_[transition];
    write( s );
}

void TcpServer::onPositionRequest( const QVector3D& pos1, const QVector3D& pos2 )
{
    QString s = commandName_[Command::PositionRequest] + " = ";
    s += QString( "%1; %2; %3; %4; %5; %6" )
        .arg( pos1.x(), 0, 'f', 4 )
        .arg( pos1.y(), 0, 'f', 4 )
        .arg( pos1.z(), 0, 'f', 4 )
        .arg( pos2.x(), 0, 'f', 4 )
        .arg( pos2.y(), 0, 'f', 4 )
        .arg( pos2.z(), 0, 'f', 4 );

    write( s );
}

void TcpServer::onParameterChanged( const Parameter& param, const double& val )
{
    QString s = commandName_[Command::Parameter] + " = ";
    s += QString( "%1; %2; 0; 0; 0; 0" ).arg( int( param ) ).arg( val, 0, 'f', 4 );
    write( s );
}

void TcpServer::onTouchEventTriggered( bool on )
{
    QString s = commandName_[Command::TouchEvent] + " = " + QString::number( int( on ) );
    write( s );
}

void TcpServer::onExternalTrajectory( const QString& fileName, const QVector<QVector<double>>& trajectory )
{
    QString s = commandName_[Command::ExternalTrajectory] + " = " + fileName;

    if( s.size() > 64 )
        QMessageBox::critical( nullptr, "Filename/path to long", "Filename is too long, please move the file to a shorter path" );
    else 
        write( s );
}

void TcpServer::onNewConnection()
{
    // Connect only, if no socket is connected until now
    if( !socket_ )
    {
        QTcpSocket* clientSocket = server_->nextPendingConnection();
        connect( clientSocket, &QTcpSocket::readyRead, this, &TcpServer::onReadyRead );
        connect( clientSocket, &QTcpSocket::stateChanged, this, &TcpServer::onSocketStateChanged );
        socket_ = clientSocket;
        emit connectionChanged( true );

        socket_->write( QByteArray::fromStdString( QString( commandName_[Command::Parameter] + "\n" ).toStdString() ) );
    }
}

void TcpServer::onSocketStateChanged( QAbstractSocket::SocketState socketState )
{
    // Delete socket, if exists and is unconnected
    if( socketState == QAbstractSocket::UnconnectedState && socket_ )
    {
        socket_->deleteLater();
        socket_ = nullptr;
        emit connectionChanged( false );
    }
}

void TcpServer::onUpdateZeroOffset( const QRectF& size )
{
    zeroOffsetMeter_ = QPointF( size.width() * 0.5 * MM_PER_PIXEL, size.height() * 0.5 * MM_PER_PIXEL );
    zeroOffsetMeter_ /= 1000.0;
}

void TcpServer::onReadyRead()
{
    QTcpSocket* sender = static_cast<QTcpSocket*>(QObject::sender());
    QByteArray data = sender->readAll();

    QString dataStr = data.toStdString().c_str();
    if( !dataStr.isEmpty() )
    {
        qDebug() << "Received data:" << dataStr;

        auto stringList = dataStr.split( "=" );
        Command param = commandName_.key( stringList.first(), Command::Undefined );
        QVariant value = stringList.last();

        

        // Simulation of touch event depending on z position
        if( stringList.first() == "State" )
        {
            emit stateChanged( QVariant( value ).value<State>() );
        }
        else if( stringList.first() == "RobotError" )
        {
            emit robotErrorChanged( errorName_[QVariant( value ).value<ErrorType>()] );
        }
        else if( stringList.first() == "CurrentPos" )
        {
            auto val = value.toString().split( ";" );
            emit currentPosChanged( val.at( 0 ).toDouble(), val.at( 1 ).toDouble(), val.at( 2 ).toDouble() );
        }
        else if( stringList.first() == "TouchEvent" )
        {
            auto pos = value.toString().split( ";" );
            if( pos.size() >= 3 )
            {
                double x = (-pos.at( 0 ).toDouble() + zeroOffsetMeter_.x()) / (mmPerPixel_ / 1000.0);
                double y = (-pos.at( 1 ).toDouble() + zeroOffsetMeter_.y()) / (mmPerPixel_ / 1000.0);
                emit simulateTouchEvent( QPointF( x, y ) );

            }
        }
        else if( stringList.first() == "Parameter" )
        {
            auto params = value.toString().split( ";" );

            for( int i = 0; i < params.size(); i++ )
            emit parameterChanged( Parameter( i ), params[i].toDouble() );
        }
        else if( stringList.first() == "PositionRequest" )
        {
            emit positionRequest( value.toBool() );
        }
        else if( stringList.first() == "RobotType" )
        {
            robotType_ = value.value<RobotType>();
            qDebug() << dataStr;
        }
        else if( param != Command::Undefined )
        {
            qDebug() << "Command undefined";
        }
    }
}

/*virtual*/ void TcpServer::write( const QString& str ) /*override*/
{
    if( socket_ )
    {
        socket_->write( QByteArray::fromStdString( QString( str + "\n" ).toStdString() ));
        qDebug() << QString( "Write command " + str );
    }
}

