#include "Controller.h"



Controller::Controller( QObject* parent /*= nullptr*/ )
{
    // Parameter definition
    parameter_[Command::Transition] = QVariant::fromValue<Transition>( Transition::Undefined );
    parameter_[Command::State] = QVariant::fromValue<State>( State::Undefined );

    // Parameter name
    commandName_[Command::Undefined] = "Undefined";
    commandName_[Command::Transition] = "Transition";
    commandName_[Command::State] = "State";
    commandName_[Command::PositionRequest] = "PositionRequest";
    commandName_[Command::CurrentPos] = "CurrentPos";
    commandName_[Command::Parameter] = "Parameter";
    commandName_[Command::TouchEvent] = "TouchEvent";
    commandName_[Command::ExternalTrajectory] = "ExternalTrajectory";

    // Transition name
    transitionName_[Transition::Undefined] = "Undefined";
    transitionName_[Transition::Home] = "Home";
    transitionName_[Transition::StartJogging] = "StartJogging";
    transitionName_[Transition::StartCalibration] = "StartCalibration";
    transitionName_[Transition::StartAutomatic] = "StartAutomatic";
    transitionName_[Transition::StartExternalTrajectory] = "StartExternalTrajectory";
    transitionName_[Transition::Stop] = "Stop";
    transitionName_[Transition::ErrorQuit] = "ErrorQuit";

    errorName_[ErrorType::NoError] = "NoError";
    errorName_[ErrorType::Singularity] = "Singularity";
    errorName_[ErrorType::NotReachable] = "NotReachable";
    errorName_[ErrorType::AxisLimitError] = "AxisLimitError";
    errorName_[ErrorType::HardStopError] = "HardStopError";
}
