#include <QRandomGenerator>
#include <qdebug.h>
#include <QMouseEvent>
#include <QtMath>
#include "CalibrationWidget.h"


CalibrationWidget::CalibrationWidget(QWidget *parent)
	: QWidget(parent)
{
	ui_.setupUi(this);

    scene_ = new QGraphicsScene( this );
    ui_.graphicsView->setScene( scene_ );   
    scene_->setBackgroundBrush( Qt::gray );

    ui_.graphicsView->installEventFilter( this );
    scene_->installEventFilter( this );

    circleCenter_ = new QGraphicsEllipseItem( 0, 0, 300, 300 );
    circleCenter_->setBrush( Qt::white );
    scene_->addItem( circleCenter_ );

    arrowX_ = new Arrow( QPoint( 0, 0 ), QPoint( 0, 0 ), "X" );
    scene_->addItem( arrowX_ );

    arrowY_ = new Arrow( QPoint( 0, 0 ), QPoint( 0, 0 ), "Y" );
    scene_->addItem( arrowY_ );
}

CalibrationWidget::~CalibrationWidget()
{
}

void CalibrationWidget::touchEvent( const QPointF& pos )
{
    if( isVisible() )
        emit touchEventTriggered();
    // Collect touch positions for calibration
    if( index_ < 3 )
        touchPositions_.push_back( QVector3D( pos.x(), pos.y(), lastZPosition_ ) );

    QPointF scenePos = ui_.graphicsView->mapToScene( pos.toPoint() );
    // Draw coordinate frame
    switch( index_ )
    {
    case 0:
        arrowX_->setEnd( scenePos.toPoint() );
        arrowX_->show();
        qDebug() << "X-position:" << touchPositions_[0];
        break;
    case 1:
        circleCenter_->setPos( scenePos - circleCenter_->boundingRect().bottomRight() / 2 );
        circleCenter_->show();
        arrowX_->setStart( scenePos.toPoint() );
        arrowY_->setStart( scenePos.toPoint() );
        qDebug() << "Cenerposition:" << touchPositions_[1];
        break;
    case 2:
        arrowY_->setEnd( scenePos.toPoint() );
        arrowY_->show();
        qDebug() << "Y-position:" << touchPositions_[2];
        break;
    default:
        index_--;
    }

    scene_->update();
    index_++;
}

void CalibrationWidget::start() 
{
    index_ = 0; 
    circleCenter_->hide();
    arrowX_->hide();
    arrowY_->hide();
    scene_->update();
    touchPositions_.clear();
}

void CalibrationWidget::resizeEvent( QResizeEvent* ) 
{
   fitView();
}

void CalibrationWidget::showEvent( QShowEvent* ) 
{
    fitView();
}

void CalibrationWidget::fitView() 
{
    QRectF rect = ui_.graphicsView->rect();
    rect = QRect( 0, 0, 1200 * 20, rect.height() / rect.width() * 1200 * 20 );
    ui_.graphicsView->fitInView( rect, Qt::KeepAspectRatio );
    ui_.graphicsView->setSceneRect( rect );

    emit resizeTriggered( ui_.graphicsView->rect() );
}

bool CalibrationWidget::eventFilter( QObject* obj, QEvent* event )
{
    if( obj == ui_.graphicsView )
    {
        if( event->type() == QEvent::MouseButtonPress )
        {
            QMouseEvent* me = static_cast<QMouseEvent*>(event);
            auto point = me->pos();

            touchEvent( point );
        }
    }
    return QWidget::eventFilter( obj, event );
}

QPair<QVector3D,QVector3D> CalibrationWidget::nextPosition( bool& valid )
{
    QPair<QVector3D, QVector3D> ret;
    valid = true;
    
    switch( index_ )
    {
    case 0: // x axis
        ret.first.setX( 0.075 );
        ret.first.setY( 0 );
        ret.first.setZ( 0 );
        break;
    case 1: // Centerpoint
        ret.first.setX( 0 );
        ret.first.setY( 0 );
        ret.first.setZ( 0 );
        break;
    case 2: // y axis
        ret.first.setX( 0 );
        ret.first.setY( 0.075 );
        ret.first.setZ( 0 );
        break;        
    default:
        valid = false;
    }

    if( index_ == 3 )
    {
        double mmPerPixelx = abs( 75.0 / (touchPositions_[0].x() - touchPositions_[1].x()) );
        double mmPerPixely = abs( 75.0 / (touchPositions_[2].y() - touchPositions_[1].y()) );
        data_.mmPerPixel_ = (mmPerPixelx + mmPerPixely) * 0.5;
        qDebug() << "mm per pixel:" << data_.mmPerPixel_;

        QVector3D offsetInPixel = touchPositions_[1];
        offsetInPixel.setZ( offsetInPixel.z() * 1000.0 / data_.mmPerPixel_ );
        qDebug() << "Offset in pixel:" << offsetInPixel;

        data_.offset_ = offsetInPixel * data_.mmPerPixel_ / 1000.0;
        qDebug() << "Offset in mm:" << data_.offset_;

        QVector3D xAxis = (touchPositions_[0] - touchPositions_[1]).normalized();
        QVector3D yAxis = (touchPositions_[2] - touchPositions_[1]).normalized();
        QVector3D zAxis = QVector3D::crossProduct( xAxis, yAxis );
        qDebug() << "Axis: " << xAxis << yAxis << zAxis;

        data_.q_ = Qt3DCore::QTransform::fromAxes( xAxis, yAxis, zAxis );
        data_.q_ = data_.q_.inverted();
        qDebug() << "Quaternion:" << data_.q_;  

        emit calibrationFinished( data_.offset_, data_.q_, data_.mmPerPixel_ );
    }

    return ret;
}

QVector3D CalibrationWidget::translatePosition( const QVector3D& val )
{
    QVector3D ret = val;
    
    ret *= data_.mmPerPixel_ / 1000.0;   // Convert to meter
    ret -= data_.offset_;                // Translate position 
    ret = data_.q_.rotatedVector( ret ); // Rotate position
    
    qDebug() << "Translation from" << val << "to" << ret;

    return ret;
}
