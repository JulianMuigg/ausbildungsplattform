#include <QRandomGenerator>
#include <qdebug.h>
#include <QMouseEvent>
#include "AutomaticWidget.h"
#include "SimplePickAndPlace.h"
#include "TrayPickAndPlace.h"
#include "Controller.h"


AutomaticWidget::AutomaticWidget(QWidget *parent)
	: QWidget(parent)
{
	ui_.setupUi(this);

    scene_ = new QGraphicsScene( this );
    ui_.graphicsView->setScene( scene_ );   
    scene_->setBackgroundBrush( Qt::black );

    ui_.graphicsView->installEventFilter( this );
    scene_->installEventFilter( this );

    // Create apps
    apps_.push_back( new TrayPickAndPlace( scene_, this ) );
    apps_.push_back( new SimplePickAndPlace( scene_, this ) );
    
    currentApp_ = apps_.first();

    for( auto app : apps_ )
        app->hideItems();
}

AutomaticWidget::~AutomaticWidget()
{
}

void AutomaticWidget::resizeEvent( QResizeEvent* ) 
{
   fitView();
}

void AutomaticWidget::showEvent( QShowEvent* ) 
{
    fitView();
}

void AutomaticWidget::fitView() 
{
    QRect viewRect = ui_.graphicsView->rect();
    rect_ = viewRect;
    rect_ = QRect( 0, 0, 550 * 20, rect_.height() / rect_.width() * 550 * 20 );
    ui_.graphicsView->fitInView( rect_, Qt::KeepAspectRatio );
    ui_.graphicsView->setSceneRect( rect_ );

    for( auto app : apps_ )
        app->setSceneRect( rect_ );
}

void AutomaticWidget::selectObject( const QPointF& point )
{
    if( isVisible() )
        emit touchEventTriggered();

    if( currentApp_ )
        currentApp_->select( ui_.graphicsView->mapToScene( point.toPoint() ) );
}

bool AutomaticWidget::eventFilter( QObject* obj, QEvent* event )
{
    if( obj == ui_.graphicsView )
    {
        if( event->type() == QEvent::MouseButtonPress )
        {
            QMouseEvent* me = static_cast<QMouseEvent*>(event);
            auto point = me->pos();

            selectObject( point );
        }
    }
    return QWidget::eventFilter( obj, event );
}

void AutomaticWidget::start( RobotType robotType )
{
    for( auto app : apps_ )
        app->hideItems();

    if( currentApp_ )
        currentApp_->generate( robotType );
}

QPair<QVector3D,QVector3D> AutomaticWidget::nextPositionPair( bool& valid, RobotType robotType )
{
    if( currentApp_ )
    {
        QPair<QVector3D, QVector3D> pos;
        while( !valid )
        {
            pos = currentApp_->nextPosition( valid );
            if( !valid )
                currentApp_->generate( robotType );
        }

        auto pos1 = ui_.graphicsView->mapFromScene( pos.first.toPointF() );
        auto pos2 = ui_.graphicsView->mapFromScene( pos.second.toPointF() );
        pos.first = QVector3D( pos1.x(), pos1.y(), pos.first.z() );
        pos.second = QVector3D( pos2.x(), pos2.y(), pos.second.z() );

        return pos;
    }
    else
    {
        valid = false;
        return QPair<QVector3D, QVector3D>();
    }
}

QStringList AutomaticWidget::appList()
{
    QStringList ret;
    for( auto app : apps_ )
        ret.push_back( app->name() );

    return ret;
}

void AutomaticWidget::setApp( const QString& name )
{
    for( auto app : apps_ )
    {
        if( app->name() == name )
        {
            currentApp_ = app;
            break;
        }
    }
}

