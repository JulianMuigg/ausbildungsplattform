#include "Object.h"
#include <QtMath>


Object::Object( const QString& icon, QPointF position, QColor color, double angle, double speed )
    : position_( position ), color_( color ), angle_( angle ), speed_( speed )
{
    setRotation( angle_ );
    setPos( position_ );

    QPixmap pixmap = QPixmap( icon );
    pixmap.scaled( 20, 20, Qt::KeepAspectRatio );
    setPixmap( pixmap );
}

QPointF Object::sceneCenterPos()
{
    QPointF p = scenePos();
    QRectF rect = boundingRect();

    p = QPointF( p.x() + rect.width() * 0.5, p.y() + rect.height() * 0.5 );

    return p;
}

