#pragma once

#include <QWidget>
#include "ui_JoggingWidget.h"

class JoggingWidget : public QWidget
{
	Q_OBJECT

public:
	JoggingWidget(QWidget *parent = Q_NULLPTR);
	~JoggingWidget();

public slots:
	void on_btn_1mm_clicked( bool on );
	void on_btn_10mm_clicked( bool on );
	void on_btn_50mm_clicked( bool on );
	void on_btn_100mm_clicked( bool on );

	void on_btn_xPlus_clicked();
	void on_btn_xMinus_clicked();
	void on_btn_yPlus_clicked();
	void on_btn_yMinus_clicked();
	void on_btn_zPlus_clicked();
	void on_btn_zMinus_clicked();

	void on_btn_xPlus_yMinus_clicked();
	void on_btn_xPlus_yPlus_clicked();
	void on_btn_xMinus_yMinus_clicked();
	void on_btn_xMinus_yPlus_clicked();


	void on_btn_home_clicked();

signals:
	void move( double x, double y, double z );
	void setCurrentPosAsHome();

private:
	Ui::JoggingWidget ui_;
	double positionChange_ = 0;
};
