#pragma once

#include <QWidget>
#include <QTimer>
#include "Chart.h"
#include "ui_LoadTrajectoryWidget.h"

class LoadTrajectoryWidget : public QWidget
{
	Q_OBJECT

public:
	LoadTrajectoryWidget(QWidget *parent = Q_NULLPTR);
	~LoadTrajectoryWidget();

	void clear();

	void setStep1( const QString& text );

public slots:
	void on_btnLoad_clicked();
	void addPlotValue();
	void on_btnStart_clicked();

signals:
	void setTrajectory( const QString& fileName, const QVector<QVector<double>>& trajectory );
	void setCurrentPosAsHome();
	

protected:
	void loadFile( const QString& fileName );

private:
	Ui::LoadTrajectoryWidget ui_;
	Chart* chart_ = new Chart( "Trajectory", "t in s", "q in m or rad", false, this );
	QString fileName_;

	QVector<QVector<double>> trajectory_;

	// Internal values and objects
	QVector<QColor> colorList_;
	QTimer timer_;
	int index_ = 0;
	double maxY_ = DBL_MIN;
	double minY_ = DBL_MAX;
};
