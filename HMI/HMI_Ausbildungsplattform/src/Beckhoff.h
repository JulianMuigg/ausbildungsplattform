#pragma once
#include <qobject.h>
#include <QTimer>
#include "tc3manager.h"
#include "tc3value.h"
#include "Controller.h"

enum class HmiIn
{
	Undefined = -1,
	State = 0,
	PositionRequest,
	RobotOut
};

enum class HmiOut
{
	Transition = 0,
	TouchEvent,
	NextPosition,
	Parameter
};

namespace Plc
{
	enum MoveType
	{
		Undefined = 0,
		MoveL = 1,            // Task space movement
		MoveJ = 2,            // Joint space movement  
		MoveExternal = 3      // Uses external trajectory for movement
	};

	enum ControllerMode
	{
		Init = 0,
		Disabled = 1,
		Enabled = 2,
		SiL = 3,
		HiL = 4
	};

	enum MaestroMode
	{
		DisabledMaestro = 0,
		EnabledMaestro = 1,
		Execute = 2,
		DisabledSiL = 3
	};
	

	struct ParameterStruct
	{
		double v_max = 0;
		double a_max = 0;
		double dq_max = 0;
		double ddq_max = 0;
		double z_max = 0;
		double homeX = 0;
		double homeY = 0;
		double homeZ = 0;
		double request = 0;
	};

	struct PositionStruct
	{
		double x = 0;
		double y = 0;
		double z = 0;
	};

	struct MoveStruct
	{
		PositionStruct pos;
		double v = 0;
		double a = 0;
		MoveType moveType = MoveType::Undefined;
		double radius = 0;
	};

	struct RobotOutStruct
	{
		MoveStruct data;
		bool positionReached = false;
		ErrorType errorRobot = ErrorType::NoError;
	};

	struct CsvRowStruct
	{
		double t = 0;
		double q1 = 0;
		double q2 = 0;
		double q3 = 0;
	};

	struct ExternalTrajectoryStruct
	{
		CsvRowStruct trajectory[1000];
		double size = 0;
	};

	struct NextPositionStruct
	{
		PositionStruct pos1;
		PositionStruct pos2;
		bool newPos = false;
	};
}


class Beckhoff : public Controller
{
	Q_OBJECT
public:
	Beckhoff( const QString& amsNetId, QObject* parent = nullptr );
	~Beckhoff();

	bool isConnected() { return manager_->isConnected(); }
	void enableHardware( bool on );

public slots:
	virtual void onTransitionChanged( const Transition& transition ) override;
	virtual void onPositionRequest( const QVector3D& pos1, const QVector3D& pos2 ) override;
	virtual void onParameterChanged( const Parameter& param, const double& val ) override;
	virtual void onTouchEventTriggered( bool on ) override;
	virtual void onExternalTrajectory( const QString& fileName, const QVector<QVector<double>>& trajectory ) override;

	void onConnectionChanged( bool connected );
	virtual void init() override;
	void onError( QString msg );
	void onValueChanged( const QVariant& v );

	void resetNewPos();

private:
	Tc3Manager* manager_ = nullptr;
	QMap<HmiIn, Tc3Value*> hmiIn_;
	QMap<HmiOut, Tc3Value*> hmiOut_;
	Tc3Value* externalTrajectory_ = nullptr;
	Tc3Value* newPos_ = nullptr;
	Tc3Value* maestroEnable_ = nullptr;
	Tc3Value* controllerEnable_ = nullptr;
	Tc3Value* robotTypeVar_ = nullptr;

	bool connected_ = false;
	QTimer resetNewPos_;
};

