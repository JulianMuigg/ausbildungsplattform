#include <QSignalBlocker>
#include "ParamWidget.h"

ParamWidget::ParamWidget(const QString& name, const QString& unit, double min, double max, int decimals, QWidget *parent)
	: QWidget(parent)
{
	ui.setupUi(this);
	ui.name->setText( name );
	ui.slider->setMinimum(min * pow(10,decimals));
	ui.slider->setMaximum(max * pow(10,decimals));
	ui.spinBox->setMinimum(min);
	ui.spinBox->setMaximum(max);
	ui.spinBox->setDecimals(decimals);
	ui.spinBox->setSuffix( " " + unit );
	ui.spinBox->setSingleStep( pow( 10, -decimals ) );
	ui.spinBox->setKeyboardTracking(false);
	ui.slider->hide();

	ui.spinBox->setStyleSheet( "QSpinBox::down - button{width: 200} QSpinBox::up - button{width: 200}" );

}

ParamWidget::~ParamWidget()
{
}

void ParamWidget::setValue(double value)
{
	ui.spinBox->setValue(value);
}

double ParamWidget::value()
{
	return ui.spinBox->value();
}

void ParamWidget::on_slider_valueChanged(int value)
{
	QSignalBlocker blocker(ui.spinBox);
	ui.spinBox->setValue(value / pow(10, ui.spinBox->decimals()));
	emit valueChanged(ui.spinBox->value());
}

void ParamWidget::on_spinBox_valueChanged(double value)
{
	QSignalBlocker blocker(ui.slider);
	ui.slider->setValue(value * pow(10, ui.spinBox->decimals()));
	emit valueChanged(ui.spinBox->value());
}

void ParamWidget::on_btnUp_clicked()
{
	ui.spinBox->setValue( ui.spinBox->value() + ui.spinBox->singleStep() );
}

void ParamWidget::on_btnDown_clicked()
{
	ui.spinBox->setValue( ui.spinBox->value() - ui.spinBox->singleStep() );
}
