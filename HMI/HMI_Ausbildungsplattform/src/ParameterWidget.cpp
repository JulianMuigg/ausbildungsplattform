#include "ParameterWidget.h"
#include "Controller.h"

ParameterWidget::ParameterWidget(QWidget *parent)
	: QWidget(parent)
{
	ui_.setupUi(this);
	
	parameter_[Parameter::v_max] = new ParamWidget( "v_max", "m/s", 0, 10, 3, this );
	parameter_[Parameter::a_max] = new ParamWidget( "a_max", "m/s^2", 0, 10, 3, this );
	parameter_[Parameter::dq_max] = new ParamWidget( "dq_max", "(m bzw. rad)/s", 0, 10, 3, this );
	parameter_[Parameter::ddq_max] = new ParamWidget( "ddq_max", "(m bzw. rad)/s^2", 0, 10, 3, this );
	parameter_[Parameter::z_max] = new ParamWidget( "z_max", "m", -1, 1, 3, this );
	parameter_[Parameter::homeX] = new ParamWidget( "homeX", "m", -1, 1, 3, this );
	parameter_[Parameter::homeY] = new ParamWidget( "homeY", "m", -1, 1, 3, this );
	parameter_[Parameter::homeZ] = new ParamWidget( "homeZ", "m", -1, 1, 3, this );

	for( auto param : parameter_ )
	{
		ui_.verticalLayout_2->addWidget( param );
		connect( param, &ParamWidget::valueChanged, this, &ParameterWidget::onParameterChanged );
	}
}

ParameterWidget::~ParameterWidget()
{
}

void ParameterWidget::setValue( Parameter parameter, double value, bool update /*=false*/ )
{
	if( parameter_.contains( parameter ) )
	{
		QSignalBlocker blocker( parameter_[parameter] );
		parameter_[parameter]->setValue( value );

		if( update )
			emit parameterChanged( parameter, value );		
	}
}

double ParameterWidget::value( Parameter parameter )
{
	if( parameter_.contains( parameter ) )
		return parameter_[parameter]->value();

	return 0;
}

void ParameterWidget::onParameterChanged( double value )
{
	ParamWidget* param = qobject_cast<ParamWidget*>(sender());

	if( param )
	{
		Parameter parameter = parameter_.key( param );
		emit parameterChanged( parameter, value );
	}
}