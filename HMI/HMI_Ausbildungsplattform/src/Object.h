#pragma once

#include <QWidget>
#include <QGraphicsPixmapItem>

class Object : public QGraphicsPixmapItem
{
public:
    Object( const QString& icon, QPointF position, QColor color, double angle, double speed );

    void setPicked( bool val = true ) { isPicked_ = val; }
    bool isPicked() { return isPicked_; }

    QPointF sceneCenterPos();
private:
    QPointF position_;
    double angle_;
    double speed_;
    QColor color_;
    bool isPicked_ = false;
};

