/****************************************************************************
** Inspired from: 
** https://doc.qt.io/qt-5/qtwidgets-graphicsview-diagramscene-example.html
****************************************************************************/


#include "arrow.h"

#include <QPainter>
#include <QPen>
#include <QtMath>

Arrow::Arrow( const QPoint& startPos, const QPoint& endPos, const QString& label, QGraphicsItem* parent ) : QGraphicsLineItem( parent )
{
    setFlag( QGraphicsItem::ItemIsSelectable, true );
    setPen( QPen( color_, 20, Qt::SolidLine, Qt::RoundCap, Qt::RoundJoin ) );

    startItem_.setPos( startPos );
    endItem_.setPos( endPos );
    label_ = label;
}

void Arrow::setStart( const QPoint& pos ) 
{ 
    startItem_.setPos( pos ); 
    updatePosition();
}

void Arrow::setEnd( const QPoint& pos ) 
{ 
    endItem_.setPos( pos ); 
    updatePosition();
}

void Arrow::updatePosition()
{
    QLineF line( startItem_.pos(), endItem_.pos() );
    setLine( line );
}

void Arrow::paint( QPainter* painter, const QStyleOptionGraphicsItem*, QWidget* )
{
    if( startItem_.collidesWithItem( &endItem_ ) )
        return;

    QPen myPen = pen();
    myPen.setColor( color_ );
    qreal arrowSize = 200;
    painter->setPen( myPen );
    painter->setBrush( color_ );
    painter->setFont( QFont( "Arial", 200 ) );

    // Draw line
    QLineF centerLine( endItem_.pos(), startItem_.pos() );
    setLine( centerLine );

    // draw arrow tip
    double angle = std::atan2( -line().dy(), line().dx() );
    QPointF arrowP1 = line().p1() + QPointF( sin( angle + M_PI / 3 ) * arrowSize,
        cos( angle + M_PI / 3 ) * arrowSize );
    QPointF arrowP2 = line().p1() + QPointF( sin( angle + M_PI - M_PI / 3 ) * arrowSize,
        cos( angle + M_PI - M_PI / 3 ) * arrowSize );

    arrowHead_.clear();
    arrowHead_ << line().p1() << arrowP1 << arrowP2;

    painter->drawLine( line() );
    painter->drawPolygon( arrowHead_ );

    // draw label
    painter->drawText( line().p1() + QPointF(arrowSize,arrowSize), label_ );
}