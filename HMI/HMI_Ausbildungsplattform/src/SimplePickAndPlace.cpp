#include <QRandomGenerator>
#include <qdebug.h>
#include <QMouseEvent>
#include "SimplePickAndPlace.h"
#include "Controller.h"


SimplePickAndPlace::SimplePickAndPlace(QGraphicsScene* scene, QObject *parent) : PickAndPlaceApp(scene, parent)
{
    placeRect_ = new QGraphicsRectItem( 0, 0, 0, 0 );
    placeRect_->setBrush( Qt::white );
    scene_->addItem( placeRect_ );

    QGraphicsEllipseItem* circle = new QGraphicsEllipseItem( 0, 0, 1500, 1500 );
    circle->setBrush( QColor( 0, 73, 130 ) );
    circle->hide();
    scene_->addItem( circle );
    selection_ = circle;
}

SimplePickAndPlace::~SimplePickAndPlace()
{
}

void SimplePickAndPlace::generate( RobotType robotType )
{
    int count = 20;
    scene_->setBackgroundBrush( QBrush( Qt::black ) );

    // Show all objects
    for( auto obj : objects_ )
        obj->show();

    placeRect_->show();

    // Use current object positions, if automatic mode has not finished
    if( currentIndex_ < count )
    {
        if( selectedObj_ )
        {
            selectedObj_->setPicked( false );
            currentIndex_--;
            selection_->hide();
        }

        return;
    }

    // Clear positions
    placePositions_.clear();

    // Get scene rect and resize place rect
    placeRect_->setRect( 0, 0, sceneRect_.width(), 600 );
    
    
    for( int i = 0; i < count; i++ )
    {
        // Generate object, if needed
        if( objects_.size() < (i + 1) )
        {
            objects_.push_back( new Object( ":/MainWindow/MCI_forum_grau20.png", QPointF( 0, 0 ), Qt::blue, 0, 0 ) );
            scene_->addItem( objects_.last() );
        }
        else
        {
            objects_.at( i )->show();
        }

        // Position object without collision to another one
        bool isValid = true;
        do
        {
            isValid = true;
            QPointF pos = QPointF( QRandomGenerator::global()->bounded( int( sceneRect_.width() * 0.1 ), int( sceneRect_.width() * 0.9 ) ), QRandomGenerator::global()->bounded( int( sceneRect_.height() * 0.15 ), int( sceneRect_.height() * 0.85 ) ) );
            objects_[i]->setPos( pos );

            for( int j = 0; j < i; j++ )
            {
                if( objects_.at( j )->collidesWithItem( objects_[i] ) )
                {
                    isValid = false;
                    break;
                }
            }

        } while( !isValid );

        // Generate a new place position
        // Get needed values for object generation
        auto br = placeRect_->boundingRect();
        auto width = br.width() / count;
        auto objBr = objects_.first()->boundingRect();
        QPointF pos2 = QPointF( (i * width + objBr.width() * 0.5), br.height() * 0.5 );
        placePositions_.push_back( QVector3D( pos2.x(), pos2.y(), 0 ) );    
    }

    currentIndex_ = 0;
}

QVector3D SimplePickAndPlace::nextPickPosition( bool& valid )
{
    valid = false;
    if( objects_.size() > currentIndex_ )
    {
        // Get nearest object
        auto pos1 = QPointF( DBL_MAX, DBL_MAX );
        for( auto obj : objects_ )
        {
            auto temp = obj->sceneCenterPos();
            if( !obj->isPicked() && pos1.manhattanLength() > temp.manhattanLength() )
                pos1 = temp;
        }
        valid = true;
        return QVector3D( pos1.x(), pos1.y(), 0 );
    }

    return QVector3D();
}

QVector3D SimplePickAndPlace::nextPlacePosition( bool& valid )
{
    valid = placePositions_.size() > currentIndex_;
    if( valid )
        return placePositions_.at( currentIndex_ );

    return QVector3D();
}

void SimplePickAndPlace::hideItems()
{
    for( auto obj : objects_ )
        obj->hide();

    placeRect_->hide();
    selection_->hide();
}
