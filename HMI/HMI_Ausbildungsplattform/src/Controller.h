#pragma once
#include <qobject.h>
#include <qtcpserver.h>
#include <qtcpsocket.h>
#include <qvector3d.h>

enum class Command
{
	Undefined = -1,
	Transition = 0,
	State = 1,
	PositionRequest = 2,
	CurrentPos = 4,
	Parameter = 5,
	TouchEvent = 6,
	ExternalTrajectory = 7,

};
Q_DECLARE_METATYPE( Command );

enum class Transition
{
	Undefined = 0,
	Home = 1,
	StartJogging = 2,
	StartCalibration = 3,
	StartAutomatic = 4,
	StartExternalTrajectory = 5,
	Stop = 6,
	ErrorQuit = 7
};
Q_DECLARE_METATYPE( Transition );

enum class State
{
	Undefined = 0,
	SafetyError = 1,
	Stopped = 2, 
	Idle = 3,
	WaitForRobot = 4,
	Jogging = 5,
	Calibration = 6,
	Automatic = 7,
	ExternalTrajectory = 8
};
Q_DECLARE_METATYPE( State );

enum class ErrorType
{
	NoError = 0,
	Singularity = 1,
	NotReachable = 2,
	AxisLimitError = 3,
	HardStopError = 4
};
Q_DECLARE_METATYPE( ErrorType );

enum class Parameter
{
	Undefined = -1,
	v_max = 0,
	a_max,
	dq_max,
	ddq_max,
	z_max,
	homeX,
	homeY,
	homeZ
};
Q_DECLARE_METATYPE( Parameter );

enum class RobotType
{
	Undefined = -1,
	Scara = 0,
	Delta360 = 1
};
Q_DECLARE_METATYPE( RobotType );

class Controller : public QObject
{
	Q_OBJECT
public:
	Controller( QObject* parent = nullptr );
	~Controller() = default;

	virtual void init() {};

	RobotType robotoType() { return robotType_; }

public slots:
	virtual void onTransitionChanged( const Transition& transition ) = 0;
	virtual void onPositionRequest( const QVector3D& pos1, const QVector3D& pos2 ) = 0;
	virtual void onParameterChanged( const Parameter& param, const double& val ) = 0;
	virtual void onTouchEventTriggered( bool on ) = 0;
	virtual void onExternalTrajectory( const QString& fileName, const QVector<QVector<double>>& trajectory ) = 0;

signals:
	void stateChanged( const State& state );
	void robotErrorChanged( const QString& error );
	void positionRequest( bool on );
	void currentPosChanged( const double& x, const double& y, const double& z );
	void parameterChanged( const Parameter& param, const double& val );
	void connectionChanged( bool connected );

protected:
	QMap<Command, QVariant> parameter_;
	QMap<Command, QString> commandName_;
	QMap<Transition, QString> transitionName_;
	QMap<ErrorType, QString> errorName_;
	RobotType robotType_ = RobotType::Undefined;
};

