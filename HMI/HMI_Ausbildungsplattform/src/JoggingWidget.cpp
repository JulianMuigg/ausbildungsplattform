#include "JoggingWidget.h"

JoggingWidget::JoggingWidget(QWidget *parent)
	: QWidget(parent)
{
	ui_.setupUi(this);

}

JoggingWidget::~JoggingWidget()
{
}

void JoggingWidget::on_btn_1mm_clicked( bool on )
{
	if( on )
	{
		positionChange_ = 0.001;
		ui_.btn_10mm->setChecked( false );
		ui_.btn_50mm->setChecked( false );
		ui_.btn_100mm->setChecked( false );
	}
}

void JoggingWidget::on_btn_10mm_clicked( bool on )
{
	if( on )
	{
		positionChange_ = 0.010;
		ui_.btn_1mm->setChecked( false );
		ui_.btn_50mm->setChecked( false );
		ui_.btn_100mm->setChecked( false );
	}
}

void JoggingWidget::on_btn_50mm_clicked( bool on )
{
	if( on )
	{
		positionChange_ = 0.050;
		ui_.btn_1mm->setChecked( false );
		ui_.btn_10mm->setChecked( false );
		ui_.btn_100mm->setChecked( false );
	}
}

void JoggingWidget::on_btn_100mm_clicked( bool on )
{
	if( on )
	{
		positionChange_ = 0.100;
		ui_.btn_1mm->setChecked( false );
		ui_.btn_10mm->setChecked( false );
		ui_.btn_50mm->setChecked( false );
	}
}

void JoggingWidget::on_btn_yPlus_clicked()
{
	emit move( positionChange_, 0, 0 );
}

void JoggingWidget::on_btn_yMinus_clicked()
{
	emit move( -positionChange_, 0, 0 );
}

void JoggingWidget::on_btn_xPlus_clicked()
{
	emit move( 0, positionChange_, 0 );
}

void JoggingWidget::on_btn_xMinus_clicked()
{
	emit move( 0, -positionChange_, 0 );
}

void JoggingWidget::on_btn_zPlus_clicked()
{
	emit move( 0, 0, positionChange_ );
}

void JoggingWidget::on_btn_zMinus_clicked()
{
	emit move( 0, 0, -positionChange_ );
}

void JoggingWidget::on_btn_xPlus_yMinus_clicked()
{
	double val = sqrt( pow( positionChange_, 2 )*0.5 );
	emit move( -val, val, 0 );
}

void JoggingWidget::on_btn_xPlus_yPlus_clicked()
{
	double val = sqrt( pow( positionChange_, 2 ) * 0.5 );
	emit move( val, val, 0 );
}

void JoggingWidget::on_btn_xMinus_yMinus_clicked()
{
	double val = sqrt( pow( positionChange_, 2 ) * 0.5 );
	emit move( -val, -val, 0 );
}

void JoggingWidget::on_btn_xMinus_yPlus_clicked()
{
	double val = sqrt( pow( positionChange_, 2 ) * 0.5 );
	emit move( val, -val, 0 );
}

void JoggingWidget::on_btn_home_clicked()
{
	emit setCurrentPosAsHome();
}
