#include <QWindow>
#include <QDateTime>
#include "MainWindow.h"
#include "Beckhoff.h"
#include "TcpServer.h"
#include "./../resource/version.h"

MainWindow::MainWindow( QString netId, QWidget *parent) : QMainWindow(parent)
{
    // Setup GUI
    ui_.setupUi(this);
    ui_.menuBar->hide(); // hide menubar
    
    // Setup  and initialise status bar
    QIcon icon;
    icon.addFile( ":/MainWindow/connected.png", QSize(), QIcon::Normal, QIcon::Off );
    connectionLabel_->setIcon( icon.pixmap( QSize( 32, 32 ) ) );
    connectionLabel_->setFlat( true );
    ui_.statusBar->addWidget( connectionLabel_ );
    ui_.statusBar->addWidget( stateLabel_ );
    ui_.statusBar->addWidget( robotErrorLabel_ );
    ui_.statusBar->addWidget( robotPosition_ );
    ui_.statusBar->addWidget( lastPositionRequest_ );
    ui_.statusBar->addWidget( calibrationData_ );

    onStateChanged( State::Undefined );
    onCurrentPosChanged( 0, 0, 0 );
    onConnectionChanged( false );
    onCalibrationFinished( QVector3D( 0, 0, 0 ), QQuaternion(), 0 );

    // Init second window
    secondWindow_->setWindowFlags( Qt::Window );
    secondWindow_->show();
    if( qApp->screens().size() > 1 )
    {
        secondWindow_->windowHandle()->setScreen( qApp->screens()[1] );
        secondWindow_->showFullScreen();
    }
    else
    {
        secondWindow_->showMaximized();
    }
    secondWindow_->setLayout( new QVBoxLayout() );

    // Init jogging wigdet
    jogging_->show();
    ui_.vLayoutLeft->addWidget( jogging_ );
    jogging_->setEnabled( false );
    connect( jogging_, &JoggingWidget::move, this, &MainWindow::onJoggingMove );
    connect( jogging_, &JoggingWidget::setCurrentPosAsHome, this, &MainWindow::onSetCurrentPosAsHome );

    // Init automatic widget
    connect( automatic_, &AutomaticWidget::touchEventTriggered, this, &MainWindow::onTouchEventTriggerd );
    secondWindow_->layout()->addWidget( automatic_ );
    ui_.mainToolBar->addSeparator();
    ui_.mainToolBar->addWidget( appSelection_ );
    appSelection_->addItems( automatic_->appList() );
    appSelection_->setSizePolicy( QSizePolicy::Maximum, QSizePolicy::Expanding );
    connect( appSelection_, &QComboBox::currentTextChanged, automatic_, &AutomaticWidget::setApp );

    // Init parameter widget
    ui_.vLayoutLeft->addWidget( parameter_ );
    connect( parameter_, &ParameterWidget::parameterChanged, this, &MainWindow::onParameterChangedLocal );
    connect( &setHomePosTimer_, &QTimer::timeout, this, &MainWindow::onSetCurrentPosAsHome );

    // Init calibration widget
    connect( calibration_, &CalibrationWidget::touchEventTriggered, this, &MainWindow::onTouchEventTriggerd ); 
    connect( calibration_, &CalibrationWidget::calibrationFinished, this, &MainWindow::onCalibrationFinished );
    secondWindow_->layout()->addWidget( calibration_);
    calibration_->hide();

    // Init load trajectory widget
    ui_.vLayoutRight->addWidget( loadTrajectory_ );
    loadTrajectory_->setEnabled( false );
    QObject::connect( loadTrajectory_, &LoadTrajectoryWidget::setTrajectory, this, &MainWindow::externalTrajectory );

    // Init controller
    Beckhoff* temp = new Beckhoff( QString( "%1:851" ).arg( netId ) );
    if( temp->isConnected() )
    {
        controller_ = temp;
        onConnectionChanged( temp->isConnected() );
    }
    else
    {
        controller_ = new TcpServer( this );
        temp->deleteLater();
    }

    QObject::connect( this, &MainWindow::transitionChanged, controller_, &Controller::onTransitionChanged );
    QObject::connect( this, &MainWindow::positionRequest, controller_, &Controller::onPositionRequest );
    QObject::connect( this, &MainWindow::parameterChanged, controller_, &Controller::onParameterChanged );
    QObject::connect( this, &MainWindow::touchEventTriggered, controller_, &Controller::onTouchEventTriggered );
    QObject::connect( this, &MainWindow::externalTrajectory, controller_, &Controller::onExternalTrajectory );

    QObject::connect( controller_, &Controller::stateChanged, this, &MainWindow::onStateChanged );
    QObject::connect( controller_, &Controller::robotErrorChanged, this, &MainWindow::onRobotErrorChanged );
    QObject::connect( controller_, &Controller::positionRequest, this, &MainWindow::onPositionRequest );
    QObject::connect( controller_, &Controller::currentPosChanged, this, &MainWindow::onCurrentPosChanged );
    QObject::connect( controller_, &Controller::parameterChanged, this, &MainWindow::onParameterChanged );

    QObject::connect( controller_, &Controller::connectionChanged, this, &MainWindow::onConnectionChanged );

    if( qobject_cast<TcpServer*>( controller_ ) )
    {
        QObject::connect( qobject_cast<TcpServer*>(controller_), &TcpServer::simulateTouchEvent, automatic_, &AutomaticWidget::selectObject );
        QObject::connect( qobject_cast<TcpServer*>(controller_), &TcpServer::simulateTouchEvent, calibration_, &CalibrationWidget::touchEvent );
        QObject::connect( calibration_, &CalibrationWidget::resizeTriggered, qobject_cast<TcpServer*>(controller_), &TcpServer::onUpdateZeroOffset );
    }

    controller_->init();

    updateTitle( controller_->robotoType() );
}

void MainWindow::updateTitle( RobotType type )
{
    // Define Window Title
    QString dateTime = QStringLiteral( __DATE__ ) + QStringLiteral( " " ) + QStringLiteral( __TIME__ );
    if( type == RobotType::Scara )
        setWindowTitle( "MCI Ausbildungsplattform (c) 2021 --- SCARA --- Version " + QString( VERSION_STR ) + " --- " + dateTime );
    else if( type == RobotType::Delta360 )
        setWindowTitle( "MCI Ausbildungsplattform (c) 2021 --- Delta360 --- Version " + QString( VERSION_STR ) + " --- " + dateTime );
    else
        setWindowTitle( "MCI Ausbildungsplattform (c) 2021 --- Version " + QString( VERSION_STR ) + " --- " + dateTime );
}

void MainWindow::on_actionHome_triggered()
{
    emit transitionChanged( Transition::Home );
    calibration_->show();
    automatic_->hide();
}

void MainWindow::on_actionStop_triggered()
{
    emit transitionChanged( Transition::Stop );
}

void MainWindow::on_actionJogging_triggered()
{
    emit transitionChanged( Transition::StartJogging );
    currentJoggingPosition_ = currentPosition_;
}
void MainWindow::on_actionCalibration_triggered()
{
    emit transitionChanged( Transition::StartCalibration );
    calibration_->show();
    automatic_->hide();
}

void MainWindow::on_actionAutomatic_triggered()
{
    emit transitionChanged( Transition::StartAutomatic );
    calibration_->hide();
    automatic_->show();
}

void MainWindow::on_actionLoadTrajectory_triggered()
{
    emit transitionChanged( Transition::StartExternalTrajectory );
    calibration_->hide();
    automatic_->show();
}

void MainWindow::on_actionErrorQuit_triggered()
{
    emit transitionChanged( Transition::ErrorQuit );
}

void MainWindow::onStateChanged( const State& state )
{
    RobotType robotType = RobotType::Undefined;
    if( controller_ )
        robotType = controller_->robotoType();

    updateTitle( robotType );

    ui_.actionHome->setEnabled( false );
    ui_.actionStop->setEnabled( false );
    ui_.actionJogging->setEnabled( false );
    ui_.actionCalibration->setEnabled( false );
    ui_.actionAutomatic->setEnabled( false );
    ui_.actionErrorQuit->setEnabled( false );
    ui_.actionLoadTrajectory->setEnabled( false );

    jogging_->setEnabled( false );
    parameter_->setEnabled( false );
    loadTrajectory_->clear();
    loadTrajectory_->setEnabled( false );
    currentState_ = state;
    switch( currentState_ )
    {
    case State::Idle:
        ui_.actionJogging->setEnabled( true );
        ui_.actionCalibration->setEnabled( true );
        if( calibration_->data().mmPerPixel_ != 0 )
        {
            ui_.actionAutomatic->setEnabled( true );
            ui_.actionLoadTrajectory->setEnabled( true );
        }
        parameter_->setEnabled( true );
        stateLabel_->setText( "State: Idle" );
        break;
    case State::Stopped:
        ui_.actionHome->setEnabled( true );
        stateLabel_->setText( "State: Stopped" );
        break;
    case State::SafetyError:
        ui_.actionErrorQuit->setEnabled( true );
        stateLabel_->setText( "State: SafetyError" );
        break;
    case State::Jogging:
        ui_.actionStop->setEnabled( true );
        stateLabel_->setText( "State: Jogging" );
        jogging_->setEnabled( true );
        break;
    case State::Calibration:
        ui_.actionStop->setEnabled( true );
        stateLabel_->setText( "State: Calibration" );
        calibration_->start();
        break;
    case State::Automatic:
        ui_.actionStop->setEnabled( true );
        stateLabel_->setText( "State: Automatic" );
        automatic_->start( robotType );
        break;
    case State::ExternalTrajectory:
        loadTrajectory_->setEnabled( true );
        ui_.actionStop->setEnabled( true );
        stateLabel_->setText( "State: External Trajectory" );
        automatic_->start( robotType );
        break;
    case State::WaitForRobot:
        stateLabel_->setText( "State: WaitForRobot" );
        ui_.actionStop->setEnabled( true );
        break;
    default:
        stateLabel_->setText( "State: Undefined" );
    }
}

void MainWindow::onRobotErrorChanged( const QString& error )
{
    robotErrorLabel_->setText( "Robot: " + error );

    if( error != "NoError" )
        robotErrorLabel_->setStyleSheet( "QLabel {background: red}" );
    else
        robotErrorLabel_->setStyleSheet( stateLabel_->styleSheet() );
}

void MainWindow::onPositionRequest( bool on )
{
    QPair<QVector3D, QVector3D> val;
    bool valid = false;

    QVector3D homePos = QVector3D(
        parameter_->value( Parameter::homeX ),
        parameter_->value( Parameter::homeY ),
        parameter_->value( Parameter::homeZ ) );

    lastPositionRequest_->setText( "Positoin request: " + QDateTime::currentDateTime().toString( "hh:mm:ss:zzz - dd.MM.yyyy" ) );

    switch( currentState_ )
    {
    case State::Jogging:
        //jogging_->setEnabled( on != 0 );
        break;
    case State::Calibration:
        if( on )
        {
            valid = false;
            val = calibration_->nextPosition( valid );
            val.first += homePos;
            val.second += homePos;
        }
        break;
    case State::Automatic:
        if( on )
        {
            valid = false;
            val = automatic_->nextPositionPair( valid, controller_->robotoType() );

            qDebug() << "Positions:" << val.first << val.second;

            val.first = calibration_->translatePosition( val.first ) + homePos;
            val.second = calibration_->translatePosition( val.second ) + homePos;
        }
        break;
    case State::ExternalTrajectory:
        if( on )
        {
            val = automatic_->nextPositionPair( valid, controller_->robotoType() );
            val.first = calibration_->translatePosition( val.first ) + homePos;
            val.second = calibration_->translatePosition( val.second ) + homePos;

            if( valid )
            {
                QString text = QString( "Create a trajectory with the following path points \n\n"
                    "P0 = [%1, %2, %3]; %m \n"
                    "P1 = [%3, %4, %6]; %m \n"
                    "P2 = [%7, %8, %9]; %m" )
                    .arg( currentPosition_.x(), 0, 'f', 4 )
                    .arg( currentPosition_.y(), 0, 'f', 4 )
                    .arg( currentPosition_.z(), 0, 'f', 4 )
                    .arg( val.first.x(), 0, 'f', 4 )
                    .arg( val.first.y(), 0, 'f', 4 )
                    .arg( parameter_->value( Parameter::z_max ), 0, 'f', 4 )
                    .arg( val.second.x(), 0, 'f', 4 )
                    .arg( val.second.y(), 0, 'f', 4 )
                    .arg( parameter_->value( Parameter::z_max ), 0, 'f', 4 );

                loadTrajectory_->setStep1( text );
            }

            valid = false;
        }
    }

    if( valid )
        emit positionRequest( val.first, val.second );

}

void MainWindow::onCurrentPosChanged( const double& x, const double& y, const double& z )
{
    currentPosition_.setX( x ); // in meter
    currentPosition_.setY( y ); // in meter
    currentPosition_.setZ( z ); // in meter

    robotPosition_->setText( QString( "x: %1m; y: %2m; z: %3m" )
        .arg( currentPosition_.x(), 0, 'f', 3 )
        .arg( currentPosition_.y(), 0, 'f', 3 )
        .arg( currentPosition_.z(), 0, 'f', 3 ) );

    switch( currentState_ )
    {
    case State::Calibration:
        calibration_->setLastZPosition( currentPosition_.z() );
        break;
    case State::Jogging:
        jogging_->setEnabled( abs( ( currentPosition_ - currentJoggingPosition_ ).length() ) < 0.005 );
        break;
    };   
}

void MainWindow::onParameterChanged( const Parameter& param, const double& val )
{
    parameter_->setValue( param, val );
}

void MainWindow::onJoggingMove( double x, double y, double z )
{
    // Change position to meter
    QVector3D pos1 = QVector3D( currentPosition_.x() + x, currentPosition_.y() + y, currentPosition_.z() + z );
    QVector3D pos2 = QVector3D( 0, 0, 0 );

    emit positionRequest( pos1, pos2 );
    jogging_->setEnabled( false );
    currentJoggingPosition_ = pos1;
}

void MainWindow::onParameterChangedLocal( Parameter type, double value )
{
    emit parameterChanged( type, value );
    jogging_->setEnabled( false );
}

void MainWindow::onConnectionChanged( bool connected )
{
    QString connectionText = "Disconnected to ";
    if( connected )
    {
        connectionText = "Connected to ";

        if( qobject_cast<Beckhoff*>(controller_) )
            connectionText += "ADS";
        else
            connectionText += "TCP/IP";
    }

    connectionLabel_->setText( connectionText );
    connectionLabel_->setEnabled( connected );
    stateLabel_->setEnabled( connected );
    robotErrorLabel_->setEnabled( connected );
    robotPosition_->setEnabled( connected );
    lastPositionRequest_->setEnabled( connected );
    calibrationData_->setVisible( connected );
}

void MainWindow::onTouchEventTriggerd()
{
    emit touchEventTriggered( true );
}

void MainWindow::onCalibrationFinished( const QVector3D& offset, const QQuaternion& q, double mmPerPixel )
{
    QString offsetStr = QString( "%1m | %2m | %3m" )
        .arg( offset.x(), 0, 'f', 3 )
        .arg( offset.y(), 0, 'f', 3 )
        .arg( offset.z(), 0, 'f', 3 );
    QString qStr = QString( "%1 ( %2, %3, %4 )" )
        .arg( q.scalar(), 0, 'f', 3 )
        .arg( q.vector().x(), 0, 'f', 3 )
        .arg( q.vector().y(), 0, 'f', 3 )
        .arg( q.vector().z(), 0, 'f', 3 );
    calibrationData_->setText( QString( "Offset: %1; Rotation: %2; mm/pixel: %3" )
        .arg( offsetStr ).arg( qStr )
        .arg( mmPerPixel, 0, 'f', 3 ) );

    on_actionStop_triggered();
}

void MainWindow::onSetCurrentPosAsHome()
{
    static int index = -1;
    if( index == 0 )
        setHomePosTimer_.start( 100 );

    switch( index )
    {
    case 0: 
        emit transitionChanged( Transition::Stop ); 
        break;
    case 1: 
        parameter_->setValue( Parameter::homeX, parameter_->value( Parameter::homeX ) + currentPosition_.x(), true ); 
        break;
    case 2: 
        parameter_->setValue( Parameter::homeY, parameter_->value( Parameter::homeY ) + currentPosition_.y(), true ); 
        break;
    case 3: 
        parameter_->setValue( Parameter::homeZ, parameter_->value( Parameter::homeZ ) + currentPosition_.z(), true ); 
        break;
    case 4: 
        emit transitionChanged( Transition::Home ); 
        break;
    default:
        index = -1;
        setHomePosTimer_.stop();
    }

    index++;
}
