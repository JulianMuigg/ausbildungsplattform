#pragma once

#include <QWidget>
#include "ui_ParameterWidget.h"

#include "ParamWidget.h"

enum class Parameter;

class ParameterWidget : public QWidget
{
	Q_OBJECT

public:
	ParameterWidget(QWidget *parent = Q_NULLPTR);
	~ParameterWidget();

	void setValue( Parameter parameter, double value, bool update = false );
	double value( Parameter parameter );

public slots:
	void onParameterChanged( double value );

signals:
	void parameterChanged( Parameter parameter, double value );

private:
	Ui::ParameterWidget ui_;

	QMap<Parameter, ParamWidget*> parameter_;
};
