#ifndef CHART_H
#define CHART_H

#include <QObject>
#include <QWidget>
#include <QtCharts/QChart>
#include <QtCharts/QChartView>
#include <QtCharts/QLineSeries>
#include <QtCharts/QSplineSeries>
#include <QtCharts/QDateTimeAxis>
#include <QtCharts/QValueAxis>

class Chart : public QtCharts::QChartView
{
    Q_OBJECT
public:
    Chart( const QString& title, const QString& xLabel, const QString& yLabel, bool enableTimeAxis, QWidget* parent = nullptr );

    void clear();
    void clearRange();
    void addPlot( const QString& name, QVector<double> xData, QVector<double> yData, const QColor& color = Qt::blue, QtCharts::QAbstractSeries::SeriesType type = QtCharts::QAbstractSeries::SeriesTypeLine );
    void appendPlotValues( const QString& name, QVector<double> xData, QVector<double> yData, int maxBufferSize, const QColor& color = Qt::blue, QtCharts::QAbstractSeries::SeriesType type = QtCharts::QAbstractSeries::SeriesTypeLine );
    void addPlotValue( const QString& name, double xData, double yData, int maxBufferSize, const QColor& color = Qt::blue, QtCharts::QAbstractSeries::SeriesType type = QtCharts::QAbstractSeries::SeriesTypeLine );   

    void setMinMaxY( double min, double max );

    QMap<QString, QVector<QPointF>> data();

    void zoomIn() { chart_->zoomIn(); }
    void zoomOut() {chart_->zoomOut();}
    void zoomReset() { chart_->zoomReset(); }

protected:
    void mouseMoveEvent( QMouseEvent* event ) override;
    void mousePressEvent( QMouseEvent* event ) override;
    void showTooltip( QMouseEvent* event );

private slots:
    void onPointAdded( int index );

private:
    QtCharts::QChart* chart_ = nullptr;
    QMap<QString, QtCharts::QXYSeries*> series_;
    QtCharts::QAbstractAxis *axisX_ = nullptr;
    QtCharts::QAbstractAxis *axisY_ = nullptr;
    QPointF maxValue_ = QPointF( std::numeric_limits<double>::min(), std::numeric_limits<double>::min() );
    QPointF minValue_ = QPointF( std::numeric_limits<double>::max(), std::numeric_limits<double>::max() );
};

#endif // CHART_H


