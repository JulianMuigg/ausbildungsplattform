#include "Beckhoff.h"

#include <qstring.h>



Beckhoff::Beckhoff( const QString& amsNetId, QObject* parent ) : Controller( parent )
{
    manager_ = new Tc3Manager( amsNetId, this );

    connect( manager_, &Tc3Manager::connectionChanged, this, &Beckhoff::onConnectionChanged );
    connect( manager_, &Tc3Manager::error, this, &Beckhoff::onError );

    connect( &resetNewPos_, &QTimer::timeout, this, &Beckhoff::resetNewPos );

    if( manager_->isConnected() )
    {
        // Create tc3 values for hmi interface
        hmiIn_[HmiIn::State] = manager_->value( "Control.hmiIn.state", sizeof( State ), Tc3Manager::NotificationType::Change );
        hmiIn_[HmiIn::PositionRequest] = manager_->value( "Control.hmiIn.positionRequest", -1, Tc3Manager::NotificationType::Change );
        hmiIn_[HmiIn::RobotOut] = manager_->value( "Control.hmiIn.robotOut", sizeof( Plc::RobotOutStruct ), Tc3Manager::NotificationType::Change );

        hmiOut_[HmiOut::Transition] = manager_->value( "Control.hmiOut._transition", sizeof( Transition ), Tc3Manager::NotificationType::None );
        hmiOut_[HmiOut::TouchEvent] = manager_->value( "Control.hmiOut.touchEvent", -1, Tc3Manager::NotificationType::None );
        hmiOut_[HmiOut::NextPosition] = manager_->value( "Control.hmiOut.nextPosition", sizeof( Plc::NextPositionStruct ), Tc3Manager::NotificationType::None );
        hmiOut_[HmiOut::Parameter] = manager_->value( "Control.hmiOut.parameter", sizeof( Plc::ParameterStruct ), Tc3Manager::NotificationType::None );

        externalTrajectory_ = manager_->value( "Control.externalTrajectory", sizeof( Plc::ExternalTrajectoryStruct ), Tc3Manager::NotificationType::None );
        newPos_ = manager_->value( "Control.hmiOut.nextPosition.newPos" );
        robotTypeVar_ = manager_->value( "MAIN.robotType" );
        robotType_ = RobotType( robotTypeVar_->get().toInt() );

        // Connect hmi out variables with onValueChanged slot
        for( auto val : hmiIn_ )
            connect( val, &Tc3Value::changed, this, &Beckhoff::onValueChanged );

        maestroEnable_ = manager_->value( "Maestro.nMaestroMode", sizeof( Plc::MaestroMode ), Tc3Manager::NotificationType::None );
        controllerEnable_ = manager_->value( "Control.nControllerMode", sizeof( Plc::ControllerMode ), Tc3Manager::NotificationType::None );
        

        maestroEnable_->set<Plc::MaestroMode>( Plc::MaestroMode::Execute );
        controllerEnable_->set<Plc::ControllerMode>( Plc::ControllerMode::Enabled );
    }
}

Beckhoff::~Beckhoff()
{
    if( manager_->isConnected() )
        controllerEnable_->set<Plc::ControllerMode>( Plc::ControllerMode::Disabled );
}

void Beckhoff::init()
{
    if( manager_->isConnected() )
    {
        // Get parameter from plc
        Plc::ParameterStruct p = hmiOut_[HmiOut::Parameter]->get<Plc::ParameterStruct>();
        emit parameterChanged( Parameter::v_max, p.v_max );
        emit parameterChanged( Parameter::a_max, p.a_max );
        emit parameterChanged( Parameter::dq_max, p.dq_max );
        emit parameterChanged( Parameter::ddq_max, p.ddq_max );
        emit parameterChanged( Parameter::z_max, p.z_max );
        emit parameterChanged( Parameter::homeX, p.homeX );
        emit parameterChanged( Parameter::homeY, p.homeY );
        emit parameterChanged( Parameter::homeZ, p.homeZ );
    }

    emit stateChanged( hmiIn_[HmiIn::State]->get<State>() );
}

void Beckhoff::onTransitionChanged( const Transition& transition )
{
    hmiOut_[HmiOut::Transition]->set<Transition>( transition );
}

void Beckhoff::onPositionRequest( const QVector3D& pos1, const QVector3D& pos2 )
{
    Plc::NextPositionStruct nextPos;
    nextPos.pos1.x = pos1.x();
    nextPos.pos1.y = pos1.y();
    nextPos.pos1.z = pos1.z();
    nextPos.pos2.x = pos2.x();
    nextPos.pos2.y = pos2.y();
    nextPos.pos2.z = pos2.z();
    nextPos.newPos = true;
    hmiOut_[HmiOut::NextPosition]->set<Plc::NextPositionStruct>( nextPos );
    resetNewPos_.start( 100 );
}

void Beckhoff::onParameterChanged( const Parameter& param, const double& val )
{
    Plc::ParameterStruct parameter;
    parameter = hmiOut_[HmiOut::Parameter]->get<Plc::ParameterStruct>();

    switch( Parameter( param ) )
    {
    case Parameter::v_max: parameter.v_max = val; break;
    case Parameter::a_max: parameter.a_max = val; break;
    case Parameter::dq_max: parameter.dq_max = val; break;
    case Parameter::ddq_max: parameter.ddq_max = val; break;
    case Parameter::z_max: parameter.z_max = val; break;
    case Parameter::homeX: parameter.homeX = val; break;
    case Parameter::homeY: parameter.homeY = val; break;
    case Parameter::homeZ: parameter.homeZ = val; break;
    default:
        qWarning() << "Parameter undefined" << int( param );
    }

    hmiOut_[HmiOut::Parameter]->set<Plc::ParameterStruct>( parameter );
}

void Beckhoff::onTouchEventTriggered( bool on )
{
    hmiOut_[HmiOut::TouchEvent]->set( on );
}

void Beckhoff::onExternalTrajectory( const QString& fileName, const QVector<QVector<double>>& trajectory )
{
    Plc::ExternalTrajectoryStruct traj;
    for( int i = 0; i < 1000; i++ )
    {
        if( i < trajectory.first().size() )
        {
            if( trajectory[0][i] != -1 )
                traj.size = i + 1;

            traj.trajectory[i].t = trajectory[0][i];
            traj.trajectory[i].q1 = trajectory[1][i];
            traj.trajectory[i].q2 = trajectory[2][i];
            traj.trajectory[i].q3 = trajectory[3][i];
        }
        else
        {
            traj.trajectory[i].t = -1;
            traj.trajectory[i].q1 = 0;
            traj.trajectory[i].q2 = 0;
            traj.trajectory[i].q3 = 0;
        }
    }

    externalTrajectory_->set<Plc::ExternalTrajectoryStruct>( traj );
    newPos_->set( true );
    resetNewPos_.start( 100 );
}



void Beckhoff::onConnectionChanged( bool connected )
{
    connected_ = connected;
    emit connectionChanged( connected );
}

void Beckhoff::onError( QString msg )
{
    qCritical() << "TwinCAT:" << msg;
}

void Beckhoff::onValueChanged(const QVariant& v )
{
    Tc3Value* sender = static_cast<Tc3Value*>(QObject::sender());
    
    if( sender )
    {                
        HmiIn in = hmiIn_.key( sender, HmiIn::Undefined );
        QString valueStr;
        Plc::RobotOutStruct robotOut;
        State state;
        ErrorType error;

        switch( in )
        {
        case HmiIn::State:
            state = hmiIn_[HmiIn::State]->get<State>();
            qDebug() << "Received data:" << sender->name() << int( state );
            emit stateChanged( state );
            newPos_->set( false );
            break;
        case HmiIn::PositionRequest:
            qDebug() << "Received data:" << sender->name() << v.toBool();
            emit positionRequest( v.toBool() );
            break;
        case HmiIn::RobotOut:
            robotOut = sender->get<Plc::RobotOutStruct>(); 
            qDebug() << "Received data:" << sender->name() << robotOut.data.pos.x << robotOut.data.pos.y << robotOut.data.pos.z;
            emit currentPosChanged( robotOut.data.pos.x, robotOut.data.pos.y, robotOut.data.pos.z );
            emit robotErrorChanged( errorName_[robotOut.errorRobot] );
            break;
        }
    }
}

void Beckhoff::resetNewPos()
{
    resetNewPos_.stop();
    newPos_->set( false );
    qDebug() << "Reset new pos";

}


