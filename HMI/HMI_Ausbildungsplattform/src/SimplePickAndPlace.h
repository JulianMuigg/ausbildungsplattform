#pragma once

#include <QObject>
#include "PickAndPlaceApp.h"

class SimplePickAndPlace : public PickAndPlaceApp
{
	Q_OBJECT

public:
	SimplePickAndPlace( QGraphicsScene* scene, QObject* parent );
	~SimplePickAndPlace();

	virtual void generate( RobotType robotType ) override;
	virtual QVector3D nextPickPosition( bool& valid ) override;
	virtual QVector3D nextPlacePosition( bool& valid ) override;
	virtual QString name() override { return "Simple Pick & Place"; }
	virtual void hideItems() override;

private:	
	QGraphicsRectItem* placeRect_ = nullptr;
	QVector<QVector3D> placePositions_;
};
