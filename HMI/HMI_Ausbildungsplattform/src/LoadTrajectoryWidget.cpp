
#include <QFileDialog>
#include <QFile>
#include <QTextStream>
#include <QProgressDialog>
#include <QFuture>
#include <QtConcurrent/qtconcurrentrun.h>
#include "LoadTrajectoryWidget.h"

LoadTrajectoryWidget::LoadTrajectoryWidget(QWidget *parent)
	: QWidget(parent)
{
	qRegisterMetaType<QtCharts::QAbstractSeries::SeriesType>( "'QtCharts::QAbstractSeries::SeriesType" );
	ui_.setupUi(this);

	ui_.step2->layout()->addWidget( chart_ );
	colorList_ = { Qt::red, Qt::green, Qt::blue, Qt::magenta, Qt::cyan, Qt::darkYellow };

	connect( &timer_, &QTimer::timeout, this, &LoadTrajectoryWidget::addPlotValue );
}

LoadTrajectoryWidget::~LoadTrajectoryWidget()
{
}

void LoadTrajectoryWidget::clear()
{
	ui_.btnStart->setEnabled( false );
	ui_.step1Text->setText( "" );
	ui_.file->setText( "" );
	chart_->clear();
	trajectory_.clear();
}

void LoadTrajectoryWidget::setStep1( const QString& text )
{
	ui_.step1Text->setText( text );
}

void LoadTrajectoryWidget::addPlotValue()
{
	for( int j = 0; j < 10; j++ )
	{
		for( int i = 1; i < trajectory_.size(); i++ )
			chart_->addPlotValue( QString( "q%1" ).arg( i ), trajectory_.at( 0 ).at( index_ ), trajectory_.at( i ).at( index_ ), 10000, colorList_.at( i - 1 ) );

		index_++;
		if( index_ >= trajectory_.first().size() )
		{
			ui_.btnStart->setEnabled( true );
			timer_.stop();
			break;
		}
	}
}

void LoadTrajectoryWidget::on_btnStart_clicked()
{
	emit setTrajectory( fileName_, trajectory_ );
}

void LoadTrajectoryWidget::on_btnLoad_clicked()
{
	minY_ = DBL_MAX;
	maxY_ = DBL_MIN;
	chart_->clear();
	trajectory_.clear();
	fileName_ = QFileDialog::getOpenFileName( this, tr( "Open Trajectory" ), qApp->applicationDirPath(), tr( "*.csv" ) );
	ui_.file->setText( fileName_ );

	QFuture<void> future = QtConcurrent::run( this, &LoadTrajectoryWidget::loadFile, fileName_ );
	future.waitForFinished();

	chart_->setMinMaxY( minY_, maxY_ );

	index_ = 0;
	if( !trajectory_.isEmpty() && !trajectory_.first().isEmpty() )
		timer_.start( 50 );

}

void LoadTrajectoryWidget::loadFile( const QString& fileName )
{
	QFile inputFile( fileName_ );
	if( inputFile.open( QIODevice::ReadWrite ) )
	{
		QTextStream in( &inputFile );
		while( !in.atEnd() )
		{
			QString line = in.readLine();
			QStringList list = line.split( "," );

			if( trajectory_.isEmpty() )
				trajectory_.resize( list.size() );

			for( int i = 0; i < list.size(); i++ )
			{
				if( i < trajectory_.size() )
				{
					trajectory_[i].push_back( list.at( i ).toDouble() );

					if( trajectory_[i].last() > maxY_ && i > 0 )
						maxY_ = trajectory_[i].last();

					if( trajectory_[i].last() < minY_ && i > 0 )
						minY_ = trajectory_[i].last();
				}
			}
		}

		if( !trajectory_.isEmpty() && trajectory_.first().size() < 1000 )
		{
			for( int i = trajectory_.first().size(); i < 1000; i++ )
				in << "-1,0.0,0.0,0.0\n";

		}

		inputFile.close();
	}

	
}
		
