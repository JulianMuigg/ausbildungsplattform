#pragma once

#include <QObject>
#include "PickAndPlaceApp.h"

class TrayPickAndPlace : public PickAndPlaceApp
{
	Q_OBJECT

public:
	TrayPickAndPlace( QGraphicsScene* scene, QObject* parent );
	~TrayPickAndPlace();

	virtual void generate( RobotType robotType ) override;
	virtual QVector3D nextPickPosition( bool& valid ) override;
	virtual QVector3D nextPlacePosition( bool& valid ) override;
	virtual QString name() override { return "Gears and Nuts to Tray"; }
	virtual void hideItems() override;

private:	
	Object* trayNuts_ = nullptr;
	Object* trayGears_ = nullptr;
	Object* currentObject_ = nullptr;
	QGraphicsEllipseItem* pickArea_ = nullptr;

	QVector<QVector3D> trayNutsPositions_;
	QVector<QVector3D> trayGearsPositions_;
};
