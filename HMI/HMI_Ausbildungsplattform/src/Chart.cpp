#include <QDebug>
#include <QDateTime>
#include <qtooltip.h>
#include <numeric>
#include "Chart.h"

Chart::Chart( const QString& title, const QString& xLabel, const QString& yLabel, bool enableTimeAxis, QWidget* parent ) : QtCharts::QChartView( parent )
{
    chart_ = new QtCharts::QChart();
    //chart_->legend()->hide();
    //chart_->setTitle( title );

    setChart( chart_ );
    //setRenderHint( QPainter::HighQualityAntialiasing );

    setRubberBand(QChartView::HorizontalRubberBand);

    auto aY = new QtCharts::QValueAxis;
    aY->setTickCount( 10 );
    aY->setTitleText( yLabel );
    chart_->addAxis( aY, Qt::AlignLeft );
    axisY_ = aY;

    if( enableTimeAxis )
    {
        auto a = new QtCharts::QDateTimeAxis;
        a->setFormat( "MMM yyyy" );
        a->setTickCount( 2 );
        a->setTitleText( xLabel );
        chart_->addAxis( a, Qt::AlignBottom );
        axisX_ = a;
    }
    else
    {
        auto a = new QtCharts::QValueAxis;
        a->setTickCount( 2 );
        a->setTitleText( xLabel );
        chart_->addAxis( a, Qt::AlignBottom );
        axisX_ = a;
    }
}

void Chart::clear()
{
    chart_->removeAllSeries();
    series_.clear();

    clearRange();
}

void Chart::clearRange()
{
    maxValue_ = QPointF( std::numeric_limits<double>::min(), std::numeric_limits<double>::min() );
    minValue_ = QPointF( std::numeric_limits<double>::max(), std::numeric_limits<double>::max() );
}

void Chart::addPlotValue( const QString& name, double x, double y, int maxBufferSize, const QColor& color, QtCharts::QAbstractSeries::SeriesType type )
{
    QtCharts::QXYSeries* serie = nullptr;
    if( series_.contains( name ) )
    {
        serie = series_[name];
    }
    else
    {
        switch( type )
        {
        case QtCharts::QAbstractSeries::SeriesType::SeriesTypeLine:
        {
            serie = new QtCharts::QLineSeries( this );
            break;
        }
        case QtCharts::QAbstractSeries::SeriesType::SeriesTypeSpline:
        {
            serie = new QtCharts::QSplineSeries( this );
            break;
        }
        default:
            qDebug() << "Type not supported";
        }

        if( serie )
        {
            series_[name] = serie;
            chart_->addSeries( serie );
            serie->attachAxis( axisX_ );
            serie->attachAxis( axisY_ );
            serie->setColor( color );
            serie->setName( name );
            serie->setUseOpenGL();

            connect( serie, &QtCharts::QXYSeries::pointAdded, this, &Chart::onPointAdded );
        }
    }

    if( serie )
    {
        serie->append( x, y );

        auto points = serie->pointsVector();
        if( points.size() > maxBufferSize )
            serie->remove( 0 );
    }
}

void Chart::setMinMaxY( double min, double max )
{
    minValue_.setY( min );
    axisY_->setMin( min );

    maxValue_.setY( max );
    axisY_->setMax( max );
}

QMap<QString, QVector<QPointF>> Chart::data()
{
    QMap<QString, QVector<QPointF>> ret;

    for( auto serie : series_ )
        ret[serie->name()] = serie->pointsVector();

    return ret;
}

void Chart::showTooltip( QMouseEvent* event )
{
    if( event->buttons() == Qt::RightButton )
    {
        auto value = chart_->mapToValue( event->pos() );
        QToolTip::showText( event->globalPos(), QString( "x: %1 / y: %2" ).arg( value.x() ).arg( value.y() ), this, QRect(), 5000 );
    }
}

void Chart::mousePressEvent( QMouseEvent* event )
{
    showTooltip( event );
    QGraphicsView::mousePressEvent( event );
}

void Chart::mouseMoveEvent( QMouseEvent* event )
{
    showTooltip( event );
    QGraphicsView::mouseMoveEvent( event );
}

void Chart::appendPlotValues( const QString& name, QVector<double> xData, QVector<double> yData, int maxBufferSize, const QColor& color, QtCharts::QAbstractSeries::SeriesType type )
{
    for( int i = 0; i < xData.size(); i++ )
    {
        addPlotValue( name, xData.at( i ), yData.at( i ), maxBufferSize, color, type );
    }
}

void Chart::addPlot( const QString& name, QVector<double> xData, QVector<double> yData, const QColor & color, QtCharts::QAbstractSeries::SeriesType type )
{
    QtCharts::QXYSeries* serie = nullptr;
    if( series_.contains( name ) )
    {
        serie = series_[name];
        serie->clear();
    }

    appendPlotValues( name, xData, yData, yData.size(), color, type );
    chart_->update();
}

void Chart::onPointAdded( int index )
{
    auto serie = qobject_cast<QtCharts::QXYSeries*>( sender() );

    if( serie )
    {
        auto currentPoint = serie->at( index );
        auto y = currentPoint.y();
        
        // get limits for y axis
        if( y > maxValue_.y() )
        {
            maxValue_.setY( y );
            axisY_->setMax( y );
        }
        if( y < minValue_.y() )
        {
            minValue_.setY( y );
            axisY_->setMin( y );
        }


        minValue_.setX( serie->at( 0 ).x() );
        maxValue_.setX( currentPoint.x() );

        // Setrange of x axis depending on axis type
        if( axisX_->type() == QtCharts::QAbstractAxis::AxisType::AxisTypeDateTime )
            axisX_->setRange( QDateTime::fromMSecsSinceEpoch( minValue_.x() ), QDateTime::fromMSecsSinceEpoch( maxValue_.x() ) );
        else
            axisX_->setRange( minValue_.x(), maxValue_.x() );

    }
}

