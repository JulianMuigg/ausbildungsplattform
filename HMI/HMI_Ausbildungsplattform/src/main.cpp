
typedef int BOOL;

#include <QtWidgets/QApplication>
#include <QApplication>
#include <QtDebug>
#include <QFile>
#include <QTextStream>
#include <QDateTime>

#include "MainWindow.h"
#include "TcpServer.h"
#include "Beckhoff.h"


void messageHandler( QtMsgType type, const QMessageLogContext& context, const QString& msg )
{
	QString dateTime = QDateTime::currentDateTime().toString( "yyyy.MM.dd-hh:mm:ss:zzz: " );
	QString txt;
	switch( type ) {
	case QtDebugMsg:
		txt = QString( "Debug: %1" ).arg( msg );
		break;
	case QtWarningMsg:
		txt = QString( "Warning: %1" ).arg( msg );
		break;
	case QtCriticalMsg:
		txt = QString( "Critical: %1" ).arg( msg );
		break;
	case QtFatalMsg:
		txt = QString( "Fatal: %1" ).arg( msg );
		abort();
	}
	QFile outFile( "./log_" + QDateTime::currentDateTime().toString( "yyyy-MM-dd" ) + ".log" );
	outFile.open( QIODevice::WriteOnly | QIODevice::Append );
	QTextStream ts( &outFile );
	ts << dateTime + txt << endl;
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

	QString netId = "";
	if( argc > 1 )
		netId = argv[1];

#ifndef _DEBUG
	qInstallMessageHandler( messageHandler );
#endif

    MainWindow w( netId );
    w.showMaximized();
    return a.exec();
}
