#pragma once
#include <qobject.h>
#include <qtcpserver.h>
#include <qtcpsocket.h>
#include <qrect.h>
#include "Controller.h"

#define MM_PER_PIXEL 0.23

class TcpServer : public Controller
{
	Q_OBJECT
public:
	TcpServer( QObject* parent = nullptr );
	~TcpServer() = default;

public slots:
	virtual void onTransitionChanged( const Transition& transition ) override;
	virtual void onPositionRequest( const QVector3D& pos1, const QVector3D& pos2 ) override;
	virtual void onParameterChanged( const Parameter& param, const double& val ) override;
	virtual void onTouchEventTriggered( bool on ) override;
	virtual void onExternalTrajectory( const QString& fileName, const QVector<QVector<double>>& trajectory ) override;

	void onNewConnection();
	void onReadyRead();
	void onSocketStateChanged( QAbstractSocket::SocketState socketState );
	void onUpdateZeroOffset( const QRectF& size );

signals:
	void simulateTouchEvent( const QPointF& pos );

protected:
	void write( const QString& str  );

private:
	QTcpServer* server_ = new QTcpServer( this );
	QTcpSocket* socket_ = nullptr;

	double zPosForTouchSimulation_ = 1; //mm
	double mmPerPixel_ = MM_PER_PIXEL;
	QPointF zeroOffsetMeter_ = QPointF( 1.920 * 0.5 * MM_PER_PIXEL, 1.080 * 0.5 * MM_PER_PIXEL );
};

