#pragma once

#include <QWidget>
#include "ui_ParamWidget.h"

class ParamWidget : public QWidget
{
	Q_OBJECT

public:
	ParamWidget(const QString& name, const QString& unit, double min, double max, int decimals, QWidget *parent = Q_NULLPTR);
	~ParamWidget();

	void setValue(double value);
	double value();

public slots:

	void on_slider_valueChanged(int value);
	void on_spinBox_valueChanged(double value);

	void on_btnUp_clicked();
	void on_btnDown_clicked();

signals: 
	void valueChanged(double value);
private:
	Ui::ParamWidget ui;
};
