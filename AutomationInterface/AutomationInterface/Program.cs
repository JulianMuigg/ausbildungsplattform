﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EnvDTE90;
using System.IO;
using TCatSysManagerLib;

namespace AutomationInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(args[0]);
            Console.WriteLine(args[1]);

            // Open visual studio
            EnvDTE.DTE dte = openVisualStudio("TcXaeShell.DTE.15.0", true);
            Thread.Sleep(1000);

            // Open TwinCat solution
            EnvDTE.Solution sol = openTwinCatSolution( dte, args[0] + ".sln");
            Thread.Sleep(1000);

            // Define TcSysManager to manipulate the solution
            ITcSysManager sysMan = sol.Projects.Item(1).Object;

            // Get all tcCom objects
            ITcSmTreeItem tcComObjects = sysMan.LookupTreeItem("TIRC^TcCOM Objects");

            // Check, if project includes wanted args[1] TcCom object 
            bool valid = false;
            foreach (ITcSmTreeItem child in tcComObjects)
            {
                Console.WriteLine(child.Name);
                Console.WriteLine(child.PathName);

                string xml = child.ProduceXml();

                if (child.Name.Contains(args[1]))
                    valid = true;
            }

            // Close visual studio before updating the TcCom objects
            dte.Quit();
            Thread.Sleep(1000);


            // Check, if directory of new created model exists
            string modulesPath = "C:\\TwinCAT\\3.1\\CustomConfig\\Modules\\" + args[1];
            if (valid && Directory.Exists(modulesPath))
            {
                // Copy the new builded files from C:\TwinCAT\3.1\CustomConfig\Modules\args[1] to args[0]\_ModuleInstall\args[1]
                CopyFilesRecursively( modulesPath, args[0] + "\\_ModuleInstall\\" + args[1]);

                // Open visual studio
                dte = openVisualStudio("TcXaeShell.DTE.15.0", true);
                Thread.Sleep(1000);

                // Open TwinCat solution
                sol = openTwinCatSolution(dte, args[0] + ".sln");

                // Define TcSysManager to manipulate the solution
                sysMan = sol.Projects.Item(1).Object;

                // Activate and refresh
                sysMan.ActivateConfiguration();
                sysMan.StartRestartTwinCAT();

                // Close visual studio
                dte.Quit();
            }
        }

        private static EnvDTE.DTE openVisualStudio(string name, bool visible)
        {
            // Open TcXaeShell
            Type t = System.Type.GetTypeFromProgID(name);
            EnvDTE.DTE dte = (EnvDTE.DTE)System.Activator.CreateInstance(t);
            dte.SuppressUI = false;
            dte.MainWindow.Visible = visible;

            return dte;
        }

        private static EnvDTE.Solution openTwinCatSolution(EnvDTE.DTE dte, string name)
        { 
            // Open defined project solution in args[0]
            EnvDTE.Solution sol = dte.Solution;
            sol.Open(@name);

            return sol;
        }

        private static void CopyFilesRecursively(string sourcePath, string targetPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            {
                Console.WriteLine(dirPath);
                Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
            }

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                Console.WriteLine(newPath);
                File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
            }
        }
    }
}
